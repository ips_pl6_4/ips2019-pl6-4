package persistence;

import persistence.gateway.ActividadGateway;
import persistence.gateway.AsistenciaGateway;
import persistence.gateway.HistorialGateway;
import persistence.gateway.ImparteGateway;
import persistence.gateway.InstalacionGateway;
import persistence.gateway.MonitorGateway;
import persistence.gateway.ReservaGateway;
import persistence.gateway.ReservaSocioGateway;
import persistence.gateway.SocioGateway;
import persistence.impl.ActividadGatewayImpl;
import persistence.impl.AsistenciaGatewayImpl;
import persistence.impl.HistorialGatewayImpl;
import persistence.impl.ImparteGatewayImpl;
import persistence.impl.InstalacionGatewayImpl;
import persistence.impl.MonitorGatewayImpl;
import persistence.impl.ReservaGatewayImpl;
import persistence.impl.ReservaSocioGatewayImpl;
import persistence.impl.SocioGatewayImpl;

/**
 * Fabrica de cauces de acceso a la persistencia.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class PersistenceFactory {
	public static MonitorGateway getMonitorGateway() {
		return new MonitorGatewayImpl();
	}

	public static ImparteGateway getImparteGateway() {
		return new ImparteGatewayImpl();
	}

	public static AsistenciaGateway getAsistenciaGateway() {
		return new AsistenciaGatewayImpl();
	}

	public static ReservaGateway getReservaGateway() {
		return new ReservaGatewayImpl();
	}

	public static SocioGateway getSocioGateway() {
		return new SocioGatewayImpl();
	}

	public static ActividadGateway getActividadGateway() {
		return new ActividadGatewayImpl();
	}

	public static HistorialGateway getHistorialGateway() {
		return new HistorialGatewayImpl();
	}

	public static ReservaSocioGateway getReservaSocioGateway() {
		return new ReservaSocioGatewayImpl();
	}

	public static InstalacionGateway getInstalacionGateway() {
		return new InstalacionGatewayImpl();
	}
}
