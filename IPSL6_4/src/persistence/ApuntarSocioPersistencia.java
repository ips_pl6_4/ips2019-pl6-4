package persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import logic.dto.SocioDTO;
import logic.util.ReservaParser;
import logic.util.container.Reserva;
import persistence.util.Jdbc;

public class ApuntarSocioPersistencia {

	/**
	 * 
	 * @param idMonitor
	 * @return
	 */
	public List<Reserva> getReservasMonitorHoy(int idMonitor) {
		String SQL = 	"select reserva.*\r\n" + 
						"from reserva, monitor, imparte\r\n" + 
						"where imparte.IDRESERVA = reserva.IDRESERVA and imparte.IDMONITOR = monitor.IDMONITOR and idMonitor = ? and reserva.fecha= ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idMonitor);
			ps.setDate(2,new Date(System.currentTimeMillis()));
			rs = ps.executeQuery();
			return new ReservaParser().parse(rs);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public List<Reserva> getActividades(int idSocio) {
		String SQL = "select re.*\r\n" + "		from asistencia,  reserva re\r\n"
				+ "		where asistencia.IDSOCIO= ? \r\n" + "		and re.IDRESERVA = asistencia.IDRESERVA";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idSocio);
			rs = ps.executeQuery();
			return new ReservaParser().parse(rs);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public Reserva getReserva(int idReserva) {
		String SQL = "select *	from reserva	where idreserva = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			ArrayList<Reserva> ar = (ArrayList<Reserva>) new ReservaParser().parse(rs);
			return ar.get(0);// esto es un poco gochada, pero si al cliente le vale :D

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public boolean socioExist(int idSocio) {
		String SQL = "select * from socio where socio.idsocio = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idSocio);
			rs = ps.executeQuery();
			if (rs.next())
				return true;
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public void ApuntarSocio(int idSocio, int idReserva) {
		Statement s = null;

		try (Connection conn = Jdbc.getConnection()) {
		s = conn.createStatement();
		s.executeQuery("Insert into Asistencia(IDSOCIO,IDRESERVA) values(" + idSocio + "," + idReserva + ")");

		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
			// throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
		
				if (s != null) {
					s.close();
				}
	
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
		
	}
	public List<SocioDTO> getSociosApuntados(int idReserva) {
		String SQL = "select socio.*\r\n" + 
				"from socio, asistencia\r\n" + 
				"where socio.idsocio = asistencia.idsocio \r\n" + 
				"and asistencia.idreserva = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<SocioDTO> toReturn = new ArrayList<SocioDTO>();
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			while(rs.next()) {
				SocioDTO socio = new SocioDTO();
				socio.socioID = rs.getInt("IDSOCIO");
				socio.name = rs.getString("NOMBRE");
				socio.apellidos = rs.getString("APELLIDOS");
				//el email y telefono no hace falta
				toReturn.add(socio);
			}				
			return toReturn;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
		
	}

}
