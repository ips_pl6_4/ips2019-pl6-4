package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import persistence.util.Jdbc;

public class RegistrarPagoPersistencia {

	public int pagoCuota(int idAlquilar,java.sql.Date fecha) {
		String SQL = "UPDATE RESERVASOCIO SET FECHAPAGO = ?, ESTADOPAGO = 'A PAGAR' where idreservasocio = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setDate(1, fecha);
			ps.setInt(2, idAlquilar);			
			return ps.executeUpdate();
			

		} catch (SQLException e) {
			// throw new RuntimeException(e.getMessage());
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
		
	}

}
