package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.RecursoActividadDTO;
import logic.dto.RecursoDTO;
import persistence.gateway.RecursoActividadGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la relacion Recurso_Actividad.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 27.10.2019
 */
public class RecursoActividadGatewayImpl implements RecursoActividadGateway {

	private Connection connection;

	@Override
	public void add(int idActividad, int idRecurso) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_RECURSO_ACTIVIDAD");

		try {
			pst = connection.prepareStatement(sql);

			pst.setLong(1, idRecurso);
			pst.setLong(2, idActividad);

			pst.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}

	@Override
	public void delete(RecursoActividadDTO recurso_actividad) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<RecursoActividadDTO> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RecursoDTO> findRecursoByActividad(int idActividad) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_RECURSO_BY_ACTIVIDAD");

		List<RecursoDTO> recursos = new ArrayList<RecursoDTO>();

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, idActividad);
			rs = pst.executeQuery();
			while (rs.next()) {
				RecursoDTO recurso = new RecursoDTO();
				recurso.idRecurso = rs.getInt("IDRECURSO");
				recurso.nombre = rs.getString("NOMBRE");
				recursos.add(recurso);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return recursos;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}
}
