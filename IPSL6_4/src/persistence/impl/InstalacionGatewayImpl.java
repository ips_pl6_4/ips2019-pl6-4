package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.InstalacionDTO;
import persistence.gateway.InstalacionGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la entidad Instalacion.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 27.10.2019
 */
public class InstalacionGatewayImpl implements InstalacionGateway {

	private Connection connection;

	@Override
	public void add(InstalacionDTO instalacion) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(InstalacionDTO instalacion) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<InstalacionDTO> findAll() {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_INSTALACION");

		List<InstalacionDTO> instalaciones = new ArrayList<InstalacionDTO>();

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();

			while (rs.next()) {
				InstalacionDTO instalacion = new InstalacionDTO();
				instalacion.idInstalacion = rs.getInt("IDINSTALACION");
				instalacion.descripcion = rs.getString("DESCRIPCION");
				instalacion.nombre = rs.getString("NOMBRE");
				instalacion.precioHora = rs.getInt("PRECIOHORA");
				instalaciones.add(instalacion);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return instalaciones;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}

	@Override
	public InstalacionDTO findById(int idInstalacion) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_INSTALACION_BY_ID");

		InstalacionDTO instalacion = null;

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idInstalacion);

			rs = pst.executeQuery();

			while (rs.next()) {
				instalacion = new InstalacionDTO();

				instalacion.idInstalacion = rs.getInt("IDINSTALACION");
				instalacion.nombre = rs.getString("NOMBRE");
				instalacion.descripcion = rs.getString("DESCRIPCION");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return instalacion;
	}

	@Override
	public List<InstalacionDTO> findAllReserved() {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_RESERVED");

		List<InstalacionDTO> instalaciones = new ArrayList<InstalacionDTO>();

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();

			while (rs.next()) {
				InstalacionDTO instalacion = new InstalacionDTO();
				instalacion.idInstalacion = rs.getInt("IDINSTALACION");
				instalacion.descripcion = rs.getString("DESCRIPCION");
				instalacion.nombre = rs.getString("NOMBRE");
				instalaciones.add(instalacion);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return instalaciones;
	}
}
