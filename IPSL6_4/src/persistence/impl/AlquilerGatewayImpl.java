package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import logic.dto.AlquilerDTO;
import persistence.gateway.AlquilerGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

public class AlquilerGatewayImpl implements AlquilerGateway {
	private Connection connection;

	@Override
	public void setConnection(Connection c) {
		this.connection = c;

	}

	@Override
	public List<AlquilerDTO> findByUser(int idSocio) {
		List<AlquilerDTO> alquileres = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_NO_PAID_RENTS");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, idSocio);
			rs = pst.executeQuery();
			alquileres = new ArrayList<AlquilerDTO>();
			while (rs.next()) {
				AlquilerDTO alquiler = new AlquilerDTO();
				alquiler.idReservaSocio = rs.getInt("IDRESERVASOCIO");
				alquiler.idSocio = rs.getInt("IDSOCIO");
				alquiler.idInstalacion = rs.getInt("IDINSTALACION");
				alquiler.horaInicial = rs.getInt("HORAINICIO");
				alquiler.horaFinal = rs.getInt("HORAFINAL");
				alquiler.fecha = rs.getDate("FECHA");
				alquiler.fechaPago = rs.getDate("FECHAPAGO");
				alquiler.estadoPago = rs.getString("ESTADOPAGO");
				alquileres.add(alquiler);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return alquileres;
	}

	@Override
	public void delete(int idReservaSocio) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_RENTING");
		try {

			pst = connection.prepareStatement(sql);
			pst.setInt(1, idReservaSocio);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

	}

	@Override
	public List<AlquilerDTO> findAlquileresFecha(Date desde, Date hasta, int idInstalacion) {
		List<AlquilerDTO> alquileres = null;

		PreparedStatement pst = null;
		ResultSet rs = null;

		String sql = Conf.getInstance().getProperty("SQL_FIND_ALQUILERES_FECHA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setDate(1, new java.sql.Date(desde.getTime()));
			pst.setDate(2, new java.sql.Date(hasta.getTime()));
			pst.setInt(3, idInstalacion);

			rs = pst.executeQuery();

			alquileres = new ArrayList<AlquilerDTO>();

			while (rs.next()) {
				AlquilerDTO reserva = new AlquilerDTO();

				reserva.idReservaSocio = rs.getInt("IDRESERVASOCIO");
				reserva.idSocio = rs.getInt("IDSOCIO");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");
				reserva.fechaPago = rs.getDate("FECHAPAGO");
				reserva.estadoPago = rs.getString("ESTADOPAGO");

				alquileres.add(reserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return alquileres;
	}

	@Override
	public void add(int idSocio, int idInstalacion, int horaInicio, int horaFin, Date fecha) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_ALQUILER");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idSocio);
			pst.setInt(2, idInstalacion);
			pst.setInt(3, horaInicio);
			pst.setInt(4, horaFin);
			pst.setDate(5, new java.sql.Date(fecha.getTime()));
			pst.setString(6, "NO PAGADA");

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}

}
