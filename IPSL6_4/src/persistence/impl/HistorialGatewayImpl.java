package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.HistorialDTO;
import logic.dto.ReservaDTO;
import persistence.gateway.HistorialGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementacion del Gateway a la entidad Historial.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class HistorialGatewayImpl implements HistorialGateway {

	private Connection connection;

	@Override
	public void guardarCambios(ReservaDTO a1, ReservaDTO a2) {
		// PreparedStatement pst = null;
		// String sql = Conf.getInstance().getProperty("SQL_ADD_ACTIVIDAD");
		//
		// try {
		// pst = connection.prepareStatement(sql);
		//
		// pst.setString(1, actividad.nombre);
		// pst.setString(2, actividad.descripcion);
		// pst.setString(3, actividad.intensidad.toString());
		//
		// pst.executeUpdate();
		// } catch (SQLException e) {
		// throw new RuntimeException(e);
		// } finally {
		// Jdbc.close(pst);
		// }
		// }
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}

	@Override
	public void add(HistorialDTO h) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_HISTORIAL");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, h.idReserva);
			pst.setInt(2, h.idInstalacionAnterior);
			pst.setInt(3, h.horaInicialAnterior);
			pst.setInt(4, h.horaFinalAnterior);
			pst.setDate(5, h.fechaAnterior);
			pst.setBoolean(6, h.isPlazasLimitadasAnterior);
			pst.setInt(7, h.numPlazasAnterior);
			pst.setString(8, h.estado);

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}

	@Override
	public void delete(int historialID) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_HISTORIAL");
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, historialID);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public List<HistorialDTO> findAll() {
		List<HistorialDTO> historiales = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_HISTORIAL");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			historiales = new ArrayList<HistorialDTO>();
			while (rs.next()) {
				HistorialDTO historialReserva = new HistorialDTO();

				historialReserva.idHistorial = rs.getInt("IDHISTORIAL");
				historialReserva.idReserva = rs.getInt("IDRESERVA");
				historialReserva.idInstalacionAnterior = rs.getInt("IDINSTALACION_ANTERIOR");
				historialReserva.horaInicialAnterior = rs.getInt("HORAINICIO_ANTERIOR");
				historialReserva.horaFinalAnterior = rs.getInt("HORAFINAL_ANTERIOR");
				historialReserva.fechaAnterior = rs.getDate("FECHA_ANTERIOR");
				historialReserva.isPlazasLimitadasAnterior = rs.getBoolean("PLAZASLIMITADAS_ANTERIOR");
				historialReserva.numPlazasAnterior = rs.getInt("NUMPLAZAS_ANTERIOR");
				historialReserva.estado = rs.getString("ESTADO");

				historiales.add(historialReserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return historiales;
	}

	@Override
	public List<HistorialDTO> findByReserva(int reservaID) {
		List<HistorialDTO> historiales = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_RESERVA_HISTORIAL");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, reservaID);
			rs = pst.executeQuery();
			historiales = new ArrayList<HistorialDTO>();
			while (rs.next()) {
				HistorialDTO historialReserva = new HistorialDTO();

				historialReserva.idHistorial = rs.getInt("IDHISTORIAL");
				historialReserva.idReserva = rs.getInt("IDRESERVA");
				historialReserva.idInstalacionAnterior = rs.getInt("IDINSTALACION_ANTERIOR");
				historialReserva.horaInicialAnterior = rs.getInt("HORAINICIO_ANTERIOR");
				historialReserva.horaFinalAnterior = rs.getInt("HORAFINAL_ANTERIOR");
				historialReserva.fechaAnterior = rs.getDate("FECHA_ANTERIOR");
				historialReserva.isPlazasLimitadasAnterior = rs.getBoolean("PLAZASLIMITADAS_ANTERIOR");
				historialReserva.numPlazasAnterior = rs.getInt("NUMPLAZAS_ANTERIOR");
				historialReserva.estado = rs.getString("ESTADO");

				historiales.add(historialReserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return historiales;
	}

}
