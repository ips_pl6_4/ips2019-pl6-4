package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.SocioDTO;
import persistence.gateway.SocioGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la entidad Socio.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class SocioGatewayImpl implements SocioGateway {

	private Connection connection;

	@Override
	public void add(SocioDTO socio) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_SOCIO");
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, socio.socioID);
			pst.setString(2, socio.name);
			pst.setString(3, socio.apellidos);
			pst.setString(4, socio.mail);
			pst.setInt(5, socio.telefono);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void delete(int socioID) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_SOCIO");
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, socioID);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public List<SocioDTO> findAll() {
		List<SocioDTO> socios = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_SOCIO");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			socios = new ArrayList<SocioDTO>();
			while (rs.next()) {
				SocioDTO socio = new SocioDTO();
				socio.socioID = rs.getInt("IDSOCIO");
				socio.name = rs.getString("NOMBRE");
				socio.apellidos = rs.getString("APELLIDOS");
				socio.mail = rs.getString("EMAIL");
				socio.telefono = Integer.parseInt(rs.getString("TELEFONO"));
				socios.add(socio);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return socios;
	}

	@Override
	public SocioDTO findById(int socioID) {
		SocioDTO socio = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_ID_SOCIO");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, socioID);
			rs = pst.executeQuery();
			if (rs.next()) {
				socio = new SocioDTO();
				socio.socioID = rs.getInt("IDSOCIO");
				socio.name = rs.getString("NOMBRE");
				socio.apellidos = rs.getString("APELLIDOS");
				socio.mail = rs.getString("EMAIL");
				socio.telefono = Integer.parseInt(rs.getString("TELEFONO"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return socio;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}
	
	@Override
	public List<SocioDTO> findWithRenting() {
		List<SocioDTO> socios = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_WITH_RENTING");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			socios = new ArrayList<SocioDTO>();
			while (rs.next()) {
				SocioDTO socio = new SocioDTO();
				socio.socioID = rs.getInt("IDSOCIO");
				socio.name = rs.getString("NOMBRE");
				socio.apellidos = rs.getString("APELLIDOS");
				socio.mail = rs.getString("EMAIL");
				socio.telefono = Integer.parseInt(rs.getString("TELEFONO"));
				socios.add(socio);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return socios;
	}


}
