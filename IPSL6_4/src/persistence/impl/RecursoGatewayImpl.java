package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.RecursoDTO;
import persistence.gateway.RecursoGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la relacion Recurso.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public class RecursoGatewayImpl implements RecursoGateway {

	private Connection connection;

	@Override
	public void add(String nombre) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_RECURSO");

		try {
			pst = connection.prepareStatement(sql);

			pst.setString(1, nombre.toUpperCase());

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}

	@Override
	public void delete(int idRecurso) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<RecursoDTO> findAll() {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_RECURSOS");

		List<RecursoDTO> recursos = new ArrayList<RecursoDTO>();

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();

			while (rs.next()) {
				RecursoDTO recurso = new RecursoDTO();

				recurso.idRecurso = rs.getInt("IDRECURSO");
				recurso.nombre = rs.getString("NOMBRE");

				recursos.add(recurso);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return recursos;
	}

	@Override
	public List<RecursoDTO> findByName(String nombre) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_NAME_RECURSO");

		List<RecursoDTO> recursos = new ArrayList<RecursoDTO>();

		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, nombre.toUpperCase());

			rs = pst.executeQuery();

			while (rs.next()) {
				RecursoDTO recurso = new RecursoDTO();
				recurso.idRecurso = rs.getInt("IDRECURSO");
				recurso.nombre = rs.getString("NOMBRE");

				recursos.add(recurso);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return recursos;
	}

	@Override
	public int maxId() {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_MAX_ID_RECURSO");

		int ultimoIdRecurso = -1;

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();

			if (rs.next())
				ultimoIdRecurso = rs.getInt(1);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return ultimoIdRecurso;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}

	@Override
	public int findRecursoByName(String nombre) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_RECURSO_BY_NAME");

		int idRecurso = -1;

		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, nombre.toUpperCase());

			rs = pst.executeQuery();

			while (rs.next()) {
				idRecurso = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return idRecurso;
	}
}
