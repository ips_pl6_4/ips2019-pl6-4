package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.IncidenciaDTO;
import persistence.gateway.IncidenciaGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementacion del Gateway de la entidad Incidencia.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class IncidenciaGatewayImpl implements IncidenciaGateway {

	private Connection connection;

	@Override
	public void add(IncidenciaDTO incidencia) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_INCIDENCIA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, incidencia.reservaID);
			pst.setString(2, incidencia.incidencia);

			pst.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void delete(IncidenciaDTO incidencia) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_INCIDENCIA");
		try {

			pst = connection.prepareStatement(sql);

			pst.setInt(1, incidencia.reservaID);
			pst.setString(2, incidencia.incidencia);

			pst.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public List<IncidenciaDTO> findAll() {
		List<IncidenciaDTO> incidencias = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_INCIDENCIA");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			incidencias = new ArrayList<IncidenciaDTO>();
			while (rs.next()) {
				IncidenciaDTO incidencia = new IncidenciaDTO();
				incidencia.reservaID = rs.getInt("IDRESERVA");
				incidencia.incidencia = rs.getString("INCIDENCIA");
				incidencias.add(incidencia);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return incidencias;
	}

	@Override
	public List<IncidenciaDTO> findIncidencias(int reservaID) {
		List<IncidenciaDTO> incidencias = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_INCIDENCIAS");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, reservaID);
			rs = pst.executeQuery();
			incidencias = new ArrayList<IncidenciaDTO>();
			while (rs.next()) {
				IncidenciaDTO incidencia = new IncidenciaDTO();
				incidencia.reservaID = rs.getInt("IDRESERVA");
				incidencia.incidencia = rs.getString("INCIDENCIA");
				incidencias.add(incidencia);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return incidencias;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}
}
