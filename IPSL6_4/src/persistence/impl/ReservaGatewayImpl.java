package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import logic.dto.ReservaDTO;
import persistence.gateway.ReservaGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la entidad Reserva.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 3.12.2019
 */
public class ReservaGatewayImpl implements ReservaGateway {

	private Connection connection;

	@Override
	public void add(ReservaDTO reserva) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_RESERVA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, reserva.idActividad);
			pst.setInt(2, reserva.idInstalacion);
			pst.setInt(3, reserva.horaInicial);
			pst.setInt(4, reserva.horaFinal);
			pst.setDate(5, reserva.fecha);
			pst.setBoolean(6, reserva.isPlazasLimitadas);
			pst.setInt(7, reserva.numPlazas);

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}

	@Override
	public void delete(int reservaID) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_RESERVA");
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, reservaID);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void update(ReservaDTO reserva) {
		PreparedStatement pst1 = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		String sql1 = Conf.getInstance().getProperty("SQL_FIND_RESERVA");
		String sql2 = Conf.getInstance().getProperty("SQL_UPDATE_RESERVA");

		try {
			pst1 = connection.prepareStatement(sql1);
			pst1.setInt(1, reserva.idReserva);
			rs = pst1.executeQuery();
			if (rs.next()) {
				pst2 = connection.prepareStatement(sql2);

				pst2.setInt(1, reserva.idActividad);
				pst2.setInt(2, reserva.idInstalacion);
				pst2.setInt(3, reserva.horaInicial);
				pst2.setInt(4, reserva.horaFinal);
				pst2.setDate(5, reserva.fecha);

				pst2.setInt(6, reserva.idReserva);

				pst2.executeUpdate();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst1);
			Jdbc.close(pst2);
		}
	}

	@Override
	public List<ReservaDTO> findAll() {
		List<ReservaDTO> reservas = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_RESERVA");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			reservas = new ArrayList<ReservaDTO>();
			while (rs.next()) {
				ReservaDTO reserva = new ReservaDTO();

				reserva.idReserva = rs.getInt("IDRESERVA");
				reserva.idActividad = rs.getInt("IDACTIVIDAD");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");
				reserva.isPlazasLimitadas = rs.getBoolean("PLAZASLIMITADAS");
				reserva.numPlazas = rs.getInt("NUMPLAZAS");
				reservas.add(reserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reservas;
	}

	@Override
	public ReservaDTO findById(int reservaID) {
		ReservaDTO reserva = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_ID_RESERVA");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, reservaID);
			rs = pst.executeQuery();
			if (rs.next()) {
				reserva = new ReservaDTO();
				reserva.idReserva = rs.getInt("IDRESERVA");
				reserva.idActividad = rs.getInt("IDACTIVIDAD");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reserva;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}

	@Override
	public List<ReservaDTO> findReservasInstalacionFecha(int idInstalacion, Date desde, Date hasta, int horaInicial,
			int horaFinal) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_RESERVAS_BY_INSTALACION_FECHA_HORAS");

		List<ReservaDTO> reservas = new ArrayList<ReservaDTO>();

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idInstalacion);
			pst.setInt(2, horaInicial);
			pst.setInt(3, horaFinal);
			pst.setDate(4, new java.sql.Date(desde.getTime()));
			pst.setDate(5, new java.sql.Date(hasta.getTime()));

			rs = pst.executeQuery();

			while (rs.next()) {
				ReservaDTO reserva = new ReservaDTO();

				reserva.idReserva = rs.getInt("IDRESERVA");
				reserva.idActividad = rs.getInt("IDACTIVIDAD");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");

				reservas.add(reserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reservas;
	}

	@Override
	public int findUltimaReservaId() {
		int id = -1;
		PreparedStatement pst = null;
		ResultSet rs = null;

		String sql = Conf.getInstance().getProperty("SQL_FIND_ULTIMA_RESERVA");

		try {
			pst = connection.prepareStatement(sql);

			rs = pst.executeQuery();

			while (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return id;
	}

	@Override
	public int findUltimoGrupoId() {
		int id = 0;
		PreparedStatement pst = null;
		ResultSet rs = null;

		String sql = Conf.getInstance().getProperty("SQL_ULTIMO_ID_GRUPO");

		try {
			pst = connection.prepareStatement(sql);

			rs = pst.executeQuery();

			while (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return id;
	}

	@Override
	public void vincularReservaGrupo(int idGrupo, int idReserva) {
		PreparedStatement pst = null;
		ResultSet rs = null;

		String sql = Conf.getInstance().getProperty("SQL_VINCULAR_RESERVA_GRUPO");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idGrupo);
			pst.setInt(2, idReserva);

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void updateInstalacionReserva(int idReserva, int idInstalacion, boolean plazasLimitadas, int numPlazas) {
		PreparedStatement pst = null;
		ResultSet rs = null;

		String sql = Conf.getInstance().getProperty("SQL_UPDATE_INSTALACION_RESERVA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idInstalacion);
			pst.setBoolean(2, plazasLimitadas);
			pst.setInt(3, numPlazas);
			pst.setInt(4, idReserva);

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void deleteGrupoReserva(int idReserva) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_GRUPO_RESERVA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idReserva);

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}

	@Override
	public List<ReservaDTO> findReservasSinSocios() {
		List<ReservaDTO> reservas = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_RESERVAS_SIN_SOCIOS");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			reservas = new ArrayList<ReservaDTO>();
			while (rs.next()) {
				ReservaDTO reserva = new ReservaDTO();

				reserva.idReserva = rs.getInt("IDRESERVA");
				reserva.idActividad = rs.getInt("IDACTIVIDAD");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");
				reserva.isPlazasLimitadas = rs.getBoolean("PLAZASLIMITADAS");
				reserva.numPlazas = rs.getInt("NUMPLAZAS");

				reservas.add(reserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reservas;
	}

	@Override
	public void updateHorarioReserva(int idReserva, Date fecha, int horaInicial, int horaFinal) {
		PreparedStatement pst = null;
		ResultSet rs = null;

		String sql = Conf.getInstance().getProperty("SQL_UPDATE_HORARIO_RESERVA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setDate(1, new java.sql.Date(fecha.getTime()));
			pst.setInt(2, horaInicial);
			pst.setInt(3, horaFinal);
			pst.setInt(4, idReserva);

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public List<ReservaDTO> obtenListaDeReservasMonitor(int monitorID) {
		List<ReservaDTO> reservas = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_RESERVAS_FROM_MONITOR");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, monitorID);
			rs = pst.executeQuery();
			reservas = new ArrayList<ReservaDTO>();
			while (rs.next()) {
				ReservaDTO reserva = new ReservaDTO();
				reserva.idReserva = rs.getInt("IDRESERVA");
				reserva.idActividad = rs.getInt("IDACTIVIDAD");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");
				reservas.add(reserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reservas;
	}

	public List<ReservaDTO> findAsignados() {
		List<ReservaDTO> reservas = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ASIGNADAS");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			reservas = new ArrayList<ReservaDTO>();
			while (rs.next()) {
				ReservaDTO reserva = new ReservaDTO();

				reserva.idReserva = rs.getInt("IDRESERVA");
				reserva.idActividad = rs.getInt("IDACTIVIDAD");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");
				reserva.isPlazasLimitadas = rs.getBoolean("PLAZASLIMITADAS");
				reserva.numPlazas = rs.getInt("NUMPLAZAS");
				reservas.add(reserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reservas;
	}

	@Override
	public List<ReservaDTO> findReservasInstalacionFecha(int idInstalacion, Date desde, Date hasta) {
		List<ReservaDTO> reservas = null;

		PreparedStatement pst = null;
		ResultSet rs = null;

		String sql = Conf.getInstance().getProperty("SQL_FIND_RESERVAS_INSTALACION_FECHA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idInstalacion);
			pst.setDate(2, new java.sql.Date(desde.getTime()));
			pst.setDate(3, new java.sql.Date(hasta.getTime()));

			rs = pst.executeQuery();

			reservas = new ArrayList<ReservaDTO>();

			while (rs.next()) {
				ReservaDTO reserva = new ReservaDTO();

				reserva.idReserva = rs.getInt("IDRESERVA");
				reserva.idActividad = rs.getInt("IDACTIVIDAD");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");
				reserva.isPlazasLimitadas = rs.getBoolean("PLAZASLIMITADAS");
				reserva.numPlazas = rs.getInt("NUMPLAZAS");

				reservas.add(reserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reservas;
	}

	@Override
	public List<ReservaDTO> findReservasSocioFecha(int idSocio, Date desde, Date hasta) {
		List<ReservaDTO> reservas = null;

		PreparedStatement pst = null;
		ResultSet rs = null;

		String sql = Conf.getInstance().getProperty("SQL_FIND_RESERVAS_SOCIO_FECHA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idSocio);
			pst.setDate(2, new java.sql.Date(desde.getTime()));
			pst.setDate(3, new java.sql.Date(hasta.getTime()));

			rs = pst.executeQuery();

			reservas = new ArrayList<ReservaDTO>();

			while (rs.next()) {
				ReservaDTO reserva = new ReservaDTO();

				reserva.idReserva = rs.getInt("IDRESERVA");
				reserva.idActividad = rs.getInt("IDACTIVIDAD");
				reserva.idInstalacion = rs.getInt("IDINSTALACION");
				reserva.horaInicial = rs.getInt("HORAINICIO");
				reserva.horaFinal = rs.getInt("HORAFINAL");
				reserva.fecha = rs.getDate("FECHA");
				reserva.isPlazasLimitadas = rs.getBoolean("PLAZASLIMITADAS");
				reserva.numPlazas = rs.getInt("NUMPLAZAS");

				reservas.add(reserva);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reservas;
	}
}
