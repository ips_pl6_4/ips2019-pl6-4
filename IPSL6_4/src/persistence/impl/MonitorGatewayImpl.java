package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.MonitorDTO;
import persistence.gateway.MonitorGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la entidad Monitor.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public class MonitorGatewayImpl implements MonitorGateway {

	private Connection connection;

	@Override
	public void add(String name) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_MONITOR");

		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, name);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void delete(int monitorID) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_MONITOR");
		try {

			pst = connection.prepareStatement(sql);
			pst.setInt(1, monitorID);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void update(MonitorDTO monitor) {
		PreparedStatement pst1 = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		String sql1 = Conf.getInstance().getProperty("SQL_FIND_MONITOR");
		String sql2 = Conf.getInstance().getProperty("SQL_UPDATE_MONITOR");

		try {
			pst1 = connection.prepareStatement(sql1);
			pst1.setInt(1, monitor.monitorID);
			rs = pst1.executeQuery();
			if (rs.next()) {
				pst2 = connection.prepareStatement(sql2);
				pst2.setString(1, monitor.name);
				pst2.setInt(2, monitor.monitorID);
				pst2.executeUpdate();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst1);
			Jdbc.close(pst2);
		}
	}

	@Override
	public List<MonitorDTO> findAll() {
		List<MonitorDTO> monitores = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_MONITOR");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			monitores = new ArrayList<MonitorDTO>();
			while (rs.next()) {
				MonitorDTO monitor = new MonitorDTO();
				monitor.monitorID = rs.getInt("IDMONITOR");
				monitor.name = rs.getString("NOMBRE");
				monitores.add(monitor);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return monitores;
	}

	@Override
	public List<MonitorDTO> findByName(String name) {
		List<MonitorDTO> monitores = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_NAME_MONITOR");

		try {
			pst = connection.prepareStatement(sql);
			pst.setString(1, name);
			rs = pst.executeQuery();
			monitores = new ArrayList<MonitorDTO>();
			while (rs.next()) {
				MonitorDTO monitor = new MonitorDTO();
				monitor.monitorID = rs.getInt("IDMONITOR");
				monitor.name = rs.getString("NOMBRE");
				monitores.add(monitor);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return monitores;
	}

	@Override
	public MonitorDTO findById(int monitorID) {
		MonitorDTO monitor = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_ID_MONITOR");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, monitorID);
			rs = pst.executeQuery();
			if (rs.next()) {
				monitor = new MonitorDTO();
				monitor.monitorID = rs.getInt("IDMONITOR");
				monitor.name = rs.getString("NOMBRE");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return monitor;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}

}
