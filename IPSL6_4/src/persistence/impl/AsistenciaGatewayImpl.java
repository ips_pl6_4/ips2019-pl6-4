package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.AsistenciaDTO;
import persistence.gateway.AsistenciaGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la relacion Asistencia.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 12.11.2019
 */
public class AsistenciaGatewayImpl implements AsistenciaGateway {

	private Connection connection;

	@Override
	public void add(AsistenciaDTO asistencia) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_ASISTENCIA");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, asistencia.reservaID);
			pst.setInt(2, asistencia.socioID);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void delete(AsistenciaDTO asistencia) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_ASISTENCIA");
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, asistencia.reservaID);
			pst.setInt(2, asistencia.socioID);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public List<AsistenciaDTO> findAll() {
		List<AsistenciaDTO> asistencias = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_ASISTENCIA");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			asistencias = new ArrayList<AsistenciaDTO>();
			while (rs.next()) {
				AsistenciaDTO asistencia = new AsistenciaDTO();
				asistencia.socioID = rs.getInt("IDSOCIO");
				asistencia.reservaID = rs.getInt("IDRESERVA");
				asistencias.add(asistencia);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return asistencias;
	}

	@Override
	public AsistenciaDTO findById(int reservaID, int socioID) {
		AsistenciaDTO asistencia = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ASISTENCIA");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, reservaID);
			pst.setInt(2, socioID);
			rs = pst.executeQuery();
			if (rs.next()) {
				asistencia = new AsistenciaDTO();
				asistencia.reservaID = rs.getInt("IDRESERVA");
				asistencia.socioID = rs.getInt("IDSOCIO");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return asistencia;
	}

	@Override
	public Integer countAsistencias(int reservaID) {
		Integer rowCount = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_COUNT_ASISTENCIA_BY_RESERVA");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			if (rs.next())
				rowCount = rs.getInt(1);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return rowCount;
	}

	@Override
	public List<AsistenciaDTO> asiste(int socioID) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ASISTENCIA_BY_SOCIO");
		List<AsistenciaDTO> asistencias = new ArrayList<AsistenciaDTO>();
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, socioID);
			rs = pst.executeQuery();
			while (rs.next()) {
				AsistenciaDTO asistencia = new AsistenciaDTO();
				asistencia.socioID = rs.getInt("IDSOCIO");
				asistencia.reservaID = rs.getInt("IDRESERVA");
				asistencias.add(asistencia);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return asistencias;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}
}
