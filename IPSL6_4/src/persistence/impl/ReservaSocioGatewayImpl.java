package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.ReservaSocioDTO;
import persistence.gateway.ReservaSocioGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la entidad ReservaSocio.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 3.12.2019
 */
public class ReservaSocioGatewayImpl implements ReservaSocioGateway {

	private Connection connection;

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}

	@Override
	public void add(ReservaSocioDTO reservaSocio) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_ALQUILER");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, reservaSocio.socioID);
			pst.setInt(2, reservaSocio.instalacionID);
			pst.setInt(3, reservaSocio.horaInicial);
			pst.setInt(4, reservaSocio.horaFinal);
			pst.setDate(5, reservaSocio.fecha);
			pst.setString(6, reservaSocio.estadoPago);

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}

	@Override
	public void delete(int reservaSocioID) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_RENTING");
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, reservaSocioID);
			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void update(ReservaSocioDTO reservaSocio) {
		PreparedStatement pst1 = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;
		String sql1 = Conf.getInstance().getProperty("SQL_FIND_RESERVASOCIO");
		String sql2 = Conf.getInstance().getProperty("SQL_UPDATE_RESERVASOCIO");

		try {
			pst1 = connection.prepareStatement(sql1);
			pst1.setInt(1, reservaSocio.reservaSocioID);
			rs = pst1.executeQuery();
			if (rs.next()) {
				pst2 = connection.prepareStatement(sql2);

				pst2.setInt(1, reservaSocio.socioID);
				pst2.setInt(2, reservaSocio.instalacionID);
				pst2.setInt(3, reservaSocio.horaInicial);
				pst2.setInt(4, reservaSocio.horaFinal);
				pst2.setDate(5, reservaSocio.fecha);
				pst2.setDate(6, reservaSocio.fechaPago);
				pst2.setString(7, reservaSocio.estadoPago);

				pst2.setInt(8, reservaSocio.reservaSocioID);

				pst2.executeUpdate();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst1);
			Jdbc.close(pst2);
		}
	}

	@Override
	public List<ReservaSocioDTO> findAll() {
		List<ReservaSocioDTO> reservas = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_RESERVASOCIO");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			reservas = new ArrayList<ReservaSocioDTO>();
			while (rs.next()) {
				ReservaSocioDTO reservaSocio = new ReservaSocioDTO();
				reservaSocio.reservaSocioID = rs.getInt("IDRESERVASOCIO");
				reservaSocio.socioID = rs.getInt("IDSOCIO");
				reservaSocio.instalacionID = rs.getInt("IDINSTALACION");
				reservaSocio.horaInicial = rs.getInt("HORAINICIO");
				reservaSocio.horaFinal = rs.getInt("HORAFINAL");
				reservaSocio.fecha = rs.getDate("FECHA");
				reservaSocio.fechaPago = rs.getDate("FECHAPAGO");
				reservaSocio.estadoPago = rs.getString("ESTADOPAGO");
				reservas.add(reservaSocio);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return reservas;
	}

	@Override
	public ReservaSocioDTO findById(int reservaSocioID) {
		ReservaSocioDTO reservaSocio = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_ID_RESERVASOCIO");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, reservaSocioID);
			rs = pst.executeQuery();
			if (rs.next()) {
				reservaSocio = new ReservaSocioDTO();
				reservaSocio.reservaSocioID = rs.getInt("IDRESERVASOCIO");
				reservaSocio.socioID = rs.getInt("IDSOCIO");
				reservaSocio.instalacionID = rs.getInt("IDINSTALACION");
				reservaSocio.horaInicial = rs.getInt("HORAINICIO");
				reservaSocio.horaFinal = rs.getInt("HORAFINAL");
				reservaSocio.fecha = rs.getDate("FECHA");
				reservaSocio.fechaPago = rs.getDate("FECHAPAGO");
				reservaSocio.estadoPago = rs.getString("ESTADOPAGO");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return reservaSocio;
	}

	@Override
	public List<ReservaSocioDTO> findBySocio(int socioID) {
		List<ReservaSocioDTO> reservas = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_SOCIO_RESERVASOCIO");

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, socioID);
			rs = pst.executeQuery();
			reservas = new ArrayList<ReservaSocioDTO>();
			while (rs.next()) {
				ReservaSocioDTO reservaSocio = new ReservaSocioDTO();
				reservaSocio.reservaSocioID = rs.getInt("IDRESERVASOCIO");
				reservaSocio.socioID = rs.getInt("IDSOCIO");
				reservaSocio.instalacionID = rs.getInt("IDINSTALACION");
				reservaSocio.horaInicial = rs.getInt("HORAINICIO");
				reservaSocio.horaFinal = rs.getInt("HORAFINAL");
				reservaSocio.fecha = rs.getDate("FECHA");
				reservaSocio.fechaPago = rs.getDate("FECHAPAGO");
				reservaSocio.estadoPago = rs.getString("ESTADOPAGO");
				reservas.add(reservaSocio);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return reservas;
	}
}
