package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.RecursoInstalacionDTO;
import persistence.gateway.RecursoInstalacionGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

public class RecursoInstalacionGatewayImpl implements RecursoInstalacionGateway {

	private Connection connection;

	@Override
	public void add(int idInstalacion, int... idRecursos) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(RecursoInstalacionDTO recurso_instalacion) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<RecursoInstalacionDTO> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RecursoInstalacionDTO> findRecursoByInstalacion(int idInstalacion) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_RECURSO_BY_INSTALACION");

		List<RecursoInstalacionDTO> recursosInstalacion = new ArrayList<RecursoInstalacionDTO>();

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idInstalacion);
			pst.setInt(2, idInstalacion);

			rs = pst.executeQuery();

			while (rs.next()) {
				RecursoInstalacionDTO recurso_instalacion = new RecursoInstalacionDTO();
				recurso_instalacion.idRecurso = rs.getInt("IDRECURSO");
				recurso_instalacion.idInstalacion = idInstalacion;
				recurso_instalacion.nombre = rs.getString("NOMBRE");
				recurso_instalacion.cantidad = rs.getInt("CANTIDAD");
				recursosInstalacion.add(recurso_instalacion);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return recursosInstalacion;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}
}
