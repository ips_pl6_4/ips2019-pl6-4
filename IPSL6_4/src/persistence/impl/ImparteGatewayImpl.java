package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import logic.dto.ImparteDTO;
import persistence.gateway.ImparteGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementacion del Gateway a la relacion Imparte.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public class ImparteGatewayImpl implements ImparteGateway {

	private Connection connection;

	@Override
	public void add(ImparteDTO imparticion) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_IMPARTE");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, imparticion.reservaID);
			pst.setInt(2, imparticion.monitorID);

			pst.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public void delete(ImparteDTO imparticion) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_DELETE_IMPARTE");
		try {

			pst = connection.prepareStatement(sql);

			pst.setInt(1, imparticion.reservaID);
			pst.setInt(2, imparticion.monitorID);

			pst.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}

	@Override
	public List<ImparteDTO> findAll() {
		List<ImparteDTO> imparticiones = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_IMPARTE");

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();
			imparticiones = new ArrayList<ImparteDTO>();
			while (rs.next()) {
				ImparteDTO imparticion = new ImparteDTO();
				imparticion.monitorID = rs.getInt("IDMONITOR");
				imparticion.reservaID = rs.getInt("IDRESERVA");
				imparticiones.add(imparticion);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return imparticiones;
	}

	@Override
	public List<Integer> imparte(int monitorID) {
		List<Integer> reservaIDs = new LinkedList<Integer>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_ACTIVIDAD_IMPARTIDA");
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, monitorID);
			rs = pst.executeQuery();
			while (rs.next())
				reservaIDs.add(rs.getInt("IDRESERVA"));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return reservaIDs;
	}

	@Override
	public List<Integer> esImpartidaPor(int activityID) {
		List<Integer> monitorIDs = new LinkedList<Integer>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_MONITOR_IMPARTE");
		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, activityID);
			rs = pst.executeQuery();
			while (rs.next())
				monitorIDs.add(rs.getInt("IDMONITOR"));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return monitorIDs;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}

	@Override
	public void update(int idMonitor, int idReserva) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_CAMBIA_MONITOR_DE_RESERVA");

		try {
			pst = connection.prepareStatement(sql);

			pst.setInt(1, idMonitor);
			pst.setInt(2, idReserva);

			pst.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
	}
}