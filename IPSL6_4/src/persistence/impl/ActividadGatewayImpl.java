package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.ActividadDTO;
import logic.dto.Intensidad;
import persistence.gateway.ActividadGateway;
import persistence.util.Conf;
import persistence.util.Jdbc;

/**
 * Implementación del Gateway a la relacion Actividad.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public class ActividadGatewayImpl implements ActividadGateway {

	private Connection connection;

	@Override
	public void add(ActividadDTO actividad) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_ADD_ACTIVIDAD");

		try {
			pst = connection.prepareStatement(sql);

			pst.setString(1, actividad.nombre);
			pst.setString(2, actividad.descripcion);
			pst.setString(3, actividad.intensidad.toString());

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}

	@Override
	public void delete(ActividadDTO actividad) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<ActividadDTO> findAll() {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_ALL_ACTIVIDAD");

		List<ActividadDTO> actividades = new ArrayList<ActividadDTO>();

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();

			while (rs.next()) {
				ActividadDTO actividad = new ActividadDTO();
				actividad.idActividad = rs.getInt("IDACTIVIDAD");
				actividad.nombre = rs.getString("NOMBRE");
				actividad.descripcion = rs.getString("DESCRIPCION");
				actividad.intensidad = Intensidad.valueOf(rs.getString("INTENSIDAD"));

				actividades.add(actividad);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return actividades;
	}

	@Override
	public ActividadDTO findById(int actividadID) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_FIND_BY_ID_ACTIVIDAD");

		ActividadDTO actividad = null;

		try {
			pst = connection.prepareStatement(sql);
			pst.setInt(1, actividadID);
			rs = pst.executeQuery();

			if (rs.next()) {
				actividad = new ActividadDTO();
				actividad.idActividad = rs.getInt("IDACTIVIDAD");
				actividad.nombre = rs.getString("NOMBRE");
				actividad.descripcion = rs.getString("DESCRIPCION");
				actividad.intensidad = Intensidad.valueOf(rs.getString("INTENSIDAD"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}
		return actividad;
	}

	@Override
	public int maxId() {
		PreparedStatement pst = null;
		ResultSet rs = null;
		String sql = Conf.getInstance().getProperty("SQL_MAX_ID_ACTIVIDAD");

		int ultimoIdActividad = -1;

		try {
			pst = connection.prepareStatement(sql);
			rs = pst.executeQuery();

			if (rs.next())
				ultimoIdActividad = rs.getInt(1);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(rs, pst);
		}

		return ultimoIdActividad;
	}

	@Override
	public void setConnection(Connection con) {
		this.connection = con;
	}

	@Override
	public void update(ActividadDTO actividad) {
		PreparedStatement pst = null;
		String sql = Conf.getInstance().getProperty("SQL_UPDATE_ACTIVIDAD");

		try {
			pst = connection.prepareStatement(sql);

			pst.setString(1, actividad.nombre);
			pst.setString(2, actividad.descripcion);
			pst.setString(3, actividad.intensidad.toString());
			pst.setInt(4, actividad.idActividad);

			pst.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			Jdbc.close(pst);
		}
	}
}
