package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import logic.Control;
import logic.util.AlquilerParser;
import persistence.util.Jdbc;

public class VerAsistentesActividadPersistencia {

	public static String obtenActividadesMonitor(int monitor) {
		String SQL = "SELECT r.IDRESERVA, a.NOMBRE FROM MONITOR m, imparte i, ACTIVIDAD a, RESERVA r WHERE m.IDMONITOR = i.IDMONITOR and i.IDRESERVA = r.IDRESERVA and r.IDACTIVIDAD = a.IDACTIVIDAD and m.IDMONITOR = ? and r.FECHA>=CURRENT_DATE";
		StringBuilder sb = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, monitor);
			rs = ps.executeQuery();
			while (rs.next()) {
				sb.append("id: " + rs.getInt(1) + " nombre: " + rs.getString(2) + "\n");
			}
			return sb.toString();
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public static boolean compruebaInicioReserva(int reserva) {
		String SQL = "SELECT r.FECHA,r.HORAINICIO FROM RESERVA r WHERE r.IDRESERVA = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, reserva);
			rs = ps.executeQuery();
			String[] hoy = AlquilerParser.getDiaString(new Date()).split(" ");
			int horaHoy = Control.convertirInt(new Date().toString().split(" ")[3].split(":")[0]);
			rs.next();
			Date d = rs.getDate(1);
			String[] dateBase = d.toString().split("-");
			if (hoy[3].equals(dateBase[0]) && AlquilerParser.getNumeroMes(hoy[2]).equals(dateBase[1])
					&& hoy[1].equals(dateBase[2])) {
				if (horaHoy < rs.getInt(2) && horaHoy > rs.getInt(2) - 2) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}

		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}

	}

	public static String obtenAsistentes(int reserva) {
		String SQL = "SELECT s.IDSOCIO, s.NOMBRE FROM ASISTENCIA a, SOCIO s WHERE a.IDRESERVA = ? and a.IDSOCIO = s.IDSOCIO";
		StringBuilder sb = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, reserva);
			rs = ps.executeQuery();
			while (rs.next()) {
				sb.append("id socio: " + rs.getInt(1) + "   Nombre socio: " + rs.getString(2) + "\n");
			}
			return sb.toString();
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

}
