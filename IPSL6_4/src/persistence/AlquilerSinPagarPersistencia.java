package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.AlquilerDTO;
import logic.dto.InstalacionDTO;
import persistence.util.Jdbc;

public class AlquilerSinPagarPersistencia {

	public List<AlquilerDTO> getAlquileres(int idSocio, String estadoPagado) {
		String SQL = "SELECT * FROM  RESERVASOCIO where idSocio = ? and estadopago = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<AlquilerDTO> alquileres = new ArrayList<AlquilerDTO>();
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1,idSocio);
			ps.setString(2,estadoPagado);
			rs = ps.executeQuery();
			while(rs.next()) {
				AlquilerDTO alquiler = new AlquilerDTO();
				alquiler.idReservaSocio = 	rs.getInt("idReservaSocio");
				alquiler.idSocio = 			rs.getInt("idSocio");
				alquiler.idInstalacion =	rs.getInt("idInstalacion");
				alquiler.horaInicial = 		rs.getInt("horaInicio");
				alquiler.horaFinal =		rs.getInt("horaFinal");
				alquiler.fecha = 			rs.getDate("fecha");
				alquiler.fechaPago = 		rs.getDate("fechaPago");
				alquiler.estadoPago = 		rs.getString("estadoPago");
				
				alquileres.add(alquiler);
			}
			return alquileres;
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public List<InstalacionDTO> getInstalaciones() {
		String SQL = "SELECT * FROM  INSTALACION";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<InstalacionDTO> instalaciones = new ArrayList<InstalacionDTO>();
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);			
			rs = ps.executeQuery();
			while(rs.next()) {
				InstalacionDTO instalacion = new InstalacionDTO();
				instalacion.idInstalacion= rs.getInt("IDINSTALACION");			
				instalacion.nombre = rs.getString("NOMBRE");
				instalacion.descripcion = rs.getString("DESCRIPCION");
				instalacion.precioHora = rs.getInt("PRECIOHORA");
				instalaciones.add(instalacion);
			}
			return instalaciones;
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

}
