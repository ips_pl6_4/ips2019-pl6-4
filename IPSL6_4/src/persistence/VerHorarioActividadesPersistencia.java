package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import logic.util.AlquilerParser;
import logic.util.container.Alquiler;
import persistence.util.Jdbc;

/**
 * Clase que se encarga de la persistencia a la hora de mostrar el horario de
 * las actividades
 * 
 * @author Ana Garcia 11-10-2019
 */
public class VerHorarioActividadesPersistencia {

	public List<Alquiler> getActividades() {
		String SQL = "SELECT I.NOMBRE,A.NOMBRE, R.HORAINICIO, R.HORAFINAL, R.FECHA, R.PLAZASLIMITADAS FROM RESERVA R , ACTIVIDAD A, INSTALACION I  WHERE R.IDACTIVIDAD = A.IDACTIVIDAD AND R.IDINSTALACION = I.IDINSTALACION AND R.FECHA>=CURRENT_DATE ORDER BY R.FECHA";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			AlquilerParser aP = new AlquilerParser(rs);
			return aP.getListaActividadesPlazaONo();
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}

	}
}
