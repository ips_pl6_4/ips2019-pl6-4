package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import persistence.util.Jdbc;

/**
 * Clase con m�todos necesarios para realizar la persistencia que tiene que ver
 * con monitores
 * 
 * @author Ana Garcia 11-10-2019
 */
public class MonitorPersistencia {

	public static boolean existeMonitor(int id) {
		String SQL = "SELECT COUNT(*) FROM MONITOR WHERE IDMONITOR = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			rs.next();
			return rs.getInt(1) == 1;
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

}
