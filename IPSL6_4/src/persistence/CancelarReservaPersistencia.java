package persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import logic.dto.SocioDTO;
import logic.util.ReservaParser;
import logic.util.container.Reserva;
import persistence.util.Jdbc;

public class CancelarReservaPersistencia {
	
	public List<Reserva> getActividadesDate(Date date) {
		String SQL = "select * from  reserva where reserva.fecha = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setDate(1, date);
			rs = ps.executeQuery();
			return new ReservaParser().parse(rs);
		} catch (SQLException e) {			
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public void CancelarReserva(int idReserva) {
		
		String SQL = "DELETE FROM RESERVA where idReserva = "+idReserva;
		Statement ps = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.createStatement();
			ps.executeQuery(SQL);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}		
		
	}

	public void eliminarMonitor(int idReserva) {
		String SQL = "DELETE FROM IMPARTE where idReserva = "+idReserva;
		Statement ps = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.createStatement();
			ps.executeQuery(SQL);
		} catch (SQLException e) {
			e.printStackTrace();			
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
		
	
	}

	public List<SocioDTO> getSocios(int idReserva) {
		ArrayList<SocioDTO> toReturn = new ArrayList<SocioDTO>();
		String SQL = "select socio.* from socio, asistencia where socio.idsocio = asistencia.idsocio and idreserva = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				SocioDTO socio = new SocioDTO();
				socio.socioID = rs.getInt("IDSOCIO");
				socio.name = rs.getString("NOMBRE");
				socio.telefono = rs.getInt("TELEFONO");
				socio.mail = rs.getString("EMAIL");
				socio.apellidos = rs.getString("APELLIDOS");
				toReturn.add(socio);
			}
			
			return toReturn;
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public void eliminarAsistencia(int idReserva) {
		String SQL = "DELETE FROM ASISTENCIA where idReserva = "+idReserva;
		Statement ps = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.createStatement();
			ps.executeQuery(SQL);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public void eliminarIncidencia(int idReserva) {
		String SQL = "DELETE FROM INCIDENCIA where idreserva = "+idReserva;
		Statement ps = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.createStatement();
			ps.executeQuery(SQL);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public void eliminarGrupoReserva(int idReserva) {
		String SQL = "DELETE FROM GRUPO_RESERVA where idreserva = "+idReserva;
		Statement ps = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.createStatement();
			ps.executeQuery(SQL);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public void aņadirAlHistorial(Reserva reserva) {
		String SQL = "INSERT INTO \"PUBLIC\".\"HISTORIAL\"\r\n" + 
				"( \"IDRESERVA\", \"IDINSTALACION_ANTERIOR\", \"HORAINICIO_ANTERIOR\", \"HORAFINAL_ANTERIOR\", \"FECHA_ANTERIOR\", \"PLAZASLIMITADAS_ANTERIOR\", \"NUMPLAZAS_ANTERIOR\", \"ESTADO\" )\r\n" + 
				"VALUES ( "+reserva.getiDReserva()+", "+reserva.getiDInstalacion()+", "+reserva.getHoraInicio()+", "+
				reserva.getHoraFinal()+" , '"+reserva.getFecha()+"', "+reserva.isPlazasLimitadas()+","+reserva.getNumPlazas()+", 'CANCELADA')";

		
		Statement ps = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.createStatement();
//			ps.setInt(1 , reserva.getiDReserva());
//			ps.setInt(2 , reserva.getiDInstalacion());
//			ps.setInt(3 , reserva.getHoraInicio());
//			ps.setInt(4 , reserva.getHoraFinal());
//			ps.setDate(5, reserva.getFecha());
//			ps.setBoolean(6, reserva.isPlazasLimitadas());
//			ps.setInt(7 , reserva.getNumPlazas());
//			ps.setString(8, "CANCELADO");
			
			ps.executeQuery(SQL);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public Reserva getReserva(int idReserva) {
		String SQL = "select *	from reserva	where idreserva = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			ArrayList<Reserva> ar = (ArrayList<Reserva>) new ReservaParser().parse(rs);
			return ar.get(0);// esto es un poco gochada, pero si al cliente le vale :D

		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public int getLastIdHistorial() {
		String SQL = "select * from historial";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			rs = ps.executeQuery();
			int max = 0;
			if (rs.next())
				if(rs.getInt(1)>max);
					max = rs.getInt(1);
			return max;

		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
}
