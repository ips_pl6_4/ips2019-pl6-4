package persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import persistence.util.Jdbc;

public class AnularReservaPersistencia {

	public void anularReserva(int idSocio, int idReserva) {
		String SQL = "DELETE FROM ASISTENCIA " + "WHERE IDRESERVA = " + idReserva + " AND IDSOCIO = " + idSocio;
		Statement s = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			s = conn.createStatement();
			s.executeQuery(SQL);
			if (s.getUpdateCount() == 0)
				throw new RuntimeException("El elemento no exite");
			// supongo que tambien se podria cerrar la ventana al acabar de anular reserva

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (s != null) {
					s.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

}
