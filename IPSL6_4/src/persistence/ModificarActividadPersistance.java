package persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.AlquilerDTO;
import logic.dto.ActividadDTO;
import logic.dto.InstalacionDTO;
import logic.dto.Intensidad;
import logic.dto.RecursoDTO;
import logic.util.ReservaParser;
import logic.util.container.Reserva;
import persistence.util.Jdbc;

public class ModificarActividadPersistance {

	public List<RecursoDTO> getRecursos() {
		String SQL = "SELECT * FROM  RECURSO";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RecursoDTO> recursos = new ArrayList<RecursoDTO>();
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);			
			rs = ps.executeQuery();
			while(rs.next()) {
				RecursoDTO recurso = new RecursoDTO();
				recurso.idRecurso = rs.getInt("IDRECURSO");
				recurso.nombre = rs.getString("NOMBRE");
				recursos.add(recurso);
			}
			return recursos;
			
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public List<ActividadDTO> getActividades() {
		String SQL = "SELECT * FROM  ACTIVIDAD";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ActividadDTO> actividades = new ArrayList<ActividadDTO>();
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);			
			rs = ps.executeQuery();
			while(rs.next()) {
				ActividadDTO actividad = new ActividadDTO();
				actividad.idActividad= rs.getInt("IDACTIVIDAD");
				String enumS = rs.getString("INTENSIDAD");
				actividad.intensidad = Intensidad.valueOf(enumS);				
				actividad.nombre = rs.getString("NOMBRE");
				actividad.descripcion = rs.getString("DESCRIPCION");
				actividades.add(actividad);
			}
			return actividades;
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public List<RecursoDTO> getRecursos(int idActividad) {
		String SQL = "SELECT recurso.* \r\n" + 
				"FROM  RECURSO, RECURSO_actividad\r\n" + 
				"WHERE idactividad = ? and recurso.idrecurso = RECURSO_actividad.idrecurso";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RecursoDTO> recursos = new ArrayList<RecursoDTO>();
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idActividad);
			rs = ps.executeQuery();
			while(rs.next()) {
				RecursoDTO recurso = new RecursoDTO();
				recurso.idRecurso = rs.getInt("IDRECURSO");
				recurso.nombre = rs.getString("NOMBRE");
				recursos.add(recurso);
			}
			return recursos;
			
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public static void addRecurso(int idActividad, int idRecurso) {
		String SQL = "INSERT INTO RECURSO_ACTIVIDAD ( \"IDRECURSO\", \"IDACTIVIDAD\" ) VALUES (?,?)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idRecurso);
			ps.setInt(2, idActividad);		
			ps.executeUpdate();
			

		} catch (SQLException e) {
			// throw new RuntimeException(e.getMessage());
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public static void removeRecurso(int idActividad, int idRecurso) {
		
		String SQL = "DELETE FROM RECURSO_ACTIVIDAD WHERE IDRECURSO = ? AND IDACTIVIDAD = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idRecurso);
			ps.setInt(2, idActividad);		
			ps.executeUpdate();
			

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public List<Reserva> getActividadesDate(Date date) {
		String SQL = "select * from  reserva where reserva.fecha = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setDate(1, date);
			rs = ps.executeQuery();
			return new ReservaParser().parse(rs);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
	public List<Reserva> getReservasActividad(int idActividad) {
		String SQL = "SELECT * FROM RESERVA where idactividad = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idActividad);
			rs = ps.executeQuery();
			return new ReservaParser().parse(rs);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public List<RecursoDTO> getRecursosByInstalacion(int idInstalacion) {
		String SQL = "SELECT recurso.* \r\n" + 
				"FROM  RECURSO, RECURSO_INSTALACION\r\n" + 
				"WHERE idinstalacion = ? and recurso.idrecurso = RECURSO_INSTALACION.idrecurso";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RecursoDTO> recursos = new ArrayList<RecursoDTO>();
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idInstalacion);
			rs = ps.executeQuery();
			while(rs.next()) {
				RecursoDTO recurso = new RecursoDTO();
				recurso.idRecurso = rs.getInt("IDRECURSO");
				recurso.nombre = rs.getString("NOMBRE");
				recursos.add(recurso);
			}
			return recursos;
			
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	

	public InstalacionDTO getInstalacion(int iDInstalacion) {
		String SQL = "SELECT * FROM INSTALACION where idInstalacion = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, iDInstalacion);
			rs = ps.executeQuery();
			rs.next();
			InstalacionDTO dto = new InstalacionDTO();
			dto.idInstalacion = rs.getInt("IDINSTALACION");
			dto.descripcion = rs.getString("DESCRIPCION");
			dto.nombre = rs.getString("NOMBRE");			
			return dto;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public List<AlquilerDTO> getAlquileresActividad(int idActividad) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
