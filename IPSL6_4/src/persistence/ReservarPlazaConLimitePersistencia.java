package persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import logic.util.ReservaParser;
import logic.util.container.Reserva;
import persistence.util.Jdbc;

/**
 * Clase encargada de la persistencia de la clase ReservarPlazaConLimite
 * Historia 7162
 * 
 * @author uo265009
 *
 */
public class ReservarPlazaConLimitePersistencia {
	public boolean isLimitado(int idReserva) {
		String SQL = "select * from RESERVA where RESERVA.PLAZASLIMITADAS = 'true' and RESERVA.IDRESERVA = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			if (rs.next())
				return true;// Si hay siguiente significa que efectivamente la reserva es limitada
			return false;

		} catch (SQLException e) {
			// throw new RuntimeException(e.getMessage());
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public void insertarAsistencia(int idSocio, int idReserva) {
//		String SQL = "Insert into Asistencia(IDSOCIO,IDRESERVA)\r\n" + 
//				"	values(?,?)";
		Statement s = null;

		try (Connection conn = Jdbc.getConnection()) {
			s = conn.createStatement();
			s.executeQuery("Insert into Asistencia(IDSOCIO,IDRESERVA) values(" + idSocio + "," + idReserva + ")");

		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
			// throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {

				if (s != null) {
					s.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	/**
	 * Metodo que devuelve las reservas de un socio
	 * 
	 * @param idSocio
	 * @return
	 */
	public List<Reserva> getActividades(int idSocio) {
		String SQL = "select re.*\r\n" + "		from asistencia,  reserva re\r\n"
				+ "		where asistencia.IDSOCIO= ? \r\n" + "		and re.IDRESERVA = asistencia.IDRESERVA";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idSocio);
			rs = ps.executeQuery();
			return new ReservaParser().parse(rs);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public List<Reserva> getActividadesDate(Date date) {
		String SQL = "select * from  reserva where reserva.fecha = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setDate(1, date);
			rs = ps.executeQuery();
			return new ReservaParser().parse(rs);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public Reserva getReserva(int idReserva) {
		String SQL = "select *	from reserva	where idreserva = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			ArrayList<Reserva> ar = (ArrayList<Reserva>) new ReservaParser().parse(rs);
			return ar.get(0);// esto es un poco gochada, pero si al cliente le vale :D

		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

	public String getNombreActividad(int idReserva) {
		String SQL = "select Nombre from Actividad where idActividad = (select idactividad from reserva where idReserva = ?)";
		PreparedStatement ps = null;
		ResultSet rs = null;

		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			if (rs.next())
				return rs.getString(1);
			return null;

		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
			// throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}

	}

	/**
	 * Devuelve el numero de plazas ocupadas a partir del id de una reserva
	 * 
	 * @param idReserva
	 * @return
	 */
	public int numeroPlazasOcupadas(int idReserva) {
		String SQL = "select * from asistencia where IDRESERVA = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			int contador = 0;
			while(rs.next())
				contador++;
			return contador;

		} catch (SQLException e) {
			// throw new RuntimeException(e.getMessage());
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

}
