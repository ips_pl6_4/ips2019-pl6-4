package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import logic.util.AlquilerParser;
import logic.util.container.Alquiler;
import persistence.util.Jdbc;

/**
 * Clase con las operaciones de persistencia necesitadas para la clase
 * VerOcupacionINstalaciones
 * 
 * @author Ana Garcia 5-10-2019
 *
 */
public class VerOcupacionInstalacionPersistencia {
	/**
	 * M�todo que devuelve los alquileres en la instalacion
	 * 
	 * @param idInstalacion Id de la instalacion
	 * @return alquileres en la instalacion de esta semana
	 */
	public List<Alquiler> getOcupacionInstalacion(int idInstalacion, String[] diaHoy) {
		String SQL = "SELECT I.NOMBRE, R.HORAINICIO, R.HORAFINAL, R.FECHA FROM INSTALACION I, RESERVA R WHERE I.IDINSTALACION = R.IDINSTALACION AND I.IDINSTALACION = ? ORDER BY R.FECHA";// Conf.getInstance().getProperty("SQL_ENCUENTRA_OCUPACION_INSTALACION");
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idInstalacion);
			rs = ps.executeQuery();
			AlquilerParser aP = new AlquilerParser(rs);
			return aP.getListaMismaSemanaSinPlazas(diaHoy);
		} catch (SQLException e) {
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}

}
