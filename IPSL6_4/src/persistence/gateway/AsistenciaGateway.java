package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.AsistenciaDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la relacion Asistencia.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public interface AsistenciaGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Asistencia
	 * 
	 * @param asistencia DTO de la nueva entrada
	 */
	void add(AsistenciaDTO asistencia);

	/**
	 * Elimina una entrada de la tabla Asistencia
	 * 
	 * @param asistencia DTO de la entrada a eliminar
	 */
	void delete(AsistenciaDTO asistencia);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Asistencia
	 * 
	 * @return Lista con todas las entradas
	 */
	List<AsistenciaDTO> findAll();

	/**
	 * Devuelve la entrada dada la clave primaria
	 * 
	 * @param reservaID ID de la reserva a la que se asiste
	 * @param socioID   ID del socio que asiste
	 * @return Entrada con dicha clave; null si no existe
	 */
	AsistenciaDTO findById(int reservaID, int socioID);

	/**
	 * Devuelve la cantidad de asistencias que hay a una reserva
	 * 
	 * @param reservaID ID de la reserva
	 * @return Cantidad de asistencias
	 */
	Integer countAsistencias(int reservaID);

	/**
	 * Devuelve las asistencias que un socio tiene a cualquier reserva
	 * 
	 * @param socioID Socio a evaluar
	 * @return Lista con todas las entradas cuya ID del socio es la especificada
	 */
	public List<AsistenciaDTO> asiste(int socioID);

}