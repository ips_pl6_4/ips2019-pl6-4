package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.ReservaSocioDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la entidad ReservaSocio.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 3.12.2019
 */
public interface ReservaSocioGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla ReservaSocio
	 * 
	 * @param reservaSocio DTO de la nueva entrada
	 */
	void add(ReservaSocioDTO reservaSocio);

	/**
	 * Elimina una entrada de la tabla ReservaSocio
	 * 
	 * @param reservaSocioID ID de la reserva a eliminar
	 */
	void delete(int reservaSocioID);

	/**
	 * Actualiza una entrada de la tabla ReservaSocio
	 * 
	 * @param reservaSocio DTO de la entrada a modificar
	 */
	void update(ReservaSocioDTO reservaSocio);

	/**
	 * Devuelve una lista con todas las entradas de la tabla ReservaSocio
	 * 
	 * @return Lista con todas las entradas
	 */
	List<ReservaSocioDTO> findAll();

	/**
	 * Encuentra la entrada de una reserva en especifico
	 * 
	 * @param reservaSocioID ID de la reserva a buscar
	 * @return DTO de la reserva buscada; null si no existe
	 */
	ReservaSocioDTO findById(int reservaSocioID);

	/**
	 * Devuelve una lista con todas las reservas pertenecientes a un socio dado
	 * 
	 * @param socioID ID del socio
	 * @return Lista con todas las entradas
	 */
	List<ReservaSocioDTO> findBySocio(int socioID);
}
