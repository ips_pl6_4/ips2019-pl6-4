package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.IncidenciaDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la entidad Incidencia.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public interface IncidenciaGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Incidencia
	 * 
	 * @param incidencia DTO de la nueva entrada
	 */
	void add(IncidenciaDTO incidencia);

	/**
	 * Elimina una entrada de la tabla Incidencia
	 * 
	 * @param incidencia DTO de la entrada a eliminar
	 */
	void delete(IncidenciaDTO incidencia);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Incidencia
	 * 
	 * @return Lista con todas las entradas
	 */
	List<IncidenciaDTO> findAll();

	/**
	 * Devuelve una lista con todas las incidencias de una reserva a una actividad
	 * de la tabla Incidencia
	 * 
	 * @param reservaID ID de la reserva de la actividad
	 * @return Lista con todas las incidencias
	 */
	List<IncidenciaDTO> findIncidencias(int reservaID);

}
