package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.SocioDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la entidad Socio.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public interface SocioGateway {
	/**
	 * Anade una entrada a la tabla Socio
	 * 
	 * @param socio DTO de la nueva entrada
	 */
	void add(SocioDTO socio);

	/**
	 * Elimina una entrada de la tabla Socio
	 * 
	 * @param socioID ID del socio a eliminar
	 */
	void delete(int socioID);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Socio
	 * 
	 * @return Lista con todas las entradas
	 */
	List<SocioDTO> findAll();

	/**
	 * Devuelve la entrada de un socio dada su ID
	 * 
	 * @param socioID ID del socio
	 * @return Entrada del socio; null si no lo hay
	 */
	SocioDTO findById(int socioID);

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	List<SocioDTO> findWithRenting();
}
