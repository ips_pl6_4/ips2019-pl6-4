package persistence.gateway;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import logic.dto.AlquilerDTO;

public interface AlquilerGateway {

	public void setConnection(Connection c);

	public List<AlquilerDTO> findByUser(int idSocio);

	public void delete(int idReservaSocio);

	List<AlquilerDTO> findAlquileresFecha(Date desde, Date hasta, int idInstalacion);

	void add(int idSocio, int idInstalacion, int horaInicio, int horaFin, Date fecha);

}
