package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.ImparteDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la relacion Imparte.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.10.2019
 */
public interface ImparteGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Imparte
	 * 
	 * @param imparticion DTO de la nueva entrada
	 */
	void add(ImparteDTO imparticion);

	/**
	 * Elimina una entrada de la tabla Imparte
	 * 
	 * @param imparticion DTO de la entrada a eliminar
	 */
	void delete(ImparteDTO imparticion);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Imparte
	 * 
	 * @return Lista con todas las entradas
	 */
	List<ImparteDTO> findAll();

	/**
	 * Dada la ID de un monitor, devuelve la ID de la actividad que imparte
	 * 
	 * @param monitorID Monitor a evaluar
	 * @return IDs de las actividades que imparte
	 */
	List<Integer> imparte(int monitorID);

	/**
	 * Dada la ID de una actividad, devuelve la ID del monitor que la imparte
	 * 
	 * @param activityID ID de la actividad
	 * @return IDs de los monitores que la imparten
	 */
	List<Integer> esImpartidaPor(int activityID);

	/**
	 * M�todo que cambia el monitor que imparte una reserva
	 * 
	 * @param idMonitor Id del nuevo monitor
	 * @param idReserva Id de la reserva a la que hay que cambiar el monitor
	 */
	void update(int idMonitor, int idReserva);
}
