package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.InstalacionDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la entidad Instalacion.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public interface InstalacionGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Instalacion
	 * 
	 * @param instalacion DTO de la nueva entrada
	 */
	void add(InstalacionDTO instalacion);

	/**
	 * Elimina una entrada de la tabla Instalacion
	 * 
	 * @param instalacion DTO de la entrada a eliminar
	 */
	void delete(InstalacionDTO instalacion);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Instalacion
	 * 
	 * @return Lista con todas las entradas
	 */
	List<InstalacionDTO> findAll();

	InstalacionDTO findById(int idInstalacion);
/**
 * M�todo que devuelve las instalaciones reservadas pr�ximamente
 * @return Lista de instalaciones reservadas proximamente
 */
	List<InstalacionDTO> findAllReserved();
}