package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.HistorialDTO;
import logic.dto.ReservaDTO;

/**
 * Interfaz de la Gateway de la entidad Historial
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public interface HistorialGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Historial
	 * 
	 * @param historial DTO de la nueva entrada
	 */
	void add(HistorialDTO historial);

	/**
	 * Elimina una entrada de la tabla Historial
	 * 
	 * @param historialID ID de la entrada a eliminar
	 */
	void delete(int historialID);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Historial
	 * 
	 * @return Lista con todas las entradas
	 */
	List<HistorialDTO> findAll();

	/**
	 * Devuelve una lista con las entradas de la tabla Historial respectivas a la
	 * reserva dada
	 * 
	 * @param reservaID ID de la reserva
	 * @return Lista con las entradas
	 */
	List<HistorialDTO> findByReserva(int reservaID);

	void guardarCambios(ReservaDTO a1, ReservaDTO a2);

}
