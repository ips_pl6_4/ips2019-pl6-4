package persistence.gateway;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import logic.dto.ReservaDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la entidad Reserva.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public interface ReservaGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con
	 *            Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Reserva
	 * 
	 * @param reserva
	 *            DTO de la nueva entrada
	 */
	void add(ReservaDTO reserva);

	/**
	 * Elimina una entrada de la tabla Reserva
	 * 
	 * @param reservaId
	 *            ID de la reserva a eliminar
	 */
	void delete(int reservaID);

	/**
	 * Actualiza una entrada de la tabla Reserva
	 * 
	 * @param reserva
	 *            DTO de la entrada a modificar
	 */
	void update(ReservaDTO reserva);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Reserva
	 * 
	 * @return Lista con todas las entradas
	 */
	List<ReservaDTO> findAll();

	/**
	 * Encuentra la entrada de una reserva en especifico
	 * 
	 * @param reservaID
	 *            ID de la reserva a buscar
	 * @return DTO de la reserva buscada; null si no existe
	 */
	ReservaDTO findById(int reservaID);

	List<ReservaDTO> findReservasInstalacionFecha(int idInstalacion, Date desde, Date hasta, int horaInicial,
			int horaFinal);

	int findUltimaReservaId();

	int findUltimoGrupoId();

	void vincularReservaGrupo(int idGrupo, int idReserva);

	void deleteGrupoReserva(int idReserva);

	void updateInstalacionReserva(int idReserva, int idInstalacion, boolean plazasLimitadas, int numPlazas);

	List<ReservaDTO> findReservasSinSocios();

	void updateHorarioReserva(int idReserva, Date fecha, int horaInicial, int horaFinal);

	List<ReservaDTO> obtenListaDeReservasMonitor(int monitorID);

	List<ReservaDTO> findReservasInstalacionFecha(int idInstalacion, Date fecha, Date hasta);

	List<ReservaDTO> findReservasSocioFecha(int idSocio, Date desde, Date hasta);
}
