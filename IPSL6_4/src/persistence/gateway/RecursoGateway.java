package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.RecursoDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la entidad Recurso.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 29.10.2019
 */
public interface RecursoGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con
	 *            Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Recurso
	 * 
	 * @param nombre
	 *            Nombre del nuevo recurso
	 */
	void add(String nombre);

	/**
	 * Elimina una entrada de la tabla Recurso
	 * 
	 * @param idRecurso
	 *            ID del recurso a eliminar
	 */
	void delete(int idRecurso);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Recurso
	 * 
	 * @return Lista con todas las entradas
	 */
	List<RecursoDTO> findAll();

	/**
	 * Devuelve los recursos con el nombre dado
	 * 
	 * @param nombre
	 *            Nombre del recurso
	 * @return Lista de recursos que se llaman asi
	 */
	List<RecursoDTO> findByName(String nombre);

	/**
	 * Devuelve el ID maximo de todas las entradas Recurso
	 * 
	 * @return ID maximo
	 */
	int maxId();

	int findRecursoByName(String nombre);
}
