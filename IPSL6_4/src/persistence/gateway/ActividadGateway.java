package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.ActividadDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la entidad Actividad.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public interface ActividadGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Actividad
	 * 
	 * @param actividad DTO de la nueva entrada
	 */
	void add(ActividadDTO actividad);

	/**
	 * Elimina una entrada de la tabla Actividad
	 * 
	 * @param actividad DTO de la entrada a eliminar
	 */
	void delete(ActividadDTO actividad);
	
	/**
	 * Actualiza una entrada de la tabla Actividad
	 * 
	 * @param actividad DTO de la entrada a modificar
	 */
	void update(ActividadDTO actividad);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Actividad
	 * 
	 * @return Lista con todas las entradas
	 */
	List<ActividadDTO> findAll();

	/**
	 * Devuelve la entrada de la entidad Actividad con la ID dada
	 * 
	 * @param actividadID ID de la entrada
	 * @return Entrada respectiva a la ID dada
	 */
	ActividadDTO findById(int actividadID);

	/**
	 * Devuelve el ID maximo de todas las entradas Actividad
	 * 
	 * @return ID maximo
	 */
	int maxId();
}