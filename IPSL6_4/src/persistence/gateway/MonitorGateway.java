package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.MonitorDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la entidad Monitor.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public interface MonitorGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una entrada a la tabla Monitor
	 * 
	 * @param name Nombre del monitor a insertar
	 */
	void add(String name);

	/**
	 * Elimina una entrada de la tabla Monitor
	 * 
	 * @param monitorID ID del monitor a eliminar
	 */
	void delete(int monitorID);

	/**
	 * Actualiza una entrada de la tabla Monitor
	 * 
	 * @param monitor DTO de la entrada a modificar
	 */
	void update(MonitorDTO monitor);

	/**
	 * Devuelve una lista con todas las entradas de la tabla Monitor
	 * 
	 * @return Lista con todas las entradas
	 */
	List<MonitorDTO> findAll();

	/**
	 * Devuelve una lista con todas las entradas de aquellos monitores que se llamen
	 * como se establezca
	 * 
	 * @param name Nombre de el/los monitor/es a encontrar
	 * @return Lista con todos los monitores que se llamen asi; null si no hay
	 */
	List<MonitorDTO> findByName(String name);

	/**
	 * Encuentra la entrada de un monitor en especifico
	 * 
	 * @param monitorID ID del monitor a buscar
	 * @return DTO del monitor buscado; null si no existe
	 */
	MonitorDTO findById(int monitorID);
}
