package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.RecursoActividadDTO;
import logic.dto.RecursoDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la relacion Recurso_Actividad.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 27.10.2019
 */
public interface RecursoActividadGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una o varias entradas a la relacion Recurso_Actividad
	 * 
	 * @param idActividad ID de la actividad
	 * @param idRecursos  IDs de los recursos a asignar
	 */
	void add(int idActividad, int idRecursos);

	/**
	 * Elimina una entrada de la relacion Recurso_Actividad
	 * 
	 * @param recurso_actividad Entrada a eliminar
	 */
	void delete(RecursoActividadDTO recurso_actividad);

	/**
	 * Devuelve una lista con todas las entradas de la relacion Recurso_Actividad
	 * 
	 * @return Lista con todas las entradas
	 */
	List<RecursoActividadDTO> findAll();

	/**
	 * Encuentra todos los recursos de una actividad
	 * 
	 * @param idActividad ID de la actividad
	 * @return Recursos de la actividad
	 */
	List<RecursoDTO> findRecursoByActividad(int idActividad);
}
