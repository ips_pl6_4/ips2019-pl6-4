package persistence.gateway;

import java.sql.Connection;
import java.util.List;

import logic.dto.RecursoInstalacionDTO;

/**
 * Interfaz a implementar por los cauces de entrada a la base de datos,
 * referente a la relacion Recurso_Instalacion.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 27.10.2019
 */
public interface RecursoInstalacionGateway {

	/**
	 * Establece una conexion dada, previamente creada
	 * 
	 * @param con Objeto de conexion
	 */
	void setConnection(Connection con);

	/**
	 * Anade una o varias entradas a la relacion Recurso_Instalacion
	 * 
	 * @param idInstalacion ID de la instalacion
	 * @param idRecursos    IDs de los recursos a asignar
	 */
	void add(int idInstalacion, int... idRecursos);

	/**
	 * Elimina una entrada de la relacion Recurso_Instalacion
	 * 
	 * @param recurso_instalacion Entrada a eliminar
	 */
	void delete(RecursoInstalacionDTO recurso_instalacion);

	/**
	 * Devuelve una lista con todas las entradas de la relacion Recurso_Instalacion
	 * 
	 * @return Lista con todas las entradas
	 */
	List<RecursoInstalacionDTO> findAll();

	/**
	 * Encuentra todos los recursos de una instalacion
	 * 
	 * @param idInstalacion ID de la instalacion
	 * @return Recursos de la instalacion
	 */
	List<RecursoInstalacionDTO> findRecursoByInstalacion(int idInstalacion);
}
