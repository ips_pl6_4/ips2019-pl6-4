package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import persistence.util.Jdbc;

public class AuxiliarMethodsPersistencia {
	public int numeroPlazasOcupadas(int idReserva) {
		String SQL = "select * from asistencia where IDRESERVA = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try (Connection conn = Jdbc.getConnection()) {
			ps = conn.prepareStatement(SQL);
			ps.setInt(1, idReserva);
			rs = ps.executeQuery();
			int contador = 0;			
			while(rs.next())
				contador++;
			return contador;

		} catch (SQLException e) {
			// throw new RuntimeException(e.getMessage());
			throw new RuntimeException("Ha ocurrido un error en la base de datos");
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (ps != null) {
					ps.close();
				}

			} catch (SQLException e) {
				throw new RuntimeException("Ha ocurrido un error en la base de datos");
			}
		}
	}
}
