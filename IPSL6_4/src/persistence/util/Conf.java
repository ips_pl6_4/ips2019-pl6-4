package persistence.util;

import java.io.IOException;
import java.util.Properties;

/**
 * Clase para obtener textos de un fichero "configuration.properties"
 * 
 * @author Ana Garcia
 * @version 9.10.2019
 */
public class Conf {
	private Properties props;
	private static Conf instance = null;
	private static final String FILE_CONF = "configuration.properties";

	/**
	 * Constructor de la clase
	 */
	public Conf() {
		this.props = new Properties();
		try {
			props.load(Conf.class.getClassLoader().getResourceAsStream(FILE_CONF));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException("File properties cannot be loaded", e);
		}

	}

	/**
	 * M�todo para crear la clase si es necesario
	 * 
	 * @returnClase creada
	 */
	public static Conf getInstance() {
		if (instance == null) {
			instance = new Conf();
		}
		return instance;
	}

	/**
	 * M�todo para obtener el texto usando la clave
	 * 
	 * @param key Clave del texto
	 * @return Texto de la clave
	 */
	public String getProperty(String key) {
		String value = props.getProperty(key);
		if (value == null) {
			throw new RuntimeException("Property not found in config file");
		}
		return value;
	}

}