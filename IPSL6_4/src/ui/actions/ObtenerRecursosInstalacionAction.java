package ui.actions;

import java.util.List;

import logic.ObtenerRecursosInstalacionBusiness;
import logic.dto.RecursoInstalacionDTO;

public class ObtenerRecursosInstalacionAction {

	private int idInstalacion;

	public ObtenerRecursosInstalacionAction(int idInstalacion) {
		this.idInstalacion = idInstalacion;
	}

	public List<RecursoInstalacionDTO> execute() {
		ObtenerRecursosInstalacionBusiness o = new ObtenerRecursosInstalacionBusiness(idInstalacion);
		return o.execute();
	}

}
