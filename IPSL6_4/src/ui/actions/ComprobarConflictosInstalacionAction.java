package ui.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import logic.ComprobarConflictoInstalacionBusiness;
import logic.dto.ConflictoDTO;

public class ComprobarConflictosInstalacionAction {

	private int idActividad;
	private int idInstalacion;
	private List<Date> fechas;
	private int horaInicial;
	private int horaFinal;

	public ComprobarConflictosInstalacionAction(int idActividad, int idInstalacion, List<Date> fechas, int horaInicial,
			int horaFinal) {
		this.idActividad = idActividad;
		this.idInstalacion = idInstalacion;
		this.fechas = fechas;
		this.horaInicial = horaInicial;
		this.horaFinal = horaFinal;
	}

	public List<ConflictoDTO> execute() {
		List<ConflictoDTO> conflictos = new ArrayList<ConflictoDTO>();

		for (Date fecha : fechas) {
			List<ConflictoDTO> conflictosFecha = new ComprobarConflictoInstalacionBusiness(idInstalacion, fecha,
					horaInicial, horaFinal, idActividad).execute();

			for (ConflictoDTO c : conflictosFecha) {
				conflictos.add(c);
			}
		}

		return conflictos;
	}

}
