package ui.actions;

import java.util.Date;

import logic.CrearReservaSocioBusiness;

public class CrearReservaSocioAction {

	private int idSocio;
	private int idInstalacion;
	private int horaInicio;
	private int horaFin;
	private Date fecha;

	public CrearReservaSocioAction(int idSocio, int idInstalacion, int horaInicio, int horaFin, Date fecha) {
		this.idSocio = idSocio;
		this.idInstalacion = idInstalacion;
		this.horaInicio  = horaInicio;
		this.horaFin = horaFin;
		this.fecha = fecha;
	}

	public void execute() {
		new CrearReservaSocioBusiness(idSocio, idInstalacion, horaInicio, horaFin, fecha).execute();
	}

}
