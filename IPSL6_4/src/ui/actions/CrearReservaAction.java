package ui.actions;

import logic.CrearReservaBusiness;
import logic.dto.ReservaDTO;

public class CrearReservaAction {

	private ReservaDTO reserva;

	public CrearReservaAction(ReservaDTO reserva) {
		this.reserva = reserva;
	}

	public int execute() {
		return new CrearReservaBusiness(reserva).execute();
	}

}
