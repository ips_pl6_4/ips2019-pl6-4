package ui.actions;

import logic.RegistrarHistorialBusiness;
import logic.dto.HistorialDTO;

public class RegistrarHistorialAction {

	private HistorialDTO h;

	public RegistrarHistorialAction(HistorialDTO h) {
		this.h = h;
	}

	public void execute() {
		new RegistrarHistorialBusiness(h).execute();
	}

}
