package ui.actions;

import java.util.List;

import logic.ObtenerSociosBusiness;
import logic.dto.SocioDTO;

public class ObtenerSociosAction {

	public List<SocioDTO> execute() {
		return new ObtenerSociosBusiness().execute();
	}

}
