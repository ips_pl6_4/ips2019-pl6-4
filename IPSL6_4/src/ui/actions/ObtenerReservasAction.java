package ui.actions;

import java.util.List;

import logic.ObtenerReservasBusiness;
import logic.dto.ReservaDTO;

public class ObtenerReservasAction {
	
	public List<ReservaDTO> execute() {
		ObtenerReservasBusiness o = new ObtenerReservasBusiness();
		return o.execute();
	}

}
