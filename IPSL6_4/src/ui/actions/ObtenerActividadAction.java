package ui.actions;

import logic.ObtenerActividadBusiness;
import logic.dto.ActividadDTO;

public class ObtenerActividadAction {

	private int idActividad;

	public ObtenerActividadAction(int idActividad) {
		this.idActividad = idActividad;
	}

	public ActividadDTO execute() {
		return new ObtenerActividadBusiness(idActividad).execute();
	}

}
