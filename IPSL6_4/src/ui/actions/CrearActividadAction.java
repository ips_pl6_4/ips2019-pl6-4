package ui.actions;

import java.util.List;

import logic.CrearActividadBusiness;
import logic.dto.ActividadDTO;
import logic.dto.Intensidad;
import logic.dto.RecursoDTO;

public class CrearActividadAction {

	public void execute(String nombre, String descripcion, Intensidad intensidad, List<RecursoDTO> recursos,
			List<RecursoDTO> recursosNuevos) {
		ActividadDTO actividad = new ActividadDTO();
		actividad.nombre = nombre;
		actividad.descripcion = descripcion;
		actividad.intensidad = intensidad;

		new CrearActividadBusiness(actividad, recursos, recursosNuevos).execute();
	}

	public RecursoDTO compruebaNoExisteRecurso(List<RecursoDTO> recursos, RecursoDTO nuevoRecurso) {
		for (RecursoDTO recursoExiste : recursos) {
			if (recursoExiste.nombre.equals(nuevoRecurso.nombre)) {
				return recursoExiste;
			}
		}

		return nuevoRecurso;
	}

}
