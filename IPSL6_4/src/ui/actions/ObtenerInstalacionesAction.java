package ui.actions;

import java.util.List;

import logic.ObtenerInstalacionesBusiness;
import logic.dto.InstalacionDTO;

public class ObtenerInstalacionesAction {

	public List<InstalacionDTO> execute() {
		ObtenerInstalacionesBusiness o = new ObtenerInstalacionesBusiness();
		return o.execute();
	}

}
