package ui.actions;

import java.util.List;

import logic.ObtenerAlquileresBusiness;
import logic.dto.AlquilerDTO;

public class ObtenerAlquileresAction {
	
	public List<AlquilerDTO> execute(int id) {
		ObtenerAlquileresBusiness o = new ObtenerAlquileresBusiness(id);
		return o.execute();
	}

}
