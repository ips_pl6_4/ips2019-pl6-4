package ui.actions;

import logic.ObtenerInstalacionBusiness;
import logic.dto.InstalacionDTO;

public class ObtenerInstalacionAction {

	private int idInstalacion;

	public ObtenerInstalacionAction(int idInstalacion) {
		this.idInstalacion = idInstalacion;
	}

	public InstalacionDTO execute() {
		return new ObtenerInstalacionBusiness(idInstalacion).execute();
	}

}
