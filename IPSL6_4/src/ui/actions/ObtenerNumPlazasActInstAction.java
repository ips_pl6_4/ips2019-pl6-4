package ui.actions;

import java.util.List;

import logic.ObtenerNumPlazasActInstBusiness;
import logic.dto.RecursoDTO;
import logic.dto.RecursoInstalacionDTO;

public class ObtenerNumPlazasActInstAction {

	private List<RecursoDTO> recursosActividad;
	private List<RecursoInstalacionDTO> recursosInstalacion;

	public ObtenerNumPlazasActInstAction(List<RecursoDTO> recursosActividad,
			List<RecursoInstalacionDTO> recursosInstalacion) {
		this.recursosActividad = recursosActividad;
		this.recursosInstalacion = recursosInstalacion;
	}

	public int execute() {
		return new ObtenerNumPlazasActInstBusiness(recursosActividad, recursosInstalacion).execute();
	}

}
