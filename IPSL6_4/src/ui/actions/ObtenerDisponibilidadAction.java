package ui.actions;

import java.util.Date;

import logic.ObtenerDisponibilidadBusiness;
import logic.ObtenerHorarioAlquileresSocioBusiness;
import logic.ObtenerHorarioSocioFechaActividadBusiness;

public class ObtenerDisponibilidadAction {

	public String[] execute(int idInstalacion, Date fecha, int idSocio) {
		// Actividades del centro
		String[] horarioInstalacion = new ObtenerDisponibilidadBusiness(idInstalacion, fecha).execute();

		// Plazas socio en actividad del centro
		String[] horarioSocioFecha = new ObtenerHorarioSocioFechaActividadBusiness(idSocio, fecha).execute();

		// Alquileres instalacion
		String[] horarioAlquileres = new ObtenerHorarioAlquileresSocioBusiness(idInstalacion, fecha).execute();

		String[] horarioFinal = new String[15];

		for (int i = 0; i < 15; i++) {
			horarioFinal[i] = "LIBRE";
		}

		for (int i = 0; i < 15; i++) {
			if (horarioInstalacion[i] != "LIBRE") {
				horarioFinal[i] = horarioInstalacion[i];
			}
		}

		for (int i = 0; i < 15; i++) {
			if (horarioSocioFecha[i] != "LIBRE") {
				horarioFinal[i] = horarioSocioFecha[i];
			}
		}

		for (int i = 0; i < 15; i++) {
			if (horarioAlquileres[i] != "LIBRE") {
				horarioFinal[i] = horarioAlquileres[i];
			}
		}

		return horarioFinal;
	}

}
