package ui.actions;

import logic.BorrarGrupoActividadBusiness;
import logic.ModificarInstalacionReservaBusiness;

public class ModificarInstalacionReservaAction {

	private int idReserva;
	private int idInstalacion;
	private boolean plazasLimitadas;
	private int numPlazas;

	public ModificarInstalacionReservaAction(int idReserva, int idInstalacion, boolean plazasLimitadas, int numPlazas) {
		this.idReserva = idReserva;
		this.idInstalacion = idInstalacion;
		this.plazasLimitadas = plazasLimitadas;
		this.numPlazas = numPlazas;
	}

	public void execute() {
		new ModificarInstalacionReservaBusiness(idReserva, idInstalacion, plazasLimitadas, numPlazas).execute();
		new BorrarGrupoActividadBusiness(idReserva).execute();
	}

}
