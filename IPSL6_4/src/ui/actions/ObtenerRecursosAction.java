package ui.actions;

import java.util.List;

import logic.ObtenerRecursosBusiness;
import logic.dto.RecursoDTO;

public class ObtenerRecursosAction {

	public List<RecursoDTO> execute() {
		return new ObtenerRecursosBusiness().execute();
	}
}
