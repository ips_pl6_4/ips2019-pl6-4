package ui.actions;

import java.util.Date;

import logic.BorrarGrupoActividadBusiness;
import logic.ModificarHorarioReservaBusiness;

public class ModificarHorarioReservaAction {

	private int idReserva;
	private Date fecha;
	private int horaInicial;
	private int horaFinal;

	public ModificarHorarioReservaAction(int idReserva, Date fecha, int horaInicial, int horaFinal) {
		this.idReserva = idReserva;
		this.fecha = fecha;
		this.horaInicial = horaInicial;
		this.horaFinal = horaFinal;
	}

	public void execute() {
		new ModificarHorarioReservaBusiness(idReserva, fecha, horaInicial, horaFinal).execute();
		new BorrarGrupoActividadBusiness(idReserva);
	}

}
