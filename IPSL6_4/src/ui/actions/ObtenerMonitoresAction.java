package ui.actions;

import java.util.List;

import logic.ObtenerMonitorBusiness;
import logic.dto.MonitorDTO;

public class ObtenerMonitoresAction {
	
	public List<MonitorDTO> execute() {
		ObtenerMonitorBusiness o = new ObtenerMonitorBusiness();
		return o.execute();
	}

}
