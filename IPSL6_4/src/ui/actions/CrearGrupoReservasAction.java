package ui.actions;

import java.util.List;

import logic.CrearGrupoReservasBusiness;

public class CrearGrupoReservasAction {

	private List<Integer> idsReservas;

	public CrearGrupoReservasAction(List<Integer> idsReservas) {
		this.idsReservas = idsReservas;
	}

	public void execute() {
		new CrearGrupoReservasBusiness(idsReservas).execute();
	}

}
