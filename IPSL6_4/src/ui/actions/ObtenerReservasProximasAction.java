package ui.actions;

import java.util.List;

import logic.ObtenerReservasProximasBusiness;
import logic.dto.ReservaDTO;

public class ObtenerReservasProximasAction {

	public List<ReservaDTO> execute() {
		return new ObtenerReservasProximasBusiness().execute();
	}

}
