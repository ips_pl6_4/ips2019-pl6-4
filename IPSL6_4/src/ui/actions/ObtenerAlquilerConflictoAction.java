package ui.actions;

import java.util.Date;

import logic.ObtenerAlquilerConflictoBusiness;
import logic.dto.AlquilerDTO;

public class ObtenerAlquilerConflictoAction {

	private int idInstalacion;
	private int horaInicio;
	private int horaFin;
	private Date fecha;

	public ObtenerAlquilerConflictoAction(int idInstalacion, int horaInicio, int horaFin, Date fecha) {
		this.idInstalacion = idInstalacion;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.fecha = fecha;
	}

	public AlquilerDTO execute() {
		horaInicio += 8;
		horaFin += 8;
		
		return new ObtenerAlquilerConflictoBusiness(idInstalacion, horaInicio, horaFin, fecha).execute();
	}

}
