package ui.actions;

import java.util.List;

import logic.ObtenerActividadesBusiness;
import logic.dto.ActividadDTO;

public class ObtenerActividadesAction {

	public List<ActividadDTO> execute() {
		ObtenerActividadesBusiness o = new ObtenerActividadesBusiness();
		return o.execute();
	}

}
