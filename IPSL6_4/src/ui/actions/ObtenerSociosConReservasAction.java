package ui.actions;

import java.util.List;

import logic.ObtenerSociosConReservasBusiness;
import logic.dto.SocioDTO;

public class ObtenerSociosConReservasAction {
	
	public List<SocioDTO> execute() {
		return new ObtenerSociosConReservasBusiness().execute();
	}

}
