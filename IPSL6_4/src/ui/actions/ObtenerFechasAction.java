package ui.actions;

import java.util.Date;
import java.util.List;

import logic.ObtenerFechasBusiness;

public class ObtenerFechasAction {

	public List<Date> execute(int idInstalacion, Date fechaInicial, Date fechaFinal, int diaSeleccionado,
			int horaInicial, int horaFinal) {
		return new ObtenerFechasBusiness(fechaInicial, fechaFinal, diaSeleccionado).execute();
	}

}
