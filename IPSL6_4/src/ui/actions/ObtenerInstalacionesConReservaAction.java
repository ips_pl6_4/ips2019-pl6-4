package ui.actions;

import java.util.List;

import logic.ObtenerInstalacionesConReservaBusiness;
import logic.dto.InstalacionDTO;

public class ObtenerInstalacionesConReservaAction {

	public List<InstalacionDTO> execute() {
		ObtenerInstalacionesConReservaBusiness o = new ObtenerInstalacionesConReservaBusiness();
		return o.execute();
	}
}
