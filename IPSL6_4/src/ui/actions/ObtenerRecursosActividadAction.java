package ui.actions;

import java.util.List;

import logic.ObtenerRecursosActividadBusiness;
import logic.dto.RecursoDTO;

public class ObtenerRecursosActividadAction {

	private int idActividad;

	public ObtenerRecursosActividadAction(int idActividad) {
		this.idActividad = idActividad;
	}

	public List<RecursoDTO> execute() {
		ObtenerRecursosActividadBusiness o = new ObtenerRecursosActividadBusiness(idActividad);
		return o.execute();
	}

}
