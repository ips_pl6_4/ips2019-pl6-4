package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.jvnet.substance.SubstanceLookAndFeel;

import logic.Control;
import logic.VerAsistentesActividad;
import logic.VerHorarioActividades;
import logic.util.SortedComboBoxModel;
import ui.control.ControlClases;
import ui.windows.AdministracionEliminaAlquilerSocio;
import ui.windows.AsignarMonitorUI;
import ui.windows.InfoPagosUI;
import ui.windows.ParteIncidenciasUI;
import ui.windows.RegistrarAsistenciaUI;
import ui.windows.ReservasActividadesUI;
import ui.windows.VentanaAccederComoMonitorConReservasHoy;
import ui.windows.VentanaAccederComoSocio;
import ui.windows.VentanaAccederComoSocioGenerica;
import ui.windows.VentanaActividadFecha;
import ui.windows.VentanaAlquilarInstalacionSocio;
import ui.windows.VentanaAlquilerSinPagar;
import ui.windows.VentanaAnularReserva;
import ui.windows.VentanaApuntarSocio;
import ui.windows.VentanaCancelarReserva;
import ui.windows.VentanaCrearActividad;
import ui.windows.VentanaCrearReserva;
import ui.windows.VentanaCrearReservaPeriodica;
import ui.windows.VentanaHorario;
import ui.windows.VentanaLista;
import ui.windows.VentanaModificaActividades;
import ui.windows.VentanaModificaReserva;
import ui.windows.VentanaModificarHorarioReservaSinSocios;
import ui.windows.VentanaModificarInstalacionReservaSinSocios;
import ui.windows.VentanaModificarRecursosActividad;
import ui.windows.VentanaRegistrarPago;
import ui.windows.VentanaSeleccionInstalacion;
import ui.windows.VentanaSociosConReservas;
import ui.windows.util.JPanelTama�o;

/**
 * Ventana para elegir rol y opciones de cada rol
 * 
 * @author Ana Garc�a 9-10-2019
 */
public class VentanaPrincipal extends JFrame {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 2L;
	private JPanel contentPane;
	private JPanel pnNorte;
	private JPanel pnInfo;
	private JPanel pnSur;
	private JComboBox<String> cbRol;
	private JLabel lbRol;
	private JLabel lbOpciones;
	private JComboBox<String> cbOpciones;
	private JPanel pnIzquierda;
	private JPanel pnDerecha;
	private JButton btEjecutar;
	/**
	 * rols de la aplicacion
	 */
	private String[] rols = new String[] { " Rol", "Administrador", "Socio", "Monitor" };
	/**
	 * Opciones por defecto
	 */
	private String[] optionsDefault = new String[] { "Opcion" };
	/**
	 * Opciones del administrador
	 */
	private String[] optionsAdministrator = new String[] { " Opcion", "Horario de Instalacion", "Crear Actividad",
			"Planificar Actividad", "Reservar Plaza Con Limite", "Asignar Monitor", "Planificar Actividad Periodica",
			"Modificar Instalaci�n Reserva", "Modificar Horario Reserva", "Modificar Actividad", "Reasigna Monitor",
			"Cancelar Reserva", "Cancelar Alquiler", "Alquilar Instalaci�n a un Socio", "Modificar Recursos Actividad",
			"Registrar pago" };

	/**
	 * Opciones para socios
	 */
	private String[] optionsSocio = new String[] { " Opcion", "Horario Actividades", "Reservar Plaza Con Limite",
			"Anular Reserva", "Asistencias A Reservas", "Alquilar Instalaci�n", "Ver Alquileres sin Pagar",
			"Cancelar Alquiler", "Informaci�n sobre Pagos" };
	/**
	 * Opciones para Monitores
	 */
	private String[] optionsMonitor = new String[] { " Opcion", "Asistentes Actividad", "Registrar Asistencia",
			"Apuntar Socio", "Parte Incidencias" };
	private JLabel lbImagen;

	private Adaptar aD;

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.ModerateSkin");

		setResizable(false);
		aD = new Adaptar();
		setTitle("Centro deportivo: Menu");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/logoIconoSample.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 604, 425);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPnSur(), BorderLayout.SOUTH);
		contentPane.add(getPnNorte(), BorderLayout.NORTH);
		contentPane.add(getPnInfo(), BorderLayout.CENTER);
		contentPane.add(getPnIzquierda(), BorderLayout.WEST);
		contentPane.add(getPnDerecha(), BorderLayout.EAST);
	}

	private JPanel getPnNorte() {
		if (pnNorte == null) {
			pnNorte = new JPanelTama�o(new Dimension(50, 100));
			pnNorte.setLayout(null);
			pnNorte.add(getCbRol());
			pnNorte.add(getLbRol());
			pnNorte.add(getLbOpciones());
			pnNorte.add(getCbOpciones());
			pnNorte.add(getBtEjecutar());
		}
		return pnNorte;
	}

	private JPanel getPnInfo() {
		if (pnInfo == null) {
			pnInfo = new JPanel();
			pnInfo.setLayout(new BorderLayout(0, 0));
			pnInfo.add(getLbImagen());
		}
		return pnInfo;
	}

	private JPanel getPnSur() {
		if (pnSur == null) {
			pnSur = new JPanelTama�o(new Dimension(100, 50));
		}
		return pnSur;
	}

	private JComboBox<String> getCbRol() {
		if (cbRol == null) {
			cbRol = new JComboBox<String>();
			cbRol.setFont(new Font("Tahoma", Font.PLAIN, 14));
			cbRol.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
					if (cbRol.getSelectedItem().equals("Administrador")) {
						establecerAdministrador();
					} else if (cbRol.getSelectedItem().equals("Socio")) {
						establecerSocio();
					} else if (cbRol.getSelectedItem().equals("Monitor")) {
						establecerMonitor();
					} else {
						establecerDefecto();
					}
				}
			});
			cbRol.setModel(new SortedComboBoxModel<String>(rols.clone()));
			cbRol.setBounds(23, 48, 142, 32);
		}
		return cbRol;
	}

	protected void establecerMonitor() {
		cbOpciones.setModel(new SortedComboBoxModel<String>(optionsMonitor.clone()));

	}

	protected boolean compruebaMonitor(int id) {
		return ControlClases.compruebaMonitor(id);
	}

	private JLabel getLbRol() {
		if (lbRol == null) {
			lbRol = new JLabel("Seleccione Rol:");
			lbRol.setFont(new Font("Tahoma", Font.BOLD, 15));
			lbRol.setBounds(23, 22, 192, 20);
		}
		return lbRol;
	}

	private JLabel getLbOpciones() {
		if (lbOpciones == null) {
			lbOpciones = new JLabel("Seleccione Opci\u00F3n:");
			lbOpciones.setFont(new Font("Tahoma", Font.BOLD, 15));
			lbOpciones.setBounds(367, 22, 142, 19);
		}
		return lbOpciones;
	}

	private JComboBox<String> getCbOpciones() {
		if (cbOpciones == null) {
			cbOpciones = new JComboBox<String>();
			cbOpciones.setFont(new Font("Tahoma", Font.PLAIN, 14));
			cbOpciones.setModel(new SortedComboBoxModel<String>(optionsDefault.clone()));
			cbOpciones.setBounds(367, 48, 211, 32);
		}
		return cbOpciones;
	}

	/**
	 * M�todo que establece las opciones por defecto
	 */
	private void establecerDefecto() {
		cbOpciones.setModel(new SortedComboBoxModel<String>(optionsDefault.clone()));
	}

	/**
	 * M�todo que establece las opciones para socios
	 */
	private void establecerSocio() {
		cbOpciones.setModel(new SortedComboBoxModel<String>(optionsSocio.clone()));
	}

	/**
	 * M�todo que establece las opciones para administrador
	 */
	private void establecerAdministrador() {
		cbOpciones.setModel(new SortedComboBoxModel<String>(optionsAdministrator.clone()));
	}

	private JPanel getPnIzquierda() {
		if (pnIzquierda == null) {
			pnIzquierda = new JPanelTama�o(new Dimension(50, 100));
		}
		return pnIzquierda;
	}

	private JPanel getPnDerecha() {
		if (pnDerecha == null) {
			pnDerecha = new JPanelTama�o(new Dimension(50, 100));
		}
		return pnDerecha;
	}

	private JButton getBtEjecutar() {
		if (btEjecutar == null) {
			btEjecutar = new JButton("Ejecutar");
			btEjecutar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btEjecutar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ejecutarOpcion();
				}
			});
			btEjecutar.setBounds(217, 57, 109, 32);
		}
		return btEjecutar;
	}

	private void ejecutarOpcion() {
		String rol, opcion;

		try {
			// Almacena el rol seleccionado
			rol = getCbRol().getSelectedItem().toString();

			// Almacena la opcion seleccionada
			opcion = getCbOpciones().getSelectedItem().toString();

			// Si el rol es ADMINISTRADOR
			if (rol.equals(rols[1])) {
				// Si la opcion es HORARIO DE INSTALACION
				if (opcion.equals(optionsAdministrator[1])) {
					pideIdInstalacion();
				}
				// Si la opcion es CREAR ACTIVIDAD
				else if (opcion.equals(optionsAdministrator[2])) {
					crearActividad();
				}
				// Si la opcion es PLANIFICAR ACTIVIDAD
				else if (opcion.equals(optionsAdministrator[3])) {
					planificarActividad();
				}
				// Si la opcion es RESERVAR PLAZA CON LIMITE
				else if (opcion.equals(optionsAdministrator[4])) {
					reservarPlazaConLimite(rol);
				}
				// Si la opcion es ASIGNAR MONITOR
				else if (opcion.equals(optionsAdministrator[5])) {
					asignarMonitor();
				}
				// Si la opcion es PLANIFICAR ACTIVIDAD PERIODICA
				else if (opcion.equals(optionsAdministrator[6])) {
					planificarActividadPeriodica();
				}
				// Si la opcion es MODIFICAR INSTALACION RESERVA SIN SOCIOS
				else if (opcion.equals(optionsAdministrator[7])) {
					modificarInstalacionReservaSinSocios();
				}
				// Si la opcion es MODIFICAR HORARIO RESERVA SIN SOCIOS
				else if (opcion.equals(optionsAdministrator[8])) {
					modificarHorarioReservaSinSocios();
				}

				else if (opcion.equals(optionsAdministrator[9])) {
					modificaActividad();
				} else if (opcion.equals(optionsAdministrator[10])) {
					modificaMonitor();
				}
				// Si la opcion es CANCELAR RESERVA
				else if (opcion.equals(optionsAdministrator[11])) {
					cancelarReserva();
				} else if (opcion.equals(optionsAdministrator[12])) {
					cancelarAlquiler();
				}
				// Si la opcion es ALQUILAR INSTALACION SOCIO
				else if (opcion.equals(optionsAdministrator[13])) {
					alquilarInstalacionSocio();
				} else if (opcion.equals(optionsAdministrator[14])) {
					modificarRecursosActividad();
					// Si la opcion es REGISTRAR PAGO
				} else if (opcion.equals(optionsAdministrator[15])) {
					registrarPago();
				}
			} // Si el rol es SOCIO
			else if (rol.equals(rols[2])) {
				// Si la opcion es HORARIO ACTIVIDADES
				if (opcion.equals(optionsSocio[1])) {
					compruebaHorarioActividades();
				}
				// Si la opcion es RESERVAR PLAZA CON LIMITE
				else if (opcion.equals(optionsSocio[2])) {
					reservarPlazaConLimite(rol);
				}
				// Si la opcion es ANULAR RESERVA
				else if (opcion.equals(optionsSocio[3])) {
					anularReserva();
				}
				// Si la opcion es ASISTENCIAS A RESERVAS
				else if (opcion.equals(optionsSocio[4])) {
					mostrarAsistencias();
				}
				// Si la opcion es ALQUILAR INSTALACION
				else if (opcion.equals(optionsSocio[5])) {
					alquilarInstalacion();
				} // Si la opcion es VER ALQUILERES SIN PAGAR
				else if (opcion.equals(optionsSocio[6])) {
					alquileresSinPagar();
				} else if (opcion.equals(optionsSocio[7])) {
					cancelarAlquilerSocio();
				} else if (opcion.equals(optionsSocio[8])) {
					informacionPagos();
				}
			} // Si el rol es MONITOR
			else if (rol.equals(rols[3])) {
				// Si la opcion es ASISTENTES ACTIVIDAD
				if (opcion.equals(optionsMonitor[1])) {
					compruebaAsistenciaMonitor();
				}
				// Si la opci�n es REGISTRAR ASISTENCIA
				else if (opcion.equals(optionsMonitor[2])) {
					registrarAsistencia();
				}
				// Si la opci�n es APUNTAR SOCIO
				else if (opcion.equals(optionsMonitor[3])) {
					apuntarSocio();
				}
				// Si la opci�n es PARTE INCIDENCIAS
				else if (opcion.equals(optionsMonitor[4])) {
					parteIncidencias();
				}
			}
		} catch (Exception i) {
			i.printStackTrace();
			muestraMensajeError(i.getMessage());
		}
	}

	private void informacionPagos() {
		InfoPagosUI v = new InfoPagosUI(this);
		v.setVisible(true);
	}

	private void cancelarAlquilerSocio() {
		try {
			VentanaSociosConReservas v = new VentanaSociosConReservas(this);
			v.setVisible(true);
		} catch (IllegalArgumentException e) {
			muestraMensajeError(e.getMessage());
		}
	}

	private void registrarPago() {
		VentanaRegistrarPago v = new VentanaRegistrarPago(this);
		v.setVisible(true);

	}

	private void alquileresSinPagar() {
		VentanaAccederComoSocioGenerica vID = new VentanaAccederComoSocioGenerica(this);
		int idsocio = vID.showDialog();
		if (idsocio != -1) {// idSocio = -1 -> no se selecciono ningun socio (cerrar ventana o cancelar)
			VentanaAlquilerSinPagar v = new VentanaAlquilerSinPagar(this, idsocio);
			v.setVisible(true);
		}
	}

	private void modificarRecursosActividad() {
		VentanaModificarRecursosActividad v = new VentanaModificarRecursosActividad(this);
		v.setVisible(true);

	}

	private void alquilarInstalacionSocio() {
		VentanaAlquilarInstalacionSocio v = new VentanaAlquilarInstalacionSocio(this);
		v.setVisible(true);
	}

	private void alquilarInstalacion() {
		VentanaAccederComoSocio v = new VentanaAccederComoSocio(this);
		v.setVisible(true);
	}

	private void cancelarAlquiler() {
		try {
			AdministracionEliminaAlquilerSocio v = new AdministracionEliminaAlquilerSocio(this);
			v.setVisible(true);
		} catch (IllegalArgumentException e) {
			muestraMensajeError(e.getMessage());
		}

	}

	private void cancelarReserva() {
		VentanaCancelarReserva v = new VentanaCancelarReserva(this);
		v.setVisible(true);
	}

	private void apuntarSocio() {
		VentanaAccederComoMonitorConReservasHoy va = new VentanaAccederComoMonitorConReservasHoy(this);
		int id = va.showDialog();
		if (id != -1) {
			VentanaApuntarSocio v = new VentanaApuntarSocio(this, id);
			v.setVisible(true);
		}
	}

	private void modificarHorarioReservaSinSocios() {
		VentanaModificarHorarioReservaSinSocios v = new VentanaModificarHorarioReservaSinSocios(this);
		v.setVisible(true);
	}

	private void modificarInstalacionReservaSinSocios() {
		VentanaModificarInstalacionReservaSinSocios v = new VentanaModificarInstalacionReservaSinSocios(this);
		v.setVisible(true);
	}

	private void planificarActividadPeriodica() {
		VentanaCrearReservaPeriodica v = new VentanaCrearReservaPeriodica(this);
		v.setVisible(true);
	}

	private void modificaMonitor() {
		VentanaModificaReserva vm = new VentanaModificaReserva(this);
		vm.setVisible(true);

	}

	private void modificaActividad() {
		VentanaModificaActividades vm = new VentanaModificaActividades(this);
		vm.setVisible(true);

	}

	private void registrarAsistencia() {
		RegistrarAsistenciaUI r = new RegistrarAsistenciaUI(this);
		r.setVisible(true);
	}

	private void asignarMonitor() {
		AsignarMonitorUI a = new AsignarMonitorUI(this);
		a.setVisible(true);
	}

	private void mostrarAsistencias() {
		ReservasActividadesUI a = new ReservasActividadesUI(this);
		a.setVisible(true);
	}

	private void anularReserva() {
		VentanaAccederComoSocioGenerica vID = new VentanaAccederComoSocioGenerica(this);		
		int idsocio = vID.showDialog();
		if(idsocio != -1) {//idSocio = -1 -> no se selecciono ningun socio (cerrar ventana o cancelar)			
			new VentanaAnularReserva(this, idsocio);
		}

	}

	/**
	 * Historias 7162 y 7170
	 */
	private void reservarPlazaConLimite(String rol) {
		VentanaAccederComoSocioGenerica vID = new VentanaAccederComoSocioGenerica(this);		
		int idsocio = vID.showDialog();
		if(idsocio != -1) {//idSocio = -1 -> no se selecciono ningun socio (cerrar ventana o cancelar)
			new VentanaActividadFecha(this,idsocio, rol);
		}

	}

	private void parteIncidencias() {
		ParteIncidenciasUI v = new ParteIncidenciasUI(this);
		v.setVisible(true);
	}

	private void crearActividad() {
		VentanaCrearActividad v = new VentanaCrearActividad(this);
		v.setVisible(true);
	}

	private void planificarActividad() {
		VentanaCrearReserva v = new VentanaCrearReserva(this);
		v.setVisible(true);
	}

	protected void muestraMensajeError(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);

	}

	private void compruebaHorarioActividades() {
		VerHorarioActividades ha = new VerHorarioActividades();
		String lista = ha.getHorarioActividades();
		new VentanaLista(this, lista);
	}

	public void compruebaOcupacionInstalacion(int id) {
		new VentanaHorario(this, id);
	}

	private void compruebaAsistenciaMonitor() {
		int id = Control.convertirInt(JOptionPane.showInputDialog("Introduce el id del monitor"));
		if (compruebaMonitor(id)) {
			ObtenActividades(id);
		} else {
			muestraMensajeError("No existe ningun monitor con id: " + id);
		}
	}

	private void ObtenActividades(int monitor) {
		String actividades = VerAsistentesActividad.obtenActividadesMonitor(monitor);
		if (actividades.equals("")) {
			muestraMensajeError("No hay futuras actividades asignadas a este monitor");
		} else {
			int id = Control.convertirInt(JOptionPane.showInputDialog(
					"Introduce el id de la reserva de actividad que la se desea comprobar la asistencia\n"
							+ actividades));
			if (!compruebaIdValido(id, actividades)) {
				muestraMensajeError("El id no est� en la lista de actividades que imparte el monitor");
			} else if (!compruebaHoraAntes(id)) {
				muestraMensajeError(
						"S�lo puedes ver la asistencia a una actividad una hora antes de que comience �sta");
			} else {
				String asistencia = VerAsistentesActividad.obtenAsistentes(id);
				new VentanaLista(this, asistencia);
			}
		}
	}

	private boolean compruebaIdValido(int id, String actividades) {
		String[] ac = actividades.split("\n");
		for (int i = 0; i < ac.length; i++) {
			if (ac[i].split(" ")[1].equals("" + id)) {
				return true;
			}
		}
		return false;
	}

	private boolean compruebaHoraAntes(int reserva) {
		return VerAsistentesActividad.compruebaInicioReserva(reserva);
	}

	private void pideIdInstalacion() {
		// return Control.convertirInt(
		// JOptionPane.showInputDialog("Introduce el id de la Instalaci�n cuya
		// disponibilidad de desea ver"));
		VentanaSeleccionInstalacion vsi = new VentanaSeleccionInstalacion(this);
		vsi.setVisible(true);
	}

	private JLabel getLbImagen() {
		if (lbImagen == null) {
			lbImagen = new JLabel("");
			lbImagen.setHorizontalAlignment(SwingConstants.RIGHT);
			lbImagen.setVerticalAlignment(SwingConstants.TOP);
			lbImagen.setPreferredSize(new Dimension(150, 150));
			lbImagen.addComponentListener(aD);
		}
		return lbImagen;
	}

	class Adaptar extends ComponentAdapter {
		@Override
		public void componentResized(ComponentEvent e) {
			adaptarImagenLabel(lbImagen, "/img/logoSample.jpg");
		}
	}

	private void adaptarImagenLabel(JLabel label, String rutaImagen) {
		Image imgOriginal = new ImageIcon(getClass().getResource(rutaImagen)).getImage();
		if ((int) (label.getWidth()) > 0 && (int) (label.getHeight()) > 0) {
			Image imgEscalada = imgOriginal.getScaledInstance((int) (label.getWidth()), (int) (label.getHeight()),
					Image.SCALE_FAST);
			label.setIcon(new ImageIcon(imgEscalada));
		} else {
			label.setIcon(null);
		}
	}
}
