package ui.control;

import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * Clase para comprobar datos introducidos por el usuario en la igu
 * 
 * @author Ana Garcia 8-10-2019
 */
public class ControlCampos {

	/**
	 * M�todo que comprueba si el textField est� vac�o
	 * 
	 * @return booleano segun si esta vacio, true no lo est�, false s� lo est�
	 */
	public static boolean compruebaVacioTxt(JTextField texto) {
		if (texto.getText().replace(" ", "").equals("")) {
			return false;
		} else {
			return true;
		}

		// return texto.getText().trim().length() != 0;
	}

	/**
	 * M�todo que comprueba si un pasword field est� vac�o
	 * 
	 * @return booleano, false est� vacio, true no lo est�
	 */
	public static boolean compruebaVacioPf(JPasswordField pf) {
		String contrase�a = String.valueOf(pf.getPassword());
		if (contrase�a.replace(" ", "").equals("")) {
			return false;
		} else {
			return true;
		}

		// return String.valueOf(pf.getPassword()).trim().length() != 0;
	}

}
