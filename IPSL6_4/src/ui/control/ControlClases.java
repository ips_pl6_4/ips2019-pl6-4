package ui.control;

import persistence.MonitorPersistencia;

public class ControlClases {

	public static boolean compruebaMonitor(int id) {
		return MonitorPersistencia.existeMonitor(id);
	}

}
