package ui.windows;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import logic.dto.InstalacionDTO;
import ui.VentanaPrincipal;
import ui.actions.ObtenerInstalacionesConReservaAction;

public class VentanaSeleccionInstalacion extends JFrame {

	private static final long serialVersionUID = 1462451692050199108L;
	private JPanel contentPane;
	private JTable table;

	private String[] nombreColumnas = { "ID", "DESCRIPCIÓN", "NOMBRE" };
	private ModeloNoEditable modeloInstalaciones;
	private List<InstalacionDTO> instalaciones;
	private ObtenerInstalacionesConReservaAction oia;
	private VentanaPrincipal vp;

	/**
	 * Create the frame.
	 */
	public VentanaSeleccionInstalacion(VentanaPrincipal vp) {
		this.vp = vp;
		oia = new ObtenerInstalacionesConReservaAction();
		instalaciones = oia.execute();
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VentanaSeleccionInstalacion.class.getResource("/img/logoIconoSample.jpg")));
		setTitle("Centro de actividades: Instalaciones");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 656, 315);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(52, 65, 544, 129);
		contentPane.add(scrollPane);

		modeloInstalaciones = new ModeloNoEditable(nombreColumnas, 0);
		table = new JTable(modeloInstalaciones);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose(); // close window

			}
		});
		btnCancelar.setBounds(507, 239, 89, 36);
		contentPane.add(btnCancelar);

		JButton btnSeguir = new JButton("Continuar");
		btnSeguir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				continuar();
			}
		});
		btnSeguir.setBounds(408, 239, 89, 36);
		contentPane.add(btnSeguir);

		JLabel lblTablaInstalaciones = new JLabel("Seleccione la instalaci\u00F3n que desea comprobar");
		lblTablaInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTablaInstalaciones.setBounds(52, 18, 445, 36);
		contentPane.add(lblTablaInstalaciones);

		setLocationRelativeTo(vp);

		mostrarInstalaciones();
	}

	protected void continuar() {
		try {
			int filaActividad = table.getSelectedRow();
			if (filaActividad == -1) {
				throw new IllegalArgumentException("No hay ninguna instalación seleccionada");
			}
			int id = Integer.parseInt((String) table.getValueAt(filaActividad, 0));
			vp.compruebaOcupacionInstalacion(id);
		} catch (Exception e) {
			muestraMensajeError(e.getMessage());
		}

	}

	private void mostrarInstalaciones() {
		String id, nombre, descripcion;

		for (InstalacionDTO instalacion : instalaciones) {
			id = String.valueOf(instalacion.idInstalacion);
			nombre = instalacion.nombre;
			descripcion = instalacion.descripcion;

			String[] nuevaFila = new String[] { id, descripcion, nombre };

			modeloInstalaciones.addRow(nuevaFila);
		}

		table.revalidate();
		table.repaint();

	}

	protected void muestraMensajeError(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);

	}
}
