package ui.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import logic.dto.HistorialDTO;
import logic.service.socio.SocioServicesFactory;
import ui.VentanaPrincipal;

/**
 * Clase auxiliar AsistenciasHistorialFrameUI
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 23.11.2019
 */
public class AsistenciasHistorialFrameUI extends JFrame {

	private static final long serialVersionUID = -7111530865010979831L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTextPane textPane;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public AsistenciasHistorialFrameUI(JFrame ventana, JTable table) {
		this.table = table;

		setTitle("Centro deportivo: Historial de las reservas seleccionadas");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/logoIconoSample.jpg")));
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));
		contentPane.add(getScrollPane());

		setLocationRelativeTo(ventana);
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTextPane());
		}
		return scrollPane;
	}

	private JTextPane getTextPane() {
		if (textPane == null) {
			textPane = new JTextPane();
			textPane.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
			textPane.setEditable(false);
			refreshTextPane();
		}
		return textPane;
	}

	private void appendLine(String text) {
		getTextPane().setText(getTextPane().getText() + text + "\n");
	}

	private void refreshTextPane() {
		getTextPane().setText("");
		int[] selectedRows = this.table.getSelectedRows();
		if (selectedRows.length == 0)
			appendLine("No ha seleccionado ninguna asistencia.");
		for (int j = 0; j < selectedRows.length; j++) {
			int i = selectedRows[j];
			int reservaID = (int) this.table.getModel().getValueAt(i, 2);
			appendLine("- Reserva con ID: " + reservaID);
			List<HistorialDTO> historial = SocioServicesFactory.getObtenerHistorialesPorReserva(reservaID);
			if (historial.size() == 0)
				appendLine("No hay ning�n registro anterior guardado de esta reserva.");
			for (HistorialDTO historialIterado : historial) {
				appendLine("---> Fecha anterior: " + historialIterado.fechaAnterior + " , Hora de inicio anterior: "
						+ historialIterado.horaInicialAnterior + " , Hora de finalizaci�n anterior: "
						+ historialIterado.horaFinalAnterior + " , N� de plazas anterior: "
						+ historialIterado.numPlazasAnterior);
			}
		}
	}
}
