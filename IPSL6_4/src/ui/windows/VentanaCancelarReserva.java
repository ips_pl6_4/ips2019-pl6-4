package ui.windows;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import logic.CancelarReserva;
import logic.util.container.Reserva;
import ui.VentanaPrincipal;
import java.awt.Toolkit;

public class VentanaCancelarReserva extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JButton btnAnularReserva;
	private JList<Reserva> list;
	private JDateChooser selectorFecha;
	private DefaultListModel<Reserva> lm;



	/**
	 * Create the frame.
	 */
	public VentanaCancelarReserva(VentanaPrincipal v) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaCancelarReserva.class.getResource("/img/logoIconoSample.jpg")));
		setResizable(false);
		setTitle("Cancelar Reserva");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 550, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getSelectorFecha());//esto primero para tener actualizado la lista
		contentPane.add(getScrollPane());
		contentPane.add(getBtnAnularReserva());
		
		setLocationRelativeTo(v);
		
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 34, 270, 205);
			scrollPane.setViewportView(getList());
		}
		return scrollPane;
	}
	private JButton getBtnAnularReserva() {
		if (btnAnularReserva == null) {
			btnAnularReserva = new JButton("Anular Reserva");
			btnAnularReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					anularReserva();
				}
			});
			btnAnularReserva.setBounds(327, 128, 183, 23);
		}
		return btnAnularReserva;
	}
	private void anularReserva() {
		try {
			int idReserva;
			try {
				idReserva = list.getSelectedValue().getiDReserva();
			} catch (NullPointerException e) {
				throw new RuntimeException("Ningun elemento seleccionado");
			}
			new CancelarReserva().execute(idReserva);
			ActualizarLista();
			JOptionPane.showMessageDialog(this, "La operación fue un exito");
		} catch (RuntimeException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	private JDateChooser getSelectorFecha() {
		if (selectorFecha == null) {
			selectorFecha = new JDateChooser();
			selectorFecha.addPropertyChangeListener(new PropertyChangeListener() {
			        @Override
			        public void propertyChange(PropertyChangeEvent e) {
			            if ("date".equals(e.getPropertyName())) {
			                ActualizarLista();//(Date) e.getNewValue() no necesito pasar el neuvo valor
			            }
			        }
					
			});
			selectorFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
			Date fechaActual = new Date();
			selectorFecha.setMinSelectableDate(fechaActual);
			selectorFecha.setDate(fechaActual);

			selectorFecha.setBounds(327, 70, 183, 36);
		}
		return selectorFecha;
	}
	private void ActualizarLista() {
		if(list != null) {
			lm = new DefaultListModel<Reserva>();			
			List<Reserva> lista = getReservas();
			for (Reserva theReserva : lista)
				lm.addElement(theReserva);
	
			list.setModel(lm);
		}
	}
	private JList<Reserva> getList() {
		if (list == null) {
			list = new JList<Reserva>();
			lm = new DefaultListModel<Reserva>();			
			List<Reserva> lista = getReservas();
			for (Reserva theReserva : lista)
				lm.addElement(theReserva);

			list.setModel(lm);
		}
		return list;
	}
	private List<Reserva> getReservas() {
		Date fecha = selectorFecha.getDate();
		return new CancelarReserva().getReservasFecha(fecha);
	}
}
