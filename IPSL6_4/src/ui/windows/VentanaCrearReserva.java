package ui.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;

import logic.dto.ActividadDTO;
import logic.dto.InstalacionDTO;
import logic.dto.RecursoDTO;
import logic.dto.RecursoInstalacionDTO;
import logic.dto.ReservaDTO;
import ui.actions.CrearReservaAction;
import ui.actions.ObtenerActividadesAction;
import ui.actions.ObtenerDisponibilidadAction;
import ui.actions.ObtenerInstalacionesAction;
import ui.actions.ObtenerNumPlazasActInstAction;
import ui.actions.ObtenerRecursosActividadAction;
import ui.actions.ObtenerRecursosInstalacionAction;

public class VentanaCrearReserva extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JLabel lblActividades;
	private JLabel lblInstalaciones;
	private JLabel lblFechaReserva;
	private JLabel lblHoraInicio;
	private JLabel lblHoraFin;
	private JLabel lblPlazasLimitadas;
	private JLabel lblRecursosInstalacion;
	private JLabel lblRecursosParaLa;
	private JLabel lblNumplazas;

	private JScrollPane scrollPaneActividades;
	private JScrollPane scrollPaneInstalaciones;

	private JTable tableInstalaciones;
	private JTable tableActividades;

	ModeloNoEditable modeloActividades;
	ModeloNoEditable modeloInstalaciones;
	ModeloNoEditable modeloRecursosActividad;
	ModeloNoEditable modeloRecursosInstalacion;

	private JDateChooser selectorFecha;

	private JCheckBox chckbxDaCompleto;

	private JSpinner spinnerHoraInicial;
	private JSpinner spinnerHoraFinal;

	private JButton btnCancelar;
	private JButton btnCrearReserva;

	private CrearReservaAction cra;

	private JScrollPane scrollPaneRecursosActividad;
	private JScrollPane scrollPaneRecursosInstalacion;

	private JTable tableRecursosActividad;
	private JTable tableRecursosInstalacion;

	List<InstalacionDTO> instalaciones;
	List<ActividadDTO> actividades;
	List<RecursoDTO> recursosActividad;
	List<RecursoInstalacionDTO> recursosInstalacion;

	private ObtenerActividadesAction oaa;
	private ObtenerInstalacionesAction oia;
	private ObtenerNumPlazasActInstAction onp;
	private JLabel lblNewLabel;
	private JTextField textFieldNumPlazas;

	public VentanaCrearReserva(JFrame ventanaPrincipal) {
		setResizable(false);
		setTitle("Planificar actividad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 861, 634);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblActividades());
		contentPane.add(getLblInstalaciones());
		contentPane.add(getScrollPaneActividades());
		contentPane.add(getScrollPaneInstalaciones());
		contentPane.add(getSelectorFecha());
		contentPane.add(getLblFechaReserva());
		contentPane.add(getChckbxDaCompleto());
		contentPane.add(getSpinnerHoraInicial());
		contentPane.add(getSpinnerHoraFinal());
		contentPane.add(getLblHoraInicio());
		contentPane.add(getLblHoraFin());
		contentPane.add(getLblPlazasLimitadas());
		contentPane.add(getBtnCancelar());
		contentPane.add(getBtnCrearReserva());
		contentPane.add(getLblRecursosParaLa());
		contentPane.add(getLblRecursosInstalacion());
		contentPane.add(getScrollPaneRecursosActividad());
		contentPane.add(getScrollPaneRecursosInstalacion());
		contentPane.add(getLblNumplazas());
		contentPane.add(getLblNewLabel());
		contentPane.add(getTextFieldNumPlazas());

		setLocationRelativeTo(ventanaPrincipal);

		oaa = new ObtenerActividadesAction();
		oia = new ObtenerInstalacionesAction();

		instalaciones = oia.execute();
		actividades = oaa.execute();

		mostrarActividades();
		mostrarInstalaciones();
	}

	private JLabel getLblActividades() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Actividades");
			lblActividades.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblActividades.setBounds(10, 11, 183, 36);
		}
		return lblActividades;
	}

	private JLabel getLblInstalaciones() {
		if (lblInstalaciones == null) {
			lblInstalaciones = new JLabel("Instalaciones");
			lblInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInstalaciones.setBounds(10, 203, 183, 36);
		}
		return lblInstalaciones;
	}

	private JScrollPane getScrollPaneActividades() {
		if (scrollPaneActividades == null) {
			scrollPaneActividades = new JScrollPane();
			scrollPaneActividades.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPaneActividades.setBounds(10, 45, 544, 129);
			scrollPaneActividades.setViewportView(getTableActividades());
		}

		return scrollPaneActividades;
	}

	private JTable getTableActividades() {
		if (tableActividades == null) {
			String[] nombreColumnas = { "ID", "NOMBRE", "INTENSIDAD" };
			modeloActividades = new ModeloNoEditable(nombreColumnas, 0);
			tableActividades = new JTable(modeloActividades);
			tableActividades.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					mostrarRecursosActividad();
					seleccionarActividadInstalacion();
				}

			});
			tableActividades.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableActividades;
	}

	private JScrollPane getScrollPaneInstalaciones() {
		if (scrollPaneInstalaciones == null) {
			scrollPaneInstalaciones = new JScrollPane();
			scrollPaneInstalaciones.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPaneInstalaciones.setBounds(10, 240, 544, 129);
			scrollPaneInstalaciones.setViewportView(getTableInstalaciones());
		}
		return scrollPaneInstalaciones;
	}

	private JTable getTableInstalaciones() {
		if (tableInstalaciones == null) {
			String[] nombreColumnas = { "ID", "NOMBRE" };
			modeloInstalaciones = new ModeloNoEditable(nombreColumnas, 0);
			tableInstalaciones = new JTable(modeloInstalaciones);
			tableInstalaciones.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					mostrarRecursosInstalacion();
					seleccionarActividadInstalacion();
				}
			});
			tableInstalaciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableInstalaciones;
	}

	protected void seleccionarActividadInstalacion() {
		int filaActividad = tableActividades.getSelectedRow();
		int filaColumna = tableInstalaciones.getSelectedRow();

		if (filaActividad != -1 && filaColumna != -1) {
			onp = new ObtenerNumPlazasActInstAction(recursosActividad, recursosInstalacion);
			int numPlazas = onp.execute();

			// Sin plazas
			if (numPlazas == -1) {
				btnCrearReserva.setEnabled(false);
				lblNumplazas.setText("NO HAY RECURSOS SUFICIENTES");

				textFieldNumPlazas.setText("");
				textFieldNumPlazas.setEnabled(false);
				textFieldNumPlazas.setEditable(false);
			}
			// Plazas infinitas
			else if (numPlazas == 0) {
				btnCrearReserva.setEnabled(true);
				lblNumplazas.setText("PLAZAS SIN LIMITE");

				textFieldNumPlazas.setText("");
				textFieldNumPlazas.setEnabled(true);
				textFieldNumPlazas.setEditable(true);
			}
			// Plazas limitadas
			else {
				btnCrearReserva.setEnabled(true);
				lblNumplazas.setText("PLAZAS LIMITADAS: " + numPlazas + " PLAZAS");

				textFieldNumPlazas.setText(String.valueOf(numPlazas));
				textFieldNumPlazas.setEnabled(true);
				textFieldNumPlazas.setEditable(false);
			}
		}
	}

	private void mostrarActividades() {
		String id, nombre, intensidad;

		for (ActividadDTO actividad : actividades) {
			id = String.valueOf(actividad.idActividad);
			nombre = actividad.nombre;
			intensidad = actividad.intensidad.toString();

			String[] nuevaFila = new String[] { id, nombre, intensidad };

			modeloActividades.addRow(nuevaFila);
		}

		tableActividades.setModel(modeloActividades);

		tableActividades.revalidate();
		tableActividades.repaint();
	}

	private void mostrarInstalaciones() {
		String id, nombre;

		for (InstalacionDTO instalacion : instalaciones) {
			id = String.valueOf(instalacion.idInstalacion);
			nombre = instalacion.nombre;

			String[] nuevaFila = new String[] { id, nombre };

			modeloInstalaciones.addRow(nuevaFila);
		}

		tableInstalaciones.setModel(modeloInstalaciones);

		tableInstalaciones.revalidate();
		tableInstalaciones.repaint();
	}

	private void mostrarRecursosActividad() {
		crearModeloRecursosActividad();
		int idActividad = actividades.get(tableActividades.getSelectedRow()).idActividad;

		String idRecurso, nombre;

		recursosActividad = new ObtenerRecursosActividadAction(idActividad).execute();

		for (RecursoDTO r : recursosActividad) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;

			String[] nuevaFila = new String[] { idRecurso, nombre };

			modeloRecursosActividad.addRow(nuevaFila);
		}

		tableRecursosActividad.setModel(modeloRecursosActividad);

		tableRecursosActividad.revalidate();
		tableRecursosActividad.repaint();
	}

	private void mostrarRecursosInstalacion() {
		crearModeloRecursosInstalacion();
		int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

		String idRecurso, nombre, cantidad;

		recursosInstalacion = new ObtenerRecursosInstalacionAction(idInstalacion).execute();

		for (RecursoInstalacionDTO r : recursosInstalacion) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;
			cantidad = String.valueOf(r.cantidad);

			String[] nuevaFila = new String[] { idRecurso, nombre, cantidad };

			modeloRecursosInstalacion.addRow(nuevaFila);
		}

		tableRecursosInstalacion.setModel(modeloRecursosInstalacion);

		tableRecursosInstalacion.revalidate();
		tableRecursosInstalacion.repaint();
	}

	private void crearModeloRecursosActividad() {
		String[] nombreColumnas = { "ID", "NOMBRE" };
		modeloRecursosActividad = new ModeloNoEditable(nombreColumnas, 0);
	}

	private void crearModeloRecursosInstalacion() {
		String[] nombreColumnas = { "ID", "NOMBRE", "CANTIDAD" };
		modeloRecursosInstalacion = new ModeloNoEditable(nombreColumnas, 0);
	}

	private JLabel getLblFechaReserva() {
		if (lblFechaReserva == null) {
			lblFechaReserva = new JLabel("Fecha reserva");
			lblFechaReserva.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblFechaReserva.setBounds(10, 414, 183, 36);
		}
		return lblFechaReserva;
	}

	private JDateChooser getSelectorFecha() {
		if (selectorFecha == null) {
			selectorFecha = new JDateChooser();
			selectorFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
			Date fechaActual = new Date();
			selectorFecha.setMinSelectableDate(fechaActual);
			selectorFecha.setDate(fechaActual);

			selectorFecha.setBounds(151, 414, 183, 36);
		}
		return selectorFecha;
	}

	private JCheckBox getChckbxDaCompleto() {
		if (chckbxDaCompleto == null) {
			chckbxDaCompleto = new JCheckBox("D\u00EDa completo");
			chckbxDaCompleto.setSelected(true);
			chckbxDaCompleto.setFont(new Font("Tahoma", Font.PLAIN, 14));
			chckbxDaCompleto.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					seleccionarDiaCompleto();
				}

			});
			chckbxDaCompleto.setBounds(387, 414, 123, 34);
		}
		return chckbxDaCompleto;
	}

	private JSpinner getSpinnerHoraInicial() {
		if (spinnerHoraInicial == null) {
			spinnerHoraInicial = new JSpinner();
			spinnerHoraInicial.setEnabled(false);
			spinnerHoraInicial.setModel(new SpinnerNumberModel(8, 8, 22, 1));
			spinnerHoraInicial.setBounds(537, 414, 46, 36);
		}
		return spinnerHoraInicial;
	}

	private JSpinner getSpinnerHoraFinal() {
		if (spinnerHoraFinal == null) {
			spinnerHoraFinal = new JSpinner();
			spinnerHoraFinal.setEnabled(false);
			spinnerHoraFinal.setModel(new SpinnerNumberModel(9, 9, 23, 1));
			spinnerHoraFinal.setBounds(609, 414, 46, 36);
		}
		return spinnerHoraFinal;
	}

	private JLabel getLblHoraInicio() {
		if (lblHoraInicio == null) {
			lblHoraInicio = new JLabel("Hora inicio");
			lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraInicio.setBounds(526, 386, 62, 17);
		}
		return lblHoraInicio;
	}

	private JLabel getLblHoraFin() {
		if (lblHoraFin == null) {
			lblHoraFin = new JLabel("Hora final");
			lblHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraFin.setBounds(607, 386, 56, 17);
		}
		return lblHoraFin;
	}

	protected void seleccionarDiaCompleto() {
		if (chckbxDaCompleto.isSelected()) {
			spinnerHoraInicial.setEnabled(false);
			spinnerHoraFinal.setEnabled(false);
		} else {
			spinnerHoraInicial.setEnabled(true);
			spinnerHoraFinal.setEnabled(true);
		}
	}

	private JLabel getLblPlazasLimitadas() {
		if (lblPlazasLimitadas == null) {
			lblPlazasLimitadas = new JLabel("Plazas");
			lblPlazasLimitadas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblPlazasLimitadas.setBounds(10, 473, 183, 36);
		}
		return lblPlazasLimitadas;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnCancelar.setBounds(10, 548, 152, 36);
		}
		return btnCancelar;
	}

	private JButton getBtnCrearReserva() {
		if (btnCrearReserva == null) {
			btnCrearReserva = new JButton("Crear reserva");
			btnCrearReserva.setEnabled(false);
			btnCrearReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					comprobarReserva();
				}
			});
			btnCrearReserva.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCrearReserva.setBounds(693, 548, 152, 36);
		}
		return btnCrearReserva;
	}

	private void comprobarReserva() {
		int filaActividad = tableActividades.getSelectedRow();
		int filaInstalacion = tableInstalaciones.getSelectedRow();

		Date fecha = selectorFecha.getDate();

		onp = new ObtenerNumPlazasActInstAction(recursosActividad, recursosInstalacion);
		int numPlazas = onp.execute();

		if (textFieldNumPlazas.getText().length() != 0) {
			numPlazas = Integer.parseInt(textFieldNumPlazas.getText());
		}

		boolean plazasLimitadas;
		if (numPlazas == 0) {
			plazasLimitadas = false;
		} else {
			plazasLimitadas = true;
		}

		int horaInicial;
		int horaFinal;

		boolean diaCompleto = chckbxDaCompleto.isSelected();
		if (diaCompleto) {
			horaInicial = 8;
			horaFinal = 23;
		} else {
			horaInicial = Integer.parseInt(spinnerHoraInicial.getValue().toString());
			horaFinal = Integer.parseInt(spinnerHoraFinal.getValue().toString());
		}

		if (!checkActividad(filaActividad)) {
			JOptionPane.showMessageDialog(this, "Debe seleccionar una actividad para hacer una reserva.",
					"Error seleccionar actividad", JOptionPane.INFORMATION_MESSAGE);
		} else if (!checkInstalacion(filaInstalacion)) {
			JOptionPane.showMessageDialog(this, "Debe seleccionar una instalacion para hacer una reserva.",
					"Error seleccionar instalación", JOptionPane.INFORMATION_MESSAGE);
		} else if (!checkHoras(diaCompleto, horaInicial, horaFinal)) {
			JOptionPane.showMessageDialog(this, "La hora inicial no puede ser anterior a la hora final.", "Error hora",
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			crearReserva(filaActividad, filaInstalacion, fecha, horaInicial, horaFinal, plazasLimitadas, numPlazas);
		}
	}

	private boolean checkActividad(int filaActividad) {
		if (filaActividad == -1) {
			return false;
		}
		return true;
	}

	private boolean checkInstalacion(int filaInstalacion) {
		if (filaInstalacion == -1) {
			return false;
		}
		return true;
	}

	private boolean checkHoras(boolean diaCompleto, int horaInicial, int horaFinal) {
		if (diaCompleto) {
			if (horaInicial == 8 && horaFinal == 23) {
				return true;
			}
		} else {
			if (horaInicial < horaFinal) {
				return true;
			}
		}
		return false;
	}

	private void crearReserva(int filaActividad, int filaInstalacion, java.util.Date fecha, int horaInicial,
			int horaFinal, boolean plazasLimitadas, int numPlazas) {
		int idActividad = actividades.get(filaActividad).idActividad;
		int idInstalacion = instalaciones.get(filaInstalacion).idInstalacion;

		int check = checkConflictos(idActividad, idInstalacion, fecha, horaInicial, horaFinal);

		if (check == 1) {
			String mensaje = "No es posible planificar la actividad debido a que la instalaci�n ya est� ocupada por otra actividad en el horario seleccionado.";
			JOptionPane.showMessageDialog(this, mensaje, "Error al planificar una actividad",
					JOptionPane.ERROR_MESSAGE);
		} else if (check == 2) {
			String mensaje = "No es posible planificar la actividad debido a que la instalaci�n ya est� alquilada por un socio en el horario seleccionado.";
			JOptionPane.showMessageDialog(this, mensaje, "Error al planificar una actividad",
					JOptionPane.ERROR_MESSAGE);
		} else {
			ReservaDTO reserva = new ReservaDTO();
			reserva.idActividad = idActividad;
			reserva.idInstalacion = idInstalacion;
			reserva.fecha = new java.sql.Date(fecha.getTime());
			reserva.horaInicial = horaInicial;
			reserva.horaFinal = horaFinal;
			reserva.isPlazasLimitadas = plazasLimitadas;
			reserva.numPlazas = numPlazas;

			cra = new CrearReservaAction(reserva);
			cra.execute();

			VentanaOperacionCorrecta correcta = new VentanaOperacionCorrecta(this,
					"La actividad ha sido planificada con exito", "Planificaci�n realizada");
			correcta.setVisible(true);
		}
	}

	private int checkConflictos(int idActividad, int idInstalacion, Date fecha, int horaInicial, int horaFinal) {
		String[] horario = new ObtenerDisponibilidadAction().execute(idInstalacion, fecha, -1);

		int resultado = 0;

		for (int i = horaInicial - 8; i < horaFinal - 8; i++) {
			if (horario[i] == "OCUPADO") {
				resultado = 1;
			} else if (horario[i] == "ALQUILADA") {
				resultado = 2;
			}
		}

		return resultado;
	}

	private JLabel getLblRecursosParaLa() {
		if (lblRecursosParaLa == null) {
			lblRecursosParaLa = new JLabel("Recursos para la actividad");
			lblRecursosParaLa.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRecursosParaLa.setBounds(576, 21, 158, 17);
		}
		return lblRecursosParaLa;
	}

	private JScrollPane getScrollPaneRecursosActividad() {
		if (scrollPaneRecursosActividad == null) {
			scrollPaneRecursosActividad = new JScrollPane();
			scrollPaneRecursosActividad.setBounds(576, 45, 269, 129);
			scrollPaneRecursosActividad.setViewportView(getTableRecursosActividad());
		}
		return scrollPaneRecursosActividad;
	}

	private JLabel getLblRecursosInstalacion() {
		if (lblRecursosInstalacion == null) {
			lblRecursosInstalacion = new JLabel("Recursos en la instalaci\u00F3n");
			lblRecursosInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRecursosInstalacion.setBounds(580, 213, 154, 17);
		}
		return lblRecursosInstalacion;
	}

	private JScrollPane getScrollPaneRecursosInstalacion() {
		if (scrollPaneRecursosInstalacion == null) {
			scrollPaneRecursosInstalacion = new JScrollPane();
			scrollPaneRecursosInstalacion.setBounds(576, 240, 269, 129);
			scrollPaneRecursosInstalacion.setViewportView(getTableRecursosInstalacion());
		}
		return scrollPaneRecursosInstalacion;
	}

	private JTable getTableRecursosActividad() {
		if (tableRecursosActividad == null) {
			tableRecursosActividad = new JTable();
			crearModeloRecursosActividad();
		}
		return tableRecursosActividad;
	}

	private JTable getTableRecursosInstalacion() {
		if (tableRecursosInstalacion == null) {
			tableRecursosInstalacion = new JTable();
			crearModeloRecursosInstalacion();
		}
		return tableRecursosInstalacion;
	}

	private JLabel getLblNumplazas() {
		if (lblNumplazas == null) {
			lblNumplazas = new JLabel("");
			lblNumplazas.setBorder(new LineBorder(new Color(0, 0, 0)));
			lblNumplazas.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNumplazas.setHorizontalAlignment(SwingConstants.CENTER);
			lblNumplazas.setBounds(151, 473, 360, 36);
		}
		return lblNumplazas;
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("N\u00FAmero de plazas");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel.setBounds(537, 472, 217, 39);
		}
		return lblNewLabel;
	}

	private JTextField getTextFieldNumPlazas() {
		if (textFieldNumPlazas == null) {
			textFieldNumPlazas = new JTextField();
			textFieldNumPlazas.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					char caracter = e.getKeyChar();
					if (textFieldNumPlazas.getText().length() == 0 && caracter == '0') {
						e.consume();
					} else if (caracter > '9' || caracter < '0') {
						e.consume();
					}
				}

				@Override
				public void keyReleased(KeyEvent arg0) {
					if (textFieldNumPlazas.getText().length() != 0) {
						lblNumplazas.setText("PLAZAS LIMITADAS: " + textFieldNumPlazas.getText() + " PLAZAS");
					} else {
						lblNumplazas.setText("PLAZAS SIN LIMITE");
					}
				}
			});
			textFieldNumPlazas.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldNumPlazas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			textFieldNumPlazas.setBounds(663, 473, 182, 36);
			textFieldNumPlazas.setColumns(10);
		}
		return textFieldNumPlazas;
	}
}
