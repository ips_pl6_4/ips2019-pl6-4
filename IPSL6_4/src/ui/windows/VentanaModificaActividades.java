package ui.windows;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.ModificaSinConflictos;
import logic.dto.ActividadDTO;
import logic.dto.Intensidad;
import ui.actions.ObtenerActividadesAction;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class VentanaModificaActividades extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTable table;
	private JLabel lblTablaActividades;
	private JTextField txNombre;
	private JLabel lblNuevoNombre;
	private JLabel lblNuevaDescripcin;
	private JLabel lblNuevaIntensidad;
	private JTextField txDescripcion;
	private JButton btnModificarNombre;
	private JButton btnModificarDescripcion;
	private JButton btnModificarIntensidad;

	private ModeloNoEditable modeloActividades;
	private List<ActividadDTO> actividades;
	private ObtenerActividadesAction oaa;
	private ModificaSinConflictos msc;
	private String[] nombreColumnas = { "ID", "NOMBRE", "INTENSIDAD", "DESCRIPCI�N" };
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	private JRadioButton rdbtnAlta;
	private JRadioButton rdbtnMedia;
	private JRadioButton rdbtnBaja;

	/**
	 * Create the frame.
	 */
	public VentanaModificaActividades(JFrame ventanaPrincipal) {
		oaa = new ObtenerActividadesAction();
		msc = new ModificaSinConflictos();
		actividades = oaa.execute();
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaModificaActividades.class.getResource("/img/logoIconoSample.jpg")));
		setTitle("Centro de deporte: modificar actividad");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 676, 564);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getScrollPane());
		contentPane.add(getLblTablaActividades());
		contentPane.add(getTxNombre());
		contentPane.add(getLblNuevoNombre());
		contentPane.add(getLblNuevaDescripcin());
		contentPane.add(getLblNuevaIntensidad());
		contentPane.add(getTxDescripcion());
		contentPane.add(getBtnModificarNombre());
		contentPane.add(getBtnModificarDescripcion());
		contentPane.add(getBtnModificarIntensidad());
		
		rdbtnAlta = new JRadioButton("ALTA");
		buttonGroup.add(rdbtnAlta);
		rdbtnAlta.setBounds(99, 420, 109, 23);
		contentPane.add(rdbtnAlta);
		
		rdbtnMedia = new JRadioButton("MODERADA");
		buttonGroup.add(rdbtnMedia);
		rdbtnMedia.setBounds(99, 457, 109, 23);
		contentPane.add(rdbtnMedia);
		
		rdbtnBaja = new JRadioButton("BAJA");
		buttonGroup.add(rdbtnBaja);
		rdbtnBaja.setBounds(99, 492, 109, 23);
		contentPane.add(rdbtnBaja);
		setLocationRelativeTo(ventanaPrincipal);
		
		mostrarActividades();
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setBounds(56, 58, 544, 129);
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}
	private JTable getTable() {
		if (table == null) {
			modeloActividades = new ModeloNoEditable(nombreColumnas, 0);
			table = new JTable(modeloActividades);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return table;
	}

	private JLabel getLblTablaActividades() {
		if (lblTablaActividades == null) {
			lblTablaActividades = new JLabel("Tabla Actividades");
			lblTablaActividades.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblTablaActividades.setBounds(62, 11, 183, 36);
		}
		return lblTablaActividades;
	}
	private JTextField getTxNombre() {
		if (txNombre == null) {
			txNombre = new JTextField();
			txNombre.setHorizontalAlignment(SwingConstants.CENTER);
			txNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txNombre.setColumns(10);
			txNombre.setBounds(99, 251, 280, 23);
		}
		return txNombre;
	}
	private JLabel getLblNuevoNombre() {
		if (lblNuevoNombre == null) {
			lblNuevoNombre = new JLabel("Nuevo nombre:");
			lblNuevoNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNuevoNombre.setBounds(100, 211, 217, 39);
		}
		return lblNuevoNombre;
	}
	private JLabel getLblNuevaDescripcin() {
		if (lblNuevaDescripcin == null) {
			lblNuevaDescripcin = new JLabel("Nueva descripci\u00F3n:");
			lblNuevaDescripcin.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNuevaDescripcin.setBounds(99, 298, 217, 39);
		}
		return lblNuevaDescripcin;
	}
	private JLabel getLblNuevaIntensidad() {
		if (lblNuevaIntensidad == null) {
			lblNuevaIntensidad = new JLabel("Nueva intensidad");
			lblNuevaIntensidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNuevaIntensidad.setBounds(100, 387, 217, 39);
		}
		return lblNuevaIntensidad;
	}
	private JTextField getTxDescripcion() {
		if (txDescripcion == null) {
			txDescripcion = new JTextField();
			txDescripcion.setHorizontalAlignment(SwingConstants.CENTER);
			txDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txDescripcion.setColumns(10);
			txDescripcion.setBounds(99, 342, 280, 23);
		}
		return txDescripcion;
	}
	private JButton getBtnModificarNombre() {
		if (btnModificarNombre == null) {
			btnModificarNombre = new JButton("Modificar");
			btnModificarNombre.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					modificaNombreActividad();
				}
			});
			btnModificarNombre.setBounds(461, 246, 89, 36);
		}
		return btnModificarNombre;
	}
	protected void modificaNombreActividad() {
		try {
			int filaActividad = table.getSelectedRow();
		if (filaActividad == -1) {
			throw new IllegalArgumentException("No hay ninguna actividad seleccionada");
		}else {
			int id = Integer.parseInt((String) table.getValueAt(filaActividad, 0));
			String nombre = getTxNombre().getText();
			compruebaNombreValido(nombre);
			msc.modificaNombre(id, nombre);
			actualizaTabla(filaActividad);
			JOptionPane.showMessageDialog(this, "La actividad "+id+" ahora se llama: "+nombre, "Operaci�n correcta", JOptionPane.INFORMATION_MESSAGE);
		}
		
		}catch(Exception e) {
			muestraMensajeError(e.getMessage());
		}
		
	}

	private void actualizaTabla(int fila) {
		modeloActividades = new ModeloNoEditable(nombreColumnas, 0);
		actividades = oaa.execute();
		mostrarActividades();
		table.setRowSelectionInterval(fila, fila);
		
	}
	private void compruebaNombreValido(String nombre) {
		if(nombre.replace(" ", "").length()==0) {
			throw new IllegalArgumentException("Introduzca un valor para el campo");
		}
		
	}

	private JButton getBtnModificarDescripcion() {
		if (btnModificarDescripcion == null) {
			btnModificarDescripcion = new JButton("Modificar");
			btnModificarDescripcion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					modificaDescripcion();
				}
			});
			btnModificarDescripcion.setBounds(461, 329, 89, 36);
		}
		return btnModificarDescripcion;
	}
	protected void modificaDescripcion() {
		try {
			int filaActividad = table.getSelectedRow();
		if (filaActividad == -1) {
			throw new IllegalArgumentException("No hay ninguna actividad seleccionada");
		}else {
			int id = Integer.parseInt((String) table.getValueAt(filaActividad, 0));
			String nombre = getTxDescripcion().getText();
			compruebaNombreValido(nombre);
			msc.modificaDescripcion(id, nombre);
			actualizaTabla(filaActividad);
			JOptionPane.showMessageDialog(this, "La actividad "+id+" ahora se describe como: "+nombre, "Operaci�n correcta", JOptionPane.INFORMATION_MESSAGE);
		}
		
		}catch(Exception e) {
			muestraMensajeError(e.getMessage());
		}
		
	}

	private JButton getBtnModificarIntensidad() {
		if (btnModificarIntensidad == null) {
			btnModificarIntensidad = new JButton("Modificar");
			btnModificarIntensidad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					modificaIntensidad();
				}
			});
			btnModificarIntensidad.setBounds(461, 450, 89, 36);
		}
		return btnModificarIntensidad;
	}
	
	protected void modificaIntensidad() {
		try {
			int filaActividad = table.getSelectedRow();
		if (filaActividad == -1) {
			throw new IllegalArgumentException("No hay ninguna actividad seleccionada");
		}else {
			int id = Integer.parseInt((String) table.getValueAt(filaActividad, 0));
			Intensidad i = getIntensidad();;
			msc.modificaIntensidad(id, i);
			actualizaTabla(filaActividad);
			JOptionPane.showMessageDialog(this, "La actividad "+id+" ahora tiene intensidad: "+i.name(), "Operaci�n correcta", JOptionPane.INFORMATION_MESSAGE);
		}
		
		}catch(Exception e) {
			muestraMensajeError(e.getMessage());
		}
		
	}

	private Intensidad getIntensidad() {
		if(rdbtnAlta.isSelected()) {
			return Intensidad.ALTA;
		}else if(rdbtnMedia.isSelected()){
			return Intensidad.MODERADA;
		}else if (rdbtnBaja.isSelected()){
			return Intensidad.BAJA;
		}else {
			throw new IllegalArgumentException("No hay ninguna intensidad marcada");
		}
	}

	protected void muestraMensajeError(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);

	}

	private void mostrarActividades() {
		String id, nombre, intensidad,descripcion;

		for (ActividadDTO actividad : actividades) {
			id = String.valueOf(actividad.idActividad);
			nombre = actividad.nombre;
			intensidad = actividad.intensidad.toString();
			descripcion = actividad.descripcion;

			String[] nuevaFila = new String[] { id, nombre, intensidad, descripcion };

			modeloActividades.addRow(nuevaFila);
		}

		table.setModel(modeloActividades);

		table.revalidate();
		table.repaint();
	}
}
