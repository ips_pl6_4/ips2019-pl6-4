package ui.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;

import logic.dto.AlquilerDTO;
import logic.dto.InstalacionDTO;
import logic.dto.RecursoInstalacionDTO;
import logic.dto.SocioDTO;
import ui.actions.CrearReservaSocioAction;
import ui.actions.ObtenerAlquilerConflictoAction;
import ui.actions.ObtenerDisponibilidadAction;
import ui.actions.ObtenerInstalacionesAction;
import ui.actions.ObtenerRecursosInstalacionAction;
import ui.actions.ObtenerSociosAction;

public class VentanaAlquilarInstalacionSocio extends JFrame {

	private String[] horas = { "08:00 a 08:59", "09:00 a 09:59", "10:00 a 10:59", "11:00 a 11:59", "12:00 a 12:59",
			"13:00 a 13:59", "14:00 a 14:59", "15:00 a 15:59", "16:00 a 16:59", "17:00 a 17:59", "18:00 a 18:59",
			"19:00 a 19:59", "20:00 a 20:59", "21:00 a 21:59", "22:00 a 22:59" };

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JLabel lblInstalaciones;
	private JScrollPane scrollPane;
	private JTable tableInstalaciones;

	ModeloNoEditable modeloInstalaciones;
	private JDateChooser selectorFecha;

	private List<InstalacionDTO> instalaciones;
	private JScrollPane scrollPane_1;
	private JLabel lblRecursosInstalacion;
	private JTable tableRecursosInstalacion;

	ModeloNoEditable modeloRecursosInstalacion;
	List<RecursoInstalacionDTO> recursosInstalacion;
	private JLabel lblHorarioInstalacin;
	private JScrollPane scrollPane_2;

	private ObtenerDisponibilidadAction od;
	private JPanel panelHorario;
	private JButton btnCancelar;
	private JButton btnVerHorario;
	private JButton btnAlquilar;

	private JLabel lblFecha;
	private JLabel lblHoraInicio;
	private JLabel lblHoraFin;
	private JSpinner spinnerHoraInicio;
	private JSpinner spinnerHoraFin;

	private int[] horasDisponibles;

	private Date fecha;
	private JCheckBox chckbxDaCompleto;
	private JLabel lblSocios;
	private JScrollPane scrollPane_3;
	private JTable tableSocios;
	private List<SocioDTO> socios;

	private int idSocio;

	public VentanaAlquilarInstalacionSocio(JFrame ventana) {
		setResizable(false);
		od = new ObtenerDisponibilidadAction();

		setTitle("Alquilar instalaci\u00F3n");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 752, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblInstalaciones());
		contentPane.add(getScrollPane());
		contentPane.add(getScrollPane_1());
		contentPane.add(getLblRecursosInstalacion());
		contentPane.add(getSelectorFecha());
		contentPane.add(getLblHorarioInstalacin());
		contentPane.add(getScrollPane_2());
		contentPane.add(getBtnCancelar());
		contentPane.add(getBtnVerHorario());
		contentPane.add(getBtnAlquilar());
		contentPane.add(getLblFecha());
		contentPane.add(getLblHoraInicio());
		contentPane.add(getLblHoraFin());
		contentPane.add(getSpinnerHoraInicio());
		contentPane.add(getSpinnerHoraFin());
		contentPane.add(getChckbxDaCompleto());
		contentPane.add(getLblSocios());
		contentPane.add(getScrollPane_3());

		setLocationRelativeTo(ventana);

		mostrarSocios();
		mostrarInstalaciones();
	}

	private JLabel getLblInstalaciones() {
		if (lblInstalaciones == null) {
			lblInstalaciones = new JLabel("Instalaciones");
			lblInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInstalaciones.setBounds(10, 143, 185, 35);
		}
		return lblInstalaciones;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 185, 343, 201);
			scrollPane.setViewportView(getTableInstalaciones());
		}
		return scrollPane;
	}

	private JTable getTableInstalaciones() {
		if (tableInstalaciones == null) {
			tableInstalaciones = new JTable();
			tableInstalaciones.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					mostrarRecursosInstalacion();

					if (tableSocios.getSelectedRow() != -1) {
						mostrarHorarioInstalacion();
						btnAlquilar.setEnabled(true);
					}
				}
			});
			tableInstalaciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableInstalaciones;
	}

	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(10, 459, 343, 134);
			scrollPane_1.setViewportView(getTableRecursosInstalacion());
		}
		return scrollPane_1;
	}

	private JLabel getLblRecursosInstalacion() {
		if (lblRecursosInstalacion == null) {
			lblRecursosInstalacion = new JLabel("Recursos instalacion");
			lblRecursosInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRecursosInstalacion.setBounds(10, 417, 205, 35);
		}
		return lblRecursosInstalacion;
	}

	private JTable getTableRecursosInstalacion() {
		if (tableRecursosInstalacion == null) {
			tableRecursosInstalacion = new JTable();
		}
		return tableRecursosInstalacion;
	}

	private void mostrarInstalaciones() {
		String[] nombreColumnas = { "ID", "NOMBRE", "PRECIO HORA" };
		modeloInstalaciones = new ModeloNoEditable(nombreColumnas, 0);

		instalaciones = new ObtenerInstalacionesAction().execute();

		String id, nombre, precioHora;

		for (InstalacionDTO instalacion : instalaciones) {
			id = String.valueOf(instalacion.idInstalacion);
			nombre = instalacion.nombre;
			precioHora = String.valueOf(instalacion.precioHora);

			String[] nuevaFila = new String[] { id, nombre, precioHora };

			modeloInstalaciones.addRow(nuevaFila);
		}

		tableInstalaciones.setModel(modeloInstalaciones);

		tableInstalaciones.revalidate();
		tableInstalaciones.repaint();
	}

	private void mostrarRecursosInstalacion() {
		String[] nombreColumnas = { "ID", "NOMBRE", "CANTIDAD" };
		modeloRecursosInstalacion = new ModeloNoEditable(nombreColumnas, 0);

		int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

		String idRecurso, nombre, cantidad;

		recursosInstalacion = new ObtenerRecursosInstalacionAction(idInstalacion).execute();

		for (RecursoInstalacionDTO r : recursosInstalacion) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;
			cantidad = String.valueOf(r.cantidad);

			String[] nuevaFila = new String[] { idRecurso, nombre, cantidad };

			modeloRecursosInstalacion.addRow(nuevaFila);
		}

		tableRecursosInstalacion.setModel(modeloRecursosInstalacion);

		tableRecursosInstalacion.revalidate();
		tableRecursosInstalacion.repaint();
	}

	private JDateChooser getSelectorFecha() {
		if (selectorFecha == null) {
			selectorFecha = new JDateChooser();
			selectorFecha.getCalendarButton().setLocation(107, 420);
			selectorFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));

			Calendar calendar = Calendar.getInstance();

			Date fechaActual = new Date();

			calendar.setTime(fechaActual);

			Date fechaInicial = calendar.getTime();

			calendar.add(Calendar.DATE, 14);

			Date fechaFinal = calendar.getTime();

			selectorFecha.setMinSelectableDate(fechaInicial);
			selectorFecha.setMaxSelectableDate(fechaFinal);
			selectorFecha.setDate(fechaInicial);

			selectorFecha.setBounds(64, 625, 128, 36);
		}
		return selectorFecha;
	}

	private JLabel getLblHorarioInstalacin() {
		if (lblHorarioInstalacin == null) {
			lblHorarioInstalacin = new JLabel("Horario Instalaci\u00F3n");
			lblHorarioInstalacin.setHorizontalAlignment(SwingConstants.CENTER);
			lblHorarioInstalacin.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblHorarioInstalacin.setBounds(431, 143, 263, 35);
		}
		return lblHorarioInstalacin;
	}

	private JScrollPane getScrollPane_2() {
		if (scrollPane_2 == null) {
			scrollPane_2 = new JScrollPane();
			scrollPane_2.setBounds(395, 185, 333, 408);
			scrollPane_2.setViewportView(getPanelHorario());
		}
		return scrollPane_2;
	}

	private JPanel getPanelHorario() {
		if (panelHorario == null) {
			panelHorario = new JPanel();
			panelHorario.setLayout(new GridLayout(15, 0, 0, 0));
		}
		return panelHorario;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cancelar();
				}
			});
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.setBounds(10, 690, 185, 36);
		}
		return btnCancelar;
	}

	protected void cancelar() {
		dispose();
	}

	private JButton getBtnVerHorario() {
		if (btnVerHorario == null) {
			btnVerHorario = new JButton("Ver horario");
			btnVerHorario.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (tableInstalaciones.getSelectedRow() != -1 && tableSocios.getSelectedRow() != -1)
						mostrarHorarioInstalacion();
				}
			});
			btnVerHorario.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnVerHorario.setBounds(214, 626, 128, 35);
		}
		return btnVerHorario;
	}

	private JButton getBtnAlquilar() {
		if (btnAlquilar == null) {
			btnAlquilar = new JButton("Alquilar");
			btnAlquilar.setEnabled(false);
			btnAlquilar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					alquilar();
				}
			});
			btnAlquilar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAlquilar.setBounds(543, 689, 185, 39);
		}
		return btnAlquilar;
	}

	private JLabel getLblFecha() {
		if (lblFecha == null) {
			lblFecha = new JLabel("Fecha");
			lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblFecha.setBounds(10, 626, 44, 35);
		}
		return lblFecha;
	}

	private JLabel getLblHoraInicio() {
		if (lblHoraInicio == null) {
			lblHoraInicio = new JLabel("Hora inicio");
			lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraInicio.setBounds(556, 604, 62, 17);
		}
		return lblHoraInicio;
	}

	private JLabel getLblHoraFin() {
		if (lblHoraFin == null) {
			lblHoraFin = new JLabel("Hora fin");
			lblHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraFin.setBounds(647, 604, 47, 17);
		}
		return lblHoraFin;
	}

	private JSpinner getSpinnerHoraInicio() {
		if (spinnerHoraInicio == null) {
			spinnerHoraInicio = new JSpinner();
			spinnerHoraInicio.setEnabled(false);
			spinnerHoraInicio.setModel(new SpinnerNumberModel(8, 8, 22, 1));
			spinnerHoraInicio.setBounds(565, 625, 44, 36);
		}
		return spinnerHoraInicio;
	}

	private JSpinner getSpinnerHoraFin() {
		if (spinnerHoraFin == null) {
			spinnerHoraFin = new JSpinner();
			spinnerHoraFin.setEnabled(false);
			spinnerHoraFin.setModel(new SpinnerNumberModel(9, 9, 23, 1));
			spinnerHoraFin.setBounds(648, 625, 44, 36);
		}
		return spinnerHoraFin;
	}

	private void mostrarHorarioInstalacion() {
		int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

		fecha = selectorFecha.getDate();

		String[] horarioInstalacion = od.execute(idInstalacion, fecha, idSocio);

		horasDisponibles = new int[15];

		panelHorario.removeAll();

		for (int i = 0; i < horas.length; i++) {
			String estado = horarioInstalacion[i];

			JButton nuevoBoton = new JButton(horas[i] + " --- " + estado);
			nuevoBoton.setForeground(Color.BLACK);

			if (estado == "OCUPADO") {
				nuevoBoton.setBackground(new Color(205, 92, 92));
				horasDisponibles[i] = 0;
			} else if (estado == "OTRA ACTIVIDAD") {
				nuevoBoton.setBackground(new Color(240, 230, 140));
				horasDisponibles[i] = 1;
			} else if (estado == "ALQUILADA") {
				nuevoBoton.setBackground(new Color(152, 251, 152));
				horasDisponibles[i] = 2;
			} else {
				nuevoBoton.setBackground(new Color(175, 238, 238));
				horasDisponibles[i] = 3;
			}

			panelHorario.add(nuevoBoton);
			nuevoBoton.setVisible(true);
		}

		panelHorario.revalidate();
		panelHorario.repaint();
	}

	private boolean comprobarHorasValidas() {
		int horaInicio;
		int horaFin;

		if (chckbxDaCompleto.isSelected()) {
			horaInicio = 8;
			horaFin = 9;
		} else {
			horaInicio = (int) spinnerHoraInicio.getValue();
			horaFin = (int) spinnerHoraFin.getValue();
		}

		horaInicio -= 8;
		horaFin -= 8;

		boolean correcto = true;

		if (horaInicio >= horaFin) {
			correcto = false;
			JOptionPane.showMessageDialog(this, "La hora inicial no puede ser anterior a la hora final.", "Error hora",
					JOptionPane.ERROR_MESSAGE);
		} else {
			for (int i = horaInicio; i < horaFin; i++) {
				int tipo = horasDisponibles[i];

				if (tipo >= 0 && tipo <= 2) {
					correcto = false;
					mostrarError(tipo, horaInicio, horaFin, fecha);
					break;
				}
			}
		}

		return correcto;
	}

	private void mostrarError(int tipo, int horaInicio, int horaFin, Date fecha) {
		if (tipo == 0) {
			errorActividadCentro();
		} else if (tipo == 1) {
			errorPlazaReservada();
		} else if (tipo == 2) {
			errorAlquiler(horaInicio, horaFin, fecha);
		}
	}

	private void errorActividadCentro() {
		String mensaje = "No es posible alquilar la instalaci�n porque esta se encuentra ocupada por una actividad del centro en el horario que ha seleccionado.";
		JOptionPane.showMessageDialog(this, mensaje, "Error al alquilar una instalaci�n", JOptionPane.ERROR_MESSAGE);
	}

	private void errorPlazaReservada() {
		String mensaje = "No es posible alquilar la instalaci�n porque usted ya tiene reservada una plaza en una actividad del centro en el horario seleccionado.";
		JOptionPane.showMessageDialog(this, mensaje, "Error al alquilar una instalaci�n", JOptionPane.ERROR_MESSAGE);
	}

	private void errorAlquiler(int horaInicio, int horaFin, Date fecha) {
		int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

		AlquilerDTO alquiler = new ObtenerAlquilerConflictoAction(idInstalacion, horaInicio, horaFin, fecha).execute();

		VentanaConflictoAlquiler v = new VentanaConflictoAlquiler(this, alquiler);
		v.setVisible(true);
	}

	private void alquilar() {
		boolean datosCorrectos = comprobarHorasValidas();

		if (datosCorrectos) {
			int horaInicio;
			int horaFin;

			int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

			if (chckbxDaCompleto.isSelected()) {
				horaInicio = 8;
				horaFin = 23;
			} else {
				horaInicio = (int) spinnerHoraInicio.getValue();
				horaFin = (int) spinnerHoraFin.getValue();
			}

			new CrearReservaSocioAction(idSocio, idInstalacion, horaInicio, horaFin, fecha).execute();

			VentanaOperacionCorrecta correcta = new VentanaOperacionCorrecta(this,
					"El alquiler ha sido realizado con exito", "Alquiler realizada");
			correcta.setVisible(true);
		}
	}

	private JCheckBox getChckbxDaCompleto() {
		if (chckbxDaCompleto == null) {
			chckbxDaCompleto = new JCheckBox("D\u00EDa completo");
			chckbxDaCompleto.setSelected(true);
			chckbxDaCompleto.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					seleccionarDiaCompleto();
				}
			});
			chckbxDaCompleto.setFont(new Font("Tahoma", Font.PLAIN, 14));
			chckbxDaCompleto.setBounds(395, 632, 133, 23);
		}
		return chckbxDaCompleto;
	}

	protected void seleccionarDiaCompleto() {
		if (chckbxDaCompleto.isSelected()) {
			spinnerHoraInicio.setEnabled(false);
			spinnerHoraFin.setEnabled(false);
		} else {
			spinnerHoraInicio.setEnabled(true);
			spinnerHoraFin.setEnabled(true);
		}
	}

	private JLabel getLblSocios() {
		if (lblSocios == null) {
			lblSocios = new JLabel("Socios");
			lblSocios.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblSocios.setBounds(10, 0, 185, 35);
		}
		return lblSocios;
	}

	private JScrollPane getScrollPane_3() {
		if (scrollPane_3 == null) {
			scrollPane_3 = new JScrollPane();
			scrollPane_3.setBounds(10, 33, 713, 99);
			scrollPane_3.setViewportView(getTableSocios());
		}
		return scrollPane_3;
	}

	private JTable getTableSocios() {
		if (tableSocios == null) {
			tableSocios = new JTable();
			tableSocios.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					idSocio = socios.get(tableSocios.getSelectedRow()).socioID;
					if (tableInstalaciones.getSelectedRow() != -1) {
						mostrarHorarioInstalacion();
						btnAlquilar.setEnabled(true);
					}
				}
			});
		}
		return tableSocios;
	}

	private void mostrarSocios() {
		String[] nombreColumnas = { "ID", "NOMBRE", "APELLIDOS", "EMAIL", "TEL�FONO" };

		ModeloNoEditable modeloSocios = new ModeloNoEditable(nombreColumnas, 0);

		socios = new ObtenerSociosAction().execute();

		String id, nombre, apellidos, email, telefono;

		for (SocioDTO socio : socios) {
			id = String.valueOf(socio.socioID);
			nombre = socio.name;
			apellidos = socio.apellidos;
			email = socio.mail;
			telefono = String.valueOf(socio.telefono);

			String[] nuevaFila = new String[] { id, nombre, apellidos, email, telefono };

			modeloSocios.addRow(nuevaFila);
		}

		tableSocios.setModel(modeloSocios);

		tableSocios.revalidate();
		tableSocios.repaint();
	}
}
