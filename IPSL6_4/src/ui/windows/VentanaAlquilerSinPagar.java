package ui.windows;

import java.awt.Toolkit;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import logic.AlquilerSinPagar;
import logic.dto.AlquilerDTO;
import logic.dto.InstalacionDTO;
import ui.VentanaPrincipal;

public class VentanaAlquilerSinPagar extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private ModeloNoEditable modeloTable;
	private JLabel lblPrecioTotal;
	private JTextField textFieldPrecioTotal;
	private int idSocio;
	private JScrollPane scrollPane;



	/**
	 * Create the frame.
	 */
	public VentanaAlquilerSinPagar(VentanaPrincipal v,int idSocio) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaAlquilerSinPagar.class.getResource("/img/logoIconoSample.jpg")));
		this.idSocio = idSocio;
		setTitle("Alquileres sin pagar ");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);		
		setBounds(100, 100, 449, 276);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblPrecioTotal());
		contentPane.add(getTextFieldPrecioTotal());
		contentPane.add(getScrollPane());
		
		setLocationRelativeTo(v);
	}
	private JTable getTable() {
		if (table == null) {
			String[] nombreColumnas = {"INSTALACION", "H.INICIO", "FECHA" ,"FECHA PAGO","PRECIO" };
			modeloTable = new ModeloNoEditable(nombreColumnas, 0);
			llenarTabla();
			table = new JTable(modeloTable);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return table;
	}
	
	private void llenarTabla() {
		List<AlquilerDTO> alquileres = new AlquilerSinPagar().getAlquileres(idSocio);
		List<InstalacionDTO> instalaciones = new AlquilerSinPagar().getInstalaciones();
		int sumatorioPrecio = 0;
		for(AlquilerDTO theAlquiler: alquileres) {
			InstalacionDTO instalacion = null;
			for(InstalacionDTO theInstalacion: instalaciones)
				if(theInstalacion.idInstalacion == theAlquiler.idInstalacion)
					instalacion = theInstalacion;		
			instalacion.toString();
			modeloTable.addRow(new String[] {instalacion.nombre, String.valueOf(theAlquiler.horaInicial), theAlquiler.fecha.toString(),theAlquiler.fechaPago.toString(),String.valueOf(instalacion.precioHora)});
		sumatorioPrecio += instalacion.precioHora;			
		}
			textFieldPrecioTotal.setText(String.valueOf(sumatorioPrecio));		
	}
	private JLabel getLblPrecioTotal() {
		if (lblPrecioTotal == null) {
			lblPrecioTotal = new JLabel("Precio Total:");
			lblPrecioTotal.setBounds(259, 214, 78, 14);
		}
		return lblPrecioTotal;
	}
	private JTextField getTextFieldPrecioTotal() {
		if (textFieldPrecioTotal == null) {
			textFieldPrecioTotal = new JTextField();
			textFieldPrecioTotal.setEditable(false);
			textFieldPrecioTotal.setBounds(347, 211, 86, 20);
			textFieldPrecioTotal.setColumns(10);
		}
		return textFieldPrecioTotal;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 37, 423, 137);
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}
}
