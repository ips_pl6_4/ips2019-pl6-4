package ui.windows;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import logic.dto.SocioDTO;
import ui.VentanaPrincipal;
import ui.actions.ObtenerSociosConReservasAction;

public class VentanaSociosConReservas extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblSocios;
	private JScrollPane scrollPane;
	private JTable tableSocios;
	private JButton btnCancelar;
	private JButton btnSiguiente;
	private List<SocioDTO> socios;

	public VentanaSociosConReservas(VentanaPrincipal ventanaPrincipal) {
		setResizable(false);
		setTitle("Acceder como socio");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 703, 308);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblSocios());
		contentPane.add(getScrollPane());
		contentPane.add(getBtnCancelar());
		contentPane.add(getBtnSiguiente());

		setLocationRelativeTo(ventanaPrincipal);
		mostrarSocios();
		
	}

	private JLabel getLblSocios() {
		if (lblSocios == null) {
			lblSocios = new JLabel("Socios");
			lblSocios.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblSocios.setBounds(17, 11, 108, 38);
		}
		return lblSocios;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(17, 49, 663, 154);
			scrollPane.setViewportView(getTableSocios());
		}
		return scrollPane;
	}

	private JTable getTableSocios() {
		if (tableSocios == null) {
			tableSocios = new JTable();
			tableSocios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableSocios.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					btnSiguiente.setEnabled(true);
				}
			});
		}
		return tableSocios;
	}
	
	protected void muestraMensajeError(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);

	}

	private void mostrarSocios() {
		String[] nombreColumnas = { "ID", "NOMBRE", "APELLIDOS", "EMAIL", "TEL�FONO" };

		ModeloNoEditable modeloSocios = new ModeloNoEditable(nombreColumnas, 0);

		socios = new ObtenerSociosConReservasAction().execute();

		String id, nombre, apellidos, email, telefono;
		if(socios.size()==0) {
			throw new IllegalArgumentException("No hay socios con reservas que cancelar");
		}
		for (SocioDTO socio : socios) {
			id = String.valueOf(socio.socioID);
			nombre = socio.name;
			apellidos = socio.apellidos;
			email = socio.mail;
			telefono = String.valueOf(socio.telefono);

			String[] nuevaFila = new String[] { id, nombre, apellidos, email, telefono };

			modeloSocios.addRow(nuevaFila);
		}

		tableSocios.setModel(modeloSocios);

		tableSocios.revalidate();
		tableSocios.repaint();
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.setBounds(17, 227, 195, 38);
		}
		return btnCancelar;
	}

	private JButton getBtnSiguiente() {
		if (btnSiguiente == null) {
			btnSiguiente = new JButton("Siguiente");
			btnSiguiente.setEnabled(false);
			btnSiguiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					seleccionarSocio();
				}
			});
			btnSiguiente.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnSiguiente.setBounds(485, 227, 195, 38);
		}
		return btnSiguiente;
	}

	protected void seleccionarSocio() {
		int idSocio = socios.get(tableSocios.getSelectedRow()).socioID;
		
		SocioEliminaAlquilerSocio v = new SocioEliminaAlquilerSocio(this, idSocio);
		v.setVisible(true);

		dispose();

	}
}
