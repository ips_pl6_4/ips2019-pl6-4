package ui.windows;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
//import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import logic.ApuntarSocio;
import logic.dto.SocioDTO;
import logic.service.socio.SocioServicesFactory;
import logic.util.LogicAuxiliar;
import logic.util.container.Reserva;
import persistence.ReservarPlazaConLimitePersistencia;
import ui.VentanaPrincipal;
import ui.windows.util.ComboBoxItemSocio;

public class VentanaApuntarSocio extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	// private JList<Reserva> list;
	private JButton btnApuntar;

	private Reserva reserva;

	// private int idMonitor;
	// private JTable table;
	private ModeloNoEditable modeloReservas;
	private JTable tableSociosApuntados;
	private JComboBox<ComboBoxItemSocio> comboBoxListaSocio;
	private List<SocioDTO> sociosApuntados;
	private JLabel lblSocioApuntados;
	private ApuntarSocio a;
	private JLabel lblActividad;
	private JLabel lblNumPlazas;
	private JTextField textFieldActividad;
	private JTextField textFieldNumPlazas;
	private JLabel lblNumPlazasTotales;
	private JTextField textFieldPlazasTotales;

	/**
	 * Create the frame.
	 */
	public VentanaApuntarSocio(VentanaPrincipal v, int idMonitor) {
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VentanaApuntarSocio.class.getResource("/img/logoIconoSample.jpg")));
		// this.idMonitor = idMonitor;
		a = new ApuntarSocio();
		reserva = a.getReservaMonitor(idMonitor);

		sociosApuntados = a.getSociosApuntados(reserva.getiDReserva());

		setResizable(false);
		setTitle("Apuntar Socio");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getTextFieldActividad());
		contentPane.add(getTextFieldNumPlazas());
		contentPane.add(getScrollPane());
		contentPane.add(getBtnApuntar());
		contentPane.add(getComboBoxListaSocio());
		contentPane.add(getLblSocioApuntados());
		contentPane.add(getLblActividad());
		contentPane.add(getLblNumPlazas());
		contentPane.add(getLblNumPlazasTotales());
		contentPane.add(getTextFieldPlazasTotales());
		llenarCabecera();

		setLocationRelativeTo(v);
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 48, 257, 181);
			scrollPane.setViewportView(getTableSociosApuntados());
			// scrollPane.setViewportView(getTable());
			// scrollPane.setViewportView(getList());
		}
		return scrollPane;
	}

	/*
	 * private JList<Reserva> getList() { if (list == null) { list = new
	 * JList<Reserva>(); DefaultListModel<Reserva> lm = new
	 * DefaultListModel<Reserva>(); List<Reserva> lista = getReservasMonitor();
	 * for (Reserva theReserva : lista) lm.addElement(theReserva);
	 * 
	 * list.setModel(lm); } return list; }
	 */

	/*
	 * private List<Reserva> getReservasMonitor() { return new
	 * ApuntarSocio().getReservasMonitor(idMonitor); }
	 */

	private JButton getBtnApuntar() {
		if (btnApuntar == null) {
			btnApuntar = new JButton("Apuntar");
			btnApuntar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					execute();
				}
			});
			btnApuntar.setBounds(336, 192, 89, 23);
		}
		return btnApuntar;
	}

	private void execute() {

		if (Integer.parseInt(textFieldNumPlazas.getText()) == 0) {

			int idReserva = reserva.getiDReserva();// (String)tablaProductos.getValueAt(filaSeleccionada,0);
			int idSocio = ((ComboBoxItemSocio) comboBoxListaSocio.getModel().getSelectedItem()).getValue().socioID;
			try {
				new ApuntarSocio().execute(idSocio, idReserva);
				actualizarTabla();
				JOptionPane.showMessageDialog(this, "La operación fue un exito");
				refreshComboBoxSocios();
				llenarCabecera();
				if (Integer.parseInt(textFieldNumPlazas.getText()) == 0) {
					btnApuntar.setEnabled(false);
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		} else
			btnApuntar.setEnabled(false);

	}
	/*
	 * private JTable getTable() { if (table == null) { String[] nombreColumnas
	 * = { "Nombre", "Hora Inicio", "Hora fin" , "Plazas Libres" };
	 * modeloReservas = new ModeloNoEditable(nombreColumnas, 0); table = new
	 * JTable(modeloReservas); actualizarTabla();
	 * table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); } return
	 * table; }
	 */

	private void actualizarTabla() {
		String[] nombreColumnas = { "ID", "Nombre", "Apellidos" };
		modeloReservas = new ModeloNoEditable(nombreColumnas, 0);
		sociosApuntados = a.getSociosApuntados(reserva.getiDReserva());
		for (SocioDTO theSocio : sociosApuntados) {
			modeloReservas.addRow(new String[] { String.valueOf(theSocio.socioID), theSocio.name, theSocio.apellidos });
		}
		tableSociosApuntados.setModel(modeloReservas);

		tableSociosApuntados.revalidate();
		tableSociosApuntados.repaint();

	}

	private JTable getTableSociosApuntados() {
		if (tableSociosApuntados == null) {
			tableSociosApuntados = new JTable();
			actualizarTabla();
		}
		return tableSociosApuntados;
	}

	private void refreshComboBoxSocios() {
		List<SocioDTO> socios = SocioServicesFactory.getSocios();

		DefaultComboBoxModel<ComboBoxItemSocio> comboSocios = new DefaultComboBoxModel<ComboBoxItemSocio>();
		for (SocioDTO socio : socios)// TODO quitar a los socios qque ya esten
										// apuntados
			if (a.fechasSocio(socio.socioID, reserva.getiDReserva()))
				comboSocios.addElement(new ComboBoxItemSocio(socio.name + " (ID: " + socio.socioID + ")", socio));

		comboBoxListaSocio.setVisible(false);
		comboBoxListaSocio.setModel(comboSocios);
		comboBoxListaSocio.setVisible(true);

	}

	private JComboBox<ComboBoxItemSocio> getComboBoxListaSocio() {
		if (comboBoxListaSocio == null) {
			comboBoxListaSocio = new JComboBox<ComboBoxItemSocio>();
			comboBoxListaSocio.setBounds(277, 161, 190, 20);
			refreshComboBoxSocios();
		}
		return comboBoxListaSocio;
	}

	private JLabel getLblSocioApuntados() {
		if (lblSocioApuntados == null) {
			lblSocioApuntados = new JLabel("Socio apuntados");
			lblSocioApuntados.setBounds(10, 27, 89, 14);
		}
		return lblSocioApuntados;
	}

	private void llenarCabecera() {
		int numeroPlazasRestantes = LogicAuxiliar.getNumeroPlazasRestantes(reserva);
		textFieldActividad.setText(new ReservarPlazaConLimitePersistencia().getNombreActividad(reserva.getiDReserva()));
		textFieldNumPlazas.setText("" + numeroPlazasRestantes);
		textFieldPlazasTotales.setText("" + reserva.getNumPlazas());

	}

	private JLabel getLblActividad() {
		if (lblActividad == null) {
			lblActividad = new JLabel("Actividad:");
			lblActividad.setBounds(277, 66, 67, 14);
		}
		return lblActividad;
	}

	private JLabel getLblNumPlazas() {
		if (lblNumPlazas == null) {
			lblNumPlazas = new JLabel("Num plazas disponibles:");
			lblNumPlazas.setBounds(277, 90, 113, 14);
		}
		return lblNumPlazas;
	}

	private JTextField getTextFieldActividad() {
		if (textFieldActividad == null) {
			textFieldActividad = new JTextField();
			textFieldActividad.setEditable(false);
			textFieldActividad.setBounds(384, 63, 86, 20);
			textFieldActividad.setColumns(10);
		}
		return textFieldActividad;
	}

	private JTextField getTextFieldNumPlazas() {
		if (textFieldNumPlazas == null) {
			textFieldNumPlazas = new JTextField();
			textFieldNumPlazas.setEditable(false);
			textFieldNumPlazas.setBounds(422, 87, 48, 20);
			textFieldNumPlazas.setColumns(10);
		}
		return textFieldNumPlazas;
	}

	private JLabel getLblNumPlazasTotales() {
		if (lblNumPlazasTotales == null) {
			lblNumPlazasTotales = new JLabel("Num plazas totales");
			lblNumPlazasTotales.setBounds(277, 115, 98, 14);
		}
		return lblNumPlazasTotales;
	}

	private JTextField getTextFieldPlazasTotales() {
		if (textFieldPlazasTotales == null) {
			textFieldPlazasTotales = new JTextField();
			textFieldPlazasTotales.setEditable(false);
			textFieldPlazasTotales.setBounds(422, 118, 48, 20);
			textFieldPlazasTotales.setColumns(10);
		}
		return textFieldPlazasTotales;
	}
}
