package ui.windows;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import logic.EliminaAlquiler;
import logic.NotificaSocio;
import logic.dto.AlquilerDTO;
import logic.dto.SocioDTO;
import ui.VentanaPrincipal;
import ui.actions.ObtenerAlquileresAction;
import ui.actions.ObtenerSociosConReservasAction;
import java.awt.Dimension;
import javax.swing.ScrollPaneConstants;

public class AdministracionEliminaAlquilerSocio extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane spSocios;
	private JList<String> listaSocios;
	private JLabel lbSocios;
	private JScrollPane spAlquileres;
	private JTable table;
	private JLabel lblAlquileres;
	private JButton btnCancelarAlquiler;
	private JButton btnCancelar;

	private ModeloNoEditable modeloAlquileres;
	private List<AlquilerDTO> alquileres;
	private ObtenerAlquileresAction oaa;
	private List<SocioDTO> socios;
	private ObtenerSociosConReservasAction osa;
	private DefaultListModel<String> lm;
	private EliminaAlquiler e;

	// private VentanaPrincipal vp;
	/**
	 * Create the frame.
	 */
	public AdministracionEliminaAlquilerSocio(VentanaPrincipal vp) {
		osa = new ObtenerSociosConReservasAction();
		oaa = new ObtenerAlquileresAction();
		socios = osa.execute();
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(AdministracionEliminaAlquilerSocio.class.getResource("/img/logoIconoSample.jpg")));
		setTitle("Centro de actividades: Cancelar un alquiler");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 757, 530);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getSpSocios());
		contentPane.add(getLbSocios());
		contentPane.add(getSpAlquileres());
		contentPane.add(getLblAlquileres());
		contentPane.add(getBtnCancelarAlquiler());
		contentPane.add(getBtnCancelar());
		setLocationRelativeTo(vp);
	}

	private JScrollPane getSpSocios() {
		if (spSocios == null) {
			spSocios = new JScrollPane();
			spSocios.setBounds(27, 63, 697, 84);
			spSocios.setViewportView(getListaSocios());
		}
		return spSocios;
	}

	private JList<String> getListaSocios() {
		if (listaSocios == null) {
			listaSocios = new JList<String>();
			lm = new DefaultListModel<String>();
			;
			if(socios.size()==0) {
				throw new IllegalArgumentException("No hay socios con reservas que cancelar");
			}
			for (SocioDTO s : socios)
				lm.addElement(s.name + " " + s.apellidos);

			listaSocios.setModel(lm);
			listaSocios.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent arg0) {
					muestraReservasSocio();
				}
			});
			listaSocios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return listaSocios;
	}

	protected void muestraReservasSocio() {
		int indice = getListaSocios().getSelectedIndex();
		int idSocio = socios.get(indice).socioID;
		actualizaTabla(idSocio);	
		

	}

	private void actualizaTabla(int idSocio) {
		alquileres = oaa.execute(idSocio);
		String[] fechas = new String[alquileres.size() + 1];
		fechas[0] = "horario";
		for (int i = 1; i < alquileres.size() + 1; i++) {
			fechas[i] = alquileres.get(i - 1).fecha.toString();
		}
		modeloAlquileres = new ModeloNoEditable(fechas, 0);
		mostrarAlquileres();

	}

	private void mostrarAlquileres() {
		if (alquileres.size() == 0) {
			getTable().setModel(new DefaultTableModel());
			table.revalidate();
			table.repaint();

		} else {
			String[] fila;
			for (int i = 8; i < 23; i++) {
				fila = new String[alquileres.size() + 1];
				fila[0] = "" + i + "-" + (i + 1);
				for (int j = 1; j < alquileres.size() + 1; j++) {
					if (alquileres.get(j - 1).horaInicial <= i && alquileres.get(j - 1).horaFinal - 1 >= i) {
						fila[j] = "OCUPADO";
					} else {
						fila[j] = "LIBRE";
					}
				}
				modeloAlquileres.addRow(fila);
			}

			table.setModel(modeloAlquileres);

			// pintaTabla();
			table.revalidate();
			table.repaint();
		}
	}

	private JLabel getLbSocios() {
		if (lbSocios == null) {
			lbSocios = new JLabel("Socios con alquileres");
			lbSocios.setFont(new Font("Tahoma", Font.PLAIN, 17));
			lbSocios.setBounds(41, 11, 167, 48);
		}
		return lbSocios;
	}

	private JScrollPane getSpAlquileres() {
		if (spAlquileres == null) {
			spAlquileres = new JScrollPane();
			spAlquileres.setBounds(45, 231, 660, 214);
			spAlquileres.setViewportView(getTable());
		}
		return spAlquileres;
	}

	private JTable getTable() {
		if (table == null) {
			String[] s = { "" };
			modeloAlquileres = new ModeloNoEditable(s, 0);
			table = new JTable(modeloAlquileres);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return table;
	}

	private JLabel getLblAlquileres() {
		if (lblAlquileres == null) {
			lblAlquileres = new JLabel("Alquileres");
			lblAlquileres.setFont(new Font("Tahoma", Font.PLAIN, 17));
			lblAlquileres.setBounds(41, 172, 88, 48);
		}
		return lblAlquileres;
	}

	private JButton getBtnCancelarAlquiler() {
		if (btnCancelarAlquiler == null) {
			btnCancelarAlquiler = new JButton("Cancelar Alquiler");
			btnCancelarAlquiler.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cancelaAlquiler();
				}
			});
			btnCancelarAlquiler.setBounds(298, 172, 135, 42);
		}
		return btnCancelarAlquiler;
	}

	protected void cancelaAlquiler() {
		try {
			int fila = table.getSelectedRow();
			int columna = table.getSelectedColumn();
			if (fila == -1 || columna == -1) {
				throw new IllegalArgumentException(
						"No hay ningun alquiler ocupado seleccionado");
			} else {
				String fecha = table.getColumnName(columna);
				String horario = (String) table.getValueAt(fila, columna);
				AlquilerDTO a = encuentraAlquiler(fecha, horario);
				if (a != null) {
					eliminarAlquiler(a);
					actualizaTabla(a.idSocio);
					JOptionPane.showMessageDialog(this, "El alquiler de la fecha " + a.fecha + " ha sido eliminado",
							"Operaci�n correcta", JOptionPane.INFORMATION_MESSAGE);
					NotificaSocio n = new NotificaSocio();
					List<SocioDTO> s = new ArrayList<SocioDTO>();
					s.add(socios.get(getListaSocios().getSelectedIndex()));
					n.notificaSocio(s);
				} else {
					throw new IllegalArgumentException("Esa hora no est� ocupada");
				}
			}

		} catch (Exception e) {
			muestraMensajeError(e.getMessage());
		}

	}

	private AlquilerDTO encuentraAlquiler(String fecha, String horario) {
		for (AlquilerDTO a : alquileres) {
			if (a.fecha.toString().equals(fecha)) {
				if (horario.equals("OCUPADO")) {
					if (a.fechaPago == null) {
						return a;
					}
					return null;
				}
			}
		}
		return null;
	}

	private void eliminarAlquiler(AlquilerDTO a) {
		e = new EliminaAlquiler(a);
		e.eliminaAlquiler();
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cerrarVentana();
				}
			});
			btnCancelar.setBounds(605, 467, 97, 23);
		}
		return btnCancelar;
	}

	protected void cerrarVentana() {
		this.dispose();

	}

	protected void muestraMensajeError(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);

	}

}