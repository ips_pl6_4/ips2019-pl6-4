package ui.windows;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import logic.ModificaMonitor;
import logic.dto.MonitorDTO;
import logic.dto.ReservaDTO;
import ui.actions.ObtenerMonitoresAction;
import ui.actions.ObtenerReservasAction;

public class VentanaModificaReserva extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scReserva;
	private JScrollPane scMonitor;
	private JButton btnNewButton;
	private JTable table;
	private JTable table_1;
	private JLabel lblTablaReservas;
	private JLabel lblTablaMonitores;

	private String[] nombreColumnasReserva = { "ID", "IDACTIVIDAD", "IDINSTALACION", "HORAINICIO", "HORAFINAL","FECHA","NUMPLAZAS" };

	private String[] nombreColumnasMonitor = { "ID", "NOMBRE"};

	private ObtenerReservasAction ora;
	private ObtenerMonitoresAction oma;
	private List<ReservaDTO> reservas;
	private List<MonitorDTO> monitores;
	private ModeloNoEditable modeloMonitores;
	private ModeloNoEditable modeloReservas;
	private ModificaMonitor mm;

	/**
	 * Create the frame.
	 */
	public VentanaModificaReserva(JFrame ventanaPrincipal) {
		ora = new ObtenerReservasAction();
		oma = new ObtenerMonitoresAction();
		mm = new ModificaMonitor();
		reservas = ora.execute();
		monitores = oma.execute();
		setResizable(false);
		setTitle("Centro de actividades: modificar monitor");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaModificaReserva.class.getResource("/img/logoIconoSample.jpg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 612, 458);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getScReserva());
		contentPane.add(getScMonitor());
		contentPane.add(getBtnNewButton());
		contentPane.add(getLblTablaReservas());
		contentPane.add(getLblTablaMonitores());
		setLocationRelativeTo(ventanaPrincipal);

		mostrarReservas();
		mostrarMonitores();
	}
	private void mostrarMonitores() {
		String id, nombre;

		for (MonitorDTO monitor : monitores) {
			id = String.valueOf(monitor.monitorID);
			nombre = monitor.name;

			String[] nuevaFila = new String[] { id, nombre};

			modeloMonitores.addRow(nuevaFila);
		}

		table_1.setModel(modeloMonitores);

		table_1.revalidate();
		table_1.repaint();
	}


	private void mostrarReservas() {
		String id, idActividad, idInstalacion, horaInicio, horaFinal, fecha, numPlazas;

		for (ReservaDTO reserva : reservas) {
			id = String.valueOf(reserva.idReserva);
			idActividad = String.valueOf(reserva.idActividad);
			idInstalacion = String.valueOf(reserva.idInstalacion);
			horaInicio = String.valueOf(reserva.horaInicial);
			horaFinal = String.valueOf(reserva.horaFinal);
			fecha = String.valueOf(reserva.fecha);
			numPlazas = String.valueOf(reserva.numPlazas);

			String[] nuevaFila = new String[] { id,idActividad,idInstalacion,horaInicio,
					horaFinal,fecha,numPlazas};

			modeloReservas.addRow(nuevaFila);
		}

		table.setModel(modeloReservas);

		table.revalidate();
		table.repaint();

	}

	private JScrollPane getScReserva() {
		if (scReserva == null) {
			scReserva = new JScrollPane();
			scReserva.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scReserva.setBounds(28, 60, 544, 129);
			scReserva.setViewportView(getTable());
		}
		return scReserva;
	}
	private JScrollPane getScMonitor() {
		if (scMonitor == null) {
			scMonitor = new JScrollPane();
			scMonitor.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scMonitor.setBounds(28, 237, 544, 129);
			scMonitor.setViewportView(getTable_1());
		}
		return scMonitor;
	}
	private JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("Modificar");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cambiaMonitor();
				}
			});
			btnNewButton.setBounds(228, 388, 89, 30);
		}
		return btnNewButton;
	}
	protected void cambiaMonitor() {
		try {
			int filaMonitor = table_1.getSelectedRow();
			int filaReserva = table.getSelectedRow();
			if (filaMonitor == -1 || filaReserva == -1) {
				throw new IllegalArgumentException("No hay suficientes datos seleccionados");
			}
			int idMonitor =Integer.parseInt((String) table_1.getValueAt(filaMonitor, 0));
			int idReserva =Integer.parseInt((String) table.getValueAt(filaReserva, 0));
			mm.modificaMecanico(idReserva, idMonitor);
			JOptionPane.showMessageDialog(this, "La El monitor "+idMonitor+" ahora imparte la reserva: "+idReserva, "Operación correcta", JOptionPane.INFORMATION_MESSAGE);
			}catch(Exception e) {
				muestraMensajeError(e.getMessage());
			}
		}		

	protected void muestraMensajeError(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);

	}
	private JTable getTable() {
		if (table == null) {
			modeloReservas = new ModeloNoEditable(nombreColumnasReserva, 0);
			table = new JTable(modeloReservas);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return table;
	}
	private JTable getTable_1() {
		if (table_1 == null) {
			modeloMonitores = new ModeloNoEditable(nombreColumnasMonitor, 0);
			table_1 = new JTable(modeloMonitores);
			table_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return table_1;
	}
	private JLabel getLblTablaReservas() {
		if (lblTablaReservas == null) {
			lblTablaReservas = new JLabel("Tabla Reservas:");
			lblTablaReservas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblTablaReservas.setBounds(28, 10, 217, 39);
		}
		return lblTablaReservas;
	}
	private JLabel getLblTablaMonitores() {
		if (lblTablaMonitores == null) {
			lblTablaMonitores = new JLabel("Tabla Monitores:");
			lblTablaMonitores.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblTablaMonitores.setBounds(28, 187, 217, 39);
		}
		return lblTablaMonitores;
	}
}
