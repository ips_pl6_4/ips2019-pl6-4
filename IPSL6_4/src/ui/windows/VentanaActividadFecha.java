package ui.windows;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import logic.ReservarPlazaConLimite;
import logic.util.container.Reserva;
import ui.VentanaPrincipal;

import java.awt.Toolkit;

public class VentanaActividadFecha extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JList<Reserva> list;
	private JScrollPane scrollPane;
	private JButton btnReservar;

	int idSocio;
	String rol;

	/**
	 * Create the frame.
	 */
	public VentanaActividadFecha(VentanaPrincipal v,int idSocio, String rol) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaActividadFecha.class.getResource("/img/logoIconoSample.jpg")));

		this.idSocio = idSocio;
		this.rol = rol;

		setTitle("Actividades Planificadas disponibles");// TODO + date.toString());
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 480, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getScrollPane());
		contentPane.add(getBtnReservar());
		
		setLocationRelativeTo(v);
		setVisible(true);
	}

	/**
	 * Metodo que devuelve las actividades a las que se puede al socio, teniendo en
	 * cuenta si eres un admin, monitor o socio
	 * 
	 * @return
	 */
	private List<Reserva> getReservasDisponibles() {
		return new ReservarPlazaConLimite().getReservasDisponibles(idSocio, rol);
	}

	private JList<Reserva> getList() {
		if (list == null) {
			list = new JList<Reserva>();
			DefaultListModel<Reserva> lm = new DefaultListModel<Reserva>();
			List<Reserva> lista = getReservasDisponibles();
			for (Reserva theReserva : lista)
				lm.addElement(theReserva);

			list.setModel(lm);
		}
		return list;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 34, 300, 220);
			scrollPane.setViewportView(getList());
		}
		return scrollPane;
	}

	private JButton getBtnReservar() {
		if (btnReservar == null) {
			btnReservar = new JButton("Reservar");
			btnReservar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					execute();
				}
			});
			btnReservar.setBounds(335, 120, 90, 25);
		}

		return btnReservar;
	}

	public void execute() {
		try {
			int idReserva;
			try {
				idReserva = list.getSelectedValue().getiDReserva();
			} catch (NullPointerException e) {
				throw new RuntimeException("Ningun elemento seleccionado");
			}
			new ReservarPlazaConLimite().execute(idSocio, idReserva, rol);
			JOptionPane.showMessageDialog(this, "La operación fue un exito");
		} catch (RuntimeException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
