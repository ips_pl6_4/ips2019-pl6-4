package ui.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import logic.dto.InstalacionDTO;
import logic.dto.ReservaSocioDTO;
import logic.dto.SocioDTO;
import logic.service.socio.SocioServicesFactory;
import ui.VentanaPrincipal;
import ui.windows.util.ComboBoxItemSocio;

/**
 * Clase InfoPagosUI
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 3.12.2019
 */
public class InfoPagosUI extends JFrame {

	private static final long serialVersionUID = 6911491950648918625L;

	private JPanel contentPane;
	private JPanel panelLists;
	private JLabel lblActividades;
	private JComboBox<ComboBoxItemSocio> comboBoxSocios;
	private JScrollPane scrollPane;
	private JTable tableAsistencias;

	private DefaultTableModel tableModel;
	private String[] headers = { "Instalacion", "Cantidad a pagar", "Hora de inicio", "Hora final", "Fecha",
			// "Fecha de pago",
			"ID Instalacion", "ID Alquiler" };

	public InfoPagosUI(JFrame ventana) {
		setTitle("Centro deportivo: Información sobre pagos realizados");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/logoIconoSample.jpg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 569, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPanelLists(), BorderLayout.NORTH);
		contentPane.add(getScrollPane(), BorderLayout.CENTER);

		setLocationRelativeTo(ventana);
	}

	private JPanel getPanelLists() {
		if (panelLists == null) {
			panelLists = new JPanel();
			panelLists.setBackground(Color.WHITE);
			panelLists.setLayout(new GridLayout(0, 2, 0, 0));
			panelLists.add(getLblSocios());
			panelLists.add(getComboBoxSocios());
		}
		return panelLists;
	}

	private JLabel getLblSocios() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Selecciónese socio:");
			lblActividades.setHorizontalAlignment(SwingConstants.CENTER);
			lblActividades.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
		}
		return lblActividades;
	}

	private JComboBox<ComboBoxItemSocio> getComboBoxSocios() {
		if (comboBoxSocios == null) {
			refreshComboBoxSocios();
			comboBoxSocios.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					refreshTableAsistencias();
				}
			});
		}
		return comboBoxSocios;
	}

	private void refreshComboBoxSocios() {
		List<SocioDTO> socios = SocioServicesFactory.getSocios();
		DefaultComboBoxModel<ComboBoxItemSocio> comboSocios = new DefaultComboBoxModel<ComboBoxItemSocio>();
		for (SocioDTO socio : socios)
			comboSocios.addElement(new ComboBoxItemSocio(socio.name + " (ID: " + socio.socioID + ")", socio));

		if (comboBoxSocios == null)
			comboBoxSocios = new JComboBox<ComboBoxItemSocio>();

		comboBoxSocios.setVisible(false);
		comboBoxSocios.setModel(comboSocios);
		comboBoxSocios.setVisible(true);

		refreshTableAsistencias();
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTableAsistencias());
		}
		return scrollPane;
	}

	private JTable getTableAsistencias() {
		if (tableAsistencias == null) {
			createTableModel();
			this.tableAsistencias = new JTable(this.tableModel);
			this.tableAsistencias.getTableHeader().setReorderingAllowed(false);
			refreshTableAsistencias();
		}
		return tableAsistencias;
	}

	private void refreshTableAsistencias() {
		if (this.tableModel == null)
			createTableModel();

		for (int i = 0; i < this.tableModel.getRowCount(); i++)
			this.tableModel.removeRow(i);

		Object[] fila = new Object[headers.length];
		SocioDTO socio = ((ComboBoxItemSocio) getComboBoxSocios().getSelectedItem()).getValue();
		List<ReservaSocioDTO> reservas = SocioServicesFactory.getObtenerReservas(socio.socioID);
//		PriorityQueue<ReservaSocioDTO> reservasSorted = new PriorityQueue<ReservaSocioDTO>(
//				(reservas.size() < 1) ? 1 : reservas.size(), ReservaSocioDTO.fromAfterToBeforeComparator());
//		if (reservas.size() >= 1)
//			reservasSorted.addAll(reservas);

		// { "Instalacion", "Cantidad a pagar", "Hora de inicio", "Hora final", "Fecha",
		// "Fecha de pago", "ID Instalacion", "ID Alquiler" }

//		while (reservasSorted.peek() != null) {
//			ReservaSocioDTO reservaSocio = reservasSorted.poll();
		for (ReservaSocioDTO reservaSocio : reservas) {
			InstalacionDTO instalacion = SocioServicesFactory.getObtenerInstalacion(reservaSocio.instalacionID);

			fila[0] = instalacion.nombre;
			fila[1] = (instalacion.precioHora * (reservaSocio.horaFinal - reservaSocio.horaInicial));
			fila[2] = reservaSocio.horaInicial;
			fila[3] = reservaSocio.horaFinal;
			fila[4] = reservaSocio.fecha.toString();
			// fila[5] = (reservaSocio.fechaPago == null ? "" :
			// reservaSocio.fechaPago.toString());
			fila[5] = instalacion.idInstalacion;
			fila[6] = reservaSocio.reservaSocioID;

			this.tableModel.addRow(fila);
		}

		getScrollPane().revalidate();
		getScrollPane().repaint();
	}

	private void createTableModel() {
		this.tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
	}
}