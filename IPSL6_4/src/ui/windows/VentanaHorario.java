package ui.windows;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.Stack;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.VerOcupacionInstalacion;
import ui.VentanaPrincipal;
/**
 * Ventana que muestra un horario
 * @author Ana Garcia
 *9-10-2019
 */
public class VentanaHorario extends JFrame {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;
	public static final int COLUMNAS = 8;
	public static final int FILAS = 15;
	private JPanel pnBase;
	private JPanel pnHorario;

	private String[][] matriz;
	private JPanel pnBotones;
	private JButton btnSiguiente;
	private JButton btnCancelar;
	private JPanel pnFecha;
	private JLabel lblNewLabel;
	private JPanel pnCentral;

	private int instalacion;
	private JPanel pnActual;
	private int numeroPaneles;
	private JPanel pn1;
	private Stack<String[][]> pila = new Stack<String[][]>();

	/**
	 * Crea la ventana, recibe la ventana de referencia y la matriz que debe copiar en forma de horario
	 */
	public VentanaHorario(VentanaPrincipal vRO, int instalacion) {
		this.instalacion = instalacion;
		this.numeroPaneles = 0;
		this.matriz = pideMatriz(0);
		setTitle("Centro deportes: ver horario");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaHorario.class.getResource("/img/logoIconoSample.jpg")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 899, 400);
		pnBase = new JPanel();
		pnBase.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnBase);
		pnBase.setLayout(new BoxLayout(pnBase, BoxLayout.X_AXIS));
		pnBase.add(getPnHorario());
		lblNewLabel.setText("Esta semana");
		llenaHorario();
		setLocationRelativeTo(vRO);//O relativo a cualquier otra ventana que est� antes
		setVisible(true);
	}
	private JPanel getPnHorario() {
		if (pnHorario == null) {
			pnHorario = new JPanel();
			pnHorario.setLayout(new BorderLayout(0, 0));
			pnHorario.add(getPnBotones(), BorderLayout.SOUTH);
			pnHorario.add(getPnFecha(), BorderLayout.NORTH);
			pnHorario.add(getPnCentral(), BorderLayout.CENTER);
		}
		return pnHorario;
	}

	private String[][] pideMatriz(int semanas){
		VerOcupacionInstalacion ve = new VerOcupacionInstalacion(instalacion);
		Date fecha = new Date();
		LocalDateTime f = LocalDateTime.from(fecha.toInstant().atZone(ZoneId.of("UTC"))).plusDays(7*semanas);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E dd LLL yyyy",Locale.ENGLISH);
		  String text =  f.format(formatter);
		return ve.mostrarDisponibilidad(text);

	}
	/**
	 * M�todo pata dibujar el horario
	 */
	private void llenaHorario() {
		for(int fila = 0; fila<matriz.length;fila++) {
			for(int columna = 0; columna<matriz[0].length;columna++) {
				JButton bt = new JButton("");
				bt.setText(matriz[fila][columna]);
				if(columna==0 || fila ==0) {
					bt.setBackground(Color.GRAY);
					pnActual.add(bt);
				}
				else {
					if(matriz[fila][columna].equals("Ocupado")){
						bt.setBackground(Color.RED);
					}
					else{
						bt.setBackground(Color.GREEN);
					}

				}
				pnActual.add(bt);
			}
		}
	}
	private JPanel getPnBotones() {
		if (pnBotones == null) {
			pnBotones = new JPanel();
			pnBotones.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			pnBotones.add(getBtnSiguiente());
			pnBotones.add(getBtnCancelar());
		}
		return pnBotones;
	}
	private JButton getBtnSiguiente() {
		if (btnSiguiente == null) {
			btnSiguiente = new JButton("Siguiente");
			btnSiguiente.setHorizontalAlignment(SwingConstants.RIGHT);
			btnSiguiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					siguiente();
				}
			});
			btnSiguiente.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return btnSiguiente;
	}
	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Anterior");
			btnCancelar.setHorizontalAlignment(SwingConstants.RIGHT);
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					anterior();
				}
			});
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return btnCancelar;
	}
	private JPanel getPnFecha() {
		if (pnFecha == null) {
			pnFecha = new JPanel();
			pnFecha.add(getLblNewLabel());
		}
		return pnFecha;
	}
	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("New label");
			lblNewLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		}
		return lblNewLabel;
	}
	private JPanel getPnCentral() {
		if (pnCentral == null) {
			pnCentral = new JPanel();
			pnCentral.setLayout(new CardLayout(0, 0));
			pnCentral.add(getPn1(), "pn1");
			pnActual = getPn1();
		}
		return pnCentral;
	}
	private JPanel getPn1() {
		if (pn1 == null) {
			pn1 = new JPanel();
			pn1.setLayout(new GridLayout(matriz.length, matriz[0].length, 0, 0));
		}
		return pn1;
	}

	private void siguiente() {
	
		pila.push(this.matriz);
		pnActual.removeAll();
		numeroPaneles++;
		if(numeroPaneles==0) {
			lblNewLabel.setText("Esta semana");
		}else {
			lblNewLabel.setText(numeroPaneles+" semana/s despu�s");
		}
		try {
			this.matriz = pideMatriz(numeroPaneles);
			}catch(IllegalArgumentException e) {
				VerOcupacionInstalacion ve = new VerOcupacionInstalacion(instalacion);
				this.matriz = ve.creaHorarioLimpio();
			}
		llenaHorario();
		for (Component c : pnHorario.getComponents()) {
			c.setVisible(true);
		}
	}

	
	private void anterior() {
		if(numeroPaneles-1>=0) {
			if(numeroPaneles-1==0) {
				lblNewLabel.setText("Esta semana");
			}else {
				lblNewLabel.setText(numeroPaneles-1+" semana/s despu�s");
			}
			pnActual.removeAll();
			numeroPaneles--;
			try {
				this.matriz = pideMatriz(numeroPaneles);
				}catch(IllegalArgumentException e) {
					VerOcupacionInstalacion ve = new VerOcupacionInstalacion(instalacion);
					this.matriz = ve.creaHorarioLimpio();
				}
			llenaHorario();
			for (Component c : pnHorario.getComponents()) {
				c.setVisible(true);
			}
		}

	}
}
