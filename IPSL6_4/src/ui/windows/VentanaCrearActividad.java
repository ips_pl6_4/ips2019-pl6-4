package ui.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import logic.dto.Intensidad;
import logic.dto.RecursoDTO;
import ui.actions.CrearActividadAction;
import ui.actions.ObtenerRecursosAction;
import javax.swing.ListSelectionModel;

public class VentanaCrearActividad extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JLabel lblNombre;
	private JLabel lblIntensidad;
	private JLabel lblDescripcion;
	private JLabel lblRecursosAnadidos;

	private JTextField textFieldNombre;
	private JTextField textFieldRecurso;

	private JTextArea textAreaDescripcion;

	private JScrollPane scrollPaneRecursos;

	private JButton btnCrearActividad;
	private JButton btnBorrarRecurso;
	private JButton btnAadirRecurso;
	private JButton btnCancelar;

	private JComboBox<Intensidad> comboBox;

	private CrearActividadAction c;

	List<RecursoDTO> recursos;
	private ObtenerRecursosAction ora;
	private JScrollPane scrollPaneRecursosActividad;
	private JLabel lblRecursosAadidos;
	private JTable tableRecursos;
	private JTable tableRecursosActividad;

	private JButton btnAadir;
	private JLabel lblNuevoRecurso;

	List<RecursoDTO> recursosActividad = new ArrayList<RecursoDTO>();
	List<RecursoDTO> recursosActividadNuevos = new ArrayList<RecursoDTO>();;

	private ModeloNoEditable modeloRecursos;
	private ModeloNoEditable modeloRecursosActividad;

	public VentanaCrearActividad(JFrame ventanaPrincipal) {
		setResizable(false);
		setTitle("Crear actividad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 629, 630);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNombre());
		contentPane.add(getLblIntensidad());
		contentPane.add(getLblDescripcion());
		contentPane.add(getLblRecursosAnadidos());
		contentPane.add(getTextFieldNombre());
		contentPane.add(getTextFieldRecurso());
		contentPane.add(getBtnAadirRecurso());
		contentPane.add(getTextAreaDescripcion());
		contentPane.add(getScrollPaneRecursos());
		contentPane.add(getBtnBorrarRecurso());
		contentPane.add(getBtnCrearActividad());
		contentPane.add(getComboBox());
		contentPane.add(getBtnCancelar());
		contentPane.add(getScrollPane_1());
		contentPane.add(getLblRecursosAadidos());
		contentPane.add(getBtnAadir());
		contentPane.add(getLblNuevoRecurso());

		setLocationRelativeTo(ventanaPrincipal);

		c = new CrearActividadAction();

		mostrarRecursos();

		mostrarRecursos();
	}

	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre");
			lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNombre.setBounds(34, 11, 121, 47);
		}
		return lblNombre;
	}

	private JLabel getLblIntensidad() {
		if (lblIntensidad == null) {
			lblIntensidad = new JLabel("Intensidad");
			lblIntensidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblIntensidad.setBounds(34, 64, 121, 47);
		}
		return lblIntensidad;
	}

	private JLabel getLblDescripcion() {
		if (lblDescripcion == null) {
			lblDescripcion = new JLabel("Descripci\u00F3n");
			lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblDescripcion.setBounds(34, 127, 121, 47);
		}
		return lblDescripcion;
	}

	private JTextField getTextFieldNombre() {
		if (textFieldNombre == null) {
			textFieldNombre = new JTextField();
			textFieldNombre.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent arg0) {
					if (textFieldNombre.getText().length() >= 20) {
						arg0.consume();
					}
				}
			});
			textFieldNombre.setFont(new Font("Tahoma", Font.PLAIN, 13));
			textFieldNombre.setBounds(135, 16, 359, 36);
			textFieldNombre.setColumns(10);
		}
		return textFieldNombre;
	}

	private JTextField getTextFieldRecurso() {
		if (textFieldRecurso == null) {
			textFieldRecurso = new JTextField();
			textFieldRecurso.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (textFieldRecurso.getText().length() >= 50) {
						e.consume();
					}
				}
			});
			textFieldRecurso.setFont(new Font("Tahoma", Font.PLAIN, 13));
			textFieldRecurso.setBorder(new LineBorder(new Color(171, 173, 179)));
			textFieldRecurso.setBounds(158, 472, 227, 36);
			textFieldRecurso.setColumns(10);
		}
		return textFieldRecurso;
	}

	private JButton getBtnAadirRecurso() {
		if (btnAadirRecurso == null) {
			btnAadirRecurso = new JButton("A\u00F1adir nuevo recurso");
			btnAadirRecurso.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					añadirNuevoRecurso();
				}
			});
			btnAadirRecurso.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAadirRecurso.setBounds(411, 472, 172, 36);
		}
		return btnAadirRecurso;
	}

	private JTextArea getTextAreaDescripcion() {
		if (textAreaDescripcion == null) {
			textAreaDescripcion = new JTextArea();
			textAreaDescripcion.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (textAreaDescripcion.getText().length() >= 250) {
						e.consume();
					}
				}
			});
			textAreaDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 13));
			textAreaDescripcion.setBorder(new LineBorder(new Color(171, 173, 179)));
			textAreaDescripcion.setWrapStyleWord(true);
			textAreaDescripcion.setLineWrap(true);
			textAreaDescripcion.setBounds(135, 127, 359, 91);
		}
		return textAreaDescripcion;
	}

	private JLabel getLblRecursosAnadidos() {
		if (lblRecursosAnadidos == null) {
			lblRecursosAnadidos = new JLabel("Recursos");
			lblRecursosAnadidos.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRecursosAnadidos.setBounds(34, 229, 89, 36);
		}
		return lblRecursosAnadidos;
	}

	private JScrollPane getScrollPaneRecursos() {
		if (scrollPaneRecursos == null) {
			scrollPaneRecursos = new JScrollPane();
			scrollPaneRecursos.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneRecursos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPaneRecursos.setBounds(34, 276, 269, 129);
			scrollPaneRecursos.setViewportView(getTableRecursos());
		}
		return scrollPaneRecursos;
	}

	private JButton getBtnBorrarRecurso() {
		if (btnBorrarRecurso == null) {
			btnBorrarRecurso = new JButton("Borrar");
			btnBorrarRecurso.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					borrarRecurso();
				}
			});
			btnBorrarRecurso.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnBorrarRecurso.setBounds(390, 416, 114, 36);
		}
		return btnBorrarRecurso;
	}

	private JButton getBtnCrearActividad() {
		if (btnCrearActividad == null) {
			btnCrearActividad = new JButton("Crear actividad");
			btnCrearActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					crearActividad();
				}
			});
			btnCrearActividad.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCrearActividad.setBounds(441, 544, 142, 36);
		}
		return btnCrearActividad;
	}

	private JComboBox<Intensidad> getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox<Intensidad>();
			comboBox.setFont(new Font("Tahoma", Font.PLAIN, 13));
			comboBox.setModel(new DefaultComboBoxModel<Intensidad>(Intensidad.values()));
			comboBox.setBounds(135, 69, 359, 36);
		}
		return comboBox;
	}

	protected void crearActividad() {
		String nombre = textFieldNombre.getText();
		String descripcion = textAreaDescripcion.getText();

		if (!checkStringNoVacia(nombre)) {
			JOptionPane.showMessageDialog(this, "Campo nombre no válido", "Error crear actividad",
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			c.execute(nombre, descripcion, (Intensidad) comboBox.getSelectedItem(), recursosActividad,
					recursosActividadNuevos);
			VentanaOperacionCorrecta correcta = new VentanaOperacionCorrecta(this,
					"La actividad ha sido creada con exito", "Actividad creada");
			correcta.setVisible(true);
		}
	}

	public boolean checkStringNoVacia(String cadena) {
		return cadena.trim().length() != 0;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.setBounds(35, 544, 142, 36);
		}
		return btnCancelar;
	}

	private JScrollPane getScrollPane_1() {
		if (scrollPaneRecursosActividad == null) {
			scrollPaneRecursosActividad = new JScrollPane();
			scrollPaneRecursosActividad.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPaneRecursosActividad.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPaneRecursosActividad.setBounds(313, 276, 269, 129);
			scrollPaneRecursosActividad.setViewportView(getTableRecursosActividad());
		}
		return scrollPaneRecursosActividad;
	}

	private JLabel getLblRecursosAadidos() {
		if (lblRecursosAadidos == null) {
			lblRecursosAadidos = new JLabel("Recursos a\u00F1adidos");
			lblRecursosAadidos.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRecursosAadidos.setBounds(313, 229, 183, 36);
		}
		return lblRecursosAadidos;
	}

	private JTable getTableRecursos() {
		if (tableRecursos == null) {
			String[] nombreColumnas = { "NOMBRE" };
			modeloRecursos = new ModeloNoEditable(nombreColumnas, 0);
			tableRecursos = new JTable(modeloRecursos);
			tableRecursos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableRecursos;
	}

	private JTable getTableRecursosActividad() {
		if (tableRecursosActividad == null) {
			String[] nombreColumnas = { "NOMBRE" };
			modeloRecursosActividad = new ModeloNoEditable(nombreColumnas, 0);
			tableRecursosActividad = new JTable(modeloRecursosActividad);
			tableRecursosActividad.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableRecursosActividad;
	}

	private JButton getBtnAadir() {
		if (btnAadir == null) {
			btnAadir = new JButton("A\u00F1adir");
			btnAadir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					añadirRecurso();
				}
			});
			btnAadir.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAadir.setBounds(111, 416, 114, 36);
		}
		return btnAadir;
	}

	private JLabel getLblNuevoRecurso() {
		if (lblNuevoRecurso == null) {
			lblNuevoRecurso = new JLabel("Nuevo recurso");
			lblNuevoRecurso.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNuevoRecurso.setBounds(34, 467, 153, 47);
		}
		return lblNuevoRecurso;
	}

	private void mostrarRecursos() {
		ora = new ObtenerRecursosAction();
		recursos = ora.execute();

		String[] nombreColumnas = { "NOMBRE" };
		modeloRecursos = new ModeloNoEditable(nombreColumnas, 0);

		String nombre;

		for (RecursoDTO recurso : recursos) {
			nombre = recurso.nombre;

			String[] nuevaFila = new String[] { nombre };

			modeloRecursos.addRow(nuevaFila);
		}

		tableRecursos.setModel(modeloRecursos);

		tableRecursos.revalidate();
		tableRecursos.repaint();
	}

	private void mostrarRecursosActividad() {
		String[] nombreColumnas = { "NOMBRE" };
		modeloRecursosActividad = new ModeloNoEditable(nombreColumnas, 0);

		String nombre;

		for (RecursoDTO recurso : recursosActividad) {
			nombre = recurso.nombre;

			String[] nuevaFila = new String[] { nombre };

			modeloRecursosActividad.addRow(nuevaFila);
		}

		for (RecursoDTO recursoNuevo : recursosActividadNuevos) {
			nombre = recursoNuevo.nombre;

			String[] nuevaFila = new String[] { nombre };

			modeloRecursosActividad.addRow(nuevaFila);
		}

		tableRecursosActividad.setModel(modeloRecursosActividad);

		tableRecursosActividad.revalidate();
		tableRecursosActividad.repaint();
	}

	private void añadirRecurso() {
		int recursoSeleccionado = tableRecursos.getSelectedRow();

		if (recursoSeleccionado != -1) {
			RecursoDTO recurso = recursos.get(recursoSeleccionado);
			if (!recursosActividad.contains(recurso)) {
				recursosActividad.add(recursos.get(recursoSeleccionado));
				mostrarRecursosActividad();
			}
		}
	}

	private void borrarRecurso() {
		int recursoActividadSeleccionado = tableRecursosActividad.getSelectedRow();

		if (recursoActividadSeleccionado != -1) {

			if (recursoActividadSeleccionado < recursosActividad.size()) {
				recursosActividad.remove(recursoActividadSeleccionado);
			} else {
				recursosActividadNuevos.remove(recursoActividadSeleccionado - recursosActividad.size());
			}

			mostrarRecursosActividad();
		}
	}

	private void añadirNuevoRecurso() {
		String nuevoRecursoNombre = textFieldRecurso.getText().toUpperCase();

		if (nuevoRecursoNombre.trim().length() > 0) {
			RecursoDTO nuevoRecurso = new RecursoDTO();
			nuevoRecurso.nombre = nuevoRecursoNombre;

			RecursoDTO recursoExiste = c.compruebaNoExisteRecurso(recursos, nuevoRecurso);

			recursosActividadNuevos.add(recursoExiste);

			mostrarRecursosActividad();

			textFieldRecurso.setText("");
		}
	}

}
