package ui.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;

import logic.dto.ActividadDTO;
import logic.dto.ConflictoDTO;
import logic.dto.HistorialDTO;
import logic.dto.InstalacionDTO;
import logic.dto.RecursoDTO;
import logic.dto.RecursoInstalacionDTO;
import logic.dto.ReservaDTO;
import ui.actions.ComprobarConflictosInstalacionAction;
import ui.actions.ModificarHorarioReservaAction;
import ui.actions.ObtenerActividadAction;
import ui.actions.ObtenerDisponibilidadAction;
import ui.actions.ObtenerInstalacionAction;
import ui.actions.ObtenerRecursosActividadAction;
import ui.actions.ObtenerRecursosInstalacionAction;
import ui.actions.ObtenerReservasProximasAction;
import ui.actions.RegistrarHistorialAction;

public class VentanaModificarHorarioReservaSinSocios extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JLabel lblActividades;
	private JLabel lblInstalaciones;
	private JLabel lblRecursosParaLa;

	ModeloNoEditable modeloActividadesPlanificadas;
	ModeloNoEditable modeloInstalaciones;
	ModeloNoEditable modeloRecursosActividad;
	ModeloNoEditable modeloRecursosInstalacion;

	private JButton btnCancelar;
	private JButton btnModificar;

	private JScrollPane scrollPaneRecursosActividad;

	private JTable tableRecursosActividad;

	List<InstalacionDTO> instalaciones;
	List<RecursoDTO> recursosActividad;
	List<RecursoInstalacionDTO> recursosInstalacion;
	private JScrollPane scrollPane;
	private JTable tableActividadesPlanificadas;
	private JLabel lblActividad;
	private JLabel lblIdActividad;
	private JLabel lblNombre;
	private JLabel lblIntensdiad;
	private JLabel labelResultadoIdActividad;
	private JLabel labelResultadoNombreActividad;
	private JLabel labelResultadoIntensidadActividad;
	private JSeparator separator;
	private JLabel lblInstalacion;
	private JLabel lblIdInstalacion;
	private JLabel labelNombreInstalacion;
	private JLabel labelResultadoIdInstalacion;
	private JLabel labelResultadoNombreInstalacion;
	private JSeparator separator_1;

	private ModeloNoEditable modeloReservas;

	private List<ReservaDTO> reservas;
	private JCheckBox chckbxDaCompleto;
	private JLabel lblHoraInicio;
	private JLabel lblHoraFin;
	private JSpinner spinnerHoraInicial;
	private JSpinner spinnerHoraFinal;

	private JDateChooser selectorFecha;

	public VentanaModificarHorarioReservaSinSocios(JFrame ventanaPrincipal) {
		setResizable(false);
		setTitle("Modificar instalacion de una reserva sin socios apuntados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 820, 580);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblActividades());
		contentPane.add(getLblInstalaciones());
		contentPane.add(getBtnCancelar());
		contentPane.add(getBtnModificar());
		contentPane.add(getLblRecursosParaLa());
		contentPane.add(getScrollPaneRecursosActividad());
		contentPane.add(getScrollPane());
		contentPane.add(getLblActividad());
		contentPane.add(getLblIdActividad());
		contentPane.add(getLblNombre());
		contentPane.add(getLblIntensdiad());
		contentPane.add(getLabelResultadoIdActividad());
		contentPane.add(getLabelResultadoNombreActividad());
		contentPane.add(getLabelResultadoIntensidadActividad());
		contentPane.add(getSeparator());
		contentPane.add(getLblInstalacion());
		contentPane.add(getLblIdInstalacion());
		contentPane.add(getLabelNombreInstalacion());
		contentPane.add(getLabelResultadoIdInstalacion());
		contentPane.add(getLabelResultadoNombreInstalacion());
		contentPane.add(getSeparator_1());
		contentPane.add(getChckbxDaCompleto());
		contentPane.add(getLblHoraInicio());
		contentPane.add(getLblHoraFin());
		contentPane.add(getSpinnerHoraInicial());
		contentPane.add(getSpinnerHoraFinal());
		contentPane.add(getSelectorFecha());

		chckbxDaCompleto.setSelected(true);

		setLocationRelativeTo(ventanaPrincipal);

		mostrarReservas();
	}

	private JLabel getLblActividades() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Actividades planificadas");
			lblActividades.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblActividades.setBounds(10, 11, 183, 36);
		}
		return lblActividades;
	}

	private JLabel getLblInstalaciones() {
		if (lblInstalaciones == null) {
			lblInstalaciones = new JLabel("Fecha reserva");
			lblInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblInstalaciones.setBounds(10, 440, 183, 36);
		}
		return lblInstalaciones;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnCancelar.setBounds(8, 500, 152, 36);
		}
		return btnCancelar;
	}

	private JButton getBtnModificar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modificar");
			btnModificar.setEnabled(false);
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					modificarReserva();
				}
			});
			btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnModificar.setBounds(624, 500, 182, 36);
		}
		return btnModificar;
	}

	private void modificarReserva() {
		ReservaDTO reserva = reservas.get(tableActividadesPlanificadas.getSelectedRow());

		int idActividad = reserva.idActividad;
		int idInstalacion = reserva.idInstalacion;

		recursosInstalacion = new ObtenerRecursosInstalacionAction(idInstalacion).execute();

		int numPlazas = reserva.numPlazas;

		boolean plazasLimitadas;

		if (numPlazas == 0) {
			plazasLimitadas = false;
		} else {
			plazasLimitadas = true;
		}

		int horaInicial;
		int horaFinal;

		boolean diaCompleto = chckbxDaCompleto.isSelected();

		if (diaCompleto) {
			horaInicial = 8;
			horaFinal = 23;
		} else {
			horaInicial = Integer.parseInt(spinnerHoraInicial.getValue().toString());
			horaFinal = Integer.parseInt(spinnerHoraFinal.getValue().toString());
		}

		Date fecha = selectorFecha.getDate();

		List<Date> fechas = new ArrayList<Date>();
		fechas.add(fecha);

		if (!checkHoras(diaCompleto, horaInicial, horaFinal)) {
			JOptionPane.showMessageDialog(this, "La hora inicial no puede ser anterior a la hora final.", "Error hora",
					JOptionPane.INFORMATION_MESSAGE);
		} else {

			List<ConflictoDTO> conflictos = new ComprobarConflictosInstalacionAction(idActividad, idInstalacion, fechas,
					horaInicial, horaFinal).execute();

			if (conflictos.size() != 0) {
				mostrarConflictos(conflictos);
			} else {
				int check = checkConflictos(idActividad, idInstalacion, reserva.fecha, horaInicial, horaFinal);

				if (check == 1) {
					String mensaje = "No es posible planificar la actividad debido a que la instalaci�n ya est� alquilada por un socio en el horario seleccionado.";
					JOptionPane.showMessageDialog(this, mensaje, "Error al planificar una actividad",
							JOptionPane.ERROR_MESSAGE);
				} else {

					int idReserva = reserva.idReserva;

					new ModificarHorarioReservaAction(idReserva, fecha, horaInicial, horaFinal).execute();

					HistorialDTO h = new HistorialDTO();

					h.idReserva = idReserva;
					h.idInstalacionAnterior = idInstalacion;
					h.horaInicialAnterior = horaInicial;
					h.horaFinalAnterior = horaFinal;
					h.fechaAnterior = new java.sql.Date(fecha.getTime());
					h.isPlazasLimitadasAnterior = plazasLimitadas;
					h.numPlazasAnterior = numPlazas;
					h.estado = "MODIFICAR HORARIO";

					new RegistrarHistorialAction(h).execute();

					VentanaOperacionCorrecta correcta = new VentanaOperacionCorrecta(this,
							"El horario de la actividad ha sido modificado con �xito", "Modificaci�n realizada");
					correcta.setVisible(true);
				}
			}
		}
	}

	private int checkConflictos(int idActividad, int idInstalacion, Date fecha, int horaInicial, int horaFinal) {
		String[] horario = new ObtenerDisponibilidadAction().execute(idInstalacion, fecha, -1);

		int resultado = 0;

		for (int i = horaInicial - 8; i < horaFinal - 8; i++) {
			if (horario[i] == "ALQUILADA") {
				resultado = 1;
			}
		}

		return resultado;
	}

	private boolean checkHoras(boolean diaCompleto, int horaInicial, int horaFinal) {
		if (diaCompleto) {
			if (horaInicial == 8 && horaFinal == 23) {
				return true;
			}
		} else {
			if (horaInicial < horaFinal) {
				return true;
			}
		}
		return false;
	}

	private void mostrarConflictos(List<ConflictoDTO> conflictos) {
		VentanaConflictos v = new VentanaConflictos(this, conflictos);
		v.setVisible(true);
	}

	private JLabel getLblRecursosParaLa() {
		if (lblRecursosParaLa == null) {
			lblRecursosParaLa = new JLabel("Recursos para la actividad");
			lblRecursosParaLa.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblRecursosParaLa.setBounds(537, 11, 230, 27);
		}
		return lblRecursosParaLa;
	}

	private JScrollPane getScrollPaneRecursosActividad() {
		if (scrollPaneRecursosActividad == null) {
			scrollPaneRecursosActividad = new JScrollPane();
			scrollPaneRecursosActividad.setBounds(537, 45, 269, 129);
			scrollPaneRecursosActividad.setViewportView(getTableRecursosActividad());
		}
		return scrollPaneRecursosActividad;
	}

	private JTable getTableRecursosActividad() {
		if (tableRecursosActividad == null) {
			tableRecursosActividad = new JTable();
		}
		return tableRecursosActividad;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 45, 501, 129);
			scrollPane.setViewportView(getTableActividadesPlanificadas());
		}
		return scrollPane;
	}

	private JTable getTableActividadesPlanificadas() {
		if (tableActividadesPlanificadas == null) {
			tableActividadesPlanificadas = new JTable();
			tableActividadesPlanificadas.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					int idActividad = reservas.get(tableActividadesPlanificadas.getSelectedRow()).idActividad;
					int idInstalacion = reservas.get(tableActividadesPlanificadas.getSelectedRow()).idInstalacion;

					mostrarRecursosActividad(idActividad);
					mostrarActividad(idActividad);
					mostrarInstalacion(idInstalacion);

					btnModificar.setEnabled(true);
				}
			});
		}
		return tableActividadesPlanificadas;
	}

	private JLabel getLblActividad() {
		if (lblActividad == null) {
			lblActividad = new JLabel("Actividad");
			lblActividad.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblActividad.setBounds(20, 183, 217, 36);
		}
		return lblActividad;
	}

	private JLabel getLblIdActividad() {
		if (lblIdActividad == null) {
			lblIdActividad = new JLabel("ID");
			lblIdActividad.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblIdActividad.setBounds(40, 230, 122, 36);
		}
		return lblIdActividad;
	}

	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre");
			lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNombre.setBounds(40, 277, 122, 36);
		}
		return lblNombre;
	}

	private JLabel getLblIntensdiad() {
		if (lblIntensdiad == null) {
			lblIntensdiad = new JLabel("Intensidad");
			lblIntensdiad.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblIntensdiad.setBounds(40, 324, 122, 36);
		}
		return lblIntensdiad;
	}

	private JLabel getLabelResultadoIdActividad() {
		if (labelResultadoIdActividad == null) {
			labelResultadoIdActividad = new JLabel("");
			labelResultadoIdActividad.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoIdActividad.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoIdActividad.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoIdActividad.setBounds(124, 230, 217, 36);
		}
		return labelResultadoIdActividad;
	}

	private JLabel getLabelResultadoNombreActividad() {
		if (labelResultadoNombreActividad == null) {
			labelResultadoNombreActividad = new JLabel("");
			labelResultadoNombreActividad.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoNombreActividad.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoNombreActividad.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoNombreActividad.setBounds(124, 277, 217, 36);
		}
		return labelResultadoNombreActividad;
	}

	private JLabel getLabelResultadoIntensidadActividad() {
		if (labelResultadoIntensidadActividad == null) {
			labelResultadoIntensidadActividad = new JLabel("");
			labelResultadoIntensidadActividad.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoIntensidadActividad.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoIntensidadActividad.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoIntensidadActividad.setBounds(124, 324, 217, 36);
		}
		return labelResultadoIntensidadActividad;
	}

	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setOrientation(SwingConstants.VERTICAL);
			separator.setBounds(401, 185, 12, 182);
		}
		return separator;
	}

	private JLabel getLblInstalacion() {
		if (lblInstalacion == null) {
			lblInstalacion = new JLabel("Instalaci\u00F3n");
			lblInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblInstalacion.setBounds(436, 183, 217, 36);
		}
		return lblInstalacion;
	}

	private JLabel getLblIdInstalacion() {
		if (lblIdInstalacion == null) {
			lblIdInstalacion = new JLabel("ID");
			lblIdInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblIdInstalacion.setBounds(462, 230, 65, 36);
		}
		return lblIdInstalacion;
	}

	private JLabel getLabelNombreInstalacion() {
		if (labelNombreInstalacion == null) {
			labelNombreInstalacion = new JLabel("Nombre");
			labelNombreInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			labelNombreInstalacion.setBounds(462, 277, 122, 36);
		}
		return labelNombreInstalacion;
	}

	private JLabel getLabelResultadoIdInstalacion() {
		if (labelResultadoIdInstalacion == null) {
			labelResultadoIdInstalacion = new JLabel("");
			labelResultadoIdInstalacion.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoIdInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoIdInstalacion.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoIdInstalacion.setBounds(539, 230, 217, 36);
		}
		return labelResultadoIdInstalacion;
	}

	private JLabel getLabelResultadoNombreInstalacion() {
		if (labelResultadoNombreInstalacion == null) {
			labelResultadoNombreInstalacion = new JLabel("");
			labelResultadoNombreInstalacion.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoNombreInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoNombreInstalacion.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoNombreInstalacion.setBounds(539, 277, 217, 36);
		}
		return labelResultadoNombreInstalacion;
	}

	private JSeparator getSeparator_1() {
		if (separator_1 == null) {
			separator_1 = new JSeparator();
			separator_1.setBounds(10, 383, 794, 17);
		}
		return separator_1;
	}

	private void mostrarReservas() {
		crearModeloReservas();

		String id, fecha, horaInicio, horaFin, plazasLimitadas, numPlazas;

		reservas = new ObtenerReservasProximasAction().execute();

		for (ReservaDTO r : reservas) {
			id = String.valueOf(r.idReserva);
			fecha = String.valueOf(r.fecha);
			horaInicio = String.valueOf(r.horaInicial);
			horaFin = String.valueOf(r.horaFinal);

			if (r.isPlazasLimitadas) {
				plazasLimitadas = "CON L�MITE";
			} else {
				plazasLimitadas = "SIN L�MITE";
			}

			if (!r.isPlazasLimitadas) {
				numPlazas = "ILIMITADAS";
			} else {
				numPlazas = String.valueOf(r.numPlazas);
			}

			String[] nuevaFila = new String[] { id, fecha, horaInicio, horaFin, plazasLimitadas, numPlazas };

			modeloReservas.addRow(nuevaFila);
		}

		tableActividadesPlanificadas.setModel(modeloReservas);

		tableActividadesPlanificadas.revalidate();
		tableActividadesPlanificadas.repaint();
	}

	private void mostrarActividad(int idActividad) {
		ActividadDTO a = new ObtenerActividadAction(idActividad).execute();

		labelResultadoIdActividad.setText(String.valueOf(a.idActividad));
		labelResultadoNombreActividad.setText(a.nombre);
		labelResultadoIntensidadActividad.setText(String.valueOf(a.intensidad));

		mostrarRecursosActividad(idActividad);
	}

	private void mostrarRecursosActividad(int idActividad) {
		crearModeloRecursosActividad();

		String idRecurso, nombre;

		recursosActividad = new ObtenerRecursosActividadAction(idActividad).execute();

		for (RecursoDTO r : recursosActividad) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;

			String[] nuevaFila = new String[] { idRecurso, nombre };

			modeloRecursosActividad.addRow(nuevaFila);
		}

		tableRecursosActividad.setModel(modeloRecursosActividad);

		tableRecursosActividad.revalidate();
		tableRecursosActividad.repaint();
	}

	private void mostrarInstalacion(int idInstalacion) {
		InstalacionDTO i = new ObtenerInstalacionAction(idInstalacion).execute();

		labelResultadoIdInstalacion.setText(String.valueOf(i.idInstalacion));
		labelResultadoNombreInstalacion.setText(i.nombre);
	}

	private void crearModeloReservas() {
		String[] nombreColumnas = { "ID", "FECHA", "HORA INICIO", "HORA FIN", "PLAZAS LIMITADAS", "N� PLAZAS" };

		modeloReservas = new ModeloNoEditable(nombreColumnas, 0);
	}

	private void crearModeloRecursosActividad() {
		String[] nombreColumnas = { "ID", "NOMBRE" };
		modeloRecursosActividad = new ModeloNoEditable(nombreColumnas, 0);
	}

	private JCheckBox getChckbxDaCompleto() {
		if (chckbxDaCompleto == null) {
			chckbxDaCompleto = new JCheckBox("D\u00EDa completo");
			chckbxDaCompleto.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					seleccionarDiaCompleto();
				}
			});
			chckbxDaCompleto.setFont(new Font("Tahoma", Font.PLAIN, 14));
			chckbxDaCompleto.setBounds(422, 446, 105, 25);
		}
		return chckbxDaCompleto;
	}

	private void seleccionarDiaCompleto() {
		if (chckbxDaCompleto.isSelected()) {
			spinnerHoraInicial.setEnabled(false);
			spinnerHoraFinal.setEnabled(false);
		} else {
			spinnerHoraInicial.setEnabled(true);
			spinnerHoraFinal.setEnabled(true);
		}
	}

	private JLabel getLblHoraInicio() {
		if (lblHoraInicio == null) {
			lblHoraInicio = new JLabel("Hora inicio");
			lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraInicio.setBounds(576, 411, 62, 17);
		}
		return lblHoraInicio;
	}

	private JLabel getLblHoraFin() {
		if (lblHoraFin == null) {
			lblHoraFin = new JLabel("Hora final");
			lblHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraFin.setBounds(679, 411, 56, 17);
		}
		return lblHoraFin;
	}

	private JSpinner getSpinnerHoraInicial() {
		if (spinnerHoraInicial == null) {
			spinnerHoraInicial = new JSpinner();
			spinnerHoraInicial.setEnabled(false);
			spinnerHoraInicial.setFont(new Font("Tahoma", Font.PLAIN, 14));
			spinnerHoraInicial.setModel(new SpinnerNumberModel(8, 8, 22, 1));
			spinnerHoraInicial.setBounds(584, 440, 46, 36);
		}
		return spinnerHoraInicial;
	}

	private JSpinner getSpinnerHoraFinal() {
		if (spinnerHoraFinal == null) {
			spinnerHoraFinal = new JSpinner();
			spinnerHoraFinal.setEnabled(false);
			spinnerHoraFinal.setModel(new SpinnerNumberModel(9, 9, 23, 1));
			spinnerHoraFinal.setBounds(684, 440, 46, 36);
		}
		return spinnerHoraFinal;
	}

	private JDateChooser getSelectorFecha() {
		if (selectorFecha == null) {
			selectorFecha = new JDateChooser();
			selectorFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
			Date fechaActual = new Date();
			selectorFecha.setMinSelectableDate(fechaActual);
			selectorFecha.setDate(fechaActual);

			selectorFecha.setBounds(158, 440, 183, 36);
		}
		return selectorFecha;
	}
}
