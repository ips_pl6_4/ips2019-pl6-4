package ui.windows.util;

import logic.dto.SocioDTO;

/**
 * Clase ComboBoxItemSocio, que encapsula un Socio
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 12.11.2019
 */
public class ComboBoxItemSocio {
	private String key;
	private SocioDTO value;

	public ComboBoxItemSocio(String key, SocioDTO value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString() {
		return value.name + " " + value.apellidos + " (ID: " + value.socioID + ")";
	}

	public String getKey() {
		return key;
	}

	public SocioDTO getValue() {
		return value;
	}
}