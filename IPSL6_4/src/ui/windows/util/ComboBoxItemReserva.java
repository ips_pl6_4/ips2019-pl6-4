package ui.windows.util;

import logic.dto.ReservaDTO;

/**
 * Clase ComboBoxItemReserva, que encapsula una Reserva
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 6.11.2019
 */
public class ComboBoxItemReserva {
	private ReservaDTO reserva;
	private String nombreActividad;

	public ComboBoxItemReserva(ReservaDTO value, String nombreActividad) {
		this.reserva = value;
		this.nombreActividad = nombreActividad;
	}

	@Override
	public String toString() {
		return this.nombreActividad + ": " + this.reserva.fecha.toString() + " de " + this.reserva.horaInicial + " a "
				+ this.reserva.horaFinal + " (ID: " + this.reserva.idReserva + ")";
	}

	public ReservaDTO getValue() {
		return reserva;
	}
}