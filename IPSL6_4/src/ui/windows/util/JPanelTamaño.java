package ui.windows.util;

import java.awt.Dimension;
import javax.swing.JPanel;

@SuppressWarnings("serial")
/**
 * Clase de JPanel para modificar su tama�o y utilizarlo en un layout
 * 
 * @author Ana Garcia 
 * 
 * 8-10-2019
 */
public class JPanelTama�o extends JPanel {
	/**
	 * Dimensiones del panel
	 */
	private Dimension size;

	/**
	 * Constructor de la clase
	 * 
	 * @param size
	 *            Tama�o de panel
	 */
	public JPanelTama�o(Dimension size) {
		this.size = size;
	}

	/**
	 * Metodo que devuelve el tama�o del panel
	 */
	@Override
	public Dimension getPreferredSize() {
		return size;
	}
}
