package ui.windows.util;

import logic.dto.MonitorDTO;

/**
 * Clase ComboBoxItemMonitor, que encapsula un Monitor
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class ComboBoxItemMonitor {
	private String key;
	private MonitorDTO value;

	public ComboBoxItemMonitor(String key, MonitorDTO value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String toString() {
		return key;
	}

	public String getKey() {
		return key;
	}

	public MonitorDTO getValue() {
		return value;
	}
}