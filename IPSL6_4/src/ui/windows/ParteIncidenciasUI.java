package ui.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.dto.ActividadDTO;
import logic.dto.IncidenciaDTO;
import logic.dto.ReservaDTO;
import logic.service.admin.AdminServicesFactory;
import logic.service.monitor.MonitorServicesFactory;
import ui.VentanaPrincipal;
import ui.windows.util.ComboBoxItemReserva;

/**
 * Clase ParteIncidenciasUI
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class ParteIncidenciasUI extends JFrame {

	private static final long serialVersionUID = 6911491950648918625L;

	private JPanel contentPane;
	private JPanel panelLists;
	private JLabel lblActividades;
	private JComboBox<ComboBoxItemReserva> comboBoxReservas;
	private JScrollPane scrollPane;
	private JPanel panelButtons;
	private JTextPane textPane;
	private JButton btnAgregarNuevaIncidencia;
	private JPanel panelReservas;
	private JPanel panelAsistencias;
	private JLabel lblAsistenciasTitulo;
	private JLabel lblAsistencias;

	public ParteIncidenciasUI(JFrame ventana) {
		setTitle("Centro deportivo: Parte de incidencias");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/logoIconoSample.jpg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 569, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPanelLists(), BorderLayout.NORTH);
		contentPane.add(getScrollPane(), BorderLayout.CENTER);
		contentPane.add(getPanelButtons(), BorderLayout.SOUTH);

		setLocationRelativeTo(ventana);
	}

	private JPanel getPanelLists() {
		if (panelLists == null) {
			panelLists = new JPanel();
			panelLists.setBackground(Color.WHITE);
			panelLists.setLayout(new GridLayout(1, 2, 0, 0));
			panelLists.add(getPanelAsistencias());
			panelLists.add(getPanelReservas());
		}
		return panelLists;
	}

	private JLabel getLblSocios() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Selecciónese reserva:");
			lblActividades.setHorizontalAlignment(SwingConstants.CENTER);
			lblActividades.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
		}
		return lblActividades;
	}

	private JComboBox<ComboBoxItemReserva> getComboBoxReservas() {
		if (comboBoxReservas == null) {
			refreshComboBoxReservas();
			comboBoxReservas.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
					refreshTextPaneIncidencias();
				}
			});
		}
		return comboBoxReservas;
	}

	private void refreshComboBoxReservas() {
		List<ReservaDTO> reservas = MonitorServicesFactory.getReservasDisponibles();
		DefaultComboBoxModel<ComboBoxItemReserva> comboReservas = new DefaultComboBoxModel<ComboBoxItemReserva>();
		for (ReservaDTO reserva : reservas) {
			ActividadDTO actividad = (reserva == null) ? null
					: AdminServicesFactory.getActividadCorrespondiente(reserva);
			String nombreActividad = (actividad == null) ? "" : actividad.nombre;
			comboReservas.addElement(new ComboBoxItemReserva(reserva, nombreActividad));
		}

		if (comboBoxReservas == null)
			comboBoxReservas = new JComboBox<ComboBoxItemReserva>();

		comboBoxReservas.setVisible(false);
		comboBoxReservas.setModel(comboReservas);
		comboBoxReservas.setVisible(true);
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTextPane());
		}
		return scrollPane;
	}

	private void refreshTextPaneIncidencias() {
		getTextPane().setText("");
		if (getComboBoxReservas().getSelectedItem() != null) {
			ComboBoxItemReserva reservaCapsule = (ComboBoxItemReserva) getComboBoxReservas().getSelectedItem();
			List<IncidenciaDTO> incidencias = MonitorServicesFactory
					.getObtenerIncidencia(reservaCapsule.getValue().idReserva);
			for (IncidenciaDTO incidencia : incidencias)
				appendLineTextPane("---> " + incidencia.incidencia);
		}
		refreshAsistencias();
	}

	private void refreshAsistencias() {
		if (getComboBoxReservas().getSelectedItem() != null) {
			ComboBoxItemReserva reservaCapsule = (ComboBoxItemReserva) getComboBoxReservas().getSelectedItem();
			int cantidadDeAsistencias = MonitorServicesFactory
					.getCantidadAsistencias(reservaCapsule.getValue().idReserva);
			getLblAsistencias().setText("" + cantidadDeAsistencias);
		}
	}

	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			panelButtons = new JPanel();
			panelButtons.setBackground(Color.WHITE);
			panelButtons.add(getBtnAgregarNuevaIncidencia());
		}
		return panelButtons;
	}

	private JTextPane getTextPane() {
		if (textPane == null) {
			textPane = new JTextPane();
			textPane.setEditable(false);
		}
		return textPane;
	}

	private JButton getBtnAgregarNuevaIncidencia() {
		if (btnAgregarNuevaIncidencia == null) {
			btnAgregarNuevaIncidencia = new JButton("Agregar nueva incidencia");
			btnAgregarNuevaIncidencia.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnAgregarNuevaIncidenciaAction();
				}
			});
		}
		return btnAgregarNuevaIncidencia;
	}

	private void btnAgregarNuevaIncidenciaAction() {
		if (getComboBoxReservas().getSelectedItem() != null) {
			ComboBoxItemReserva reservaCapsule = (ComboBoxItemReserva) getComboBoxReservas().getSelectedItem();
			String incidencia = JOptionPane.showInputDialog(this,
					"¿Qué incidencia ha ocurrido en la reserva " + reservaCapsule + "?");
			if (incidencia != null) {
				if (incidencia.equals(""))
					JOptionPane.showMessageDialog(this, "Por favor, escriba algo en la incidencia");
				else {
					IncidenciaDTO incidenciaData = new IncidenciaDTO();
					incidenciaData.reservaID = reservaCapsule.getValue().idReserva;
					incidenciaData.incidencia = incidencia;
					MonitorServicesFactory.setInsertarIncidencia(incidenciaData);
					refreshTextPaneIncidencias();
				}
			}
		}
	}

	private void appendLineTextPane(String text) {
		textPane.setText(textPane.getText() + text + "\n");
	}

	private JPanel getPanelReservas() {
		if (panelReservas == null) {
			panelReservas = new JPanel();
			panelReservas.setBackground(Color.WHITE);
			panelReservas.setLayout(new GridLayout(2, 1, 0, 0));
			panelReservas.add(getLblSocios());
			panelReservas.add(getComboBoxReservas());
		}
		return panelReservas;
	}

	private JPanel getPanelAsistencias() {
		if (panelAsistencias == null) {
			panelAsistencias = new JPanel();
			panelAsistencias.setBackground(Color.WHITE);
			panelAsistencias.setLayout(new GridLayout(2, 1, 0, 0));
			panelAsistencias.add(getLblAsistenciasTitulo());
			panelAsistencias.add(getLblAsistencias());
		}
		return panelAsistencias;
	}

	private JLabel getLblAsistenciasTitulo() {
		if (lblAsistenciasTitulo == null) {
			lblAsistenciasTitulo = new JLabel("Número aproximado de asistentes:");
			lblAsistenciasTitulo.setHorizontalAlignment(SwingConstants.CENTER);
			lblAsistenciasTitulo.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
		}
		return lblAsistenciasTitulo;
	}

	private JLabel getLblAsistencias() {
		if (lblAsistencias == null) {
			lblAsistencias = new JLabel("");
			lblAsistencias.setForeground(Color.RED);
			lblAsistencias.setHorizontalAlignment(SwingConstants.CENTER);
			lblAsistencias.setFont(new Font("Arial Narrow", Font.BOLD, 18));
		}
		return lblAsistencias;
	}
}