package ui.windows;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import logic.AnularReserva;
import logic.util.container.Reserva;
import persistence.ReservarPlazaConLimitePersistencia;
import ui.VentanaPrincipal;

import java.awt.Toolkit;

public class VentanaAnularReserva extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JButton btnAnular;
	private JList<Reserva> list;

	private int idSocio;

	/**
	 * Create the frame.
	 */
	public VentanaAnularReserva(VentanaPrincipal v,int id) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaAnularReserva.class.getResource("/img/logoIconoSample.jpg")));
		setResizable(false);

		this.idSocio = id;

		setTitle("Anular reserva del socio:" + id);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 480, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getScrollPane());
		contentPane.add(getBtnAnular());
		
		setLocationRelativeTo(v);
		setVisible(true);
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 34, 300, 220);
			scrollPane.setViewportView(getList());
		}
		return scrollPane;
	}

	private JButton getBtnAnular() {
		if (btnAnular == null) {
			btnAnular = new JButton("Anular");
			btnAnular.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					execute();
				}
			});
			btnAnular.setBounds(335, 120, 90, 25);
		}
		return btnAnular;
	}

	public void execute() {
		// new AnularReserva().execute(idSocio, idReserva); //TODO sacar la id de la
		// reserva
		try {
			int idReserva;
			try {
				idReserva = list.getSelectedValue().getiDReserva();
			} catch (NullPointerException e) {
				throw new RuntimeException("Ningun elemento seleccionado");
			}
			new AnularReserva().execute(idSocio, idReserva);
			JOptionPane.showMessageDialog(this, "La operación fue un exito");
		} catch (RuntimeException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}

	}

	private JList<Reserva> getList() {
		if (list == null) {
			list = new JList<Reserva>();
			list = new JList<Reserva>();
			DefaultListModel<Reserva> lm = new DefaultListModel<Reserva>();
			List<Reserva> lista = new ReservarPlazaConLimitePersistencia().getActividades(idSocio);
			for (Reserva theReserva : lista)
				lm.addElement(theReserva);

			list.setModel(lm);
		}
		return list;
	}
}
