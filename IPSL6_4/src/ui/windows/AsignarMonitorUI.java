package ui.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.dto.ActividadDTO;
import logic.dto.ImparteDTO;
import logic.dto.MonitorDTO;
import logic.dto.ReservaDTO;
import logic.service.admin.AdminServicesFactory;
import ui.VentanaPrincipal;
import ui.windows.util.ComboBoxItemMonitor;
import ui.windows.util.ComboBoxItemReserva;

/**
 * Clase AsignarMonitorUI
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public class AsignarMonitorUI extends JFrame {

	private static final long serialVersionUID = 6911491950648918625L;

	private JPanel contentPane;
	private JPanel panelLists;
	private JPanel panelActividades;
	private JPanel panelMonitor;
	private JLabel lblActividades;
	private JComboBox<ComboBoxItemMonitor> comboBoxMonitores;
	private JLabel lblMonitores;
	private JComboBox<ComboBoxItemReserva> comboBoxActividades;
	private JPanel panelButtons;
	private JButton btnAsignar;
	private JTextPane textPaneAsignaciones;
	private JScrollPane scrollPane;

	public AsignarMonitorUI(JFrame ventana) {
		setTitle("Centro deportivo: Asignar monitor a una actividad");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/logoIconoSample.jpg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 592, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPanelLists(), BorderLayout.NORTH);
		contentPane.add(getPanelButtons(), BorderLayout.SOUTH);
		contentPane.add(getScrollPane(), BorderLayout.CENTER);

		setLocationRelativeTo(ventana);
	}

	private JPanel getPanelLists() {
		if (panelLists == null) {
			panelLists = new JPanel();
			panelLists.setBackground(Color.WHITE);
			panelLists.setLayout(new GridLayout(0, 2, 0, 0));
			panelLists.add(getPanelActividades());
			panelLists.add(getPanelMonitor());
		}
		return panelLists;
	}

	private JPanel getPanelActividades() {
		if (panelActividades == null) {
			panelActividades = new JPanel();
			panelActividades.setBackground(Color.WHITE);
			panelActividades.setLayout(new BorderLayout(0, 0));
			panelActividades.add(getLblActividades(), BorderLayout.NORTH);
			panelActividades.add(getComboBoxActividades(), BorderLayout.SOUTH);
		}
		return panelActividades;
	}

	private JPanel getPanelMonitor() {
		if (panelMonitor == null) {
			panelMonitor = new JPanel();
			panelMonitor.setBackground(Color.WHITE);
			panelMonitor.setLayout(new BorderLayout(0, 0));
			panelMonitor.add(getLblMonitores(), BorderLayout.NORTH);
			panelMonitor.add(getComboBoxMonitores(), BorderLayout.SOUTH);
		}
		return panelMonitor;
	}

	private JLabel getLblActividades() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Actividad a asignarle un monitor:");
			lblActividades.setHorizontalAlignment(SwingConstants.CENTER);
			lblActividades.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
		}
		return lblActividades;
	}

	private JComboBox<ComboBoxItemMonitor> getComboBoxMonitores() {
		if (comboBoxMonitores == null)
			refreshComboBoxMonitores();
		return comboBoxMonitores;
	}

	private JLabel getLblMonitores() {
		if (lblMonitores == null) {
			lblMonitores = new JLabel("Monitor al que asignarle la actividad:");
			lblMonitores.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
			lblMonitores.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblMonitores;
	}

	private JComboBox<ComboBoxItemReserva> getComboBoxActividades() {
		if (comboBoxActividades == null)
			refreshComboBoxActividades();
		return comboBoxActividades;
	}

	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			panelButtons = new JPanel();
			panelButtons.setBackground(Color.WHITE);
			panelButtons.add(getBtnAsignar());
		}
		return panelButtons;
	}

	private JButton getBtnAsignar() {
		if (btnAsignar == null) {
			btnAsignar = new JButton("Asignar");
			btnAsignar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnAsignarAction();
				}
			});
		}
		return btnAsignar;
	}

	private void btnAsignarAction() {
		if (comboBoxActividades.getSelectedItem() != null && comboBoxMonitores.getSelectedItem() != null) {
			ComboBoxItemMonitor capsuleMonitor = (ComboBoxItemMonitor) comboBoxMonitores.getSelectedItem();
			ComboBoxItemReserva capsuleReserva = (ComboBoxItemReserva) comboBoxActividades.getSelectedItem();
			ImparteDTO imparticion = new ImparteDTO();
			imparticion.monitorID = capsuleMonitor.getValue().monitorID;
			imparticion.reservaID = capsuleReserva.getValue().idReserva;
			boolean flag = AdminServicesFactory.setImparticion(imparticion);
			if (flag)
				appendLineTextPaneAsignaciones("El monitor '" + capsuleMonitor.getKey()
						+ "' ha sido asignado a la actividad '" + capsuleReserva.toString() + "'.");
			else
				appendLineTextPaneAsignaciones("ERROR: El monitor '" + capsuleMonitor.getKey()
						+ "' no ha podido ser asignado a la actividad '" + capsuleReserva.toString()
						+ "' debido a que ya imparte otra actividad en el mismo horario.");
			refreshComboBoxMonitores();
			refreshComboBoxActividades();
		} else
			appendLineTextPaneAsignaciones("Por favor, seleccione un monitor y una actividad adecuados.");
	}

	private void refreshComboBoxMonitores() {
		List<MonitorDTO> monitores = AdminServicesFactory.getMonitores();
		DefaultComboBoxModel<ComboBoxItemMonitor> comboMonitores = new DefaultComboBoxModel<ComboBoxItemMonitor>();
		for (MonitorDTO monitor : monitores)
			comboMonitores
					.addElement(new ComboBoxItemMonitor(monitor.name + " (ID: " + monitor.monitorID + ")", monitor));

		if (comboBoxMonitores == null)
			comboBoxMonitores = new JComboBox<ComboBoxItemMonitor>();

		comboBoxMonitores.setVisible(false);
		comboBoxMonitores.setModel(comboMonitores);
		comboBoxMonitores.setVisible(true);
	}

	private void refreshComboBoxActividades() {
		List<ReservaDTO> reservas = AdminServicesFactory.getReservasSinMonitor();
		DefaultComboBoxModel<ComboBoxItemReserva> comboReservas = new DefaultComboBoxModel<ComboBoxItemReserva>();
		for (ReservaDTO reserva : reservas) {
			ActividadDTO actividad = (reserva == null) ? null
					: AdminServicesFactory.getActividadCorrespondiente(reserva);
			String nombreActividad = (actividad == null) ? "" : actividad.nombre;
			comboReservas.addElement(new ComboBoxItemReserva(reserva, nombreActividad));
		}

		if (comboBoxActividades == null)
			comboBoxActividades = new JComboBox<ComboBoxItemReserva>();

		comboBoxActividades.setVisible(false);
		comboBoxActividades.setModel(comboReservas);
		comboBoxActividades.setVisible(true);
	}

	private JTextPane getTextPaneAsignaciones() {
		if (textPaneAsignaciones == null) {
			textPaneAsignaciones = new JTextPane();
			textPaneAsignaciones.setEditable(false);
		}
		return textPaneAsignaciones;
	}

	private void appendLineTextPaneAsignaciones(String text) {
		textPaneAsignaciones.setText(textPaneAsignaciones.getText() + text + "\n");
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTextPaneAsignaciones());
		}
		return scrollPane;
	}
}