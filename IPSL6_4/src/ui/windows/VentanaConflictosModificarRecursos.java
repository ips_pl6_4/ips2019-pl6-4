package ui.windows;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.dto.ConflictoModificarRecursoDTO;

public class VentanaConflictosModificarRecursos extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JLabel lblNoSeHan;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton btnAceptar;

	public VentanaConflictosModificarRecursos(JFrame ventana, List<ConflictoModificarRecursoDTO> conflictos) {
		setModal(true);
		setResizable(false);

		setTitle("Conflictos generados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 510, 412);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNoSeHan());
		contentPane.add(getBtnAceptar());
		contentPane.add(getScrollPane());

		String texto = "";

		for (ConflictoModificarRecursoDTO c : conflictos) {
			texto += c.toString() + "\n";
		}

		textArea.setText(texto);

		setLocationRelativeTo(ventana);
	}

	private JLabel getLblNoSeHan() {
		if (lblNoSeHan == null) {
			lblNoSeHan = new JLabel("No se han realizado las reservas debido a los siguientes conflictos");
			lblNoSeHan.setHorizontalAlignment(SwingConstants.CENTER);
			lblNoSeHan.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNoSeHan.setBounds(0, 0, 504, 67);
		}
		return lblNoSeHan;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(24, 66, 456, 239);
			scrollPane.setViewportView(getTextArea());
		}
		return scrollPane;
	}

	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			textArea.setLocation(25, 0);
			textArea.setEditable(false);
			textArea.setFont(new Font("Tahoma", Font.PLAIN, 14));
			textArea.setWrapStyleWord(true);
			textArea.setLineWrap(true);
		}
		return textArea;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton("Aceptar");
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAceptar.setBounds(178, 331, 149, 41);
		}
		return btnAceptar;
	}
}
