package ui.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;

import logic.dto.ActividadDTO;
import logic.dto.ConflictoDTO;
import logic.dto.InstalacionDTO;
import logic.dto.RecursoDTO;
import logic.dto.RecursoInstalacionDTO;
import logic.dto.ReservaDTO;
import ui.actions.ComprobarConflictosInstalacionAction;
import ui.actions.CrearGrupoReservasAction;
import ui.actions.CrearReservaAction;
import ui.actions.ObtenerActividadesAction;
import ui.actions.ObtenerDisponibilidadAction;
import ui.actions.ObtenerFechasAction;
import ui.actions.ObtenerInstalacionesAction;
import ui.actions.ObtenerNumPlazasActInstAction;
import ui.actions.ObtenerRecursosActividadAction;
import ui.actions.ObtenerRecursosInstalacionAction;

public class VentanaCrearReservaPeriodica extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JLabel lblActividades;
	private JLabel lblInstalaciones;
	private JLabel lblHoraInicio;
	private JLabel lblHoraFin;
	private JLabel lblPlazasLimitadas;
	private JLabel lblRecursosInstalacion;
	private JLabel lblRecursosParaLa;
	private JLabel lblNumplazas;

	private JScrollPane scrollPaneActividades;
	private JScrollPane scrollPaneInstalaciones;

	private JTable tableInstalaciones;
	private JTable tableActividades;

	ModeloNoEditable modeloActividades;
	ModeloNoEditable modeloInstalaciones;
	ModeloNoEditable modeloRecursosActividad;
	ModeloNoEditable modeloRecursosInstalacion;

	private JCheckBox chckbxDaCompleto;

	private JSpinner spinnerHoraInicial;
	private JSpinner spinnerHoraFinal;

	private JButton btnCancelar;
	private JButton btnCrearReserva;

	private JScrollPane scrollPaneRecursosActividad;
	private JScrollPane scrollPaneRecursosInstalacion;

	private JTable tableRecursosActividad;
	private JTable tableRecursosInstalacion;

	List<InstalacionDTO> instalaciones;
	List<ActividadDTO> actividades;
	List<RecursoDTO> recursosActividad;
	List<RecursoInstalacionDTO> recursosInstalacion;

	private ObtenerActividadesAction oaa;
	private ObtenerInstalacionesAction oia;
	private ObtenerNumPlazasActInstAction onp;

	private JLabel lblNewLabel;
	private JTextField textFieldNumPlazas;
	private JLabel lblFechas;
	private JLabel lblFechaInicial;
	private JLabel lblFechaFinal;
	private JDateChooser dateChooserFechaInicial;
	private JDateChooser dateChooserFechaFinal;
	private JLabel label;
	private JDateChooser dateChooserFechaDesde;
	private JLabel lblDurante;
	private JSpinner spinner;
	private JLabel lblSemanas;
	private JLabel lblDaYHora;
	private JComboBox<String> comboBoxDias;
	private JLabel lblDa;
	private JSeparator separator;
	private JSeparator separator_1;
	private JSeparator separator_2;
	private JRadioButton rdbtnIntervaloDeFechas;
	private JRadioButton rdbtnNmeroDeSemanas;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	public VentanaCrearReservaPeriodica(JFrame ventanaPrincipal) {
		setResizable(false);
		setTitle("Planificar actividades peri\u00F3dicas");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 968, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblActividades());
		contentPane.add(getLblInstalaciones());
		contentPane.add(getScrollPaneActividades());
		contentPane.add(getScrollPaneInstalaciones());
		contentPane.add(getChckbxDaCompleto());
		contentPane.add(getSpinnerHoraInicial());
		contentPane.add(getSpinnerHoraFinal());
		contentPane.add(getLblHoraInicio());
		contentPane.add(getLblHoraFin());
		contentPane.add(getLblPlazasLimitadas());
		contentPane.add(getBtnCancelar());
		contentPane.add(getBtnCrearReserva());
		contentPane.add(getLblRecursosParaLa());
		contentPane.add(getLblRecursosInstalacion());
		contentPane.add(getScrollPaneRecursosActividad());
		contentPane.add(getScrollPaneRecursosInstalacion());
		contentPane.add(getLblNumplazas());
		contentPane.add(getLblNewLabel());
		contentPane.add(getTextFieldNumPlazas());
		contentPane.add(getLblFechas());
		contentPane.add(getLblFechaInicial());
		contentPane.add(getLblFechaFinal());
		contentPane.add(getDateChooserFechaInicial());
		contentPane.add(getDateChooserFechaFinal());
		contentPane.add(getLabel());
		contentPane.add(getDateChooserFechaDesde());
		contentPane.add(getLblDurante());
		contentPane.add(getSpinner());
		contentPane.add(getLblSemanas());
		contentPane.add(getLblDaYHora());
		contentPane.add(getComboBoxDias());
		contentPane.add(getLblDa());
		contentPane.add(getSeparator());
		contentPane.add(getSeparator_1());
		contentPane.add(getSeparator_2());
		contentPane.add(getRdbtnIntervaloDeFechas());
		contentPane.add(getRdbtnNmeroDeSemanas());

		setLocationRelativeTo(ventanaPrincipal);

		oaa = new ObtenerActividadesAction();
		oia = new ObtenerInstalacionesAction();

		instalaciones = oia.execute();
		actividades = oaa.execute();

		mostrarActividades();
		mostrarInstalaciones();
	}

	private JLabel getLblActividades() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Actividades");
			lblActividades.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblActividades.setBounds(10, 11, 183, 36);
		}
		return lblActividades;
	}

	private JLabel getLblInstalaciones() {
		if (lblInstalaciones == null) {
			lblInstalaciones = new JLabel("Instalaciones");
			lblInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInstalaciones.setBounds(10, 203, 183, 36);
		}
		return lblInstalaciones;
	}

	private JScrollPane getScrollPaneActividades() {
		if (scrollPaneActividades == null) {
			scrollPaneActividades = new JScrollPane();
			scrollPaneActividades.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPaneActividades.setBounds(10, 45, 614, 129);
			scrollPaneActividades.setViewportView(getTableActividades());
		}

		return scrollPaneActividades;
	}

	private JTable getTableActividades() {
		if (tableActividades == null) {
			String[] nombreColumnas = { "ID", "NOMBRE", "INTENSIDAD" };
			modeloActividades = new ModeloNoEditable(nombreColumnas, 0);
			tableActividades = new JTable(modeloActividades);
			tableActividades.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					mostrarRecursosActividad();
					seleccionarActividadInstalacion();
				}

			});
			tableActividades.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableActividades;
	}

	private JScrollPane getScrollPaneInstalaciones() {
		if (scrollPaneInstalaciones == null) {
			scrollPaneInstalaciones = new JScrollPane();
			scrollPaneInstalaciones.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPaneInstalaciones.setBounds(10, 240, 614, 129);
			scrollPaneInstalaciones.setViewportView(getTableInstalaciones());
		}
		return scrollPaneInstalaciones;
	}

	private JTable getTableInstalaciones() {
		if (tableInstalaciones == null) {
			String[] nombreColumnas = { "ID", "NOMBRE" };
			modeloInstalaciones = new ModeloNoEditable(nombreColumnas, 0);
			tableInstalaciones = new JTable(modeloInstalaciones);
			tableInstalaciones.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					mostrarRecursosInstalacion();
					seleccionarActividadInstalacion();
				}
			});
			tableInstalaciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableInstalaciones;
	}

	protected void seleccionarActividadInstalacion() {
		int filaActividad = tableActividades.getSelectedRow();
		int filaColumna = tableInstalaciones.getSelectedRow();

		if (filaActividad != -1 && filaColumna != -1) {
			onp = new ObtenerNumPlazasActInstAction(recursosActividad, recursosInstalacion);
			int numPlazas = onp.execute();

			// Sin plazas
			if (numPlazas == -1) {
				btnCrearReserva.setEnabled(false);
				lblNumplazas.setText("NO HAY RECURSOS SUFICIENTES");

				textFieldNumPlazas.setText("");
				textFieldNumPlazas.setEnabled(false);
				textFieldNumPlazas.setEditable(false);
			}
			// Plazas infinitas
			else if (numPlazas == 0) {
				btnCrearReserva.setEnabled(true);
				lblNumplazas.setText("PLAZAS SIN LIMITE");

				textFieldNumPlazas.setText("");
				textFieldNumPlazas.setEnabled(true);
				textFieldNumPlazas.setEditable(true);
			}
			// Plazas limitadas
			else {
				btnCrearReserva.setEnabled(true);
				lblNumplazas.setText("PLAZAS LIMITADAS: " + numPlazas + " PLAZAS");

				textFieldNumPlazas.setText(String.valueOf(numPlazas));
				textFieldNumPlazas.setEnabled(true);
				textFieldNumPlazas.setEditable(false);
			}
		}
	}

	private void mostrarActividades() {
		String id, nombre, intensidad;

		for (ActividadDTO actividad : actividades) {
			id = String.valueOf(actividad.idActividad);
			nombre = actividad.nombre;
			intensidad = actividad.intensidad.toString();

			String[] nuevaFila = new String[] { id, nombre, intensidad };

			modeloActividades.addRow(nuevaFila);
		}

		tableActividades.setModel(modeloActividades);

		tableActividades.revalidate();
		tableActividades.repaint();
	}

	private void mostrarInstalaciones() {
		String id, nombre;

		for (InstalacionDTO instalacion : instalaciones) {
			id = String.valueOf(instalacion.idInstalacion);
			nombre = instalacion.nombre;

			String[] nuevaFila = new String[] { id, nombre };

			modeloInstalaciones.addRow(nuevaFila);
		}

		tableInstalaciones.setModel(modeloInstalaciones);

		tableInstalaciones.revalidate();
		tableInstalaciones.repaint();
	}

	private void mostrarRecursosActividad() {
		crearModeloRecursosActividad();
		int idActividad = actividades.get(tableActividades.getSelectedRow()).idActividad;

		String idRecurso, nombre;

		recursosActividad = new ObtenerRecursosActividadAction(idActividad).execute();

		for (RecursoDTO r : recursosActividad) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;

			String[] nuevaFila = new String[] { idRecurso, nombre };

			modeloRecursosActividad.addRow(nuevaFila);
		}

		tableRecursosActividad.setModel(modeloRecursosActividad);

		tableRecursosActividad.revalidate();
		tableRecursosActividad.repaint();
	}

	private void mostrarRecursosInstalacion() {
		crearModeloRecursosInstalacion();
		int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

		String idRecurso, nombre, cantidad;

		recursosInstalacion = new ObtenerRecursosInstalacionAction(idInstalacion).execute();

		for (RecursoInstalacionDTO r : recursosInstalacion) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;
			cantidad = String.valueOf(r.cantidad);

			String[] nuevaFila = new String[] { idRecurso, nombre, cantidad };

			modeloRecursosInstalacion.addRow(nuevaFila);
		}

		tableRecursosInstalacion.setModel(modeloRecursosInstalacion);

		tableRecursosInstalacion.revalidate();
		tableRecursosInstalacion.repaint();
	}

	private void crearModeloRecursosActividad() {
		String[] nombreColumnas = { "ID", "NOMBRE" };
		modeloRecursosActividad = new ModeloNoEditable(nombreColumnas, 0);
	}

	private void crearModeloRecursosInstalacion() {
		String[] nombreColumnas = { "ID", "NOMBRE", "CANTIDAD" };
		modeloRecursosInstalacion = new ModeloNoEditable(nombreColumnas, 0);
	}

	private JCheckBox getChckbxDaCompleto() {
		if (chckbxDaCompleto == null) {
			chckbxDaCompleto = new JCheckBox("D\u00EDa completo");
			chckbxDaCompleto.setSelected(true);
			chckbxDaCompleto.setFont(new Font("Tahoma", Font.PLAIN, 14));
			chckbxDaCompleto.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					seleccionarDiaCompleto();
				}

			});
			chckbxDaCompleto.setBounds(623, 539, 123, 34);
		}
		return chckbxDaCompleto;
	}

	private JSpinner getSpinnerHoraInicial() {
		if (spinnerHoraInicial == null) {
			spinnerHoraInicial = new JSpinner();
			spinnerHoraInicial.setEnabled(false);
			spinnerHoraInicial.setModel(new SpinnerNumberModel(8, 8, 22, 1));
			spinnerHoraInicial.setBounds(777, 538, 46, 36);
		}
		return spinnerHoraInicial;
	}

	private JSpinner getSpinnerHoraFinal() {
		if (spinnerHoraFinal == null) {
			spinnerHoraFinal = new JSpinner();
			spinnerHoraFinal.setEnabled(false);
			spinnerHoraFinal.setModel(new SpinnerNumberModel(9, 9, 23, 1));
			spinnerHoraFinal.setBounds(853, 538, 46, 36);
		}
		return spinnerHoraFinal;
	}

	private JLabel getLblHoraInicio() {
		if (lblHoraInicio == null) {
			lblHoraInicio = new JLabel("Hora inicio");
			lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraInicio.setBounds(769, 510, 62, 17);
		}
		return lblHoraInicio;
	}

	private JLabel getLblHoraFin() {
		if (lblHoraFin == null) {
			lblHoraFin = new JLabel("Hora final");
			lblHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraFin.setBounds(848, 510, 56, 17);
		}
		return lblHoraFin;
	}

	protected void seleccionarDiaCompleto() {
		if (chckbxDaCompleto.isSelected()) {
			spinnerHoraInicial.setEnabled(false);
			spinnerHoraFinal.setEnabled(false);
		} else {
			spinnerHoraInicial.setEnabled(true);
			spinnerHoraFinal.setEnabled(true);
		}
	}

	private JLabel getLblPlazasLimitadas() {
		if (lblPlazasLimitadas == null) {
			lblPlazasLimitadas = new JLabel("Plazas");
			lblPlazasLimitadas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblPlazasLimitadas.setBounds(10, 630, 88, 36);
		}
		return lblPlazasLimitadas;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnCancelar.setBounds(10, 692, 152, 36);
		}
		return btnCancelar;
	}

	private JButton getBtnCrearReserva() {
		if (btnCrearReserva == null) {
			btnCrearReserva = new JButton("Crear reserva");
			btnCrearReserva.setEnabled(false);
			btnCrearReserva.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					crearReserva();
				}
			});
			btnCrearReserva.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCrearReserva.setBounds(769, 692, 183, 36);
		}
		return btnCrearReserva;
	}

	private JLabel getLblRecursosParaLa() {
		if (lblRecursosParaLa == null) {
			lblRecursosParaLa = new JLabel("Recursos para la actividad");
			lblRecursosParaLa.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRecursosParaLa.setBounds(658, 21, 326, 17);
		}
		return lblRecursosParaLa;
	}

	private JScrollPane getScrollPaneRecursosActividad() {
		if (scrollPaneRecursosActividad == null) {
			scrollPaneRecursosActividad = new JScrollPane();
			scrollPaneRecursosActividad.setBounds(658, 45, 294, 129);
			scrollPaneRecursosActividad.setViewportView(getTableRecursosActividad());
		}
		return scrollPaneRecursosActividad;
	}

	private JLabel getLblRecursosInstalacion() {
		if (lblRecursosInstalacion == null) {
			lblRecursosInstalacion = new JLabel("Recursos en la instalaci\u00F3n");
			lblRecursosInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRecursosInstalacion.setBounds(662, 213, 322, 17);
		}
		return lblRecursosInstalacion;
	}

	private JScrollPane getScrollPaneRecursosInstalacion() {
		if (scrollPaneRecursosInstalacion == null) {
			scrollPaneRecursosInstalacion = new JScrollPane();
			scrollPaneRecursosInstalacion.setBounds(658, 240, 294, 129);
			scrollPaneRecursosInstalacion.setViewportView(getTableRecursosInstalacion());
		}
		return scrollPaneRecursosInstalacion;
	}

	private JTable getTableRecursosActividad() {
		if (tableRecursosActividad == null) {
			tableRecursosActividad = new JTable();
			crearModeloRecursosActividad();
		}
		return tableRecursosActividad;
	}

	private JTable getTableRecursosInstalacion() {
		if (tableRecursosInstalacion == null) {
			tableRecursosInstalacion = new JTable();
			crearModeloRecursosInstalacion();
		}
		return tableRecursosInstalacion;
	}

	private JLabel getLblNumplazas() {
		if (lblNumplazas == null) {
			lblNumplazas = new JLabel("");
			lblNumplazas.setBorder(new LineBorder(new Color(0, 0, 0)));
			lblNumplazas.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNumplazas.setHorizontalAlignment(SwingConstants.CENTER);
			lblNumplazas.setBounds(112, 630, 465, 36);
		}
		return lblNumplazas;
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("N\u00FAmero de plazas");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel.setBounds(623, 630, 123, 39);
		}
		return lblNewLabel;
	}

	private JTextField getTextFieldNumPlazas() {
		if (textFieldNumPlazas == null) {
			textFieldNumPlazas = new JTextField();
			textFieldNumPlazas.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					char caracter = e.getKeyChar();
					if (textFieldNumPlazas.getText().length() == 0 && caracter == '0') {
						e.consume();
					} else if (caracter > '9' || caracter < '0') {
						e.consume();
					}
				}

				@Override
				public void keyReleased(KeyEvent arg0) {
					if (textFieldNumPlazas.getText().length() != 0) {
						lblNumplazas.setText("PLAZAS LIMITADAS: " + textFieldNumPlazas.getText() + " PLAZAS");
					} else {
						lblNumplazas.setText("PLAZAS SIN LIMITE");
					}
				}
			});
			textFieldNumPlazas.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldNumPlazas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			textFieldNumPlazas.setBounds(749, 631, 182, 36);
			textFieldNumPlazas.setColumns(10);
		}
		return textFieldNumPlazas;
	}

	private JLabel getLblFechas() {
		if (lblFechas == null) {
			lblFechas = new JLabel("Fechas");
			lblFechas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblFechas.setBounds(10, 392, 183, 36);
		}
		return lblFechas;
	}

	private JLabel getLblFechaInicial() {
		if (lblFechaInicial == null) {
			lblFechaInicial = new JLabel("Desde");
			lblFechaInicial.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblFechaInicial.setBounds(38, 467, 62, 36);
		}
		return lblFechaInicial;
	}

	private JLabel getLblFechaFinal() {
		if (lblFechaFinal == null) {
			lblFechaFinal = new JLabel("Hasta");
			lblFechaFinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblFechaFinal.setBounds(277, 467, 62, 36);
		}
		return lblFechaFinal;
	}

	private JDateChooser getDateChooserFechaInicial() {
		if (dateChooserFechaInicial == null) {
			dateChooserFechaInicial = new JDateChooser();
			dateChooserFechaInicial.setFont(new Font("Tahoma", Font.PLAIN, 14));
			dateChooserFechaInicial.setBounds(112, 467, 130, 36);

			Date fechaActual = new Date();
			dateChooserFechaInicial.setMinSelectableDate(fechaActual);
			dateChooserFechaInicial.setDate(fechaActual);
		}
		return dateChooserFechaInicial;
	}

	private JDateChooser getDateChooserFechaFinal() {
		if (dateChooserFechaFinal == null) {
			dateChooserFechaFinal = new JDateChooser();
			dateChooserFechaFinal.setFont(new Font("Tahoma", Font.PLAIN, 14));
			dateChooserFechaFinal.setBounds(349, 467, 130, 36);

			Date fechaActual = new Date();

			Calendar calendar = Calendar.getInstance();

			calendar.setTime(fechaActual);
			calendar.add(Calendar.DATE, 1);

			fechaActual = calendar.getTime();

			dateChooserFechaFinal.setMinSelectableDate(fechaActual);
			dateChooserFechaFinal.setDate(fechaActual);
		}
		return dateChooserFechaFinal;
	}

	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("Desde");
			label.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label.setBounds(39, 553, 62, 36);
		}
		return label;
	}

	private JDateChooser getDateChooserFechaDesde() {
		if (dateChooserFechaDesde == null) {
			dateChooserFechaDesde = new JDateChooser();
			dateChooserFechaDesde.setEnabled(false);
			dateChooserFechaDesde.setFont(new Font("Tahoma", Font.PLAIN, 14));
			dateChooserFechaDesde.setBounds(113, 553, 130, 36);

			Date fechaActual = new Date();
			dateChooserFechaDesde.setMinSelectableDate(fechaActual);
			dateChooserFechaDesde.setDate(fechaActual);
		}
		return dateChooserFechaDesde;
	}

	private JLabel getLblDurante() {
		if (lblDurante == null) {
			lblDurante = new JLabel("Durante");
			lblDurante.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblDurante.setBounds(277, 553, 62, 36);
		}
		return lblDurante;
	}

	private JSpinner getSpinner() {
		if (spinner == null) {
			spinner = new JSpinner();
			spinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
			spinner.setEnabled(false);
			spinner.setBounds(376, 552, 77, 39);
		}
		return spinner;
	}

	private JLabel getLblSemanas() {
		if (lblSemanas == null) {
			lblSemanas = new JLabel("semanas");
			lblSemanas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblSemanas.setBounds(479, 553, 62, 36);
		}
		return lblSemanas;
	}

	private JLabel getLblDaYHora() {
		if (lblDaYHora == null) {
			lblDaYHora = new JLabel("D\u00EDa y hora");
			lblDaYHora.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblDaYHora.setBounds(623, 392, 183, 36);
		}
		return lblDaYHora;
	}

	private JComboBox<String> getComboBoxDias() {
		if (comboBoxDias == null) {
			comboBoxDias = new JComboBox<String>();
			comboBoxDias.setBackground(Color.WHITE);
			comboBoxDias.setFont(new Font("Tahoma", Font.PLAIN, 15));
			comboBoxDias.setModel(new DefaultComboBoxModel<String>(
					new String[] { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" }));
			comboBoxDias.setBounds(710, 437, 199, 41);
		}
		return comboBoxDias;
	}

	private JLabel getLblDa() {
		if (lblDa == null) {
			lblDa = new JLabel("D\u00EDa");
			lblDa.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblDa.setBounds(623, 439, 67, 36);
		}
		return lblDa;
	}

	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setBounds(10, 383, 942, 10);
		}
		return separator;
	}

	private JSeparator getSeparator_1() {
		if (separator_1 == null) {
			separator_1 = new JSeparator();
			separator_1.setBounds(10, 600, 942, 10);
		}
		return separator_1;
	}

	private JSeparator getSeparator_2() {
		if (separator_2 == null) {
			separator_2 = new JSeparator();
			separator_2.setOrientation(SwingConstants.VERTICAL);
			separator_2.setBounds(601, 391, 12, 198);
		}
		return separator_2;
	}

	private JRadioButton getRdbtnIntervaloDeFechas() {
		if (rdbtnIntervaloDeFechas == null) {
			rdbtnIntervaloDeFechas = new JRadioButton("Intervalo de fechas");
			buttonGroup.add(rdbtnIntervaloDeFechas);
			rdbtnIntervaloDeFechas.setSelected(true);
			rdbtnIntervaloDeFechas.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					boolean selected = rdbtnIntervaloDeFechas.isSelected();

					dateChooserFechaInicial.setEnabled(selected);
					dateChooserFechaFinal.setEnabled(selected);
				}
			});
			rdbtnIntervaloDeFechas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			rdbtnIntervaloDeFechas.setBounds(10, 435, 152, 25);
		}
		return rdbtnIntervaloDeFechas;
	}

	private JRadioButton getRdbtnNmeroDeSemanas() {
		if (rdbtnNmeroDeSemanas == null) {
			rdbtnNmeroDeSemanas = new JRadioButton("N\u00FAmero de semanas");
			buttonGroup.add(rdbtnNmeroDeSemanas);
			rdbtnNmeroDeSemanas.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					boolean selected = rdbtnNmeroDeSemanas.isSelected();

					dateChooserFechaDesde.setEnabled(selected);
					spinner.setEnabled(selected);
				}
			});
			rdbtnNmeroDeSemanas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			rdbtnNmeroDeSemanas.setBounds(10, 521, 329, 25);
		}
		return rdbtnNmeroDeSemanas;
	}

	@SuppressWarnings("deprecation")
	protected void crearReserva() {
		int idActividad;
		int idInstalacion;

		Date fechaInicial;
		Date fechaFinal;

		int diaSeleccionado;

		int horaInicial;
		int horaFinal;

		boolean plazasLimitadas;
		int numPlazas;

		// Obtener actividad e instalaci�n
		idActividad = actividades.get(tableActividades.getSelectedRow()).idActividad;
		idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

		// Obtener fechas inicial y final
		if (rdbtnIntervaloDeFechas.isSelected()) {
			fechaInicial = dateChooserFechaInicial.getDate();
			fechaFinal = dateChooserFechaFinal.getDate();
		} else {
			fechaInicial = dateChooserFechaDesde.getDate();
			fechaFinal = calcularFechaFinal(fechaInicial, (Integer) spinner.getValue());
		}

		// Obtener d�a de la semana
		if (comboBoxDias.getSelectedIndex() == 6) {
			diaSeleccionado = 0;
		} else {
			diaSeleccionado = comboBoxDias.getSelectedIndex() + 1;
		}

		// Obtener horas inicial y final
		boolean diaCompleto = chckbxDaCompleto.isSelected();
		if (diaCompleto) {
			horaInicial = 8;
			horaFinal = 23;
		} else {
			horaInicial = Integer.parseInt(spinnerHoraInicial.getValue().toString());
			horaFinal = Integer.parseInt(spinnerHoraFinal.getValue().toString());
		}

		// Obtener tipo y n�mero de plazas
		ObtenerNumPlazasActInstAction onp = new ObtenerNumPlazasActInstAction(recursosActividad, recursosInstalacion);
		numPlazas = onp.execute();

		if (textFieldNumPlazas.getText().length() != 0) {
			numPlazas = Integer.parseInt(textFieldNumPlazas.getText());
		}

		if (numPlazas == 0) {
			plazasLimitadas = false;
		} else {
			plazasLimitadas = true;
		}

		if (!checkHoras(diaCompleto, horaInicial, horaFinal)) {
			JOptionPane.showMessageDialog(this, "La hora inicial no puede ser anterior a la hora final.", "Error hora",
					JOptionPane.INFORMATION_MESSAGE);
		} else if (!checkFechas(fechaInicial, fechaFinal)) {
			JOptionPane.showMessageDialog(this, "La fecha inicial no puede ser anterior a la fecha final.",
					"Error fecha", JOptionPane.INFORMATION_MESSAGE);
		} else {
			List<Date> fechas = new ObtenerFechasAction().execute(idInstalacion, fechaInicial, fechaFinal,
					diaSeleccionado, horaInicial, horaFinal);

			if (fechas.size() == 0) {
				JOptionPane.showMessageDialog(this,
						"No se ha seleccionado ninguna fecha para realizar la actividad. Por favor, seleccione nuevas fechas o un d�a distinto.",
						"Ning�n d�a seleccionado", JOptionPane.INFORMATION_MESSAGE);
			} else {
				List<ConflictoDTO> conflictos = new ComprobarConflictosInstalacionAction(idActividad, idInstalacion,
						fechas, horaInicial, horaFinal).execute();

				if (conflictos.size() != 0) {
					mostrarConflictos(conflictos);
				} else {
					boolean valido = true;
					for (Date f : fechas) {
						int check = checkConflictos(idActividad, idInstalacion, f, horaInicial, horaFinal);

						if (check == 1) {
							String mensaje = "No es posible planificar la actividad debido a que la instalaci�n\nya est� ocupada por otra actividad el d�a "
									+ f.getDay() + " del mes " + f.getMonth() + " del a�o " + (f.getYear() + 1900);
							JOptionPane.showMessageDialog(this, mensaje, "Error al planificar una actividad",
									JOptionPane.ERROR_MESSAGE);
							valido = false;
							break;
						} else if (check == 2) {
							String mensaje = "No es posible planificar la actividad debido a que la instalaci�n\nya est� alquilada por un socio el d�a "
									+ f.getDay() + " del mes " + f.getMonth() + " del a�o " + (f.getYear() + 1900);
							JOptionPane.showMessageDialog(this, mensaje, "Error al planificar una actividad",
									JOptionPane.ERROR_MESSAGE);
							valido = false;
							break;
						}
					}

					if (valido) {
						List<Integer> idsReservas = crearReservas(idActividad, idInstalacion, fechaInicial, fechaFinal,
								diaSeleccionado, horaInicial, horaFinal, plazasLimitadas, numPlazas, fechas);

						new CrearGrupoReservasAction(idsReservas).execute();

						VentanaOperacionCorrecta correcta = new VentanaOperacionCorrecta(this,
								"La actividad ha sido planificada con �xito", "Planificaci�n realizada");
						correcta.setVisible(true);
					}
				}
			}
		}
	}

	private int checkConflictos(int idActividad, int idInstalacion, Date fecha, int horaInicial, int horaFinal) {
		String[] horario = new ObtenerDisponibilidadAction().execute(idInstalacion, fecha, -1);

		int resultado = 0;

		for (int i = horaInicial - 8; i < horaFinal - 8; i++) {
			if (horario[i] == "OCUPADO") {
				resultado = 1;
			} else if (horario[i] == "ALQUILADA") {
				resultado = 2;
			}
		}

		return resultado;
	}

	private void mostrarConflictos(List<ConflictoDTO> conflictos) {
		VentanaConflictos v = new VentanaConflictos(this, conflictos);
		v.setVisible(true);
	}

	private boolean checkFechas(Date fechaInicial, Date fechaFinal) {
		if (fechaFinal.compareTo(fechaInicial) < 0)
			return false;

		return true;
	}

	private boolean checkHoras(boolean diaCompleto, int horaInicial, int horaFinal) {
		if (diaCompleto) {
			if (horaInicial == 8 && horaFinal == 23) {
				return true;
			}
		} else {
			if (horaInicial < horaFinal) {
				return true;
			}
		}
		return false;
	}

	private List<Integer> crearReservas(int idActividad, int idInstalacion, Date fechaInicial, Date fechaFinal,
			int diaSeleccionado, int horaInicial, int horaFinal, boolean plazasLimitadas, int numPlazas,
			List<Date> fechas) {
		List<Integer> idReservas = new ArrayList<Integer>();

		for (Date d : fechas) {
			ReservaDTO reserva = new ReservaDTO();

			reserva.idActividad = idActividad;
			reserva.idInstalacion = idInstalacion;
			reserva.fecha = new java.sql.Date(d.getTime());
			reserva.horaInicial = horaInicial;
			reserva.horaFinal = horaFinal;
			reserva.isPlazasLimitadas = plazasLimitadas;
			reserva.numPlazas = numPlazas;

			int nuevoId = new CrearReservaAction(reserva).execute();
			if (nuevoId != -1) {
				idReservas.add(nuevoId);
			}
		}

		return idReservas;
	}

	private Date calcularFechaFinal(Date fechaInicial, Integer numeroSemanas) {
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(fechaInicial);
		calendar.add(Calendar.WEEK_OF_YEAR, numeroSemanas - 1);
		calendar.add(Calendar.DATE, 6);

		Date fechaFinal = calendar.getTime();

		return fechaFinal;
	}
}