package ui.windows;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import logic.ApuntarSocio;
import logic.dto.MonitorDTO;
import ui.VentanaPrincipal;
import ui.actions.ObtenerMonitoresAction;

public class VentanaAccederComoMonitorConReservasHoy extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblSocios;
	private JScrollPane scrollPane;
	private JTable tableMonitor;
	private JButton btnCancelar;
	private JButton btnSiguiente;
	private List<MonitorDTO> monitores;
	VentanaPrincipal ventanaPrincipal;
	int result = -1;

	public VentanaAccederComoMonitorConReservasHoy(VentanaPrincipal ventanaPrincipal) {
		
		this.ventanaPrincipal = ventanaPrincipal;
		setResizable(false);
		setModal(true);
		setTitle("Acceder como Monitor");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 703, 308);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblSocios());
		contentPane.add(getScrollPane());
		contentPane.add(getBtnCancelar());
		contentPane.add(getBtnSiguiente());
		

		setLocationRelativeTo(ventanaPrincipal);

		mostrarSocios();
	}

	private JLabel getLblSocios() {
		if (lblSocios == null) {
			lblSocios = new JLabel("Socios");
			lblSocios.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblSocios.setBounds(17, 11, 108, 38);
		}
		return lblSocios;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(17, 49, 663, 154);
			scrollPane.setViewportView(getTableSocios());
		}
		return scrollPane;
	}

	private JTable getTableSocios() {
		if (tableMonitor == null) {
			tableMonitor = new JTable();
			tableMonitor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableMonitor.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					btnSiguiente.setEnabled(true);
				}
			});
		}
		return tableMonitor;
	}
	
	private void mostrarSocios() {
		String[] nombreColumnas = { "ID", "NOMBRE"};

		ModeloNoEditable modeloSocios = new ModeloNoEditable(nombreColumnas, 0);

		monitores = new ObtenerMonitoresAction().execute();
		ApuntarSocio a = new ApuntarSocio();
		for(int i = monitores.size()-1; i >= 0; i--) {
			if(a.getReservaMonitor(monitores.get(i).monitorID) == null)
				monitores.remove(i);
		}

		String id, nombre;

		for (MonitorDTO monitor : monitores) {
			id = String.valueOf(monitor.monitorID);
			nombre = monitor.name;
			

			String[] nuevaFila = new String[] { id, nombre};

			modeloSocios.addRow(nuevaFila);
		}

		tableMonitor.setModel(modeloSocios);

		tableMonitor.revalidate();
		tableMonitor.repaint();
	}
	

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.setBounds(17, 227, 195, 38);
		}
		return btnCancelar;
	}

	private JButton getBtnSiguiente() {
		if (btnSiguiente == null) {
			btnSiguiente = new JButton("Siguiente");
			btnSiguiente.setEnabled(false);
			btnSiguiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					seleccionarSocio();
				}
			});
			btnSiguiente.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnSiguiente.setBounds(485, 227, 195, 38);
		}
		return btnSiguiente;
	}

	protected void seleccionarSocio() {
		int idSocio = monitores.get(tableMonitor.getSelectedRow()).monitorID;
		result = idSocio;
		dispose();		
	}
	public int showDialog() {
		setVisible(true);
		return result;
	}
}
