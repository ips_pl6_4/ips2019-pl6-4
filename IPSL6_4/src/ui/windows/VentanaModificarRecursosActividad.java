package ui.windows;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import logic.ModificarActividad;
import logic.dto.ActividadDTO;
import logic.dto.ConflictoModificarRecursoDTO;
import logic.dto.RecursoDTO;
import ui.VentanaPrincipal;

public class VentanaModificarRecursosActividad extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPaneTodosRecursos;
	private JScrollPane scrollPaneRecursosActividad;
	private JScrollPane scrollPaneActividades;
	private JButton btnAadir;
	private JButton btnQuitar;
	private JTable tableTodosRecursos;
	private JTable tableRecursosActividad;
	private JTable tableActividades;
	private ModeloNoEditable modeloRecursos;
	private JScrollPane scrollPane;
	private ModeloNoEditable modeloActividades;
	private ModeloNoEditable modeloRecursosActividades;
	private List<ActividadDTO> actividades;//lista de todas las actividades	
	private List<RecursoDTO> recursos; //Lista de todos los recursos
	private List<RecursoDTO> recursosActividad; //Lista de los recursos de la actividad seleccionada
	private JTextArea textAreaDescripcion;
	private JLabel lblDescripcion;



	/**
	 * Create the frame.
	 */
	public VentanaModificarRecursosActividad(VentanaPrincipal v) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaModificarRecursosActividad.class.getResource("/img/logoIconoSample.jpg")));
		setResizable(false);
		setTitle("Modificar recursos de una actividad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 555, 345);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getScrollPaneTodosRecursos());
		contentPane.add(getScrollPaneRecursosActividad());
		contentPane.add(getScrollPaneActividades());
		contentPane.add(getBtnAadir());
		contentPane.add(getBtnQuitar());
		contentPane.add(getScrollPane());
		contentPane.add(getLblDescripcion());
		
		setLocationRelativeTo(v);
	}
	private JScrollPane getScrollPaneTodosRecursos() {
		if (scrollPaneTodosRecursos == null) {
			scrollPaneTodosRecursos = new JScrollPane();
			scrollPaneTodosRecursos.setBounds(265, 27, 264, 108);
			scrollPaneTodosRecursos.setViewportView(getTableTodosRecursos());
		}
		
		return scrollPaneTodosRecursos;
	}
	private JScrollPane getScrollPaneRecursosActividad() {
		if (scrollPaneRecursosActividad == null) {
			scrollPaneRecursosActividad = new JScrollPane();
			scrollPaneRecursosActividad.setBounds(265, 180, 264, 114);
			scrollPaneRecursosActividad.setViewportView(getTableRecursosActividad());
		}
		return scrollPaneRecursosActividad;
	}
	private JScrollPane getScrollPaneActividades() {
		if (scrollPaneActividades == null) {
			scrollPaneActividades = new JScrollPane();			
			scrollPaneActividades.setBounds(24, 27, 218, 108);
			scrollPaneActividades.setViewportView(getTableActividades());
		}
		return scrollPaneActividades;
	}
	

	private JTable getTableTodosRecursos() {
		if (tableTodosRecursos == null) {
			String[] nombreColumnas = {"NOMBRE" };
			modeloRecursos = new ModeloNoEditable(nombreColumnas, 0);
			tableTodosRecursos = new JTable(modeloRecursos);
			llenarTablaTodosRecursos();
			tableTodosRecursos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		
		return tableTodosRecursos;
	}
	private void llenarTablaTodosRecursos() {
		String[] nombreColumnas = {"NOMBRE"};
		modeloRecursos = new ModeloNoEditable(nombreColumnas, 0);
		recursos = new ModificarActividad().getRecursos();
		for(RecursoDTO theRecurso: recursos) {			
			modeloRecursos.addRow(new String[] {theRecurso.nombre});
		}
		tableTodosRecursos.setModel(modeloRecursos);
		tableTodosRecursos.revalidate();
		tableTodosRecursos.repaint();
		
	}

	private JTable getTableRecursosActividad() {
		if (tableRecursosActividad == null) {
			String[] nombreColumnas = {"NOMBRE"};
			modeloRecursosActividades = new ModeloNoEditable(nombreColumnas, 0);
			tableRecursosActividad = new JTable(modeloRecursosActividades);
			tableRecursosActividad.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableRecursosActividad;
	}
	private void llenarTablaRecursosActividad(int idActividad){
		String[] nombreColumnas = {"NOMBRE"};
		modeloRecursosActividades = new ModeloNoEditable(nombreColumnas,0);
		recursosActividad = new ModificarActividad().getRecursos(idActividad);
		for(RecursoDTO theRecurso: recursosActividad) {
			modeloRecursosActividades.addRow(new String[] {theRecurso.nombre});
		}
		tableRecursosActividad.setModel(modeloRecursosActividades);
		tableRecursosActividad.revalidate();
		tableRecursosActividad.repaint();
	}
	private JTable getTableActividades() {
		if (tableActividades == null) {
			String[] nombreColumnas = {"NOMBRE","INTENSIDAD"};
			modeloActividades = new ModeloNoEditable(nombreColumnas, 0);
			tableActividades = new JTable(modeloActividades);
			tableActividades.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					executeTableActividades();
				}
			});
			llenarTablaActividades();
			tableActividades.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
	
		return tableActividades;
	}
	private void executeTableActividades() {		
		ActividadDTO actividad = actividades.get(tableActividades.getSelectedRow());
		llenarTablaRecursosActividad(actividad.idActividad);
		actualizarDescripcion(actividad.descripcion);
	}
	private void actualizarDescripcion(String descripcion) {
		textAreaDescripcion.setText(descripcion);
		
		
	}
	private void llenarTablaActividades() {
		String[] nombreColumnas = {"NOMBRE","INTENSIDAD"};
		modeloActividades = new ModeloNoEditable(nombreColumnas,0);
		actividades = new ModificarActividad().getActividades();
		for(ActividadDTO theRecurso:actividades) {
			modeloActividades.addRow(new String[] {theRecurso.nombre,String.valueOf(theRecurso.intensidad)});
		}
		tableActividades.setModel(modeloActividades);
		tableActividades.revalidate();
		tableActividades.repaint();
		
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setBounds(24, 180, 218, 114);
			scrollPane.setViewportView(getTextAreaDescripcion());
		}
		return scrollPane;
	}
	private JButton getBtnAadir() {
		if (btnAadir == null) {
			btnAadir = new JButton("A\u00F1adir");
			btnAadir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					executeAdd();
				}
			});
			btnAadir.setBounds(305, 146, 89, 23);
		}
		return btnAadir;
	}
	private void executeAdd() {
		
		
		if(tableActividades.getSelectionModel().isSelectionEmpty()) {
			if(tableTodosRecursos.getSelectionModel().isSelectionEmpty())
				JOptionPane.showMessageDialog(this, "Selecciona una Actividad y un Recurso para a�adir");
			else
				JOptionPane.showMessageDialog(this, "Selecciona una Actividad para a�adir el recurso");
		}else if(tableTodosRecursos.getSelectionModel().isSelectionEmpty())
			JOptionPane.showMessageDialog(this, "Selecciona un Recurso para a�adir a la Actividad");
		else { //Hemos seleccionado una actividad y un recurso, seguimos con las comprobaciones
			ActividadDTO actividad = actividades.get(tableActividades.getSelectedRow());
			RecursoDTO recurso = recursos.get(tableTodosRecursos.getSelectedRow());
			
			if(new ModificarActividad().comprobarRecursoEnActividad(recurso, recursosActividad))
				JOptionPane.showMessageDialog(this, "Ese Recurso ya est� en al actividad");
			else {
				List<ConflictoModificarRecursoDTO> listaConflictos = new ModificarActividad().comprobarRecursosdeReservas(actividad,recurso);
				List<ConflictoModificarRecursoDTO> listaConflictos2 = new ModificarActividad().comprobarReservasSimultaneas(actividad);
				for(ConflictoModificarRecursoDTO theConflicto : listaConflictos2)
					listaConflictos.add(theConflicto);
				if(listaConflictos.isEmpty()) {
					new ModificarActividad().addRecurso(actividad, recurso);
					llenarTablaRecursosActividad(actividad.idActividad);
				}else {//Mostramos los errores
					VentanaConflictosModificarRecursos v = new VentanaConflictosModificarRecursos(this, listaConflictos);
					v.setVisible(true);
				}					
			}
		}
	}
	
	private JButton getBtnQuitar() {
		if (btnQuitar == null) {
			btnQuitar = new JButton("Quitar");
			btnQuitar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					executeRemove();
				}				
			});
			btnQuitar.setBounds(404, 146, 89, 23);
		}
		return btnQuitar;
	}
	private void executeRemove() {
		if(tableActividades.getSelectionModel().isSelectionEmpty()) {
			if(tableRecursosActividad.getSelectionModel().isSelectionEmpty())
				JOptionPane.showMessageDialog(this, "Selecciona una Actividad y un Recurso para quitar");
			else
				JOptionPane.showMessageDialog(this, "Selecciona una Actividad para quitar el recurso");
		}else if(tableRecursosActividad.getSelectionModel().isSelectionEmpty())
			JOptionPane.showMessageDialog(this, "Selecciona un Recurso para quitar de la Actividad");
		else {
			ActividadDTO actividad = actividades.get(tableActividades.getSelectedRow());
			RecursoDTO recurso = recursosActividad.get(tableRecursosActividad.getSelectedRow());
			List<ConflictoModificarRecursoDTO> listaConflictos = new ModificarActividad().comprobarReservasSimultaneas(actividad);
			if(listaConflictos.isEmpty()) {
				new ModificarActividad().removeRecurso(actividad, recurso);
				llenarTablaRecursosActividad(actividad.idActividad);
			}else {
				VentanaConflictosModificarRecursos v = new VentanaConflictosModificarRecursos(this, listaConflictos);
				v.setVisible(true);
			}
			
			
		}
		
	}
	private JTextArea getTextAreaDescripcion() {
		if (textAreaDescripcion == null) {
			textAreaDescripcion = new JTextArea();
			textAreaDescripcion.setEditable(false);
			textAreaDescripcion.setLineWrap(true);
		}
		return textAreaDescripcion;
	}
	private JLabel getLblDescripcion() {
		if (lblDescripcion == null) {
			lblDescripcion = new JLabel("Descripci\u00F3n");
			lblDescripcion.setBounds(24, 165, 64, 14);
		}
		return lblDescripcion;
	}
}
			