package ui.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import logic.dto.ActividadDTO;
import logic.dto.ConflictoDTO;
import logic.dto.HistorialDTO;
import logic.dto.InstalacionDTO;
import logic.dto.RecursoDTO;
import logic.dto.RecursoInstalacionDTO;
import logic.dto.ReservaDTO;
import ui.actions.ComprobarConflictosInstalacionAction;
import ui.actions.ModificarInstalacionReservaAction;
import ui.actions.ObtenerActividadAction;
import ui.actions.ObtenerDisponibilidadAction;
import ui.actions.ObtenerInstalacionAction;
import ui.actions.ObtenerInstalacionesAction;
import ui.actions.ObtenerNumPlazasActInstAction;
import ui.actions.ObtenerRecursosActividadAction;
import ui.actions.ObtenerRecursosInstalacionAction;
import ui.actions.ObtenerReservasProximasAction;
import ui.actions.RegistrarHistorialAction;

public class VentanaModificarInstalacionReservaSinSocios extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JLabel lblActividades;
	private JLabel lblInstalaciones;
	private JLabel lblPlazasLimitadas;
	private JLabel lblRecursosInstalacion;
	private JLabel lblRecursosParaLa;
	private JLabel lblNumplazas;
	private JScrollPane scrollPaneInstalaciones;

	private JTable tableInstalaciones;

	ModeloNoEditable modeloActividadesPlanificadas;
	ModeloNoEditable modeloInstalaciones;
	ModeloNoEditable modeloRecursosActividad;
	ModeloNoEditable modeloRecursosInstalacion;

	private JButton btnCancelar;
	private JButton btnModificar;

	private JScrollPane scrollPaneRecursosActividad;
	private JScrollPane scrollPaneRecursosInstalacion;

	private JTable tableRecursosActividad;
	private JTable tableRecursosInstalacion;

	List<InstalacionDTO> instalaciones;
	List<RecursoDTO> recursosActividad;
	List<RecursoInstalacionDTO> recursosInstalacion;

	private JLabel lblNewLabel;
	private JTextField textFieldNumPlazas;
	private JScrollPane scrollPane;
	private JTable tableActividadesPlanificadas;
	private JLabel lblActividad;
	private JLabel lblIdActividad;
	private JLabel lblNombre;
	private JLabel lblIntensdiad;
	private JLabel labelResultadoIdActividad;
	private JLabel labelResultadoNombreActividad;
	private JLabel labelResultadoIntensidadActividad;
	private JSeparator separator;
	private JLabel lblInstalacion;
	private JLabel lblIdInstalacion;
	private JLabel labelNombreInstalacion;
	private JLabel labelResultadoIdInstalacion;
	private JLabel labelResultadoNombreInstalacion;
	private JSeparator separator_1;

	private ModeloNoEditable modeloReservas;

	private List<ReservaDTO> reservas;

	public VentanaModificarInstalacionReservaSinSocios(JFrame ventanaPrincipal) {
		setResizable(false);
		setTitle("Modificar instalacion de una reserva sin socios apuntados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 820, 751);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblActividades());
		contentPane.add(getLblInstalaciones());
		contentPane.add(getScrollPaneInstalaciones());
		contentPane.add(getLblPlazasLimitadas());
		contentPane.add(getBtnCancelar());
		contentPane.add(getBtnModificar());
		contentPane.add(getLblRecursosParaLa());
		contentPane.add(getLblRecursosInstalacion());
		contentPane.add(getScrollPaneRecursosActividad());
		contentPane.add(getScrollPaneRecursosInstalacion());
		contentPane.add(getLblNumplazas());
		contentPane.add(getLblNewLabel());
		contentPane.add(getTextFieldNumPlazas());
		contentPane.add(getScrollPane());
		contentPane.add(getLblActividad());
		contentPane.add(getLblIdActividad());
		contentPane.add(getLblNombre());
		contentPane.add(getLblIntensdiad());
		contentPane.add(getLabelResultadoIdActividad());
		contentPane.add(getLabelResultadoNombreActividad());
		contentPane.add(getLabelResultadoIntensidadActividad());
		contentPane.add(getSeparator());
		contentPane.add(getLblInstalacion());
		contentPane.add(getLblIdInstalacion());
		contentPane.add(getLabelNombreInstalacion());
		contentPane.add(getLabelResultadoIdInstalacion());
		contentPane.add(getLabelResultadoNombreInstalacion());
		contentPane.add(getSeparator_1());

		setLocationRelativeTo(ventanaPrincipal);

		mostrarReservas();
		mostrarInstalaciones();
	}

	private JLabel getLblActividades() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Actividades planificadas");
			lblActividades.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblActividades.setBounds(10, 11, 183, 36);
		}
		return lblActividades;
	}

	private JLabel getLblInstalaciones() {
		if (lblInstalaciones == null) {
			lblInstalaciones = new JLabel("Instalaciones");
			lblInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblInstalaciones.setBounds(12, 390, 183, 36);
		}
		return lblInstalaciones;
	}

	private JScrollPane getScrollPaneInstalaciones() {
		if (scrollPaneInstalaciones == null) {
			scrollPaneInstalaciones = new JScrollPane();
			scrollPaneInstalaciones.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPaneInstalaciones.setBounds(12, 427, 501, 129);
			scrollPaneInstalaciones.setViewportView(getTableInstalaciones());
		}
		return scrollPaneInstalaciones;
	}

	private JTable getTableInstalaciones() {
		if (tableInstalaciones == null) {
			String[] nombreColumnas = { "ID", "NOMBRE" };
			modeloInstalaciones = new ModeloNoEditable(nombreColumnas, 0);
			tableInstalaciones = new JTable(modeloInstalaciones);
			tableInstalaciones.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;
					mostrarRecursosInstalacion(idInstalacion);

					seleccionarActividadInstalacion();
				}
			});
			tableInstalaciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableInstalaciones;
	}

	private JLabel getLblPlazasLimitadas() {
		if (lblPlazasLimitadas == null) {
			lblPlazasLimitadas = new JLabel("Plazas");
			lblPlazasLimitadas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblPlazasLimitadas.setBounds(10, 600, 183, 36);
		}
		return lblPlazasLimitadas;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnCancelar.setBounds(8, 675, 152, 36);
		}
		return btnCancelar;
	}

	private JButton getBtnModificar() {
		if (btnModificar == null) {
			btnModificar = new JButton("Modificar");
			btnModificar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					modificarReserva();
				}
			});
			btnModificar.setEnabled(false);
			btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnModificar.setBounds(624, 675, 182, 36);
		}
		return btnModificar;
	}

	private JLabel getLblRecursosParaLa() {
		if (lblRecursosParaLa == null) {
			lblRecursosParaLa = new JLabel("Recursos para la actividad");
			lblRecursosParaLa.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblRecursosParaLa.setBounds(537, 11, 230, 27);
		}
		return lblRecursosParaLa;
	}

	private JScrollPane getScrollPaneRecursosActividad() {
		if (scrollPaneRecursosActividad == null) {
			scrollPaneRecursosActividad = new JScrollPane();
			scrollPaneRecursosActividad.setBounds(537, 45, 269, 129);
			scrollPaneRecursosActividad.setViewportView(getTableRecursosActividad());
		}
		return scrollPaneRecursosActividad;
	}

	private JLabel getLblRecursosInstalacion() {
		if (lblRecursosInstalacion == null) {
			lblRecursosInstalacion = new JLabel("Recursos en la instalaci\u00F3n");
			lblRecursosInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblRecursosInstalacion.setBounds(543, 390, 224, 27);
		}
		return lblRecursosInstalacion;
	}

	private JScrollPane getScrollPaneRecursosInstalacion() {
		if (scrollPaneRecursosInstalacion == null) {
			scrollPaneRecursosInstalacion = new JScrollPane();
			scrollPaneRecursosInstalacion.setBounds(539, 427, 269, 129);
			scrollPaneRecursosInstalacion.setViewportView(getTableRecursosInstalacion());
		}
		return scrollPaneRecursosInstalacion;
	}

	private JTable getTableRecursosActividad() {
		if (tableRecursosActividad == null) {
			tableRecursosActividad = new JTable();
		}
		return tableRecursosActividad;
	}

	private JTable getTableRecursosInstalacion() {
		if (tableRecursosInstalacion == null) {
			tableRecursosInstalacion = new JTable();
		}
		return tableRecursosInstalacion;
	}

	private JLabel getLblNumplazas() {
		if (lblNumplazas == null) {
			lblNumplazas = new JLabel("");
			lblNumplazas.setBorder(new LineBorder(new Color(0, 0, 0)));
			lblNumplazas.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNumplazas.setHorizontalAlignment(SwingConstants.CENTER);
			lblNumplazas.setBounds(151, 600, 275, 36);
		}
		return lblNumplazas;
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("N\u00FAmero de plazas");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel.setBounds(500, 599, 217, 39);
		}
		return lblNewLabel;
	}

	private JTextField getTextFieldNumPlazas() {
		if (textFieldNumPlazas == null) {
			textFieldNumPlazas = new JTextField();
			textFieldNumPlazas.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					char caracter = e.getKeyChar();
					if (textFieldNumPlazas.getText().length() == 0 && caracter == '0') {
						e.consume();
					} else if (caracter > '9' || caracter < '0') {
						e.consume();
					}
				}

				@Override
				public void keyReleased(KeyEvent arg0) {
					if (textFieldNumPlazas.getText().length() != 0) {
						lblNumplazas.setText("PLAZAS LIMITADAS: " + textFieldNumPlazas.getText() + " PLAZAS");
					} else {
						lblNumplazas.setText("PLAZAS SIN LIMITE");
					}
				}
			});
			textFieldNumPlazas.setHorizontalAlignment(SwingConstants.CENTER);
			textFieldNumPlazas.setFont(new Font("Tahoma", Font.PLAIN, 14));
			textFieldNumPlazas.setBounds(624, 600, 184, 36);
			textFieldNumPlazas.setColumns(10);
		}
		return textFieldNumPlazas;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 45, 501, 129);
			scrollPane.setViewportView(getTableActividadesPlanificadas());
		}
		return scrollPane;
	}

	private JTable getTableActividadesPlanificadas() {
		if (tableActividadesPlanificadas == null) {
			tableActividadesPlanificadas = new JTable();
			tableActividadesPlanificadas.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					int idActividad = reservas.get(tableActividadesPlanificadas.getSelectedRow()).idActividad;
					int idInstalacion = reservas.get(tableActividadesPlanificadas.getSelectedRow()).idInstalacion;

					mostrarRecursosActividad(idActividad);
					mostrarActividad(idActividad);
					mostrarInstalacion(idInstalacion);

					seleccionarActividadInstalacion();
				}
			});
		}
		return tableActividadesPlanificadas;
	}

	private JLabel getLblActividad() {
		if (lblActividad == null) {
			lblActividad = new JLabel("Actividad");
			lblActividad.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblActividad.setBounds(20, 183, 217, 36);
		}
		return lblActividad;
	}

	private JLabel getLblIdActividad() {
		if (lblIdActividad == null) {
			lblIdActividad = new JLabel("ID");
			lblIdActividad.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblIdActividad.setBounds(40, 230, 122, 36);
		}
		return lblIdActividad;
	}

	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre");
			lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNombre.setBounds(40, 277, 122, 36);
		}
		return lblNombre;
	}

	private JLabel getLblIntensdiad() {
		if (lblIntensdiad == null) {
			lblIntensdiad = new JLabel("Intensidad");
			lblIntensdiad.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblIntensdiad.setBounds(40, 324, 122, 36);
		}
		return lblIntensdiad;
	}

	private JLabel getLabelResultadoIdActividad() {
		if (labelResultadoIdActividad == null) {
			labelResultadoIdActividad = new JLabel("");
			labelResultadoIdActividad.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoIdActividad.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoIdActividad.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoIdActividad.setBounds(124, 230, 217, 36);
		}
		return labelResultadoIdActividad;
	}

	private JLabel getLabelResultadoNombreActividad() {
		if (labelResultadoNombreActividad == null) {
			labelResultadoNombreActividad = new JLabel("");
			labelResultadoNombreActividad.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoNombreActividad.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoNombreActividad.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoNombreActividad.setBounds(124, 277, 217, 36);
		}
		return labelResultadoNombreActividad;
	}

	private JLabel getLabelResultadoIntensidadActividad() {
		if (labelResultadoIntensidadActividad == null) {
			labelResultadoIntensidadActividad = new JLabel("");
			labelResultadoIntensidadActividad.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoIntensidadActividad.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoIntensidadActividad.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoIntensidadActividad.setBounds(124, 324, 217, 36);
		}
		return labelResultadoIntensidadActividad;
	}

	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setOrientation(SwingConstants.VERTICAL);
			separator.setBounds(401, 185, 12, 182);
		}
		return separator;
	}

	private JLabel getLblInstalacion() {
		if (lblInstalacion == null) {
			lblInstalacion = new JLabel("Instalaci\u00F3n");
			lblInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblInstalacion.setBounds(436, 183, 217, 36);
		}
		return lblInstalacion;
	}

	private JLabel getLblIdInstalacion() {
		if (lblIdInstalacion == null) {
			lblIdInstalacion = new JLabel("ID");
			lblIdInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblIdInstalacion.setBounds(462, 230, 65, 36);
		}
		return lblIdInstalacion;
	}

	private JLabel getLabelNombreInstalacion() {
		if (labelNombreInstalacion == null) {
			labelNombreInstalacion = new JLabel("Nombre");
			labelNombreInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			labelNombreInstalacion.setBounds(462, 277, 122, 36);
		}
		return labelNombreInstalacion;
	}

	private JLabel getLabelResultadoIdInstalacion() {
		if (labelResultadoIdInstalacion == null) {
			labelResultadoIdInstalacion = new JLabel("");
			labelResultadoIdInstalacion.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoIdInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoIdInstalacion.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoIdInstalacion.setBounds(539, 230, 217, 36);
		}
		return labelResultadoIdInstalacion;
	}

	private JLabel getLabelResultadoNombreInstalacion() {
		if (labelResultadoNombreInstalacion == null) {
			labelResultadoNombreInstalacion = new JLabel("");
			labelResultadoNombreInstalacion.setHorizontalAlignment(SwingConstants.CENTER);
			labelResultadoNombreInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 16));
			labelResultadoNombreInstalacion.setBorder(new LineBorder(new Color(0, 0, 0)));
			labelResultadoNombreInstalacion.setBounds(539, 277, 217, 36);
		}
		return labelResultadoNombreInstalacion;
	}

	private JSeparator getSeparator_1() {
		if (separator_1 == null) {
			separator_1 = new JSeparator();
			separator_1.setBounds(10, 383, 794, 17);
		}
		return separator_1;
	}

	private void mostrarReservas() {
		crearModeloReservas();

		String id, fecha, horaInicio, horaFin, plazasLimitadas, numPlazas;

		reservas = new ObtenerReservasProximasAction().execute();

		for (ReservaDTO r : reservas) {
			id = String.valueOf(r.idReserva);
			fecha = String.valueOf(r.fecha);
			horaInicio = String.valueOf(r.horaInicial);
			horaFin = String.valueOf(r.horaFinal);

			if (r.isPlazasLimitadas) {
				plazasLimitadas = "CON L�MITE";
			} else {
				plazasLimitadas = "SIN L�MITE";
			}

			if (!r.isPlazasLimitadas) {
				numPlazas = "ILIMITADAS";
			} else {
				numPlazas = String.valueOf(r.numPlazas);
			}

			String[] nuevaFila = new String[] { id, fecha, horaInicio, horaFin, plazasLimitadas, numPlazas };

			modeloReservas.addRow(nuevaFila);
		}

		tableActividadesPlanificadas.setModel(modeloReservas);

		tableActividadesPlanificadas.revalidate();
		tableActividadesPlanificadas.repaint();
	}

	private void mostrarActividad(int idActividad) {
		ActividadDTO a = new ObtenerActividadAction(idActividad).execute();

		labelResultadoIdActividad.setText(String.valueOf(a.idActividad));
		labelResultadoNombreActividad.setText(a.nombre);
		labelResultadoIntensidadActividad.setText(String.valueOf(a.intensidad));

		mostrarRecursosActividad(idActividad);
	}

	private void mostrarRecursosActividad(int idActividad) {
		crearModeloRecursosActividad();

		String idRecurso, nombre;

		recursosActividad = new ObtenerRecursosActividadAction(idActividad).execute();

		for (RecursoDTO r : recursosActividad) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;

			String[] nuevaFila = new String[] { idRecurso, nombre };

			modeloRecursosActividad.addRow(nuevaFila);
		}

		tableRecursosActividad.setModel(modeloRecursosActividad);

		tableRecursosActividad.revalidate();
		tableRecursosActividad.repaint();
	}

	private void mostrarInstalacion(int idInstalacion) {
		InstalacionDTO i = new ObtenerInstalacionAction(idInstalacion).execute();

		labelResultadoIdInstalacion.setText(String.valueOf(i.idInstalacion));
		labelResultadoNombreInstalacion.setText(i.nombre);
	}

	private void mostrarRecursosInstalacion(int idInstalacion) {
		crearModeloRecursosInstalacion();

		String idRecurso, nombre, cantidad;

		recursosInstalacion = new ObtenerRecursosInstalacionAction(idInstalacion).execute();

		for (RecursoInstalacionDTO r : recursosInstalacion) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;
			cantidad = String.valueOf(r.cantidad);

			String[] nuevaFila = new String[] { idRecurso, nombre, cantidad };

			modeloRecursosInstalacion.addRow(nuevaFila);
		}

		tableRecursosInstalacion.setModel(modeloRecursosInstalacion);

		tableRecursosInstalacion.revalidate();
		tableRecursosInstalacion.repaint();
	}

	private void crearModeloRecursosInstalacion() {
		String[] nombreColumnas = { "ID", "NOMBRE", "CANTIDAD" };
		modeloRecursosInstalacion = new ModeloNoEditable(nombreColumnas, 0);
	}

	private void crearModeloReservas() {
		String[] nombreColumnas = { "ID", "FECHA", "HORA INICIO", "HORA FIN", "PLAZAS LIMITADAS", "N� PLAZAS" };

		modeloReservas = new ModeloNoEditable(nombreColumnas, 0);
	}

	private void crearModeloRecursosActividad() {
		String[] nombreColumnas = { "ID", "NOMBRE" };
		modeloRecursosActividad = new ModeloNoEditable(nombreColumnas, 0);
	}

	private void mostrarInstalaciones() {
		instalaciones = new ObtenerInstalacionesAction().execute();

		String id, nombre;

		for (InstalacionDTO instalacion : instalaciones) {
			id = String.valueOf(instalacion.idInstalacion);
			nombre = instalacion.nombre;

			String[] nuevaFila = new String[] { id, nombre };

			modeloInstalaciones.addRow(nuevaFila);
		}

		tableInstalaciones.setModel(modeloInstalaciones);

		tableInstalaciones.revalidate();
		tableInstalaciones.repaint();
	}

	private void seleccionarActividadInstalacion() {
		int filaReserva = tableActividadesPlanificadas.getSelectedRow();
		int filaInstalacion = tableInstalaciones.getSelectedRow();

		if (filaReserva != -1 && filaInstalacion != -1) {
			int numPlazas = new ObtenerNumPlazasActInstAction(recursosActividad, recursosInstalacion).execute();

			// Sin plazas
			if (numPlazas == -1) {
				btnModificar.setEnabled(false);
				lblNumplazas.setText("NO HAY RECURSOS SUFICIENTES");

				textFieldNumPlazas.setText("");
				textFieldNumPlazas.setEnabled(false);
				textFieldNumPlazas.setEditable(false);
			}
			// Plazas infinitas
			else if (numPlazas == 0) {
				btnModificar.setEnabled(true);
				lblNumplazas.setText("PLAZAS SIN LIMITE");

				textFieldNumPlazas.setText("");
				textFieldNumPlazas.setEnabled(true);
				textFieldNumPlazas.setEditable(true);
			}
			// Plazas limitadas
			else {
				btnModificar.setEnabled(true);
				lblNumplazas.setText("PLAZAS LIMITADAS: " + numPlazas + " PLAZAS");

				textFieldNumPlazas.setText(String.valueOf(numPlazas));
				textFieldNumPlazas.setEnabled(true);
				textFieldNumPlazas.setEditable(false);
			}
		}
	}

	private void modificarReserva() {
		int filaReserva = tableActividadesPlanificadas.getSelectedRow();
		int filaInstalacion = tableInstalaciones.getSelectedRow();

		ReservaDTO reserva = reservas.get(filaReserva);
		InstalacionDTO instalacion = instalaciones.get(filaInstalacion);

		int numPlazas = new ObtenerNumPlazasActInstAction(recursosActividad, recursosInstalacion).execute();

		if (textFieldNumPlazas.getText().length() != 0) {
			numPlazas = Integer.parseInt(textFieldNumPlazas.getText());
		}

		boolean plazasLimitadas;

		if (numPlazas == 0) {
			plazasLimitadas = false;
		} else {
			plazasLimitadas = true;
		}

		int horaInicial = reserva.horaInicial;
		int horaFinal = reserva.horaFinal;
		int idInstalacion = instalacion.idInstalacion;
		int idActividad = reserva.idActividad;

		List<Date> fechas = new ArrayList<Date>();
		fechas.add(reserva.fecha);

		List<ConflictoDTO> conflictos = new ComprobarConflictosInstalacionAction(idActividad, idInstalacion, fechas,
				horaInicial, horaFinal).execute();

		if (conflictos.size() != 0) {
			mostrarConflictos(conflictos);
		} else {
			int check = checkConflictos(idActividad, idInstalacion, reserva.fecha, horaInicial, horaFinal);

			if (check == 1) {
				String mensaje = "No es posible planificar la actividad debido a que la instalaci�n ya est� alquilada por un socio en el horario seleccionado.";
				JOptionPane.showMessageDialog(this, mensaje, "Error al planificar una actividad",
						JOptionPane.ERROR_MESSAGE);
			} else {

				int idReserva = reserva.idReserva;

				new ModificarInstalacionReservaAction(idReserva, idInstalacion, plazasLimitadas, numPlazas).execute();

				HistorialDTO h = new HistorialDTO();

				h.idReserva = idReserva;
				h.idInstalacionAnterior = reserva.idInstalacion;
				h.horaInicialAnterior = reserva.horaInicial;
				h.horaFinalAnterior = reserva.horaFinal;
				h.fechaAnterior = reserva.fecha;
				h.isPlazasLimitadasAnterior = reserva.isPlazasLimitadas;
				h.numPlazasAnterior = reserva.numPlazas;
				h.estado = "MODIFICAR INSTALACION";

				new RegistrarHistorialAction(h).execute();

				VentanaOperacionCorrecta correcta = new VentanaOperacionCorrecta(this,
						"La instalaci�n de la actividad ha sido modificada con �xito", "Modificaci�n realizada");
				correcta.setVisible(true);
			}
		}
	}

	private int checkConflictos(int idActividad, int idInstalacion, Date fecha, int horaInicial, int horaFinal) {
		String[] horario = new ObtenerDisponibilidadAction().execute(idInstalacion, fecha, -1);

		int resultado = 0;

		for (int i = horaInicial - 8; i < horaFinal - 8; i++) {
			if (horario[i] == "ALQUILADA") {
				resultado = 1;
			}
		}

		return resultado;
	}

	private void mostrarConflictos(List<ConflictoDTO> conflictos) {
		VentanaConflictos v = new VentanaConflictos(this, conflictos);
		v.setVisible(true);
	}
}
