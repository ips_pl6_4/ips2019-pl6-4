package ui.windows;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class VentanaOperacionCorrecta extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JFrame ventana;
	private String mensaje;
	private JLabel lblMensaje;
	private JButton btnAceptar;

	public VentanaOperacionCorrecta(JFrame ventana, String mensaje, String titulo) {
		setModal(true);
		setResizable(false);
		this.ventana = ventana;
		this.mensaje = mensaje;

		setTitle(titulo);

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 423, 169);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblMensaje());
		contentPane.add(getBtnAceptar());

		setLocationRelativeTo(ventana);
	}

	private JLabel getLblMensaje() {
		if (lblMensaje == null) {
			lblMensaje = new JLabel(mensaje);
			lblMensaje.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
			lblMensaje.setBounds(0, 11, 417, 66);
		}
		return lblMensaje;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton("Aceptar");
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
					ventana.dispose();
				}
			});
			btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAceptar.setBounds(134, 88, 149, 41);
		}
		return btnAceptar;
	}
}
