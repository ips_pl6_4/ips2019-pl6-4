package ui.windows;


import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import logic.RegistrarPago;
import logic.dto.AlquilerDTO;
import logic.dto.InstalacionDTO;
import logic.dto.SocioDTO;
import logic.service.socio.SocioServicesFactory;
import ui.VentanaPrincipal;
import ui.windows.util.ComboBoxItemSocio;

public class VentanaRegistrarPago extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnPagoMetalico;
	private JButton btnPagoCuota;
	private JComboBox<ComboBoxItemSocio> comboBoxListaSocio;
	private JScrollPane scrollPane;
	private JTable table;
	private ModeloNoEditable modeloAlquiler;
	private List<InstalacionDTO> instalaciones;
	private List<AlquilerDTO>  alquileres;



	/**
	 * Create the frame.
	 */
	public VentanaRegistrarPago(VentanaPrincipal vp) {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaRegistrarPago.class.getResource("/img/logoIconoSample.jpg")));
		setTitle("Registrar pago");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getBtnPagoMetalico());
		contentPane.add(getBtnPagoCuota());
		contentPane.add(getComboBoxListaSocio());
		contentPane.add(getScrollPane());
		
		llenarTable();
		setLocationRelativeTo(vp);
	}
	private JButton getBtnPagoMetalico() {
		if (btnPagoMetalico == null) {
			btnPagoMetalico = new JButton("Pago Met\u00E1lico");
			btnPagoMetalico.setBounds(393, 105, 117, 23);
		}
		return btnPagoMetalico;
	}
	private JButton getBtnPagoCuota() {
		if (btnPagoCuota == null) {
			btnPagoCuota = new JButton("Pago Cuota");
			btnPagoCuota.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					executePagoCuota();
				}				
			});
			btnPagoCuota.setBounds(393, 139, 117, 23);
		}
		return btnPagoCuota;
	}
	
	private void executePagoCuota() {
		if(!table.getSelectionModel().isSelectionEmpty()) {
			int idAlquiler = alquileres.get(table.getSelectedRow()).idReservaSocio;
			if(new RegistrarPago().pagoCuota(idAlquiler) == 1) {
				JOptionPane.showMessageDialog(this,"Operación realizada con exito");
				llenarTable();
			}
				
		}else
			JOptionPane.showMessageDialog(this,"Selecciona un alquiler");
		
		
		
		
	}
	private void refreshComboBoxSocios() {
		List<SocioDTO> socios = SocioServicesFactory.getSocios();
		DefaultComboBoxModel<ComboBoxItemSocio> comboSocios = new DefaultComboBoxModel<ComboBoxItemSocio>();
		for (SocioDTO socio : socios)
			comboSocios.addElement(new ComboBoxItemSocio(socio.name + " (ID: " + socio.socioID + ")", socio));
		
		
		comboBoxListaSocio.setVisible(false);
		comboBoxListaSocio.setModel(comboSocios);
		comboBoxListaSocio.setVisible(true);

		
	}

	private JComboBox<ComboBoxItemSocio> getComboBoxListaSocio() {
		if (comboBoxListaSocio == null) {
			comboBoxListaSocio = new JComboBox<ComboBoxItemSocio>();
			comboBoxListaSocio.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {					
					llenarTable();
				}
			
			});
			comboBoxListaSocio.setBounds(345, 59, 212, 20);
			refreshComboBoxSocios();
		}
		return comboBoxListaSocio;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 24, 291, 227);
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}
	private JTable getTable() {
		if (table == null) {
			String[] nombreColumnas = {"INSTALACION","HORA","PRECIO"};
			modeloAlquiler = new ModeloNoEditable(nombreColumnas, 0);
			
			table = new JTable(modeloAlquiler);
		}
		return table;
	}
	private void llenarTable() {
		String[] nombreColumnas = {"INSTALACION","HORA","PRECIO"};
		modeloAlquiler = new ModeloNoEditable(nombreColumnas, 0);
		SocioDTO socio = ((ComboBoxItemSocio)comboBoxListaSocio.getSelectedItem()).getValue();
		instalaciones = new RegistrarPago().getInstalaciones();
		alquileres =  new RegistrarPago().getAlquileres(socio.socioID);
		for(AlquilerDTO theAlquiler:alquileres) {
			InstalacionDTO instalacion = null;
			for(InstalacionDTO theInstalacion: instalaciones)
				if(theInstalacion.idInstalacion == theAlquiler.idInstalacion)
					instalacion = theInstalacion;			
			modeloAlquiler.addRow(new String[] {instalacion.nombre,String.valueOf(theAlquiler.horaInicial),String.valueOf(instalacion.precioHora)});
		}
		table.setModel(modeloAlquiler);
		table.revalidate();
		table.repaint();
		
		
	}
}
