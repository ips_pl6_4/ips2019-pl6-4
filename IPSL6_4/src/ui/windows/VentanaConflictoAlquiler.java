package ui.windows;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.dto.AlquilerDTO;

public class VentanaConflictoAlquiler extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JLabel lblNoSeHan;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton btnAceptar;

	private AlquilerDTO alquiler;

	public VentanaConflictoAlquiler(JFrame ventana, AlquilerDTO alquiler) {
		this.alquiler = alquiler;

		setModal(true);
		setResizable(false);

		setTitle("Conflictos generados");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 401, 412);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNoSeHan());
		contentPane.add(getBtnAceptar());
		contentPane.add(getScrollPane());

		setLocationRelativeTo(ventana);
	}

	private JLabel getLblNoSeHan() {
		if (lblNoSeHan == null) {
			lblNoSeHan = new JLabel("La instalaci�n ya est� alquilada en ese horario");
			lblNoSeHan.setHorizontalAlignment(SwingConstants.CENTER);
			lblNoSeHan.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNoSeHan.setBounds(7, 0, 381, 67);
		}
		return lblNoSeHan;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 66, 371, 239);
			scrollPane.setViewportView(getTextArea());
		}
		return scrollPane;
	}

	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			textArea.setLocation(13, 0);
			textArea.setEditable(false);
			textArea.setFont(new Font("Tahoma", Font.PLAIN, 14));
			textArea.setWrapStyleWord(true);
			textArea.setLineWrap(true);
			textArea.setText(alquiler.toString());
		}
		return textArea;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton("Aceptar");
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					dispose();
				}
			});
			btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAceptar.setBounds(123, 331, 149, 41);
		}
		return btnAceptar;
	}
}
