package ui.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import logic.dto.ActividadDTO;
import logic.dto.AsistenciaDTO;
import logic.dto.HistorialDTO;
import logic.dto.ReservaDTO;
import logic.dto.SocioDTO;
import logic.service.socio.SocioServicesFactory;
import ui.VentanaPrincipal;
import ui.windows.util.ComboBoxItemSocio;

/**
 * Clase ReservasActividadesUI
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 23.11.2019
 */
public class ReservasActividadesUI extends JFrame {

	private static final long serialVersionUID = 6911491950648918625L;

	private JPanel contentPane;
	private JPanel panelLists;
	private JLabel lblActividades;
	private JComboBox<ComboBoxItemSocio> comboBoxSocios;
	private JScrollPane scrollPane;
	private JTable tableAsistencias;

	private DefaultTableModel tableModel;
	private String[] headers = { "Nombre", "ID actividad", "ID reserva", "Hora de inicio", "Hora de finalización",
			"Fecha", "Estado" };
	private JPanel panelButtons;
	private JButton btnHistorial;

	public ReservasActividadesUI(JFrame ventana) {
		setTitle("Centro deportivo: Listado de las reservas a actividades");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/logoIconoSample.jpg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 569, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPanelLists(), BorderLayout.NORTH);
		contentPane.add(getScrollPane(), BorderLayout.CENTER);
		contentPane.add(getPanelButtons(), BorderLayout.SOUTH);

		setLocationRelativeTo(ventana);
	}

	private JPanel getPanelLists() {
		if (panelLists == null) {
			panelLists = new JPanel();
			panelLists.setBackground(Color.WHITE);
			panelLists.setLayout(new GridLayout(0, 2, 0, 0));
			panelLists.add(getLblSocios());
			panelLists.add(getComboBoxSocios());
		}
		return panelLists;
	}

	private JLabel getLblSocios() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Selecciónese socio:");
			lblActividades.setHorizontalAlignment(SwingConstants.CENTER);
			lblActividades.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
		}
		return lblActividades;
	}

	private JComboBox<ComboBoxItemSocio> getComboBoxSocios() {
		if (comboBoxSocios == null) {
			refreshComboBoxSocios();
			comboBoxSocios.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					refreshTableAsistencias();
				}
			});
		}
		return comboBoxSocios;
	}

	private void refreshComboBoxSocios() {
		List<SocioDTO> socios = SocioServicesFactory.getSocios();
		DefaultComboBoxModel<ComboBoxItemSocio> comboSocios = new DefaultComboBoxModel<ComboBoxItemSocio>();
		for (SocioDTO socio : socios)
			comboSocios.addElement(new ComboBoxItemSocio(socio.name + " (ID: " + socio.socioID + ")", socio));

		if (comboBoxSocios == null)
			comboBoxSocios = new JComboBox<ComboBoxItemSocio>();

		comboBoxSocios.setVisible(false);
		comboBoxSocios.setModel(comboSocios);
		comboBoxSocios.setVisible(true);

		refreshTableAsistencias();
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTableAsistencias());
		}
		return scrollPane;
	}

	private JTable getTableAsistencias() {
		if (tableAsistencias == null) {
			createTableModel();
			this.tableAsistencias = new JTable(this.tableModel);
			this.tableAsistencias.getTableHeader().setReorderingAllowed(false);
			refreshTableAsistencias();
		}
		return tableAsistencias;
	}

	@SuppressWarnings("deprecation")
	private void refreshTableAsistencias() {
		if (this.tableModel == null)
			createTableModel();

		for (int i = 0; i < this.tableModel.getRowCount(); i++)
			this.tableModel.removeRow(i);

		Object[] fila = new Object[headers.length];
		SocioDTO socio = ((ComboBoxItemSocio) getComboBoxSocios().getSelectedItem()).getValue();
		List<AsistenciaDTO> asistencias = SocioServicesFactory.getObtenerAsistencias(socio.socioID);
		for (AsistenciaDTO asistencia : asistencias) {
			ReservaDTO reserva = SocioServicesFactory.getReservaCorrespondiente(asistencia);
			List<HistorialDTO> historial = SocioServicesFactory.getObtenerHistorialesPorReserva(asistencia.reservaID);
			String estado = (reserva == null) ? ((historial.size() > 0) ? "Cancelada" : "Inexistente")
					: ((historial.size() > 0) ? "Modificada" : "Normal");

			if (estado.equals("Cancelada")) {
				HistorialDTO ultimoHistorial = historial.get(0);
				Iterator<HistorialDTO> iteratorHistorial = historial.iterator();
				while (iteratorHistorial.hasNext()) {
					HistorialDTO historialIterado = iteratorHistorial.next();
					if (historialIterado.fechaAnterior.after(ultimoHistorial.fechaAnterior))
						ultimoHistorial = historialIterado;
				}

				if (ultimoHistorial.fechaAnterior.after(new Date())
						|| ultimoHistorial.fechaAnterior.getDate() == (new Date().getDate())) {
					ActividadDTO actividad = SocioServicesFactory.getActividadCorrespondiente(reserva);

					fila[0] = actividad.nombre;
					fila[1] = actividad.idActividad;
					fila[2] = ultimoHistorial.idReserva;
					fila[3] = ultimoHistorial.horaInicialAnterior;
					fila[4] = ultimoHistorial.horaFinalAnterior;
					fila[5] = ultimoHistorial.fechaAnterior;
					fila[6] = estado;

					this.tableModel.addRow(fila);
				}
			} else {
				if (reserva.fecha.after(new Date()) || reserva.fecha.getDate() == (new Date().getDate())) {
					ActividadDTO actividad = SocioServicesFactory.getActividadCorrespondiente(reserva);

					fila[0] = actividad.nombre;
					fila[1] = actividad.idActividad;
					fila[2] = reserva.idReserva;
					fila[3] = reserva.horaInicial;
					fila[4] = reserva.horaFinal;
					fila[5] = reserva.fecha;
					fila[6] = estado;

					this.tableModel.addRow(fila);
				}
			}
		}
		getScrollPane().revalidate();
		getScrollPane().repaint();
	}

	private void createTableModel() {
		this.tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
	}

	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			panelButtons = new JPanel();
			panelButtons.setBackground(Color.WHITE);
			panelButtons.add(getBtnHistorial());
		}
		return panelButtons;
	}

	private JButton getBtnHistorial() {
		if (btnHistorial == null) {
			btnHistorial = new JButton("Historial");
			btnHistorial.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnHistorial();
				}
			});
			btnHistorial.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
		}
		return btnHistorial;
	}

	private void btnHistorial() {
		AsistenciasHistorialFrameUI frame = new AsistenciasHistorialFrameUI(this, this.tableAsistencias);
		frame.setVisible(true);
	}
}