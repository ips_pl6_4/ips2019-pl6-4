package ui.windows;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import logic.EliminaAlquiler;
import logic.dto.AlquilerDTO;
import ui.actions.ObtenerAlquileresAction;

public class SocioEliminaAlquilerSocio extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JTable table;
	private JLabel label;
	private JButton btCancelarAlquiler;
	private JButton btCancelar;

	private ModeloNoEditable modeloAlquileres;
	private List<AlquilerDTO> alquileres;
	private ObtenerAlquileresAction oaa;
	private EliminaAlquiler e;
	private int socioID;

	/**
	 * Create the frame.
	 */
	public SocioEliminaAlquilerSocio(VentanaSociosConReservas v, int socioID) {
		oaa = new ObtenerAlquileresAction();
		this.socioID = socioID;
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(SocioEliminaAlquilerSocio.class.getResource("/img/logoIconoSample.jpg")));
		setTitle("Centro de actividades: Eliminar Alquiler");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 602, 460);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getScrollPane());
		contentPane.add(getLabel());
		contentPane.add(getBtCancelarAlquiler());
		contentPane.add(getBtCancelar());
		actualizaTabla();
		setLocationRelativeTo(v);
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(72, 56, 422, 277);
			scrollPane.setViewportView(getTable());
		}
		return scrollPane;
	}

	private JTable getTable() {
		if (table == null) {
			String[] s = { "" };
			modeloAlquileres = new ModeloNoEditable(s, 0);
			table = new JTable(modeloAlquileres);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return table;
	}

	private void actualizaTabla() {
		alquileres = oaa.execute(socioID);
		String[] fechas = new String[alquileres.size() + 1];
		fechas[0] = "horario";
		for (int i = 1; i < alquileres.size() + 1; i++) {
			fechas[i] = alquileres.get(i - 1).fecha.toString();
		}
		modeloAlquileres = new ModeloNoEditable(fechas, 0);
		mostrarAlquileres();

	}

	private void mostrarAlquileres() {
		if (alquileres.size() == 0) {
			getTable().setModel(new DefaultTableModel());
			table.revalidate();
			table.repaint();

		} else {
			String[] fila;
			for (int i = 8; i < 23; i++) {
				fila = new String[alquileres.size() + 1];
				fila[0] = "" + i + "-" + (i + 1);
				for (int j = 1; j < alquileres.size() + 1; j++) {
					if (alquileres.get(j - 1).horaInicial <= i && alquileres.get(j - 1).horaFinal - 1 >= i) {
						fila[j] = "OCUPADO";
					} else {
						fila[j] = "LIBRE";
						
					}
				}
				modeloAlquileres.addRow(fila);
			}

			table.setModel(modeloAlquileres);

			table.revalidate();
			table.repaint();
		}
	}

	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("Alquileres");
			label.setFont(new Font("Tahoma", Font.PLAIN, 17));
			label.setBounds(72, 11, 88, 48);
		}
		return label;
	}

	private JButton getBtCancelarAlquiler() {
		if (btCancelarAlquiler == null) {
			btCancelarAlquiler = new JButton("Cancelar Alquiler");
			btCancelarAlquiler.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					cancelaAlquiler();
				}
			});
			btCancelarAlquiler.setBounds(118, 366, 299, 30);
		}
		return btCancelarAlquiler;
	}

	private JButton getBtCancelar() {
		if (btCancelar == null) {
			btCancelar = new JButton("Cancelar");
			btCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cerrarVentana();
				}
			});
			btCancelar.setBounds(489, 397, 97, 23);
		}
		return btCancelar;
	}

	protected void cancelaAlquiler() {
		try {
			int fila = table.getSelectedRow();
			int columna = table.getSelectedColumn();
			if (fila == -1 || columna == -1) {
				throw new IllegalArgumentException("No hay ningun alquiler ocupado seleccionado");
			} else {
				String fecha = table.getColumnName(columna);
				String horario = (String) table.getValueAt(fila, columna);
				AlquilerDTO a = encuentraAlquiler(fecha, horario);
				if (a != null) {
					eliminarAlquiler(a);
					actualizaTabla();
					JOptionPane.showMessageDialog(this, "El alquiler de la fecha " + a.fecha + " ha sido eliminado",
							"Operaci�n correcta", JOptionPane.INFORMATION_MESSAGE);
				} else {
					throw new IllegalArgumentException("Esa hora no est� ocupada");
				}
			}

		} catch (Exception e) {
			muestraMensajeError(e.getMessage());
		}

	}

	private AlquilerDTO encuentraAlquiler(String fecha, String horario) {
		for (AlquilerDTO a : alquileres) {
			if (a.fecha.toString().equals(fecha)) {
				if (horario.equals("OCUPADO")) {
					if (a.fechaPago == null) {
						return a;
					}
					return null;
				}
			}
		}
		return null;
	}

	private void eliminarAlquiler(AlquilerDTO a) {
		if (e == null) {
			e = new EliminaAlquiler(a);
		}

		e.eliminaAlquiler();
	}

	protected void cerrarVentana() {
		this.dispose();

	}

	protected void muestraMensajeError(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);

	}
}
