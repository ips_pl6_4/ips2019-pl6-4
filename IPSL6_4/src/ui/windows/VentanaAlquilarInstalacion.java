package ui.windows;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;

import logic.dto.InstalacionDTO;
import logic.dto.RecursoInstalacionDTO;
import ui.actions.CrearReservaSocioAction;
import ui.actions.ObtenerDisponibilidadAction;
import ui.actions.ObtenerInstalacionesAction;
import ui.actions.ObtenerRecursosInstalacionAction;

public class VentanaAlquilarInstalacion extends JFrame {

	private String[] horas = { "08:00 a 08:59", "09:00 a 09:59", "10:00 a 10:59", "11:00 a 11:59", "12:00 a 12:59",
			"13:00 a 13:59", "14:00 a 14:59", "15:00 a 15:59", "16:00 a 16:59", "17:00 a 17:59", "18:00 a 18:59",
			"19:00 a 19:59", "20:00 a 20:59", "21:00 a 21:59", "22:00 a 22:59" };

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JFrame ventanaSeleccionarSocio;

	private JLabel lblInstalaciones;
	private JScrollPane scrollPane;
	private JTable tableInstalaciones;

	ModeloNoEditable modeloInstalaciones;
	private JDateChooser selectorFecha;

	private List<InstalacionDTO> instalaciones;
	private JScrollPane scrollPane_1;
	private JLabel lblRecursosInstalacion;
	private JTable tableRecursosInstalacion;

	ModeloNoEditable modeloRecursosInstalacion;
	List<RecursoInstalacionDTO> recursosInstalacion;
	private JLabel lblHorarioInstalacin;
	private JScrollPane scrollPane_2;

	private ObtenerDisponibilidadAction od;
	private JPanel panelHorario;
	private JButton btnCancelar;
	private JButton btnVerHorario;
	private JButton btnAlquilar;

	private int idSocio;
	private JLabel lblFecha;
	private JLabel lblHoraInicio;
	private JLabel lblHoraFin;
	private JSpinner spinnerHoraInicio;
	private JSpinner spinnerHoraFin;

	private int[] horasDisponibles;

	private Date fecha;
	private JCheckBox chckbxDaCompleto;

	public VentanaAlquilarInstalacion(JFrame ventana, int idSocio) {
		setResizable(false);
		od = new ObtenerDisponibilidadAction();

		setTitle("Alquilar instalaci\u00F3n");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 762, 637);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblInstalaciones());
		contentPane.add(getScrollPane());
		contentPane.add(getScrollPane_1());
		contentPane.add(getLblRecursosInstalacion());
		contentPane.add(getSelectorFecha());
		contentPane.add(getLblHorarioInstalacin());
		contentPane.add(getScrollPane_2());
		contentPane.add(getBtnCancelar());
		contentPane.add(getBtnVerHorario());
		contentPane.add(getBtnAlquilar());
		contentPane.add(getLblFecha());
		contentPane.add(getLblHoraInicio());
		contentPane.add(getLblHoraFin());
		contentPane.add(getSpinnerHoraInicio());
		contentPane.add(getSpinnerHoraFin());
		contentPane.add(getChckbxDaCompleto());

		setLocationRelativeTo(ventana);

		this.ventanaSeleccionarSocio = ventana;
		this.idSocio = idSocio;

		mostrarInstalaciones();
	}

	private JLabel getLblInstalaciones() {
		if (lblInstalaciones == null) {
			lblInstalaciones = new JLabel("Instalaciones");
			lblInstalaciones.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblInstalaciones.setBounds(20, 11, 185, 35);
		}
		return lblInstalaciones;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(20, 53, 343, 199);
			scrollPane.setViewportView(getTableInstalaciones());
		}
		return scrollPane;
	}

	private JTable getTableInstalaciones() {
		if (tableInstalaciones == null) {
			tableInstalaciones = new JTable();
			tableInstalaciones.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					mostrarRecursosInstalacion();
					mostrarHorarioInstalacion();
					btnAlquilar.setEnabled(true);
				}
			});
			tableInstalaciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return tableInstalaciones;
	}

	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(20, 327, 343, 134);
			scrollPane_1.setViewportView(getTableRecursosInstalacion());
		}
		return scrollPane_1;
	}

	private JLabel getLblRecursosInstalacion() {
		if (lblRecursosInstalacion == null) {
			lblRecursosInstalacion = new JLabel("Recursos instalacion");
			lblRecursosInstalacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRecursosInstalacion.setBounds(20, 285, 205, 35);
		}
		return lblRecursosInstalacion;
	}

	private JTable getTableRecursosInstalacion() {
		if (tableRecursosInstalacion == null) {
			tableRecursosInstalacion = new JTable();
		}
		return tableRecursosInstalacion;
	}

	private void mostrarInstalaciones() {
		String[] nombreColumnas = { "ID", "NOMBRE", "PRECIO HORA" };
		modeloInstalaciones = new ModeloNoEditable(nombreColumnas, 0);

		instalaciones = new ObtenerInstalacionesAction().execute();

		String id, nombre, precioHora;

		for (InstalacionDTO instalacion : instalaciones) {
			id = String.valueOf(instalacion.idInstalacion);
			nombre = instalacion.nombre;
			precioHora = String.valueOf(instalacion.precioHora);

			String[] nuevaFila = new String[] { id, nombre, precioHora };

			modeloInstalaciones.addRow(nuevaFila);
		}

		tableInstalaciones.setModel(modeloInstalaciones);

		tableInstalaciones.revalidate();
		tableInstalaciones.repaint();
	}

	private void mostrarRecursosInstalacion() {
		String[] nombreColumnas = { "ID", "NOMBRE", "CANTIDAD" };
		modeloRecursosInstalacion = new ModeloNoEditable(nombreColumnas, 0);

		int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

		String idRecurso, nombre, cantidad;

		recursosInstalacion = new ObtenerRecursosInstalacionAction(idInstalacion).execute();

		for (RecursoInstalacionDTO r : recursosInstalacion) {
			idRecurso = String.valueOf(r.idRecurso);
			nombre = r.nombre;
			cantidad = String.valueOf(r.cantidad);

			String[] nuevaFila = new String[] { idRecurso, nombre, cantidad };

			modeloRecursosInstalacion.addRow(nuevaFila);
		}

		tableRecursosInstalacion.setModel(modeloRecursosInstalacion);

		tableRecursosInstalacion.revalidate();
		tableRecursosInstalacion.repaint();
	}

	private JDateChooser getSelectorFecha() {
		if (selectorFecha == null) {
			selectorFecha = new JDateChooser();
			selectorFecha.getCalendarButton().setLocation(107, 420);
			selectorFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));

			Calendar calendar = Calendar.getInstance();

			Date fechaActual = new Date();

			calendar.setTime(fechaActual);
			calendar.add(Calendar.DATE, 1);

			Date fechaInicial = calendar.getTime();

			calendar.add(Calendar.DATE, 14);

			Date fechaFinal = calendar.getTime();

			selectorFecha.setMinSelectableDate(fechaInicial);
			selectorFecha.setMaxSelectableDate(fechaFinal);
			selectorFecha.setDate(fechaInicial);

			selectorFecha.setBounds(74, 493, 128, 36);
		}
		return selectorFecha;
	}

	private JLabel getLblHorarioInstalacin() {
		if (lblHorarioInstalacin == null) {
			lblHorarioInstalacin = new JLabel("Horario Instalaci\u00F3n");
			lblHorarioInstalacin.setHorizontalAlignment(SwingConstants.CENTER);
			lblHorarioInstalacin.setFont(new Font("Tahoma", Font.PLAIN, 15));
			lblHorarioInstalacin.setBounds(441, 11, 263, 35);
		}
		return lblHorarioInstalacin;
	}

	private JScrollPane getScrollPane_2() {
		if (scrollPane_2 == null) {
			scrollPane_2 = new JScrollPane();
			scrollPane_2.setBounds(405, 53, 333, 408);
			scrollPane_2.setViewportView(getPanelHorario());
		}
		return scrollPane_2;
	}

	private JPanel getPanelHorario() {
		if (panelHorario == null) {
			panelHorario = new JPanel();
			panelHorario.setLayout(new GridLayout(15, 0, 0, 0));
		}
		return panelHorario;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton("Cancelar");
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cancelar();
				}
			});
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnCancelar.setBounds(20, 556, 185, 36);
		}
		return btnCancelar;
	}

	protected void cancelar() {
		dispose();
		ventanaSeleccionarSocio.setLocationRelativeTo(this);
		ventanaSeleccionarSocio.setVisible(true);
	}

	private JButton getBtnVerHorario() {
		if (btnVerHorario == null) {
			btnVerHorario = new JButton("Ver horario");
			btnVerHorario.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (tableInstalaciones.getSelectedRow() != -1)
						mostrarHorarioInstalacion();
				}
			});
			btnVerHorario.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnVerHorario.setBounds(224, 494, 128, 35);
		}
		return btnVerHorario;
	}

	private JButton getBtnAlquilar() {
		if (btnAlquilar == null) {
			btnAlquilar = new JButton("Alquilar");
			btnAlquilar.setEnabled(false);
			btnAlquilar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					alquilar();
				}
			});
			btnAlquilar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnAlquilar.setBounds(553, 555, 185, 39);
		}
		return btnAlquilar;
	}

	private JLabel getLblFecha() {
		if (lblFecha == null) {
			lblFecha = new JLabel("Fecha");
			lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblFecha.setBounds(20, 494, 44, 35);
		}
		return lblFecha;
	}

	private JLabel getLblHoraInicio() {
		if (lblHoraInicio == null) {
			lblHoraInicio = new JLabel("Hora inicio");
			lblHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraInicio.setBounds(548, 472, 62, 17);
		}
		return lblHoraInicio;
	}

	private JLabel getLblHoraFin() {
		if (lblHoraFin == null) {
			lblHoraFin = new JLabel("Hora fin");
			lblHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblHoraFin.setBounds(639, 472, 47, 17);
		}
		return lblHoraFin;
	}

	private JSpinner getSpinnerHoraInicio() {
		if (spinnerHoraInicio == null) {
			spinnerHoraInicio = new JSpinner();
			spinnerHoraInicio.setEnabled(false);
			spinnerHoraInicio.setModel(new SpinnerNumberModel(8, 8, 22, 1));
			spinnerHoraInicio.setBounds(557, 493, 44, 36);
		}
		return spinnerHoraInicio;
	}

	private JSpinner getSpinnerHoraFin() {
		if (spinnerHoraFin == null) {
			spinnerHoraFin = new JSpinner();
			spinnerHoraFin.setEnabled(false);
			spinnerHoraFin.setModel(new SpinnerNumberModel(9, 9, 23, 1));
			spinnerHoraFin.setBounds(640, 493, 44, 36);
		}
		return spinnerHoraFin;
	}

	private void mostrarHorarioInstalacion() {
		int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

		fecha = selectorFecha.getDate();

		String[] horarioInstalacion = od.execute(idInstalacion, fecha, idSocio);

		horasDisponibles = new int[15];

		panelHorario.removeAll();

		for (int i = 0; i < horas.length; i++) {
			String estado = horarioInstalacion[i];

			JButton nuevoBoton = new JButton(horas[i] + " --- " + estado);
			nuevoBoton.setForeground(Color.BLACK);

			if (estado == "OCUPADO") {
				nuevoBoton.setBackground(new Color(205, 92, 92));
				horasDisponibles[i] = 0;
			} else if (estado == "OTRA ACTIVIDAD") {
				nuevoBoton.setBackground(new Color(240, 230, 140));
				horasDisponibles[i] = 1;
			} else if (estado == "ALQUILADA") {
				nuevoBoton.setBackground(new Color(152, 251, 152));
				horasDisponibles[i] = 2;
			} else {
				nuevoBoton.setBackground(new Color(175, 238, 238));
				horasDisponibles[i] = 3;
			}

			panelHorario.add(nuevoBoton);
			nuevoBoton.setVisible(true);
		}

		panelHorario.revalidate();
		panelHorario.repaint();
	}

	private boolean comprobarHorasValidas() {
		int horaInicio;
		int horaFin;

		if (chckbxDaCompleto.isSelected()) {
			horaInicio = 8;
			horaFin = 9;
		} else {
			horaInicio = (int) spinnerHoraInicio.getValue();
			horaFin = (int) spinnerHoraFin.getValue();
		}

		horaInicio -= 8;
		horaFin -= 8;

		boolean correcto = true;

		if (horaInicio >= horaFin) {
			correcto = false;
			JOptionPane.showMessageDialog(this, "La hora inicial no puede ser anterior a la hora final.", "Error hora",
					JOptionPane.ERROR_MESSAGE);
		} else {
			for (int i = horaInicio; i < horaFin; i++) {
				int tipo = horasDisponibles[i];

				if (tipo >= 0 && tipo <= 2) {
					correcto = false;
					mostrarError(tipo);
					break;
				}
			}
		}

		return correcto;
	}

	private void mostrarError(int tipo) {
		if (tipo == 0) {
			errorActividadCentro();
		} else if (tipo == 1) {
			errorPlazaReservada();
		} else if (tipo == 2) {
			errorAlquiler();
		}
	}

	private void errorActividadCentro() {
		String mensaje = "No es posible alquilar la instalación porque esta se encuentra ocupada por una actividad del centro en el horario que ha seleccionado.";
		JOptionPane.showMessageDialog(this, mensaje, "Error al alquilar una instalación", JOptionPane.ERROR_MESSAGE);
	}

	private void errorPlazaReservada() {
		String mensaje = "No es posible alquilar la instalación porque usted ya tiene reservada una plaza en una actividad del centro en el horario seleccionado.";
		JOptionPane.showMessageDialog(this, mensaje, "Error al alquilar una instalación", JOptionPane.ERROR_MESSAGE);
	}

	private void errorAlquiler() {
		String mensaje = "No es posible alquilar la instalación porque esta ya ha sido alquilada en el horario seleccionado.";
		JOptionPane.showMessageDialog(this, mensaje, "Error al alquilar una instalación", JOptionPane.ERROR_MESSAGE);
	}

	private void alquilar() {
		boolean datosCorrectos = comprobarHorasValidas();

		if (datosCorrectos) {
			int horaInicio;
			int horaFin;

			int idInstalacion = instalaciones.get(tableInstalaciones.getSelectedRow()).idInstalacion;

			if (chckbxDaCompleto.isSelected()) {
				horaInicio = 8;
				horaFin = 23;
			} else {
				horaInicio = (int) spinnerHoraInicio.getValue();
				horaFin = (int) spinnerHoraFin.getValue();
			}

			new CrearReservaSocioAction(idSocio, idInstalacion, horaInicio, horaFin, fecha).execute();

			VentanaOperacionCorrecta correcta = new VentanaOperacionCorrecta(this,
					"El alquiler ha sido realizado con exito", "Alquiler realizada");
			correcta.setVisible(true);
		}
	}

	private JCheckBox getChckbxDaCompleto() {
		if (chckbxDaCompleto == null) {
			chckbxDaCompleto = new JCheckBox("D\u00EDa completo");
			chckbxDaCompleto.setSelected(true);
			chckbxDaCompleto.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					seleccionarDiaCompleto();
				}
			});
			chckbxDaCompleto.setFont(new Font("Tahoma", Font.PLAIN, 14));
			chckbxDaCompleto.setBounds(405, 500, 115, 23);
		}
		return chckbxDaCompleto;
	}

	protected void seleccionarDiaCompleto() {
		if (chckbxDaCompleto.isSelected()) {
			spinnerHoraInicio.setEnabled(false);
			spinnerHoraFin.setEnabled(false);
		} else {
			spinnerHoraInicio.setEnabled(true);
			spinnerHoraFin.setEnabled(true);
		}
	}
}
