package ui.windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import logic.dto.ActividadDTO;
import logic.dto.AsistenciaDTO;
import logic.dto.ReservaDTO;
import logic.dto.SocioDTO;
import logic.service.admin.AdminServicesFactory;
import logic.service.monitor.MonitorServicesFactory;
import ui.VentanaPrincipal;
import ui.windows.util.ComboBoxItemReserva;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Clase RegistrarAsistenciaUI
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public class RegistrarAsistenciaUI extends JFrame {

	private static final long serialVersionUID = 6911491950648918625L;

	private JPanel contentPane;
	private JPanel panelLists;
	private JLabel lblActividades;
	private JComboBox<ComboBoxItemReserva> comboBoxActividades;
	private JPanel panelButtons;
	private JTextPane textPaneAsistencias;
	private JButton btnRegistrarAsistencia;
	private JScrollPane scrollPaneConsole;
	private JPanel panelCentral;
	private JScrollPane scrollPaneTable;
	private JTable tableSocios;

	private DefaultTableModel tableModel;
	private String[] headers = { "ID", "Nombre", "Apellidos", "Email", "Tel�fono" };

	public RegistrarAsistenciaUI(JFrame ventana) {
		setTitle("Centro deportivo: Registrar asistencia");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/logoIconoSample.jpg")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 592, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPanelLists(), BorderLayout.NORTH);
		contentPane.add(getPanelButtons(), BorderLayout.SOUTH);
		contentPane.add(getPanelCentral(), BorderLayout.CENTER);

		setLocationRelativeTo(ventana);
	}

	private JPanel getPanelLists() {
		if (panelLists == null) {
			panelLists = new JPanel();
			panelLists.setBackground(Color.WHITE);
			panelLists.setLayout(new GridLayout(0, 2, 0, 0));
			panelLists.add(getLblActividades());
			panelLists.add(getComboBoxActividades());
		}
		return panelLists;
	}

	private JLabel getLblActividades() {
		if (lblActividades == null) {
			lblActividades = new JLabel("Seleccione la reserva de una actividad:");
			lblActividades.setHorizontalAlignment(SwingConstants.CENTER);
			lblActividades.setFont(new Font("Arial Narrow", Font.PLAIN, 14));
		}
		return lblActividades;
	}

	private JComboBox<ComboBoxItemReserva> getComboBoxActividades() {
		if (comboBoxActividades == null) {
			comboBoxActividades = new JComboBox<ComboBoxItemReserva>();
			comboBoxActividades.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
					refreshTableSocios();
				}
			});
			refreshComboBoxActividades();
		}
		return comboBoxActividades;
	}

	private void refreshComboBoxActividades() {
		if (comboBoxActividades != null)
			comboBoxActividades.removeAll();
		List<ReservaDTO> reservas = MonitorServicesFactory.getReservasDisponibles();
		DefaultComboBoxModel<ComboBoxItemReserva> comboReservas = new DefaultComboBoxModel<ComboBoxItemReserva>();
		for (ReservaDTO reserva : reservas) {
			ActividadDTO actividad = (reserva == null) ? null
					: AdminServicesFactory.getActividadCorrespondiente(reserva);
			String nombreActividad = (actividad == null) ? "" : actividad.nombre;
			comboReservas.addElement(new ComboBoxItemReserva(reserva, nombreActividad));
		}

		if (comboBoxActividades == null)
			comboBoxActividades = new JComboBox<ComboBoxItemReserva>();

		comboBoxActividades.setVisible(false);
		comboBoxActividades.setModel(comboReservas);
		comboBoxActividades.setVisible(true);

		refreshTableSocios();
	}

	private void refreshTableSocios() {
		if (comboBoxActividades.getSelectedItem() != null) {
			if (this.tableModel == null)
				createTableModel();

			this.tableModel.setRowCount(0);

			Object[] fila = null;
			List<SocioDTO> noAsistentes = MonitorServicesFactory
					.getSociosSinAsistencia(((ComboBoxItemReserva) comboBoxActividades.getSelectedItem()).getValue());
			for (SocioDTO socio : noAsistentes) {
				fila = new Object[headers.length];

				fila[0] = socio.socioID;
				fila[1] = socio.name;
				fila[2] = socio.apellidos;
				fila[3] = socio.mail;
				fila[4] = socio.telefono;

				this.tableModel.addRow(fila);
			}
		}
	}

	private void createTableModel() {
		this.tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
	}

	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			panelButtons = new JPanel();
			panelButtons.setBackground(Color.WHITE);
			panelButtons.add(getBtnRegistrarAsistencia());
		}
		return panelButtons;
	}

	private JTextPane getTextPaneAsignaciones() {
		if (textPaneAsistencias == null) {
			textPaneAsistencias = new JTextPane();
			textPaneAsistencias.setEditable(false);
		}
		return textPaneAsistencias;
	}

	private void appendLineAsistencias(String text) {
		textPaneAsistencias.setText(textPaneAsistencias.getText() + text + "\n");
	}

	private JButton getBtnRegistrarAsistencia() {
		if (btnRegistrarAsistencia == null) {
			btnRegistrarAsistencia = new JButton("Registrar asistencia");
			btnRegistrarAsistencia.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnRegistrarAsistenciaAction();
				}
			});
		}
		return btnRegistrarAsistencia;
	}

	private void btnRegistrarAsistenciaAction() {
		if (comboBoxActividades.getSelectedItem() != null) {
			int[] selectedRows = this.tableSocios.getSelectedRows();
			if (selectedRows.length == 0)
				appendLineAsistencias("No hay socios seleccionados.");
			for (int j = 0; j < selectedRows.length; j++) {
				int i = selectedRows[j];
				if (this.tableModel.getRowCount() != 0) {
					if (i < this.tableModel.getRowCount()) {
						SocioDTO socio = MonitorServicesFactory
								.getSocioPorID((int) tableSocios.getModel().getValueAt(i, 0));
						ReservaDTO reserva = ((ComboBoxItemReserva) comboBoxActividades.getSelectedItem()).getValue();
						AsistenciaDTO asistencia = new AsistenciaDTO();
						asistencia.reservaID = reserva.idReserva;
						asistencia.socioID = socio.socioID;
						MonitorServicesFactory.setRegistrarAsistencia(asistencia);
						refreshTableSocios();
						appendLineAsistencias("Registrada la asistencia del socio '" + socio.name + "' (ID: "
								+ socio.socioID + ") a la actividad con ID: " + reserva.idReserva + ".");
					} else
						appendLineAsistencias("" + i);
				}
				for (int k = j + 1; k < selectedRows.length; k++)
					selectedRows[k]--;
			}
		} else
			appendLineAsistencias("Por favor, seleccione un socio y una actividad adecuados.");
	}

	private JScrollPane getScrollPaneConsole() {
		if (scrollPaneConsole == null) {
			scrollPaneConsole = new JScrollPane();
			scrollPaneConsole.setViewportView(getTextPaneAsignaciones());

		}
		return scrollPaneConsole;
	}

	private JPanel getPanelCentral() {
		if (panelCentral == null) {
			panelCentral = new JPanel();
			panelCentral.setLayout(new GridLayout(1, 2, 0, 0));
			panelCentral.add(getScrollPaneTable());
			panelCentral.add(getScrollPaneConsole());
		}
		return panelCentral;
	}

	private JScrollPane getScrollPaneTable() {
		if (scrollPaneTable == null) {
			scrollPaneTable = new JScrollPane();
			scrollPaneTable.setViewportView(getTableSocios());
		}
		return scrollPaneTable;
	}

	private JTable getTableSocios() {
		if (tableSocios == null) {
			createTableModel();
			this.tableSocios = new JTable(this.tableModel);
			this.tableSocios.getTableHeader().setReorderingAllowed(false);
			refreshTableSocios();
		}
		return tableSocios;
	}
}
