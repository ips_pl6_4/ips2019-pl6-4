package ui.windows;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import ui.VentanaPrincipal;
/**
 * Ventana que muestra un textArea con una lista
 * @author Ana Garcia
 *11-10-2019
 */
public class VentanaLista extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3L;
	private JPanel contentPane;
	private JTextArea textArea;


	/**
	 * Create the frame.
	 */
	public VentanaLista(VentanaPrincipal vRO, String mensaje) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaLista.class.getResource("/img/logoIconoSample.jpg")));
		setResizable(false);
		setTitle("Centro deportes: Lista");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 620, 384);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{51, 511, 0};
		gbl_contentPane.rowHeights = new int[]{41, 280, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 1;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		textArea = new JTextArea();
		panel.add(textArea);
		textArea.setEditable(false);
		textArea.setText(mensaje);
		textArea.setColumns(10);
		setLocationRelativeTo(vRO);//O relativo a cualquier otra ventana que est� antes
		setVisible(true);
	}
}
