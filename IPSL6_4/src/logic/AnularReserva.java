package logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import logic.util.container.Reserva;
import persistence.AnularReservaPersistencia;
import persistence.ReservarPlazaConLimitePersistencia;

public class AnularReserva {

	public void execute(int idSocio, int idReserva) {
		ReservarPlazaConLimitePersistencia perAux = new ReservarPlazaConLimitePersistencia();
		AnularReservaPersistencia per = new AnularReservaPersistencia();
		Reserva reserva = perAux.getReserva(idReserva);// la reserva de la cual intentamos anular reserva

		// �Comprobar si es de plazas limitadas?

		if (!perAux.isLimitado(idReserva)) {
			throw new RuntimeException("No es de plazas limitadas");
		}

		// tiene que ser antes de la actividad
		Calendar c = Calendar.getInstance();
		int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11
		String sMes;
		if (mes < 10)
			sMes = "0" + mes;
		else
			sMes = "" + mes;
		String fechaActual = c.get(Calendar.YEAR) + "-" + sMes + "-" + c.get(Calendar.DATE);
		String fechaReserva = reserva.getFecha().toString();
//
//		if (Integer.parseInt(fechaActual.substring(0, 4)) <= Integer.parseInt(fechaReserva.substring(0, 4)))
//			if (Integer.parseInt(fechaActual.substring(5, 7)) <= Integer.parseInt(fechaReserva.substring(5, 7)))
//				if (Integer.parseInt(fechaActual.substring(8)) <= Integer.parseInt(fechaReserva.substring(8)))
//					if (c.get(Calendar.HOUR_OF_DAY) >= reserva.getHoraInicio())
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date fechaReservaDate = sdf.parse(fechaActual);
			java.util.Date fechaActualDate = sdf.parse(fechaReserva);
			if(fechaActualDate.before(fechaReservaDate))
				throw new RuntimeException("Solo puedes anular reservas antes de su realizaci�n");
			else {
				if(fechaActualDate.equals(fechaReservaDate))
					if(reserva.getHoraInicio() < c.get(Calendar.HOUR_OF_DAY))
						throw new RuntimeException("Solo puedes anular reservas antes de su realizaci�n");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
						
						//

		// La bd no est� preparada para este codigo
//		Date fechaActual = new Date(System.currentTimeMillis());		
//		if(fechaActual.after(reserva.getFecha()))
//			throw new RuntimeException("Solo puedes anular reservas antes de su realizaci�n");

		// anular
		per.anularReserva(idSocio, idReserva);

	}

}
