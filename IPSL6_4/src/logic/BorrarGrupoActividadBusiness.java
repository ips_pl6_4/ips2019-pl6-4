package logic;

import java.sql.Connection;
import java.sql.SQLException;

import persistence.gateway.ReservaGateway;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class BorrarGrupoActividadBusiness {

	private int idReserva;

	public BorrarGrupoActividadBusiness(int idReserva) {
		this.idReserva = idReserva;
	}

	public void execute() {
		Connection c = null;

		try {
			c = Jdbc.getConnection();

			ReservaGateway r = new ReservaGatewayImpl();
			r.setConnection(c);

			r.deleteGrupoReserva(idReserva);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
