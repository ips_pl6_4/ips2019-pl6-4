package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.RecursoInstalacionDTO;
import persistence.gateway.RecursoInstalacionGateway;
import persistence.impl.RecursoInstalacionGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerRecursosInstalacionBusiness {
	private int idInstalacion;

	public ObtenerRecursosInstalacionBusiness(int idInstalacion) {
		this.idInstalacion = idInstalacion;
	}

	public List<RecursoInstalacionDTO> execute() {
		List<RecursoInstalacionDTO> lista = null;

		Connection c = null;
		try {
			c = Jdbc.getConnection();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}

		RecursoInstalacionGateway pr = new RecursoInstalacionGatewayImpl();
		pr.setConnection(c);

		lista = pr.findRecursoByInstalacion(idInstalacion);

		return lista;
	}

}
