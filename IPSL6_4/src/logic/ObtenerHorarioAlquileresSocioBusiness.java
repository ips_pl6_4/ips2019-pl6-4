package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import logic.dto.AlquilerDTO;
import persistence.gateway.AlquilerGateway;
import persistence.impl.AlquilerGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerHorarioAlquileresSocioBusiness {

	private Date fecha;
	private int idInstalacion;

	public ObtenerHorarioAlquileresSocioBusiness(int idInstalacion, Date fecha) {
		this.idInstalacion = idInstalacion;
		this.fecha = fecha;
	}

	public String[] execute() {
		String[] horas = new String[15];

		for (int i = 0; i < horas.length; i++) {
			horas[i] = "LIBRE";
		}

		Connection c = null;

		try {
			c = Jdbc.getConnection();

			AlquilerGateway ag = new AlquilerGatewayImpl();
			ag.setConnection(c);

			Calendar calendar = Calendar.getInstance();

			calendar.setTime(fecha);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);

			Date desde = calendar.getTime();

			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);

			Date hasta = calendar.getTime();

			List<AlquilerDTO> alquileres = ag.findAlquileresFecha(desde, hasta, idInstalacion);

			int horaIni, horaFin;

			for (int i = 0; i < horas.length; i++) {
				horaIni = i + 8;
				horaFin = horaIni + 1;

				for (AlquilerDTO a : alquileres) {
					if (a.horaInicial <= horaIni && a.horaFinal >= horaFin) {
						horas[i] = "ALQUILADA";
						break;
					}
				}
			}

		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}

		return horas;
	}

}
