package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import logic.dto.ConflictoDTO;
import logic.dto.ReservaDTO;
import persistence.gateway.ActividadGateway;
import persistence.gateway.ReservaGateway;
import persistence.impl.ActividadGatewayImpl;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class ComprobarConflictoInstalacionBusiness {

	private int idInstalacion;
	private Date fecha;
	private int horaInicial;
	private int horaFinal;
	private int idActividad;

	public ComprobarConflictoInstalacionBusiness(int idInstalacion, Date fecha, int horaInicial, int horaFinal,
			int idActividad) {
		this.idInstalacion = idInstalacion;
		this.fecha = fecha;
		this.horaInicial = horaInicial;
		this.horaFinal = horaFinal;
		this.idActividad = idActividad;
	}

	public List<ConflictoDTO> execute() {
		List<ConflictoDTO> conflictos = new ArrayList<ConflictoDTO>();
		Connection c = null;

		try {
			c = Jdbc.getConnection();
			c.setAutoCommit(false);

			ReservaGateway r = new ReservaGatewayImpl();
			r.setConnection(c);

			Date desde = obtenerFechaFormateada();

			Calendar calendar = Calendar.getInstance();

			calendar.setTime(desde);
			calendar.add(Calendar.DATE, 1);

			Date hasta = calendar.getTime();

			List<ReservaDTO> reservas = r.findReservasInstalacionFecha(idInstalacion, desde, hasta, horaInicial,
					horaFinal);

			for (ReservaDTO reservaConflicto : reservas) {
				ConflictoDTO conflicto = new ConflictoDTO();

				ActividadGateway ag = new ActividadGatewayImpl();
				ag.setConnection(c);

				conflicto.idInstalacion = idInstalacion;

				conflicto.fechaReservaPlanificada = reservaConflicto.fecha;
				conflicto.idReservaPlanificada = reservaConflicto.idReserva;
				conflicto.nombreActividadPlanificada = ag.findById(reservaConflicto.idActividad).nombre;
				conflicto.horaInicialReservaPlanificada = reservaConflicto.horaInicial;
				conflicto.horaFinalReservaPlanificada = reservaConflicto.horaFinal;

				conflicto.fechaNuevaReserva = fecha;
				conflicto.nombreNuevaActividad = ag.findById(idActividad).nombre;
				conflicto.horaInicialNuevaReserva = horaInicial;
				conflicto.horaFinalNuevaReserva = horaFinal;

				conflictos.add(conflicto);
			}

			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return conflictos;
	}

	private Date obtenerFechaFormateada() {
		Date fechaFormateada = null;

		try {
			String pattern = "yyyy-MM-dd";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			fechaFormateada = simpleDateFormat.parse(simpleDateFormat.format(fecha));
		} catch (ParseException e) {
			System.err.println("Error al obtener la fecha para la bbdd");
		}

		return fechaFormateada;

	}

}
