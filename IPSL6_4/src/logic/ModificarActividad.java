package logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import logic.dto.ActividadDTO;
import logic.dto.ConflictoModificarRecursoDTO;
import logic.dto.InstalacionDTO;
import logic.dto.RecursoDTO;
import logic.util.LogicAuxiliar;
import logic.util.container.Reserva;
import persistence.ModificarActividadPersistance;

public class ModificarActividad {

	public List<RecursoDTO> getRecursos() {
		return new ModificarActividadPersistance().getRecursos();
	}

	public List<ActividadDTO> getActividades() {		
		return new ModificarActividadPersistance().getActividades();
	}


	public List<RecursoDTO> getRecursos(int idActividad) {
		return new ModificarActividadPersistance().getRecursos(idActividad);
	}

	public void addRecurso(ActividadDTO actividad, RecursoDTO recurso) {
		//List<RecursoDTO> recursosDeActividad = 
		ModificarActividadPersistance.addRecurso(actividad.idActividad,recurso.idRecurso);
		
	}
	private InstalacionDTO getInstalacion(int iDInstalacion) {		
		return new ModificarActividadPersistance().getInstalacion(iDInstalacion);
	}
	
	private List<RecursoDTO> getRecursosByInstalacion(int iDInstalacion) {
		return new ModificarActividadPersistance().getRecursosByInstalacion(iDInstalacion);
	}

	/**
	 * Devuelve true si recurso esta en recursoActividad
	 * @param recurso
	 * @param recursosActividad
	 */
	public boolean comprobarRecursoEnActividad(RecursoDTO recurso, List<RecursoDTO> recursosActividad) {
	
		for(RecursoDTO theRecurso:recursosActividad)
			if(theRecurso.equals(recurso))
				return true;
		return false;
		
	}

	public void removeRecurso(ActividadDTO actividad, RecursoDTO recurso) {
		ModificarActividadPersistance.removeRecurso(actividad.idActividad,recurso.idRecurso);
	}
	/**
	 * Devuelve true si alguna reserva se est� efectuando ahora mismo
	 * @param actividad
	 * @return
	 */
	public List<ConflictoModificarRecursoDTO> comprobarReservasSimultaneas(ActividadDTO actividad) {
		List<ConflictoModificarRecursoDTO> toReturn = new ArrayList<ConflictoModificarRecursoDTO>();
		List<Reserva> reservas = new ModificarActividadPersistance().getReservasActividad(actividad.idActividad);
		//TODO hacerlo tambien con alquileres
		
		String date = LogicAuxiliar.getTodaysDate();
		int hora = LogicAuxiliar.getHour();
		
		for(Reserva theReserva: reservas)
			if(date.equals(theReserva.getFecha().toString()))//mismo dia
				if(theReserva.getHoraInicio() <= hora && theReserva.getHoraFinal() > hora) {
					ConflictoModificarRecursoDTO dto = new ConflictoModificarRecursoDTO();
					dto.actividad = actividad;					
					dto.instalacion = getInstalacion(theReserva.getiDInstalacion());
					dto.fecha = theReserva.getFecha().toString();
					dto.conflicto = "No puedes modificar una actividad cuando hay una reserva o alquiler activa";
					toReturn.add(dto);
				}
		
							
		return toReturn;
	}
	/**
	 * Devuelve false si una instalacion de las reservas de la actividad no tiene el recurso pasado como parametro
	 * @param actividad
	 * @return
	 */
	public List<ConflictoModificarRecursoDTO> comprobarRecursosdeReservas(ActividadDTO actividad,RecursoDTO recurso) {
		List<ConflictoModificarRecursoDTO> toReturn = new ArrayList<ConflictoModificarRecursoDTO>();
		List<Reserva> reservas = new ModificarActividadPersistance().getReservasActividad(actividad.idActividad);
		for(Reserva theReserva:reservas) {
			if(!isReservaPasada(theReserva)) {
				boolean aux = false;
				List<RecursoDTO> recursos = getRecursosByInstalacion(theReserva.getiDInstalacion());
				for(RecursoDTO theRecurso:recursos)
					if(theRecurso.equals(recurso))
						aux = true;
				if(!aux) {
					ConflictoModificarRecursoDTO dto = new ConflictoModificarRecursoDTO();
					dto.actividad = actividad;
					dto.recurso = recurso;				
					dto.instalacion = getInstalacion(theReserva.getiDInstalacion());
					dto.fecha = theReserva.getFecha().toString();
					dto.conflicto = "La instalaci�n no tiene el recurso a a�adir en la actividad";
					toReturn.add(dto);
				}	
		}
		}			
		return toReturn;
	}
	/**
	 * True si la reserva ya se realizo
	 * @param theReserva
	 * @return
	 */
	private boolean isReservaPasada(Reserva theReserva) {
		Calendar c = Calendar.getInstance();
		int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11
		String sMes;
		if (mes < 10)
			sMes = "0" + mes;
		else
			sMes = "" + mes;
		String fechaActual = c.get(Calendar.YEAR) + "-" + sMes + "-" + c.get(Calendar.DATE);
		String fechaReserva = theReserva.getFecha().toString();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date fechaReservaDate = sdf.parse(fechaActual);
			java.util.Date fechaActualDate = sdf.parse(fechaReserva);
			if(fechaActualDate.before(fechaReservaDate))
				return true;
			else {
				if(fechaActualDate.equals(fechaReservaDate))
					if(theReserva.getHoraInicio() < c.get(Calendar.HOUR_OF_DAY))
						return true;
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
						
	}

	

	

	
}
