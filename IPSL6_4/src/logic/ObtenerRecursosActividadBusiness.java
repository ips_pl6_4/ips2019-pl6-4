package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.RecursoDTO;
import persistence.gateway.RecursoActividadGateway;
import persistence.impl.RecursoActividadGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerRecursosActividadBusiness {

	private int idActividad;

	public ObtenerRecursosActividadBusiness(int idActividad) {
		this.idActividad = idActividad;
	}

	public List<RecursoDTO> execute() {
		List<RecursoDTO> lista = null;

		Connection c = null;
		try {
			c = Jdbc.getConnection();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}

		RecursoActividadGateway pa = new RecursoActividadGatewayImpl();
		pa.setConnection(c);

		lista = pa.findRecursoByActividad(idActividad);

		return lista;
	}

}
