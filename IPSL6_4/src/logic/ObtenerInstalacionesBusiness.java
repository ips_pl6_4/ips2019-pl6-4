package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.InstalacionDTO;
import persistence.gateway.InstalacionGateway;
import persistence.impl.InstalacionGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerInstalacionesBusiness {

	public List<InstalacionDTO> execute() {
		List<InstalacionDTO> lista = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();

			InstalacionGateway pi = new InstalacionGatewayImpl();
			pi.setConnection(c);

			lista = pi.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

}
