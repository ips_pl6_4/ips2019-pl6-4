package logic;

import java.util.List;

import logic.dto.SocioDTO;
import ui.windows.VentanaLista;

/**
 * Clase que muestra los socios afentados por una modificacion o eliminacion
 *@author Ana Garcia
 *12/11/2019
 */
public class NotificaSocio {
	
	public void notificaSocio(List<SocioDTO> socios) {
		StringBuilder sb = new StringBuilder();
		sb.append("Socios afectados por la anterior operaci�n: \n\n");
		for(SocioDTO socio : socios) {
			sb.append("Nombre: "+socio.name+" Apellidos: "+socio.apellidos
					+" Correo: "+socio.mail+" Tel�fono: "+socio.telefono+"\n");
		}
		VentanaLista vt = new VentanaLista(null,sb.toString());
		vt.setVisible(true);
	}

}
