package logic;

import java.util.Calendar;
import java.util.List;

import logic.util.LogicAuxiliar;
import logic.util.container.Reserva;
import persistence.ReservarPlazaConLimitePersistencia;

/**
 * Clase para reservar una plaza de una actividad con limite de plazas
 * 
 * Historia 7162
 * 
 * @author uo265009
 *
 */
public class ReservarPlazaConLimite {
	
	ReservarPlazaConLimitePersistencia per = new ReservarPlazaConLimitePersistencia();

	/**
	 * Metodo principal de la clase return �boolean? si se hizo correctamente o no
	 */

	public void execute(int idSocio, int idReserva, String rol) {
		
		List<Reserva> reservas = per.getActividades(idSocio);// reservas de ese socio
		Reserva reserva = per.getReserva(idReserva);// la reserva de la cual intentamos reservar plaza

		// Comprobacion 1: �Realmente la actividad es de plazas limitadas?
			//Estas comprobaciones se hacen cuando mostramos las actividades
		// Comprobacion 2: plazas disponibles
			//Estas comprobaciones se hacen cuando mostramos las actividades
		if(!new LogicAuxiliar().plazasDisponibles(idReserva))
			throw new RuntimeException("No hay plazas para esa actividad");
		// Comprobacion 3: El socio tiene m�s reservas simultaneamente?
		for (Reserva theReserva : reservas) {
			LogicAuxiliar.compararFechasSimultaneas(reserva, theReserva);
		}

		// Comprobacion Rango correcto de horas para hacer la reserva
			//Estas comprobaciones se hacen cuando mostramos las actividades
//		
//		String fechaReserva = reserva.getFecha().toString();
//		// String fechaActual = Calendar.getInstance().getTime().toString();
//		// //Integer.parseInt(new
//		// Date(Calendar.getInstance().getTime().getTime()).toString().substring(8)))
//		Calendar c = Calendar.getInstance();
//		// int dia = c.get(Calendar.DATE);
//		int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11
//		String sMes;
//		if (mes < 10)
//			sMes = "0" + mes;
//		else
//			sMes = "" + mes;
//		String fechaActual = c.get(Calendar.YEAR) + "-" + sMes + "-" + c.get(Calendar.DATE);
//		String diaSiguienteActual = fechaActual.substring(0, 8) + (Integer.parseInt(fechaActual.substring(8)) + 1);
//		boolean condicion1 = diaSiguienteActual.equals(fechaReserva);
//		boolean condicion2 = fechaActual.equals(fechaReserva);
//		boolean condicion3 = c.get(Calendar.HOUR_OF_DAY) < reserva.getHoraInicio();
//		boolean condicion4 = rol.equals("Socio");
//		if (!(condicion1 && condicion4 || condicion2 && condicion3)) // suponemos el < y no un <=
//			throw new RuntimeException("No puedes hacer la reserva en esta hora");
//
//		
		
		// Podemos hacer la reserva para ese cliente
		per.insertarAsistencia(idSocio, idReserva);
		// System.out.println("Se ha introducido");
	}


	/**
	 * Return true si es de plazas limitadas
	 * False si no es de plazas limitadas
	 * @param idReserva
	 * @return
	 */
	public boolean isPlazasLimitadas(int idReserva) {		
		if (per.isLimitado(idReserva)) {
			return true;
		}
		return false;
		
	}
	
	/**
	 * Devuelve una lista de reservas disponibles para el socio
	 * @param 
	 */
	
	public List<Reserva> getReservasDisponibles(int idSocio, String rol) {		
		List<Reserva> toReturn = null;
		Calendar c = Calendar.getInstance();
		
		
		toReturn = per.getActividadesDate(new java.sql.Date(System.currentTimeMillis()));//primero cogemos todas las del mismo dia
		
		//Ahora vamos a quitar los que no cumplan las condiciones
		//Que la Reserva se realice como m�ximo una hora antes			
		for(int i = toReturn.size()-1; i >= 0; i--) {//Tiene que ser un for para poder llamar a remove() (con foreach casca)
			int horaLimite = toReturn.get(i).getHoraInicio()-1;		
			if(horaLimite == -1)
				horaLimite = 23;
			if(c.get(Calendar.HOUR_OF_DAY) >= horaLimite)//si es mayor o igual entonces est� fuera
				toReturn.remove(i);
		}
		
		
		//ponemos la condicion del socio despu�s en un intento de que est�n ordenados		
		if(rol.equals("Socio")){
			int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11
			String sMes;
			if (mes < 10)
				sMes = "0" + mes;
			else
				sMes = "" + mes;
			String fechaActual = c.get(Calendar.YEAR) + "-" + sMes + "-" + c.get(Calendar.DATE);
			String diaSiguienteActual = fechaActual.substring(0, 8) + (Integer.parseInt(fechaActual.substring(8)) + 1);
			
			
			List<Reserva> reservasDiaSiguiente = per.getActividadesDate(java.sql.Date.valueOf(diaSiguienteActual));
			for(Reserva theReserva:reservasDiaSiguiente)
				toReturn.add(theReserva);
		}		
		
		//Las que no sean de plazas limitadas no nos interesan
		for(int i = toReturn.size()-1; i >= 0; i--) {
			if(!isPlazasLimitadas(toReturn.get(i).getiDReserva()))// || !plazasDisponibles(toReturn.get(i).getiDReserva())
				toReturn.remove(i);			
		}
	
		
		return toReturn;
	}

}
