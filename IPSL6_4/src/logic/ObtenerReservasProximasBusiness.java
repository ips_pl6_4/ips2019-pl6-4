package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import logic.dto.ReservaDTO;
import persistence.gateway.ReservaGateway;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerReservasProximasBusiness {

	public List<ReservaDTO> execute() {
		List<ReservaDTO> reservasProximas = new ArrayList<ReservaDTO>();
		List<ReservaDTO> lista = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();

			ReservaGateway r = new ReservaGatewayImpl();
			r.setConnection(c);

			lista = r.findReservasSinSocios();

			for (ReservaDTO res : lista) {
				if (sinEmpezar(res)) {
					reservasProximas.add(res);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reservasProximas;
	}

	private boolean sinEmpezar(ReservaDTO res) {
		return res.fecha.after(new java.sql.Date((new Date()).getTime()));
	}

}
