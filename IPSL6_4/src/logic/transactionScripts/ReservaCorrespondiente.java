package logic.transactionScripts;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.AsistenciaDTO;
import logic.dto.ReservaDTO;
import persistence.PersistenceFactory;
import persistence.gateway.ReservaGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ReservaCorrespondiente
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 13.11.2019
 */
public class ReservaCorrespondiente {

	public ReservaDTO reserva;
	private AsistenciaDTO asistencia;

	public ReservaCorrespondiente(AsistenciaDTO asistencia) {
		super();
		this.asistencia = asistencia;
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			ReservaGateway reservaGW = PersistenceFactory.getReservaGateway();
			reservaGW.setConnection(con);
			con.setAutoCommit(false);

			if (this.asistencia != null)
				this.reserva = reservaGW.findById(this.asistencia.reservaID);

			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexión");
		}
	}
}