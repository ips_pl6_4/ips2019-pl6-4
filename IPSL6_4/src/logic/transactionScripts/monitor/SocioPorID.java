package logic.transactionScripts.monitor;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.SocioDTO;
import persistence.PersistenceFactory;
import persistence.gateway.SocioGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion SocioPorID
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public class SocioPorID {

	private int socioID;
	public SocioDTO socio;

	public SocioPorID(int socioID) {
		super();
		this.socioID = socioID;
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			SocioGateway socioGW = PersistenceFactory.getSocioGateway();

			socioGW.setConnection(con);
			con.setAutoCommit(false);

			this.socio = socioGW.findById(this.socioID);

			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}
}
