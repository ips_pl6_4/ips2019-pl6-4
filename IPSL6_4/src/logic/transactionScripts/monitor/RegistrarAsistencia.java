package logic.transactionScripts.monitor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.AsistenciaDTO;
import logic.transactionScripts.BusinessException;
import persistence.PersistenceFactory;
import persistence.gateway.AsistenciaGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion RegistrarAsistencia. Dado un usuario, un monitor
 * registra su asistencia a una actividad.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.10.2019
 */
public class RegistrarAsistencia {

	private AsistenciaDTO asistencia;

	public RegistrarAsistencia(AsistenciaDTO asistencia) {
		super();
		this.asistencia = asistencia;
	}

	public void execute() throws BusinessException {
		try (Connection connection = Jdbc.getConnection()) {
			AsistenciaGateway aG = PersistenceFactory.getAsistenciaGateway();
			aG.setConnection(connection);
			connection.setAutoCommit(false);
			List<AsistenciaDTO> asistencias = aG.findAll();
			for (AsistenciaDTO asistencia : asistencias) {
				if (asistencia.reservaID == this.asistencia.reservaID
						&& asistencia.socioID == this.asistencia.socioID) {
					connection.rollback();
					throw new BusinessException("Ya existe una asistencia del socio a dicha actividad");
				}
			}
			aG.add(this.asistencia);
			connection.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}

}
