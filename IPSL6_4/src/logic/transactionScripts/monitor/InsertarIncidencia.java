package logic.transactionScripts.monitor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.IncidenciaDTO;
import logic.transactionScripts.BusinessException;
import persistence.gateway.IncidenciaGateway;
import persistence.impl.IncidenciaGatewayImpl;
import persistence.util.Jdbc;

/**
 * Clase de transaccion InsertarIncidencia. Inserta una incidencia ocurrida en
 * una reserva (dada su ID) de una actividad.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public class InsertarIncidencia {

	private IncidenciaDTO incidencia;

	public InsertarIncidencia(IncidenciaDTO incidencia) {
		super();
		this.incidencia = incidencia;
	}

	public void execute() throws BusinessException {
		try (Connection connection = Jdbc.getConnection()) {
			IncidenciaGateway iG = new IncidenciaGatewayImpl();
			iG.setConnection(connection);
			connection.setAutoCommit(false);

			List<IncidenciaDTO> incidencias = iG.findAll();

			boolean exists = false;

			for (IncidenciaDTO incidencia : incidencias) {
				if (incidencia.reservaID == this.incidencia.reservaID
						&& incidencia.incidencia.equals(this.incidencia.incidencia)) {
					exists = true;
					break;
				}
			}

			if (exists) {
				connection.rollback();
				throw new BusinessException("Ya existe la incidencia a insertar");
			} else {
				iG.add(this.incidencia);
				connection.commit();
			}

		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}

}
