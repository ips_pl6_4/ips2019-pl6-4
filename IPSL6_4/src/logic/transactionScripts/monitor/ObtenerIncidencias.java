package logic.transactionScripts.monitor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.IncidenciaDTO;
import persistence.gateway.IncidenciaGateway;
import persistence.impl.IncidenciaGatewayImpl;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ObtenerIncidencias. Obtiene todas las incidencias de una
 * reserva a una actividad.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class ObtenerIncidencias {

	private int reservaID;
	public List<IncidenciaDTO> incidencias;

	public ObtenerIncidencias(int reservaID) {
		super();
		this.reservaID = reservaID;
	}

	public void execute() {
		try (Connection connection = Jdbc.getConnection()) {
			IncidenciaGateway iG = new IncidenciaGatewayImpl();
			iG.setConnection(connection);
			connection.setAutoCommit(false);

			this.incidencias = iG.findIncidencias(this.reservaID);

			connection.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}

}
