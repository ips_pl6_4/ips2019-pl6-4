package logic.transactionScripts.monitor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.dto.AsistenciaDTO;
import logic.dto.SocioDTO;
import persistence.PersistenceFactory;
import persistence.gateway.AsistenciaGateway;
import persistence.gateway.SocioGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion SociosSinAsistencia
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public class SociosSinAsistencia {

	private int reservaID;
	public List<SocioDTO> noAsistentes;

	public SociosSinAsistencia(int reservaID) {
		super();
		this.reservaID = reservaID;
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			SocioGateway socioGW = PersistenceFactory.getSocioGateway();
			AsistenciaGateway asistenciaGW = PersistenceFactory.getAsistenciaGateway();

			socioGW.setConnection(con);
			asistenciaGW.setConnection(con);
			con.setAutoCommit(false);

			this.noAsistentes = new ArrayList<SocioDTO>();
			List<SocioDTO> socios = socioGW.findAll();
			for (SocioDTO socio : socios) {
				AsistenciaDTO asistencia = asistenciaGW.findById(this.reservaID, socio.socioID);
				if (asistencia == null && !this.noAsistentes.contains(socio))
					this.noAsistentes.add(socio);
			}
			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}
}
