package logic.transactionScripts.monitor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.ReservaDTO;
import persistence.PersistenceFactory;
import persistence.gateway.ReservaGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ReservasDisponibles
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 20.10.2019
 */
public class ReservasDisponibles {

	public List<ReservaDTO> reservas;

	public ReservasDisponibles() {
		super();
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			ReservaGateway reservaGW = PersistenceFactory.getReservaGateway();

			reservaGW.setConnection(con);
			con.setAutoCommit(false);

			this.reservas = reservaGW.findAll();

			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}
}
