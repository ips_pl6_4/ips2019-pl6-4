package logic.transactionScripts.monitor;

import java.sql.Connection;
import java.sql.SQLException;

import logic.transactionScripts.BusinessException;
import persistence.PersistenceFactory;
import persistence.gateway.AsistenciaGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion CantidadAsistencias
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class CantidadAsistencias {

	public int asistencias;
	private int reservaID;

	public CantidadAsistencias(int reservaID) {
		super();
		this.reservaID = reservaID;
	}

	public void execute() throws BusinessException {
		try (Connection con = Jdbc.getConnection()) {
			AsistenciaGateway asistenciaGW = PersistenceFactory.getAsistenciaGateway();

			asistenciaGW.setConnection(con);
			con.setAutoCommit(false);

			Integer rowCount = asistenciaGW.countAsistencias(this.reservaID);

			if (rowCount == null) {
				con.rollback();
				throw new BusinessException("Error al contar las asistencias");
			} else {
				this.asistencias = rowCount.intValue();
				con.commit();
			}
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}
}
