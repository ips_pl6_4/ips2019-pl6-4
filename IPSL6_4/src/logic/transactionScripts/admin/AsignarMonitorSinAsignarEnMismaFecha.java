package logic.transactionScripts.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.ImparteDTO;
import logic.dto.ReservaDTO;
import logic.transactionScripts.BusinessException;
import persistence.PersistenceFactory;
import persistence.gateway.ImparteGateway;
import persistence.gateway.ReservaGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion AsignarMonitorSinAsignarEnMismaFecha
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public class AsignarMonitorSinAsignarEnMismaFecha {

	private int monitorID;
	private int reservaID;

	public AsignarMonitorSinAsignarEnMismaFecha(int monitorID, int reservaID) {
		super();
		this.monitorID = monitorID;
		this.reservaID = reservaID;
	}

	@SuppressWarnings("deprecation")
	public void execute() throws BusinessException {
		try (Connection con = Jdbc.getConnection()) {

			ImparteGateway imparteGW = PersistenceFactory.getImparteGateway();
			ReservaGateway reservaGW = PersistenceFactory.getReservaGateway();

			imparteGW.setConnection(con);
			reservaGW.setConnection(con);
			con.setAutoCommit(false);

			// Verify
			List<Integer> reservasQueImparte = imparteGW.imparte(this.monitorID);
			boolean flag = true;

			ReservaDTO reservaQueSeAsigna = reservaGW.findById(this.reservaID);
			for (Integer reservaID : reservasQueImparte) {
				ReservaDTO reserva = reservaGW.findById(reservaID.intValue());

				// Encontrada reserva impartida en la misma fecha
				if (reserva.fecha.getDate() == reservaQueSeAsigna.fecha.getDate()) {

					// Encontrada reserva que se solapa en el horario con la reserva a asignar
					if (reserva.horaInicial == reservaQueSeAsigna.horaInicial
							|| reserva.horaFinal == reservaQueSeAsigna.horaFinal)
						flag = false;
					else if (reserva.horaInicial > reservaQueSeAsigna.horaInicial
							&& reserva.horaInicial < reservaQueSeAsigna.horaFinal)
						flag = false;
					else if (reserva.horaFinal > reservaQueSeAsigna.horaInicial
							&& reserva.horaFinal < reservaQueSeAsigna.horaFinal)
						flag = false;
					else if (reservaQueSeAsigna.horaInicial > reserva.horaInicial
							&& reservaQueSeAsigna.horaInicial < reserva.horaFinal)
						flag = false;
					else if (reservaQueSeAsigna.horaFinal > reserva.horaInicial
							&& reservaQueSeAsigna.horaFinal < reserva.horaFinal)
						flag = false;
				}
			}

			// Proceed
			if (flag) {
				ImparteDTO imparticion = new ImparteDTO();
				imparticion.monitorID = this.monitorID;
				imparticion.reservaID = this.reservaID;
				imparteGW.add(imparticion);
				con.commit();
			} else {
				con.rollback();
				throw new BusinessException("El monitor ya imparte una reserva en un horario que solapado");
			}
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}
}