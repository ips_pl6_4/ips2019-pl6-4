package logic.transactionScripts.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import logic.dto.ImparteDTO;
import logic.dto.ReservaDTO;
import persistence.PersistenceFactory;
import persistence.gateway.ImparteGateway;
import persistence.gateway.ReservaGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ActividadesSinMonitor
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class ReservasSinMonitor {
	public List<ReservaDTO> reservas;

	public ReservasSinMonitor() {
		super();
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			ImparteGateway imparteGW = PersistenceFactory.getImparteGateway();
			ReservaGateway reservaGW = PersistenceFactory.getReservaGateway();

			imparteGW.setConnection(con);
			reservaGW.setConnection(con);
			con.setAutoCommit(false);

			List<ImparteDTO> imparticiones = imparteGW.findAll();
			List<ReservaDTO> reservas = reservaGW.findAll();

			this.reservas = new LinkedList<ReservaDTO>(reservas);

			for (ReservaDTO reserva : reservas)
				for (ImparteDTO imparticion : imparticiones)
					if (reserva.idReserva == imparticion.reservaID)
						this.reservas.remove(reserva);

			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}
}
