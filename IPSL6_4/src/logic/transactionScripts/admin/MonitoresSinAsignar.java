package logic.transactionScripts.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import logic.dto.ImparteDTO;
import logic.dto.MonitorDTO;
import persistence.PersistenceFactory;
import persistence.gateway.ImparteGateway;
import persistence.gateway.MonitorGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion MonitoresSinAsignar. {Monitores sin asignar} = {Todos
 * los monitores} - {Monitores que imparten}
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class MonitoresSinAsignar {
	public List<MonitorDTO> monitores;

	public MonitoresSinAsignar() {
		super();
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			ImparteGateway imparteGW = PersistenceFactory.getImparteGateway();
			MonitorGateway monitorGW = PersistenceFactory.getMonitorGateway();

			imparteGW.setConnection(con);
			monitorGW.setConnection(con);
			con.setAutoCommit(false);

			List<ImparteDTO> imparticiones = imparteGW.findAll();
			List<MonitorDTO> monitores = monitorGW.findAll();

			this.monitores = new LinkedList<MonitorDTO>(monitores);

			for (MonitorDTO monitor : monitores)
				for (ImparteDTO imparticion : imparticiones)
					if (monitor.monitorID == imparticion.monitorID)
						this.monitores.remove(monitor);

			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}
}
