package logic.transactionScripts.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.MonitorDTO;
import persistence.PersistenceFactory;
import persistence.gateway.MonitorGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion Monitores.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public class Monitores {
	public List<MonitorDTO> monitores;

	public Monitores() {
		super();
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			MonitorGateway monitorGW = PersistenceFactory.getMonitorGateway();
			monitorGW.setConnection(con);
			con.setAutoCommit(false);

			this.monitores = monitorGW.findAll();

			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexión");
		}
	}
}