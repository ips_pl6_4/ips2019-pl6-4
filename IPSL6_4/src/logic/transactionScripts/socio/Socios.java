package logic.transactionScripts.socio;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.SocioDTO;
import persistence.PersistenceFactory;
import persistence.gateway.SocioGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion SociosSinAsistencia
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 12.11.2019
 */
public class Socios {

	public List<SocioDTO> socios;

	public Socios() {
		super();
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			SocioGateway socioGW = PersistenceFactory.getSocioGateway();
			socioGW.setConnection(con);
			con.setAutoCommit(false);

			this.socios = socioGW.findAll();
			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}
}
