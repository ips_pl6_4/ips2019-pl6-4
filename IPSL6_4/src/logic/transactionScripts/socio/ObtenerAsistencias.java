package logic.transactionScripts.socio;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.AsistenciaDTO;
import logic.transactionScripts.BusinessException;
import persistence.PersistenceFactory;
import persistence.gateway.AsistenciaGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ObtenerAsistencias. Dado un socio, obtiene todas sus
 * asistencias a cualquier reserva.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 12.11.2019
 */
public class ObtenerAsistencias {

	private int socioID;
	public List<AsistenciaDTO> asistencias;

	public ObtenerAsistencias(int socioID) {
		super();
		this.socioID = socioID;
	}

	public void execute() throws BusinessException {
		try (Connection connection = Jdbc.getConnection()) {
			AsistenciaGateway aG = PersistenceFactory.getAsistenciaGateway();
			aG.setConnection(connection);
			connection.setAutoCommit(false);
			this.asistencias = aG.asiste(this.socioID);
			connection.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}

}