package logic.transactionScripts.socio;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.InstalacionDTO;
import persistence.PersistenceFactory;
import persistence.gateway.InstalacionGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ObtenerInstalacion. Dada una ID, obtiene la instalacion
 * a la que le pertenece.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 4.12.2019
 */
public class ObtenerInstalacion {

	private int instalacionID;
	public InstalacionDTO instalacion;

	public ObtenerInstalacion(int instalacionID) {
		super();
		this.instalacionID = instalacionID;
	}

	public void execute() {
		try (Connection connection = Jdbc.getConnection()) {
			InstalacionGateway iG = PersistenceFactory.getInstalacionGateway();
			iG.setConnection(connection);
			connection.setAutoCommit(false);
			this.instalacion = iG.findById(this.instalacionID);
			connection.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}

}
