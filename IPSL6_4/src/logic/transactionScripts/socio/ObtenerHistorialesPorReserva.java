package logic.transactionScripts.socio;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.HistorialDTO;
import persistence.PersistenceFactory;
import persistence.gateway.HistorialGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ObtenerHistoriales. Obtiene todos los historiales de una
 * reserva.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class ObtenerHistorialesPorReserva {

	private int reservaID;
	public List<HistorialDTO> historiales;

	public ObtenerHistorialesPorReserva(int reservaID) {
		super();
		this.reservaID = reservaID;
	}

	public void execute() {
		try (Connection connection = Jdbc.getConnection()) {
			HistorialGateway hG = PersistenceFactory.getHistorialGateway();
			hG.setConnection(connection);
			connection.setAutoCommit(false);
			this.historiales = hG.findByReserva(this.reservaID);
			connection.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}

}
