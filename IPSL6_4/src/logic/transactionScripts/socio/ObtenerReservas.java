package logic.transactionScripts.socio;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.ReservaSocioDTO;
import persistence.PersistenceFactory;
import persistence.gateway.ReservaSocioGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ObtenerReservas. Obtiene todas las ReservaSocio
 * pertenecientes a un socio.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 3.12.2019
 */
public class ObtenerReservas {

	private int socioID;
	public List<ReservaSocioDTO> reservas;

	public ObtenerReservas(int socioID) {
		super();
		this.socioID = socioID;
	}

	public void execute() {
		try (Connection connection = Jdbc.getConnection()) {
			ReservaSocioGateway rsG = PersistenceFactory.getReservaSocioGateway();
			rsG.setConnection(connection);
			connection.setAutoCommit(false);
			this.reservas = rsG.findBySocio(this.socioID);
			connection.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}
	}

}
