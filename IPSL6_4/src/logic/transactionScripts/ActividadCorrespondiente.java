package logic.transactionScripts;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.ActividadDTO;
import logic.dto.ReservaDTO;
import persistence.PersistenceFactory;
import persistence.gateway.ActividadGateway;
import persistence.util.Jdbc;

/**
 * Clase de transaccion ActividadCorrespondiente
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public class ActividadCorrespondiente {

	public ActividadDTO actividad;
	private ReservaDTO reserva;

	public ActividadCorrespondiente(ReservaDTO reserva) {
		super();
		this.reserva = reserva;
	}

	public void execute() {
		try (Connection con = Jdbc.getConnection()) {
			ActividadGateway actividadGW = PersistenceFactory.getActividadGateway();
			actividadGW.setConnection(con);
			con.setAutoCommit(false);

			if (this.reserva != null)
				this.actividad = actividadGW.findById(this.reserva.idActividad);

			con.commit();
		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexión");
		}
	}
}