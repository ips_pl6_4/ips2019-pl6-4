package logic;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.AlquilerDTO;
import persistence.gateway.AlquilerGateway;
import persistence.impl.AlquilerGatewayImpl;
import persistence.util.Jdbc;

public class EliminaAlquiler {

	private AlquilerDTO alquiler;

	public EliminaAlquiler(AlquilerDTO alquiler) {
		this.alquiler = alquiler;
	}

	public void eliminaAlquiler() {

		Connection con = null;
		try {
			con = Jdbc.getConnection();
			AlquilerGateway ag = new AlquilerGatewayImpl();
			ag.setConnection(con);
			ag.delete(alquiler.idReservaSocio);
			con.commit();
		} catch (SQLException e) {
			try {
				if (con != null) {
					con.rollback();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} catch (IllegalArgumentException e) {
			try {
				if (con != null) {
					con.rollback();
				}
				throw e;
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}

}
