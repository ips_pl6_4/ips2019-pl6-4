package logic;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import logic.dto.SocioDTO;
import logic.util.container.Reserva;
import persistence.CancelarReservaPersistencia;

/**
 * Cancelar reserva se refiere a eliminar el la fila elegida de la tabla Reserva
 * no confundir con anular reserva, que quita al socio de los apuntados
 * @author arvo
 *
 */
public class CancelarReserva {
	
	CancelarReservaPersistencia p = new CancelarReservaPersistencia();

	public List<Reserva> getReservasFecha(java.util.Date fecha) {
		//Caso 1, "fecha" es el dia de hoy, entonces habr� que filtrar las reservas que ya se hayan realizado y las que esten en proceso		
		//Caso 2, es una fecha posterior, sin probleam		
		//Caso 3, la fecha es anterior, no deberia pasar porque el jcalendar no lo permite.
		
		List<Reserva> toReturn = null;
		Calendar c = Calendar.getInstance();
		int horaActual = c.get(Calendar.HOUR_OF_DAY);
		Date fechaSQL = new Date(fecha.getTime());
		toReturn = p.getActividadesDate(fechaSQL);
				
		
		int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11
		String sMes;
		if (mes < 10) {sMes = "0" + mes;}
		else {sMes = "" + mes;}
		String fechaActual = c.get(Calendar.YEAR) + "-" + sMes + "-" + c.get(Calendar.DATE);
		Date fechaActualSQL = Date.valueOf(fechaActual);
		if (fechaActualSQL.toString().equals(fechaSQL.toString())) {//Es el mismo dia y por lo tanto hay que quitar las que ya se hayan realizado
			
			
			for(int i = toReturn.size()-1; i >= 0; i--) {//Tiene que ser un for para poder llamar a remove() (con foreach casca)
				int horaLimite = toReturn.get(i).getHoraInicio();		
				if(horaLimite == -1)
					horaLimite = 23;
				if(horaActual >= horaLimite)//si es mayor o igual entonces est� fuera
					toReturn.remove(i);
			}
			
		}
		
		return toReturn;
	}

	public void execute(int idReserva) {
		//Llegados a este punto ya sabemos que que la reserva existe y que ni ha finalizado ni esta en progreso
		
		
		//Sacar el monitor?? //TODO
		//Eliminar fila de imparte
		p.eliminarMonitor(idReserva);
		//Sacar los usuarios afectados
		List<SocioDTO> sociosAfectados = p.getSocios(idReserva);
		//Eliminar todas las filas de Asistencia
		p.eliminarAsistencia(idReserva);
		p.eliminarGrupoReserva(idReserva);
		p.eliminarIncidencia(idReserva);
		//a�adir al historial
		Reserva reserva = p.getReserva(idReserva);
		p.a�adirAlHistorial(reserva);
		//sacar el idHistorial
		//int idHistorial = p.getLastIdHistorial(); //saca el mayor id del historial que se supone que es el mayor
		//eliminar Reserva
		p.CancelarReserva(idReserva);
		
		
		
		//Avisar a los socios que est�n afectados
//		for(SocioDTO socio:sociosAfectados)
//			System.out.println( socio.socioID + " "+socio.name);
		new NotificaSocio().notificaSocio(sociosAfectados);
		
	}

}
