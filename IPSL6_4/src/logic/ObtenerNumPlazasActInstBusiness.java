package logic;

import java.util.ArrayList;
import java.util.List;

import logic.dto.RecursoDTO;
import logic.dto.RecursoInstalacionDTO;

public class ObtenerNumPlazasActInstBusiness {

	private List<RecursoDTO> recursosActividad;
	private List<RecursoInstalacionDTO> recursosInstalacion;

	public ObtenerNumPlazasActInstBusiness(List<RecursoDTO> recursosActividad,
			List<RecursoInstalacionDTO> recursosInstalacion) {
		this.recursosActividad = recursosActividad;
		this.recursosInstalacion = recursosInstalacion;
	}

	public int execute() {
		if (recursosActividad.size() == 0) {
			return 0;
		}

		int numPlazas = -1;

		List<Integer> recursosNecesarios = getIdsRecursosNecesarios();
		List<Integer> recursosDisponibles = getIdsRecursosDisponibles();

		for (int idNecesario : recursosNecesarios) {
			if (!recursosDisponibles.contains(idNecesario)) {
				return numPlazas;
			}
		}

		numPlazas = getMenorCantidadRecursos(recursosNecesarios);

		return numPlazas;
	}

	private List<Integer> getIdsRecursosNecesarios() {
		List<Integer> recursosNecesarios = new ArrayList<Integer>();

		for (RecursoDTO r : recursosActividad) {
			recursosNecesarios.add(r.idRecurso);
		}

		return recursosNecesarios;
	}

	private List<Integer> getIdsRecursosDisponibles() {
		List<Integer> recursosDisponibles = new ArrayList<Integer>();

		for (RecursoInstalacionDTO r : recursosInstalacion) {
			recursosDisponibles.add(r.idRecurso);
		}

		return recursosDisponibles;
	}

	private int getMenorCantidadRecursos(List<Integer> recursosNecesarios) {
		int numPlazas = 0;

		List<RecursoInstalacionDTO> recursosNecesariosEnInstalacion = new ArrayList<RecursoInstalacionDTO>();

		for (RecursoInstalacionDTO r : recursosInstalacion)
			if (recursosNecesarios.contains(r.idRecurso))
				recursosNecesariosEnInstalacion.add(r);

		for (RecursoInstalacionDTO r : recursosNecesariosEnInstalacion)
			numPlazas = (numPlazas == 0 || r.cantidad < numPlazas) ? r.cantidad : numPlazas;

		return numPlazas;
	}

}
