package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import persistence.gateway.AlquilerGateway;
import persistence.impl.AlquilerGatewayImpl;
import persistence.util.Jdbc;

public class CrearReservaSocioBusiness {

	private int idSocio;
	private int idInstalacion;
	private int horaInicio;
	private int horaFin;
	private Date fecha;

	private Connection c;

	public CrearReservaSocioBusiness(int idSocio, int idInstalacion, int horaInicio, int horaFin, Date fecha) {
		this.idSocio = idSocio;
		this.idInstalacion = idInstalacion;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.fecha = fecha;
	}

	public void execute() {
		try {
			c = Jdbc.getConnection();

			AlquilerGateway ag = new AlquilerGatewayImpl();
			ag.setConnection(c);

			int hIni, hFin;

			for (hIni = horaInicio; hIni < horaFin; hIni++) {
				hFin = hIni + 1;

				ag.add(idSocio, idInstalacion, hIni, hFin, fecha);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
