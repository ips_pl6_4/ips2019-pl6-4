package logic.util;

import java.util.Calendar;

import logic.util.container.Reserva;
import persistence.AuxiliarMethodsPersistencia;
import persistence.ReservarPlazaConLimitePersistencia;

public class LogicAuxiliar {
	ReservarPlazaConLimitePersistencia per = new ReservarPlazaConLimitePersistencia();

	
	/**
	 * Comparar fechas simultaneas
	 */
	
	/**
	 * Comprar Hora
	 */
	/**
	 * Gets
	 */
	
	public static int getNumeroPlazasRestantes(Reserva reserva) {		
		return reserva.getNumPlazas() - getNumeroPlazasOcupadas(reserva);
	}
	public static int getNumeroPlazasOcupadas(Reserva reserva) {
		int toReturn  = new AuxiliarMethodsPersistencia().numeroPlazasOcupadas(reserva.getiDReserva());
		return toReturn;
	}
	
	public static void compararFechasSimultaneas(Reserva reserva, Reserva theReserva) throws RuntimeException {
		// String reservaDia = reserva.getFecha().toString().substring(8);
		// String reservaComprobar = theReserva.getFecha().toString().substring(8);
		if (reserva.getFecha().equals(theReserva.getFecha()))// El dia es el mismo
			if (!(reserva.getHoraFinal() < theReserva.getHoraInicio()
					|| theReserva.getHoraFinal() < reserva.getHoraInicio()))// si true entonces es porque se solapan
				throw new RuntimeException("La actividad se solapa con otra");

	}
	public boolean plazasDisponibles(int idReserva) {		
		Reserva reserva = per.getReserva(idReserva);
		if (reserva.getNumPlazas() <= per.numeroPlazasOcupadas(idReserva))
			return false;
		return true;
	}
	public static String getTodaysDate() {
		Calendar c = Calendar.getInstance();
		int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11
		String sMes;
		if (mes < 10)
			sMes = "0" + mes;
		else
			sMes = "" + mes;
		String dia;
		if(c.get(Calendar.DATE) < 10)
			dia =  "0" + c.get(Calendar.DATE);
		else
			dia = "" + c.get(Calendar.DATE);
		String fechaActual = c.get(Calendar.YEAR) + "-" + sMes + "-" + dia;
		return fechaActual;
	}
	public static int getHour() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.HOUR_OF_DAY);
	}
}
