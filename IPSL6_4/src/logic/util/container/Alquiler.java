package logic.util.container;

import java.util.Date;

/**
 * Clase que representa ser un alquiler de una instalaci�n
 * 
 * @author Ana Garc�a 7-10-2019
 *
 */
public class Alquiler {

	/**
	 * Atributo que representa el nombre de la instalaci�n alquilada
	 */
	private String instalacion;
	/**
	 * Atributo que representa la hora en la que empieza el alquiler
	 */
	private int horaInicio; // horario: de 8:00 a 23:00
	/**
	 * Atributo que representa la hora a la que acaba el alquiler
	 */
	private int horaFinal; // maximo 2 horas y se reserva por horas
	/**
	 * Atributo que representa si el evento que se alquila tiene plazas limitadas o
	 * no
	 */
	private boolean plazasLimitadas;

	/**
	 * Atributo que representa el d�a de la reserva
	 */

	private Date dia;

	/**
	 * Atributo que representa el nombre de la Actividad
	 */
	private String nombreActividad;

	/**
	 * Constructor con 3 par�metros de la clase
	 * 
	 * @param instalacion Instalaci�n que se reserva
	 * @param horaInicio  Hora y d�a en la que empieza el alquiler
	 * @param horaFinal   Hora y d�a en la que acaba el alquiler
	 */
	public Alquiler(String instalacion, int horaInicio, int horaFinal, Date dia) {
		setInstalacion(instalacion);
		setHoraInicio(horaInicio);
		setHoraFinal(horaFinal);
		setDia(dia);
		nombreActividad = "";
		plazasLimitadas = false;
	}

	/**
	 * Constructor con 3 par�metros de la clase
	 * 
	 * @param instalacion Instalaci�n que se reserva
	 * @param nombre      Nombre de la actividad que se imparte
	 * @param horaInicio  Hora y d�a en la que empieza el alquiler
	 * @param horaFinal   Hora y d�a en la que acaba el alquiler
	 * @param plazas      Booleano para indicar si el evento para el que se alquila
	 *                    tiene plazas limitadas o no
	 */
	public Alquiler(String instalacion, String nombre, int horaInicio, int horaFinal, Date dia, boolean plazas) {
		setInstalacion(instalacion);
		setHoraInicio(horaInicio);
		setHoraFinal(horaFinal);
		setDia(dia);
		nombreActividad = nombre;
		plazasLimitadas = plazas;
	}

	/**
	 * M�todo que establece la instalaci�n que se alquila
	 * 
	 * @param instalacion Instalaci�n que se alquila
	 */
	private void setInstalacion(String instalacion) {
		this.instalacion = instalacion;
	}

	/**
	 * M�todo que devuelve el nombre de la instalaci�n alquilada
	 * 
	 * @return Nombre de la instalaci�n alquilada
	 */
	public String getInstalacion() {
		return instalacion;
	}

	/**
	 * M�todo que establece la hora de inicio del alquiler
	 * 
	 * @param horaInicio Hora de inicio del alquiler
	 */
	private void setHoraInicio(int horaInicio) {
		this.horaInicio = horaInicio;
	}

	/**
	 * M�todo que devuelve la hora de inicio del alquiler
	 * 
	 * @return Hora de inicio del alquiler
	 */
	public int getHoraInicio() {
		return horaInicio;
	}

	/**
	 * M�todo que establece la hora en la que termina el alquiler
	 * 
	 * @param horaFinal Hora en la que termina el alquiler
	 */
	private void setHoraFinal(int horaFinal) {
		this.horaFinal = horaFinal;
	}

	/**
	 * M�todo que devuelve la hora en la que termina el alquiler
	 * 
	 * @return Hora en la que temrina el alquiler
	 */
	public int getHoraFinal() {
		return horaFinal;
	}

	/**
	 * M�todo que establece el d�a del alquiler
	 * 
	 * @param dia D�a del alquiler
	 */
	private void setDia(Date dia) {
		this.dia = dia;
	}

	/**
	 * M�todo que devuelve el d�a del alquiler
	 * 
	 * @return D�a del alquiler
	 */
	public Date getDia() {
		return dia;
	}

	/**
	 * M�todo que devuelve si el alquiler tiene plazas limitadas o no
	 * 
	 * @return booleano, true si las tiene, false si no
	 */
	public boolean getPlazasLimitadas() {
		return plazasLimitadas;
	}

	/**
	 * M�todo que devuelve el nombre de la actividad impartida
	 * 
	 * @return Nombre de la actividad impartida
	 */
	public String getActividad() {
		return nombreActividad;
	}
}
