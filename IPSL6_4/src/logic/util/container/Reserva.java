package logic.util.container;

import java.sql.Date;

import persistence.ReservarPlazaConLimitePersistencia;

/**
 * Clase analoga a Reserva de la base de datos
 * 
 * @author uo265009
 *
 */
@SuppressWarnings("unused")
public class Reserva {
	private int iDReserva;
	private int iDActividad;
	private int iDInstalacion;
	private int horaInicio;
	private int horaFinal;
	private Date fecha;
	private boolean plazasLimitadas;
	private int numPlazas;

	/**
	 * Constructor con todo
	 */
	public Reserva(int iDReserva, int iDActividad, int iDInstalacion, int horaInicio, int horaFinal, Date fecha,
			boolean plazasLimitadas, int numPlazas) {
		this.iDReserva = iDReserva;
		this.iDActividad = iDActividad;
		this.iDInstalacion = iDInstalacion;
		this.horaInicio = horaInicio;
		this.horaFinal = horaFinal;
		this.fecha = fecha;
		this.plazasLimitadas = plazasLimitadas;
		this.numPlazas = numPlazas;
	}

	public int getiDReserva() {
		return iDReserva;
	}

	public int getiDActividad() {
		return iDActividad;
	}

	public int getiDInstalacion() {
		return iDInstalacion;
	}

	public int getHoraInicio() {
		return horaInicio;
	}

	public int getHoraFinal() {
		return horaFinal;
	}

	public Date getFecha() {
		return fecha;
	}

	public boolean isPlazasLimitadas() {
		return plazasLimitadas;
	}

	public int getNumPlazas() {
		return numPlazas;
	}

	private void setiDReserva(int iDReserva) {
		this.iDReserva = iDReserva;
	}

	private void setiDActividad(int iDActividad) {
		this.iDActividad = iDActividad;
	}

	private void setiDInstalacion(int iDInstalacion) {
		this.iDInstalacion = iDInstalacion;
	}

	private void setHoraInicio(int horaInicio) {
		this.horaInicio = horaInicio;
	}

	private void setHoraFinal(int horaFinal) {
		this.horaFinal = horaFinal;
	}

	private void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	private void setPlazasLimitadas(boolean plazasLimitadas) {
		this.plazasLimitadas = plazasLimitadas;
	}

	// TODO Llamar aqui a la base de datos es una gochada
	public String toString() {
		String toReturn = "";
		// System.out.println(iDReserva);
		// toReturn += iDReserva + " ";
		toReturn += new ReservarPlazaConLimitePersistencia().getNombreActividad(iDReserva) + " ";
		toReturn += "Horario: " + horaInicio + "->";
		toReturn += horaFinal + " del ";
		toReturn += fecha.toString();// no hace falta pero me apetece
		return toReturn;
	}

}
