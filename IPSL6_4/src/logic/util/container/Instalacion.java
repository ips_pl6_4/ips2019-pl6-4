package logic.util.container;

/**
 * Clase que representa la información de una Instalación
 * @author Ana García
 * 7-10-2019
 */
public class Instalacion {

	/**
	 * Atributo que representa el id de la instalación
	 */
	private int id;
	/**
	 * Atributo que repesenta el nombre de la instalación
	 */
	private String nombre;

	/**
	 * Constructor con dos parámetros de la clase, el id y
	 * el nombre de la instalación
	 * @param id Id de la instalacion
	 * @param nombre Nombre de la instalacion
	 */
	public Instalacion(int id, String nombre) {
		setId(id);
		setNombre(nombre);
	}
	/**
	 * Método que establece el nombre de la instalación
	 * @param nombre Nombre de la instalación
	 */
	private void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Método que devuelve el nombre de la instalación
	 * @return Nombre de la instalación
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Método que establece el id de la instalación
	 * @param id Id de la instalación
	 */
	private void setId(int id) {
		this.id = id;
	}
	/**
	 * Método que devuelve el id de la instalación
	 * @return Id de la instalación
	 */
	public int getId() {
		return this.id;
	}
}
