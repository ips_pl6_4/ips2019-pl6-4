package logic.util;

import java.util.Calendar;

import logic.util.container.Reserva;
import persistence.AuxiliarMethodsPersistencia;

public class LogicAuxiliarMethods {

	
	/**
	 * Comparar fechas simultaneas
	 */
	
	/**
	 * Comprar Hora
	 */
	/**
	 * Gets
	 */
	
	public static int getNumeroPlazasRestantes(Reserva reserva) {		
		return reserva.getNumPlazas() - getNumeroPlazasOcupadas(reserva);
	}
	public static int getNumeroPlazasOcupadas(Reserva reserva) {
		int toReturn  = new AuxiliarMethodsPersistencia().numeroPlazasOcupadas(reserva.getiDReserva());
		return toReturn;
	}
	public static String todaysDate() {
		Calendar c = Calendar.getInstance();
		int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11
		String sMes;
		if (mes < 10)
			sMes = "0" + mes;
		else
			sMes = "" + mes;
		return c.get(Calendar.YEAR) + "-" + sMes + "-" + c.get(Calendar.DATE);
	}
}
