package logic.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import logic.util.container.Reserva;

/**
 * Parser de Reserva
 * 
 * @author arvo
 *
 */
public class ReservaParser {

	public List<Reserva> parse(ResultSet rs) throws SQLException {
		List<Reserva> toReturn = new ArrayList<Reserva>();
		while (rs.next()) {
			Reserva reserva = new Reserva(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5),
					(java.sql.Date) rs.getObject(6), rs.getBoolean(7), rs.getInt(8));
			toReturn.add(reserva);
		}
		rs.close();

		return toReturn;
	}

}
