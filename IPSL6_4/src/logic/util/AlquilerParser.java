package logic.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import logic.Control;
import logic.VerOcupacionInstalacion;
import logic.util.container.Alquiler;

/**
 * Clase que se encarga de parsear alquileres
 * 
 * @author Ana Garcia 07-10-2019
 */

public class AlquilerParser {

	ResultSet rs;

	public AlquilerParser(ResultSet rs) {
		this.rs = rs;
	}

	/**
	 * M�todo que devuelve la lista de los alquileres a partir del resultSet
	 * 
	 * @return Lista de Alquileres
	 * @throws SQLException Si ocurre alg�n error en la base
	 */
	public List<Alquiler> getListaMismaSemanaSinPlazas(String[] diaHoy) throws SQLException {
		List<Alquiler> alquileres = new ArrayList<Alquiler>();
		while (rs.next()) {
			Alquiler a = new Alquiler(rs.getString(1), rs.getInt(2), rs.getInt(3), (Date) rs.getObject(4));
			if(compruebaMismaSemana(a, diaHoy)) {
				alquileres.add(a);
			}
				
		}
//		if (alquileres.size() == 0) {
//			throw new IllegalArgumentException("La instalaci�n no existe o no ha sido alquilada esta semana");
//		}
		rs.close();
		return alquileres;
	}

	/**
	 * M�todo que comprueba si el dia sacado de la base de datos corresponde a esta
	 * semana
	 * 
	 * @param a Alquiler con el d�a
	 * @return booleano, true si es de la misma semana, false si no lo es
	 */
	private boolean compruebaMismaSemana(Alquiler a,String[] diaHoy) {
		String[] diaAlquiler = a.getDia().toString().split("-");
		if (diaHoy[3].equals(diaAlquiler[0]) && diaHoy[2].equals(diaAlquiler[1])) {
			int diaSemanaHoy = VerOcupacionInstalacion.getDiaSemana(diaHoy[0]);
			int diaHoyInt = Control.convertirInt(diaHoy[1]);
			int diaAlquilerInt = Control.convertirInt(diaAlquiler[2]);
			int diferencia = 0;
			if (diaAlquilerInt < diaHoyInt) {
				diferencia = diaSemanaHoy - 1;
				for (int i = diferencia; i > 0; i--) {
					if (diaAlquilerInt + i == diaHoyInt) {
						return true;
					}
				}
			} else if (diaAlquilerInt > diaHoyInt) {
				diferencia = 7 - diaSemanaHoy;
				for (int i = diferencia; i > 0; i--) {
					if (diaAlquilerInt - i == diaHoyInt) {
						return true;
					}
				}
			} else {
				return true;
			}
		}
		return false;
	}

	/**
	 * M�todo que obtiene el d�a de hoy en String con el formato Thru 10 Oct 2019
	 * 
	 * @param dia Dia de hoy
	 * @return String con el d�a en el formato descrito
	 */
	public static String getDiaString(Date dia) {
		// Thu Oct 10 19:28:02 CEST 2019
		String[] str = dia.toString().split(" ");
		return "" + str[0] + " " + str[2] + " " + str[1] + " " + str[5];
	}

	/**
	 * M�todo que devuelve el mes introducido en n�mero. El mes debe ser: Jan, Feb,
	 * Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
	 * 
	 * @param mes Mes del que se desea conocer el n�mero
	 * @return n�mero del mes
	 */
	public static String getNumeroMes(String mes) {
		switch (mes) {
		case "Jan":
			return "" + 1;
		case "Feb":
			return "" + 2;
		case "Mar":
			return "" + 3;
		case "Apr":
			return "" + 4;
		case "May":
			return "" + 5;
		case "Jun":
			return "" + 6;
		case "Jul":
			return "" + 7;
		case "Aug":
			return "" + 8;
		case "Sep":
			return "" + 9;
		case "Oct":
			return "" + 10;
		case "Nov":
			return "" + 11;
		default:
			return "" + 12;
		}

	}

	/**
	 * M�todo que devuelve una lista de alquileres con, adem�s de los par�metros
	 * normales, el nombre de la actividad que se imparte y si esta tiene plazas
	 * limitadas o no
	 * 
	 * @return lista de alquileres
	 * @throws SQLException Si hay alg�n error en el resultSet
	 */
	public List<Alquiler> getListaActividadesPlazaONo() throws SQLException {
		List<Alquiler> alquileres = new ArrayList<Alquiler>();
		while (rs.next()) {
			Alquiler a = new Alquiler(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4),
					(Date) rs.getObject(5), rs.getBoolean(6));
			alquileres.add(a);

		}
		rs.close();
		return alquileres;
	}

}
