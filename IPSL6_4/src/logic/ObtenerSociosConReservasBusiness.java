package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.SocioDTO;
import persistence.gateway.SocioGateway;
import persistence.impl.SocioGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerSociosConReservasBusiness {
	
	

	public List<SocioDTO> execute() {
		List<SocioDTO> lista = null;
		Connection c = null;
		try {
			c = Jdbc.getConnection();
			c.setAutoCommit(false);

			SocioGateway pa = new SocioGatewayImpl();
			pa.setConnection(c);

			lista = pa.findWithRenting();

			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return lista;
	}

}
