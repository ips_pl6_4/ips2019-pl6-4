package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.ActividadDTO;
import persistence.impl.ActividadGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerActividadesBusiness {

	public List<ActividadDTO> execute() {
		List<ActividadDTO> lista = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();
			c.setAutoCommit(false);

			ActividadGatewayImpl pa = new ActividadGatewayImpl();
			pa.setConnection(c);

			lista = pa.findAll();

			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return lista;
	}

}
