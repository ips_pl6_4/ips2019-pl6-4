package logic;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ObtenerFechasBusiness {

	private Date fechaInicial;
	private Date fechaFinal;
	private int diaSeleccionado;

	public ObtenerFechasBusiness(Date fechaInicial, Date fechaFinal, int diaSeleccionado) {
		this.fechaInicial = fechaInicial;
		this.fechaFinal = fechaFinal;
		this.diaSeleccionado = diaSeleccionado;
	}

	@SuppressWarnings("deprecation")
	public List<Date> execute() {
		List<Date> fechas = new ArrayList<Date>();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fechaInicial);

		while (calendar.getTime().getTime() <= fechaFinal.getTime()) {
			Date fecha = calendar.getTime();

			if (fecha.getDay() == diaSeleccionado) {
				fechas.add(fecha);
			}

			calendar.add(Calendar.DATE, 1);
		}

		return fechas;
	}

}
