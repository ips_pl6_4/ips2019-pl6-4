package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import persistence.gateway.ReservaGateway;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class ModificarHorarioReservaBusiness {

	private int idReserva;
	private Date fecha;
	private int horaInicial;
	private int horaFinal;

	public ModificarHorarioReservaBusiness(int idReserva, Date fecha, int horaInicial, int horaFinal) {
		this.idReserva = idReserva;
		this.fecha = fecha;
		this.horaInicial = horaInicial;
		this.horaFinal = horaFinal;
	}

	public void execute() {
		Connection c = null;

		try {
			c = Jdbc.getConnection();

			ReservaGateway r = new ReservaGatewayImpl();
			r.setConnection(c);

			r.updateHorarioReserva(idReserva, fecha, horaInicial, horaFinal);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
