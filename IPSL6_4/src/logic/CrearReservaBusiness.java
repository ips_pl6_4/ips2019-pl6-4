package logic;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.ReservaDTO;
import persistence.gateway.ReservaGateway;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class CrearReservaBusiness {

	private ReservaDTO reserva;

	private Connection c;

	public CrearReservaBusiness(ReservaDTO reserva) {
		this.reserva = reserva;
	}

	public int execute() {
		int idReserva = -1;

		try {
			c = Jdbc.getConnection();

			ReservaGateway pr = new ReservaGatewayImpl();
			pr.setConnection(c);

			pr.add(reserva);
			
			idReserva = pr.findUltimaReservaId();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return idReserva;
	}

}
