package logic;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.HistorialDTO;
import persistence.gateway.HistorialGateway;
import persistence.impl.HistorialGatewayImpl;
import persistence.util.Jdbc;

public class RegistrarHistorialBusiness {

	private HistorialDTO h;

	public RegistrarHistorialBusiness(HistorialDTO h) {
		this.h = h;
	}

	public void execute() {
		Connection c = null;

		try {
			c = Jdbc.getConnection();

			HistorialGateway r = new HistorialGatewayImpl();
			r.setConnection(c);
			r.add(h);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
