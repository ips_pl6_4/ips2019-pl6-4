package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.ActividadDTO;
import logic.dto.RecursoDTO;
import persistence.gateway.RecursoActividadGateway;
import persistence.gateway.RecursoGateway;
import persistence.impl.ActividadGatewayImpl;
import persistence.impl.RecursoActividadGatewayImpl;
import persistence.impl.RecursoGatewayImpl;
import persistence.util.Jdbc;

public class CrearActividadBusiness {

	private ActividadDTO actividad;
	private List<RecursoDTO> recursos;
	private List<RecursoDTO> recursosNuevos;

	private Connection c;

	public CrearActividadBusiness(ActividadDTO actividad, List<RecursoDTO> recursos, List<RecursoDTO> recursosNuevos) {
		this.actividad = actividad;
		this.recursos = recursos;
		this.recursosNuevos = recursosNuevos;
	}

	public void execute() {
		try {
			c = Jdbc.getConnection();

			ActividadGatewayImpl pa = new ActividadGatewayImpl();
			pa.setConnection(c);

			pa.add(actividad);
			int idActividad = pa.maxId();

			crearRecursosNuevos();

			crearRecursosActividad(idActividad);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void crearRecursosNuevos() {
		RecursoGateway pr = new RecursoGatewayImpl();
		pr.setConnection(c);

		for (RecursoDTO nuevoRecurso : recursosNuevos) {
			pr.add(nuevoRecurso.nombre);

			RecursoDTO rec = nuevoRecurso;
			rec.idRecurso = pr.maxId();

			recursos.add(rec);
		}
	}

	private void crearRecursosActividad(int idActividad) {
		RecursoActividadGateway pra = new RecursoActividadGatewayImpl();
		pra.setConnection(c);

		for (RecursoDTO r : recursos) {
			pra.add(idActividad, r.idRecurso);
		}
	}
}
