package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.AlquilerDTO;
import persistence.gateway.AlquilerGateway;
import persistence.impl.AlquilerGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerAlquileresBusiness {
	
	private int idSocio;

	public ObtenerAlquileresBusiness(int id) {
		this.idSocio = id;
	}

	public List<AlquilerDTO> execute() {
		List<AlquilerDTO> lista = null;
		Connection c = null;
		try {
			c = Jdbc.getConnection();
			c.setAutoCommit(false);

			AlquilerGateway pa = new AlquilerGatewayImpl();
			pa.setConnection(c);

			lista = pa.findByUser(idSocio);

			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return lista;
	}

}
