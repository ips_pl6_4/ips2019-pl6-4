package logic;
/**
 * Clase para agrupar los m�todos de control de datos
 * @author Ana Garcia
 * 5-10-2019
 *
 */
public class Control {
	/**
	 * M�todo que intenta convetir un String en un entero
	 * @param entero String con un posible entero
	 * @return el entero leido o una IllegalArgumentException si el valor no era un entero
	 */
	public static int convertirInt(String entero) {
		try {
			return Integer.parseInt(entero);
		}catch(NumberFormatException e) {
			throw new IllegalArgumentException("El valor introducido no es un entero");
		}

	}

}
