package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import persistence.gateway.ReservaGateway;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class CrearGrupoReservasBusiness {

	private List<Integer> idsReservas;

	private Connection c;

	public CrearGrupoReservasBusiness(List<Integer> idsReservas) {
		this.idsReservas = idsReservas;
	}

	public void execute() {
		try {
			c = Jdbc.getConnection();

			ReservaGateway pr = new ReservaGatewayImpl();
			pr.setConnection(c);

			int idGrupo = pr.findUltimoGrupoId();
			idGrupo++;

			for (int idReserva : idsReservas) {
				pr.vincularReservaGrupo(idGrupo, idReserva);
			}

			c.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
