package logic;
/**
 * Como administraci�n quiero ver la ocupaci�n de las instalaciones 
 * para conocer su disponibilidad
 * 
 * Para una instalaci�n seleccionada, se mostrar� en franjas horarias de una hora, de 08:00 a 22:59, su ocupaci�n 
 * de siete en siete d�as, comenzando por el lunes de la semana actual. Ser� necesario distinguir mediante un c�digo de colores las 
 * reservas para actividades del centro y los huecos libres.
 * 
 * 
 * La franja horaria se ve en una reserva, deber� ir iterando por las reservas
 */
import java.util.List;

import logic.util.container.Alquiler;
import persistence.VerOcupacionInstalacionPersistencia;
/**
 * Clase que se encarga de la l�gica para mostrar un horario con la disponibilidad de una instalaci�n
 * @author Ana Garc�a
 * 8-10-2019
 */
public class VerOcupacionInstalacion {
	/**
	 * Id de la instalaci�n a comprobar
	 */
	private int idInstalacion;

	private String[] cabeceras = {"Horario","Lunes","Martes","Mi�rcoles","Jueves","Viernes","S�bado","Domingo"};
	private String[] horario = {"","8:00 a 8:59","9:00 a 9:59","10:00 a 10:59","11:00 a 11:59","12:00 a 12:59","13:00 a 13:59","14:00 a 14:59","15:00 a 15:59",
			"16:00 a 16:59","17:00 a 17:59","18:00 a 18:59","19:00 a 19:59","20:00 a 20:59","21:00 a 21:59","22:00 a 22:59"};
	/**
	 * Constructor con un par�metro de la clase
	 * @param idInstalacion Id de la instalaci�n a comprobar
	 */
	public VerOcupacionInstalacion(int idInstalacion) {

		setId(idInstalacion);

	}
	/**
	 * M�todo que establece el id de la instalaci�n a comprobar
	 * @param idInstalacion Id de la instalaci�n a comprobar
	 */
	private void setId(int idInstalacion) {
		this.idInstalacion = idInstalacion;

	}
	/**
	 * M�todo que muestra los alquileres de la semana de la instalaci�n
	 * @return Lista con los alquileres
	 */
	public String[][] mostrarDisponibilidad(String dia) {
		String [][] horario = creaHorarioLimpio();
		VerOcupacionInstalacionPersistencia vep = new VerOcupacionInstalacionPersistencia();
		String[] diaHoy = dia.split(" "); //AlquilerParser.getDiaString(new java.util.Date())
		List<Alquiler> alquileres = vep.getOcupacionInstalacion(idInstalacion,diaHoy);
		int diaSemanaHoy = VerOcupacionInstalacion.getDiaSemana(diaHoy[0]);
		int lunes = Control.convertirInt(diaHoy[1])-diaSemanaHoy+1;
		for(int i = 0; i< alquileres.size();i++) {
			int columna = Control.convertirInt(alquileres.get(i).getDia().toString().split("-")[2])-lunes+1;
			int filaInicio = alquileres.get(i).getHoraInicio()-7;
			int filaFinal = alquileres.get(i).getHoraFinal()-7;
			for(int j = 0; j<filaFinal-filaInicio;j++) {
				horario[filaInicio+j][columna]="Ocupado";
			}
		}
//		for(int i = 0; i<horario.length;i++) {
//			for(int j = 0; j< horario[0].length;j++) {
//				System.out.print(horario[i][j]+"   ");
//			}
//			System.out.println("");
		//}
		return horario;
	}

	public String[][] creaHorarioLimpio() {
		String[][] matriz = new String[horario.length][cabeceras.length];
		for (int fila = 0; fila<horario.length;fila++) {
			for(int columna = 0; columna<cabeceras.length;columna++) {
				if(columna==0 && fila==0) {
					matriz[fila][columna]=cabeceras[columna];
				}
				else if(fila==0){
					matriz[fila][columna]=cabeceras[columna];
				}else if(columna == 0) {
					matriz[fila][columna]=horario[fila];
				}
				else {
					matriz[fila][columna] = "Disponible";
				}
			}
		}
		return matriz;
	}
	/**
	 * M�todo que devuelve el n�mero del d�a de la semana con formato
	 * Sun, Mon, Tue, Wed, Thu, Fri, Sat
	 * @param dia String con el dia en el formato anterior
	 * @return Numero del dia de la semana
	 */
	public static int getDiaSemana(String dia) {
		switch(dia) {
		case "Mon":
			return 1;
		case "Tue":
			return 2;
		case "Wed":
			return 3;
		case "Thu":
			return 4;
		case "Fri":
			return 5;
		case "Sat":
			return 6;
		default:
			return 7;
		}

	}
}
