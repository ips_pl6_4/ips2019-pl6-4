package logic;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.ActividadDTO;
import persistence.impl.ActividadGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerActividadBusiness {

	private int idActividad;

	public ObtenerActividadBusiness(int idActividad) {
		this.idActividad = idActividad;
	}

	public ActividadDTO execute() {
		ActividadDTO actividad = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();
			c.setAutoCommit(false);

			ActividadGatewayImpl pa = new ActividadGatewayImpl();
			pa.setConnection(c);

			actividad = pa.findById(idActividad);

			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return actividad;
	}

}
