package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.SocioDTO;
import persistence.gateway.SocioGateway;
import persistence.impl.SocioGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerSociosBusiness {

	public List<SocioDTO> execute() {
		List<SocioDTO> lista = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();

			SocioGateway s = new SocioGatewayImpl();
			s.setConnection(c);

			lista = s.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

}
