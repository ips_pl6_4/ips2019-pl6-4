package logic;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import logic.dto.AlquilerDTO;
import logic.dto.InstalacionDTO;
import persistence.AlquilerSinPagarPersistencia;
import persistence.RegistrarPagoPersistencia;

public class RegistrarPago {

	public List<AlquilerDTO> getAlquileres(int idSocio) {		
		return new AlquilerSinPagarPersistencia().getAlquileres(idSocio,"NO PAGADA");
	}

	public List<InstalacionDTO> getInstalaciones() {		
		return new AlquilerSinPagarPersistencia().getInstalaciones();
	}

	public int pagoCuota(int idAlquiler) {
		Calendar c = Calendar.getInstance();
		int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11		
		int year = c.get(Calendar.YEAR);
		int day = c.get(Calendar.DATE);
		
		if(day > 19)//va para el siguiente mes
			mes += 2;
		else
			mes += 1;
		
		String sMes;
		if (mes < 10)
			sMes = "0" + mes;
		else if(mes > 12)
			sMes = "" + (mes-12);
		else
			sMes = "" + mes;	
		
		String fecha = "" + year + "-" + sMes + "-01";		
		Date fechaSQL = Date.valueOf(fecha);
		return new RegistrarPagoPersistencia().pagoCuota(idAlquiler,fechaSQL);
	}

}
