package logic;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.InstalacionDTO;
import persistence.gateway.InstalacionGateway;
import persistence.impl.InstalacionGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerInstalacionBusiness {

	private int idInstalacion;

	public ObtenerInstalacionBusiness(int idInstalacion) {
		this.idInstalacion = idInstalacion;
	}

	public InstalacionDTO execute() {
		InstalacionDTO instalacion = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();
			c.setAutoCommit(false);

			InstalacionGateway i = new InstalacionGatewayImpl();
			i.setConnection(c);

			instalacion = i.findById(idInstalacion);

			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return instalacion;
	}

}
