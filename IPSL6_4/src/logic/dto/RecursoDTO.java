package logic.dto;

/**
 * Objeto de transferencia de datos de la entidad Recurso
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public class RecursoDTO {
	public int idRecurso;
	public String nombre;
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof RecursoDTO)
			if(idRecurso == ((RecursoDTO) o).idRecurso)
				return true;
		return false;
	}
}
