package logic.dto;

/**
 * Objeto de transferencia de datos de la entidad Incidencia
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public class IncidenciaDTO {
	public int reservaID;
	public String incidencia;
}
