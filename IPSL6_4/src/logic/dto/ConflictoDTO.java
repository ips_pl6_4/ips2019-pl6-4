package logic.dto;

import java.util.Date;

public class ConflictoDTO {
	public int idInstalacion;

	public Date fechaReservaPlanificada;
	public int idReservaPlanificada;
	public String nombreActividadPlanificada;
	public int horaInicialReservaPlanificada;
	public int horaFinalReservaPlanificada;

	public Date fechaNuevaReserva;
	public String nombreNuevaActividad;
	public int horaInicialNuevaReserva;
	public int horaFinalNuevaReserva;

	@Override
	public String toString() {
		String toString = "";

		toString += "Conflicto en la instalación con ID " + idInstalacion + " con fecha " + fechaReservaPlanificada
				+ "\n";
		toString += "\tDatos de la actividad que genera el conflicto:\n";
		toString += "\t\tID: " + idReservaPlanificada + "\n";
		toString += "\t\tNombre: " + nombreActividadPlanificada + "\n";
		toString += "\t\tHora inicio: " + horaInicialReservaPlanificada + "\n";
		toString += "\t\tHora fin: " + horaFinalReservaPlanificada + "\n";

		toString += "\tDatos de la nueva actividad:\n";
		toString += "\t\tNombre: " + nombreNuevaActividad + "\n";
		toString += "\t\tHora inicio: " + horaInicialNuevaReserva + "\n";
		toString += "\t\tHora fin: " + horaFinalNuevaReserva + "\n";

		return toString;
	}
}
