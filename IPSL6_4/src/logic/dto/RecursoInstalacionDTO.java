package logic.dto;

/**
 * Objeto de transferencia de datos de la relacion Recurso_Instalacion
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public class RecursoInstalacionDTO {
	public int idRecurso;
	public int idInstalacion;
	public String nombre;
	public int cantidad;
}
