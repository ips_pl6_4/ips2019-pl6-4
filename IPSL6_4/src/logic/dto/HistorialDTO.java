package logic.dto;

public class HistorialDTO {

	public int idHistorial;
	public int idReserva;
	public int idInstalacionAnterior;
	public int horaInicialAnterior;
	public int horaFinalAnterior;
	public java.sql.Date fechaAnterior;
	public boolean isPlazasLimitadasAnterior;
	public int numPlazasAnterior;
	public String estado;
}
