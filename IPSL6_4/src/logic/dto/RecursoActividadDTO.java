package logic.dto;

/**
 * Objeto de transferencia de datos de la relacion Recurso_Actividad
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public class RecursoActividadDTO {
	public int idRecurso;
	public int idActividad;
}
