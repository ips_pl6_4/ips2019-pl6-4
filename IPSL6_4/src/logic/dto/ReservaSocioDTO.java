package logic.dto;

import java.sql.Date;
import java.util.Comparator;

/**
 * Objeto de transferencia de datos de la entidad ReservaSocio
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 3.12.2019
 */
public class ReservaSocioDTO {
	public int reservaSocioID;
	public int socioID;
	public int instalacionID;
	public int horaInicial;
	public int horaFinal;
	public Date fecha;
	public Date fechaPago;
	public String estadoPago;

	public static Comparator<ReservaSocioDTO> fromAfterToBeforeComparator() {
		return new ReservaSocioDTOFromAfterToBeforeComparator();
	}
}

/**
 * Clase auxiliar ReservaSocioDTOFromAfterToBeforeComparator. Comparador entre
 * dos objetos ReservaSocioDTO, teniendo mas preferencia aquel cuya fecha de
 * pago sea posterior a la del otro.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 4.12.2019
 */
class ReservaSocioDTOFromAfterToBeforeComparator implements Comparator<ReservaSocioDTO> {

	@Override
	public int compare(ReservaSocioDTO res1, ReservaSocioDTO res2) {
		if (res1 == null && res2 == null)
			return 0;
		else if (res1 == null)
			return -1;
		else if (res2 == null)
			return 1;
		else if (res1.fechaPago.after(res2.fechaPago))
			return 1;
		else if (res1.fechaPago.before(res2.fechaPago))
			return -1;
		return 0;
	}
}