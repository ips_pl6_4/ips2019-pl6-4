package logic.dto;

/**
 * Objeto de transferencia de datos de la relacion Imparte.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 10.10.2019
 */
public class ImparteDTO {
	public int monitorID;
	public int reservaID;
}
