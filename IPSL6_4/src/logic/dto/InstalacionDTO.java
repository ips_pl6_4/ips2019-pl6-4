package logic.dto;

/**
 * Objeto de transferencia de datos de la entidad Instalacion
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public class InstalacionDTO {
	public int idInstalacion;
	public String nombre;
	public String descripcion;
	public int precioHora;
}
