package logic.dto;

/**
 * Objeto de transferencia de datos de la entidad Monitor.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 10.10.2019
 */
public class MonitorDTO {
	public int monitorID;
	public String name;
}
