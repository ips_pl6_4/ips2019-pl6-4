package logic.dto;

import java.sql.Date;

/**
 * Objeto de transferencia de datos de la entidad Reserva
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public class ReservaDTO {
	public int idReserva;
	public int idActividad;
	public int idInstalacion;
	public Date fecha;
	public int horaInicial;
	public int horaFinal;
	public boolean isPlazasLimitadas;
	public int numPlazas;
}
