package logic.dto;

/**
 * Enumerado Intensidad
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public enum Intensidad {

	BAJA, MODERADA, ALTA

}
