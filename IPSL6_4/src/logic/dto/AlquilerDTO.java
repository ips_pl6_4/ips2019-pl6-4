package logic.dto;

import java.sql.Date;

public class AlquilerDTO {
	public int idReservaSocio;
	public int idSocio;
	public int idInstalacion;
	public int horaInicial;
	public int horaFinal;
	public Date fecha;
	public Date fechaPago;
	public String estadoPago;

	@Override
	public String toString() {
		String toString = "ID ALQUILER: " + idReservaSocio;
		toString += "\n\tID SOCIO: " + idSocio;
		toString += "\n\tID INSTALACION: " + idInstalacion;
		toString += "\n\tHORA INICIAL: " + horaInicial;
		toString += "\n\tHORA FINAL: " + horaFinal;
		toString += "\n\tFECHA: " + fecha;

		return toString;
	}

}
