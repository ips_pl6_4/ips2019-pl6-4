package logic.dto;

/**
 * Objeto de transferencia de datos de la entidad Actividad
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 26.10.2019
 */
public class ActividadDTO {
	@Override
	public String toString() {
		return "Actividad [idActividad=" + idActividad + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", intensidad=" + intensidad + "]";
	}

	public int idActividad;
	public String nombre;
	public String descripcion;
	public Intensidad intensidad;
}
