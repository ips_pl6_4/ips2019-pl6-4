package logic.dto;

/**
 * Objeto de transferencia de datos de la relacion Asistencia.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 10.10.2019
 */
public class AsistenciaDTO {
	public int reservaID;
	public int socioID;
}
