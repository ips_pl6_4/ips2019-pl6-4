package logic.dto;

/**
 * Objeto de transferencia de datos de la entidad Socio.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class SocioDTO {
	public int socioID;
	public String name;
	public String apellidos;
	public int telefono;
	public String mail;
}
