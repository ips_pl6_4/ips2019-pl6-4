package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.MonitorDTO;
import persistence.impl.MonitorGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerMonitorBusiness {
	
	public List<MonitorDTO> execute() {
		List<MonitorDTO> lista = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();
			c.setAutoCommit(false);

			MonitorGatewayImpl pa = new MonitorGatewayImpl();
			pa.setConnection(c);

			lista = pa.findAll();

			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return lista;
	}

}
