package logic;

import persistence.VerAsistentesActividadPersistencia;

public class VerAsistentesActividad {

	public static String obtenAsistentes(int id) {
		return VerAsistentesActividadPersistencia.obtenAsistentes(id);
	}

	public static String obtenActividadesMonitor(int monitor) {
		return VerAsistentesActividadPersistencia.obtenActividadesMonitor(monitor);
	}

	public static boolean compruebaInicioReserva(int reserva) {
		return VerAsistentesActividadPersistencia.compruebaInicioReserva(reserva);
	}

}
