package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.RecursoDTO;
import persistence.gateway.RecursoGateway;
import persistence.impl.RecursoGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerRecursosBusiness {

	public List<RecursoDTO> execute() {
		List<RecursoDTO> lista = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();

			RecursoGateway rg = new RecursoGatewayImpl();
			rg.setConnection(c);

			lista = rg.findAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return lista;
	}

}
