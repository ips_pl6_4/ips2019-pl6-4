package logic.service.monitor;

import java.util.List;

import logic.dto.AsistenciaDTO;
import logic.dto.IncidenciaDTO;
import logic.dto.ReservaDTO;
import logic.dto.SocioDTO;

/**
 * Clase factoria de servicio
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class MonitorServicesFactory {
	public static List<SocioDTO> getSociosSinAsistencia(ReservaDTO asistencia) {
		ServiceSociosSinAsistencia sssa = new ServiceSociosSinAsistencia();
		sssa.setData(asistencia);
		return sssa.getData();
	}

	public static List<ReservaDTO> getReservasDisponibles() {
		ServiceReservasDisponibles srd = new ServiceReservasDisponibles();
		return srd.getData();
	}

	public static void setRegistrarAsistencia(AsistenciaDTO asistencia) {
		ServiceRegistrarAsistencia sra = new ServiceRegistrarAsistencia();
		sra.setData(asistencia);
	}

	public static SocioDTO getSocioPorID(int socioID) {
		ServiceSocioPorID sspid = new ServiceSocioPorID();
		sspid.setData(socioID);
		return sspid.getData();
	}

	public static void setInsertarIncidencia(IncidenciaDTO incidencia) {
		ServiceInsertarIncidencia sii = new ServiceInsertarIncidencia();
		sii.setData(incidencia);
	}

	public static List<IncidenciaDTO> getObtenerIncidencia(int reservaID) {
		ServiceObtenerIncidencias soi = new ServiceObtenerIncidencias();
		soi.setData(reservaID);
		return soi.getData();
	}

	public static int getCantidadAsistencias(int reservaID) {
		ServiceCantidadAsistencias sca = new ServiceCantidadAsistencias();
		sca.setData(reservaID);
		return sca.getData().intValue();
	}
}
