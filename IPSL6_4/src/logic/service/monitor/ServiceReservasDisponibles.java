package logic.service.monitor;

import java.util.List;

import logic.dto.ReservaDTO;
import logic.service.ServiceGet;
import logic.transactionScripts.monitor.ReservasDisponibles;

/**
 * Implementacion del servicio que lista las reservas disponibles
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 20.10.2019
 */
public class ServiceReservasDisponibles implements ServiceGet<List<ReservaDTO>> {

	private List<ReservaDTO> reservas;

	@Override
	public void execute() {
		ReservasDisponibles rd = new ReservasDisponibles();
		rd.execute();
		this.reservas = rd.reservas;
	}

	@Override
	public List<ReservaDTO> getData() {
		if (this.reservas == null)
			execute();
		return this.reservas;
	}

}
