package logic.service.monitor;

import logic.dto.AsistenciaDTO;
import logic.service.ServiceSet;
import logic.transactionScripts.BusinessException;
import logic.transactionScripts.monitor.RegistrarAsistencia;

/**
 * Implementacion del servicio que registra una asistencia
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 20.10.2019
 */
public class ServiceRegistrarAsistencia implements ServiceSet<AsistenciaDTO> {

	private AsistenciaDTO asistencia;

	@Override
	public void execute() {
		RegistrarAsistencia ra = new RegistrarAsistencia(this.asistencia);
		try {
			ra.execute();
		} catch (BusinessException e) {
		}
	}

	@Override
	public void setData(AsistenciaDTO data) {
		this.asistencia = data;
		execute();
	}

}
