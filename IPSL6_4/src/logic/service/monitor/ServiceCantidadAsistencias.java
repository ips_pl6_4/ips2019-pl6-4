package logic.service.monitor;

import logic.service.ServiceGet;
import logic.service.ServiceSet;
import logic.transactionScripts.monitor.CantidadAsistencias;

/**
 * Implementacion del servicio que cuenta las asistencias a la reserva de una
 * actividad
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class ServiceCantidadAsistencias implements ServiceGet<Integer>, ServiceSet<Integer> {

	private int reservaID;
	public int asistencias;

	@Override
	public void setData(Integer data) {
		if (data != null)
			this.reservaID = data.intValue();
	}

	@Override
	public void execute() {
		CantidadAsistencias ca = new CantidadAsistencias(this.reservaID);
		this.asistencias = ca.asistencias;
	}

	@Override
	public Integer getData() {
		execute();
		return asistencias;
	}

}
