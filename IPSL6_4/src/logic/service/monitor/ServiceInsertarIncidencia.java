package logic.service.monitor;

import logic.dto.IncidenciaDTO;
import logic.service.ServiceSet;
import logic.transactionScripts.BusinessException;
import logic.transactionScripts.monitor.InsertarIncidencia;

/**
 * Implementacion del servicio que inserta una incidencia a una reserva de una
 * actividad
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class ServiceInsertarIncidencia implements ServiceSet<IncidenciaDTO> {

	private IncidenciaDTO incidencia;

	@Override
	public void execute() {
		InsertarIncidencia ii = new InsertarIncidencia(this.incidencia);
		try {
			ii.execute();
		} catch (BusinessException e) {
		}
	}

	@Override
	public void setData(IncidenciaDTO data) {
		this.incidencia = data;
		execute();
	}

}
