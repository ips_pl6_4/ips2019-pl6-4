package logic.service.monitor;

import java.util.List;

import logic.dto.IncidenciaDTO;
import logic.service.ServiceGet;
import logic.service.ServiceSet;
import logic.transactionScripts.monitor.ObtenerIncidencias;

/**
 * Implementacion del servicio que obtiene todas las incidencias de una reserva
 * a una actividad.
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class ServiceObtenerIncidencias implements ServiceGet<List<IncidenciaDTO>>, ServiceSet<Integer> {

	private Integer reservaID;
	public List<IncidenciaDTO> incidencias;

	@Override
	public void setData(Integer data) {
		this.reservaID = data;
	}

	@Override
	public void execute() {
		ObtenerIncidencias oi = new ObtenerIncidencias(this.reservaID.intValue());
		oi.execute();
		this.incidencias = oi.incidencias;
	}

	@Override
	public List<IncidenciaDTO> getData() {
		if (this.incidencias == null)
			execute();
		return this.incidencias;
	}

}
