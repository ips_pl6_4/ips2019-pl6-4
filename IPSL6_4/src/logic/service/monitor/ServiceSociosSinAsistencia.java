package logic.service.monitor;

import java.util.List;

import logic.dto.ReservaDTO;
import logic.dto.SocioDTO;
import logic.service.ServiceGet;
import logic.service.ServiceSet;
import logic.transactionScripts.monitor.SociosSinAsistencia;

/**
 * Implementacion del servicio que lista los socios que no asisten a una
 * actividad dada
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class ServiceSociosSinAsistencia implements ServiceSet<ReservaDTO>, ServiceGet<List<SocioDTO>> {

	private ReservaDTO reserva;
	public List<SocioDTO> sociosSinAsistencia;

	@Override
	public List<SocioDTO> getData() {
		if (this.sociosSinAsistencia == null)
			execute();
		return this.sociosSinAsistencia;
	}

	@Override
	public void execute() {
		SociosSinAsistencia ssa = null;
		if (this.reserva != null) {
			ssa = new SociosSinAsistencia(this.reserva.idReserva);
			ssa.execute();
			this.sociosSinAsistencia = ssa.noAsistentes;
		}
	}

	@Override
	public void setData(ReservaDTO data) {
		this.reserva = data;
	}

}
