package logic.service.monitor;

import logic.dto.SocioDTO;
import logic.service.ServiceGet;
import logic.service.ServiceSet;
import logic.transactionScripts.monitor.SocioPorID;

/**
 * Implementacion del servicio que devuelve la entrada de un socio dada su ID
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 16.11.2019
 */
public class ServiceSocioPorID implements ServiceSet<Integer>, ServiceGet<SocioDTO> {

	private Integer socioID;
	public SocioDTO socio;

	@Override
	public SocioDTO getData() {
		if (this.socio == null)
			execute();
		return this.socio;
	}

	@Override
	public void execute() {
		SocioPorID spid = null;
		if (this.socioID != null) {
			spid = new SocioPorID(this.socioID.intValue());
			spid.execute();
			this.socio = spid.socio;
		}
	}

	@Override
	public void setData(Integer data) {
		this.socioID = data;
	}

}
