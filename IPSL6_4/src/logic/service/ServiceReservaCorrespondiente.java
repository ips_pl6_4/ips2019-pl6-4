package logic.service;

import logic.dto.AsistenciaDTO;
import logic.dto.ReservaDTO;
import logic.transactionScripts.BusinessException;
import logic.transactionScripts.ReservaCorrespondiente;

/**
 * Implementacion del servicio que busca la reserva correspondiente a una
 * asistencia
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 13.11.2019
 */
public class ServiceReservaCorrespondiente implements ServiceGet<ReservaDTO>, ServiceSet<AsistenciaDTO> {

	private AsistenciaDTO asistencia;
	private ReservaDTO reserva;

	@Override
	public void execute() {
		ReservaCorrespondiente aC = new ReservaCorrespondiente(this.asistencia);
		aC.execute();
		this.reserva = aC.reserva;
	}

	@Override
	public ReservaDTO getData() {
		execute();
		return this.reserva;
	}

	@Override
	public void setData(AsistenciaDTO data) throws BusinessException {
		if (data == null)
			throw new BusinessException("Reservas nulas no corresponden a ninguna actividad");
		else
			this.asistencia = data;
	}

}