package logic.service;

import logic.dto.ActividadDTO;
import logic.dto.ReservaDTO;
import logic.transactionScripts.ActividadCorrespondiente;
import logic.transactionScripts.BusinessException;

/**
 * Implementacion del servicio que busca la actividad correspondiente a una
 * reserva
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public class ServiceActividadCorrespondiente implements ServiceGet<ActividadDTO>, ServiceSet<ReservaDTO> {

	private ActividadDTO actividad;
	private ReservaDTO reserva;

	@Override
	public void execute() {
		ActividadCorrespondiente aC = new ActividadCorrespondiente(this.reserva);
		aC.execute();
		this.actividad = aC.actividad;
	}

	@Override
	public ActividadDTO getData() {
		execute();
		return this.actividad;
	}

	@Override
	public void setData(ReservaDTO data) throws BusinessException {
		if (data == null)
			throw new BusinessException("Reservas nulas no corresponden a ninguna actividad");
		else
			this.reserva = data;
	}

}