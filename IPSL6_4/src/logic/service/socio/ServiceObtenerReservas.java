package logic.service.socio;

import java.util.List;

import logic.dto.ReservaSocioDTO;
import logic.service.ServiceGet;
import logic.service.ServiceSet;
import logic.transactionScripts.socio.ObtenerReservas;

/**
 * Implementacion del servicio que obtiene las reservas pertenecientes a un
 * socio dado
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 3.12.2019
 */
public class ServiceObtenerReservas implements ServiceGet<List<ReservaSocioDTO>>, ServiceSet<Integer> {

	private int socioID;
	private List<ReservaSocioDTO> reservas;

	@Override
	public void setData(Integer data) {
		if (data != null)
			this.socioID = data.intValue();
	}

	@Override
	public void execute() {
		ObtenerReservas or = new ObtenerReservas(this.socioID);
		or.execute();
		this.reservas = or.reservas;
	}

	@Override
	public List<ReservaSocioDTO> getData() {
		if (this.reservas == null)
			execute();
		return this.reservas;
	}

}
