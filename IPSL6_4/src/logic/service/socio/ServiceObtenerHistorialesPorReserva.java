package logic.service.socio;

import java.util.List;

import logic.dto.HistorialDTO;
import logic.service.ServiceGet;
import logic.service.ServiceSet;
import logic.transactionScripts.socio.ObtenerHistorialesPorReserva;

/**
 * Implementacion del servicio que obtiene los historiales de una reserva
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 17.11.2019
 */
public class ServiceObtenerHistorialesPorReserva implements ServiceGet<List<HistorialDTO>>, ServiceSet<Integer> {

	private int reservaID;
	private List<HistorialDTO> historiales;

	@Override
	public void setData(Integer data) {
		if (data != null)
			this.reservaID = data.intValue();
	}

	@Override
	public void execute() {
		ObtenerHistorialesPorReserva ohpr = new ObtenerHistorialesPorReserva(this.reservaID);
		ohpr.execute();
		this.historiales = ohpr.historiales;
	}

	@Override
	public List<HistorialDTO> getData() {
		if (this.historiales == null)
			execute();
		return this.historiales;
	}

}
