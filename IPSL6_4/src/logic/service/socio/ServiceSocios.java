package logic.service.socio;

import java.util.List;

import logic.dto.SocioDTO;
import logic.service.ServiceGet;
import logic.transactionScripts.socio.Socios;

/**
 * Implementacion del servicio que lista los socios
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 12.11.2019
 */
public class ServiceSocios implements ServiceGet<List<SocioDTO>> {

	public List<SocioDTO> socios;

	@Override
	public List<SocioDTO> getData() {
		if (this.socios == null)
			execute();
		return this.socios;
	}

	@Override
	public void execute() {
		Socios s = null;
		s = new Socios();
		s.execute();
		this.socios = s.socios;
	}

}
