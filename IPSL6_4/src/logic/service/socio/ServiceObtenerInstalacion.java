package logic.service.socio;

import logic.dto.InstalacionDTO;
import logic.service.ServiceGet;
import logic.service.ServiceSet;
import logic.transactionScripts.socio.ObtenerInstalacion;

public class ServiceObtenerInstalacion implements ServiceGet<InstalacionDTO>, ServiceSet<Integer> {

	private int instalacionID;
	private InstalacionDTO instalacion;

	@Override
	public void setData(Integer data) {
		if (data != null)
			this.instalacionID = data.intValue();
	}

	@Override
	public void execute() {
		ObtenerInstalacion oI = new ObtenerInstalacion(this.instalacionID);
		oI.execute();
		this.instalacion = oI.instalacion;
	}

	@Override
	public InstalacionDTO getData() {
		if (this.instalacion == null)
			execute();
		return this.instalacion;
	}

}
