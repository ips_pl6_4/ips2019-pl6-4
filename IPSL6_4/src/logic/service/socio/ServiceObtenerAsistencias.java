package logic.service.socio;

import java.util.List;

import logic.dto.AsistenciaDTO;
import logic.service.ServiceGet;
import logic.service.ServiceSet;
import logic.transactionScripts.BusinessException;
import logic.transactionScripts.socio.ObtenerAsistencias;

/**
 * Implementacion del servicio que obtiene las asistencias de un socio
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 12.11.2019
 */
public class ServiceObtenerAsistencias implements ServiceSet<Integer>, ServiceGet<List<AsistenciaDTO>> {

	private int socioID;
	private List<AsistenciaDTO> asistencias;

	@Override
	public void execute() {
		ObtenerAsistencias oA = new ObtenerAsistencias(this.socioID);
		try {
			oA.execute();
			this.asistencias = oA.asistencias;
		} catch (BusinessException e) {
		}
	}

	@Override
	public void setData(Integer data) {
		this.socioID = data.intValue();
	}

	@Override
	public List<AsistenciaDTO> getData() {
		execute();
		return this.asistencias;
	}

}
