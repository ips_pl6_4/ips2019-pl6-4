package logic.service.socio;

import java.util.List;

import logic.dto.ActividadDTO;
import logic.dto.AsistenciaDTO;
import logic.dto.HistorialDTO;
import logic.dto.InstalacionDTO;
import logic.dto.ReservaDTO;
import logic.dto.ReservaSocioDTO;
import logic.dto.SocioDTO;
import logic.service.ServiceActividadCorrespondiente;
import logic.service.ServiceReservaCorrespondiente;
import logic.transactionScripts.BusinessException;

/**
 * Clase factoria de servicio para el rol de socio
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 3.12.2019
 */
public class SocioServicesFactory {

	public static InstalacionDTO getObtenerInstalacion(int instalacionID) {
		ServiceObtenerInstalacion soi = new ServiceObtenerInstalacion();
		soi.setData(instalacionID);
		return soi.getData();
	}

	public static List<ReservaSocioDTO> getObtenerReservas(int socioID) {
		ServiceObtenerReservas sor = new ServiceObtenerReservas();
		sor.setData(socioID);
		return sor.getData();
	}

	public static List<HistorialDTO> getObtenerHistorialesPorReserva(int reservaID) {
		ServiceObtenerHistorialesPorReserva sohpr = new ServiceObtenerHistorialesPorReserva();
		sohpr.setData(reservaID);
		return sohpr.getData();
	}

	public static ReservaDTO getReservaCorrespondiente(AsistenciaDTO asistenciaDTO) {
		ServiceReservaCorrespondiente src = new ServiceReservaCorrespondiente();
		try {
			src.setData(asistenciaDTO);
		} catch (BusinessException e) {
			throw new RuntimeException("Reserva nula");
		}
		return src.getData();
	}

	public static ActividadDTO getActividadCorrespondiente(ReservaDTO reservaDTO) {
		ServiceActividadCorrespondiente sac = new ServiceActividadCorrespondiente();
		try {
			sac.setData(reservaDTO);
		} catch (BusinessException e) {
			throw new RuntimeException("Reserva nula");
		}
		return sac.getData();
	}

	public static List<AsistenciaDTO> getObtenerAsistencias(int socioID) {
		ServiceObtenerAsistencias soa = new ServiceObtenerAsistencias();
		soa.setData(socioID);
		return soa.getData();
	}

	public static List<SocioDTO> getSocios() {
		ServiceSocios ss = new ServiceSocios();
		return ss.getData();
	}
}
