package logic.service;

import logic.transactionScripts.BusinessException;

/**
 * Interfaz de Servicio desde la UI hacia la logica para obtener datos
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public interface ServiceGet<T> {
	/**
	 * Ejecuta el servicio pertinente
	 */
	public void execute() throws BusinessException;

	/**
	 * Devuelve el dato proporcionado por el servicio.
	 * 
	 * @return Dato
	 */
	public T getData() throws BusinessException;
}
