package logic.service.admin;

import java.util.List;

import logic.dto.ReservaDTO;
import logic.service.ServiceGet;
import logic.transactionScripts.admin.ReservasSinMonitor;

/**
 * Implementacion del servicio que proporciona las reservas sin monitor
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class ServiceReservasSinMonitor implements ServiceGet<List<ReservaDTO>> {

	private List<ReservaDTO> data;

	@Override
	public void execute() {
		ReservasSinMonitor rsm = new ReservasSinMonitor();
		rsm.execute();
		this.data = rsm.reservas;
	}

	@Override
	public List<ReservaDTO> getData() {
		if (this.data == null)
			execute();
		return this.data;
	}
}
