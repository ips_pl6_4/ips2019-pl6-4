package logic.service.admin;

import java.util.List;

import logic.dto.ActividadDTO;
import logic.dto.ImparteDTO;
import logic.dto.MonitorDTO;
import logic.dto.ReservaDTO;
import logic.service.ServiceActividadCorrespondiente;
import logic.transactionScripts.BusinessException;

/**
 * Clase factoria de servicio
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 2.11.2019
 */
public class AdminServicesFactory {
	public static List<ReservaDTO> getReservasSinMonitor() {
		return (new ServiceReservasSinMonitor()).getData();
	}

	public static List<MonitorDTO> getMonitoresSinAsignar() {
		return (new ServiceMonitoresSinAsignar()).getData();
	}

	public static boolean setImparticion(ImparteDTO imparticion) {
		ServiceAsignarMonitor sam = new ServiceAsignarMonitor();
		try {
			sam.setData(imparticion);
			return true;
		} catch (BusinessException e) {
			return false;
		}
	}

	public static List<MonitorDTO> getMonitores() {
		return (new ServiceMonitores()).getData();
	}

	public static ActividadDTO getActividadCorrespondiente(ReservaDTO reservaDTO) {
		ServiceActividadCorrespondiente sac = new ServiceActividadCorrespondiente();
		try {
			sac.setData(reservaDTO);
		} catch (BusinessException e) {
			throw new RuntimeException("Reserva nula");
		}
		return sac.getData();
	}
}
