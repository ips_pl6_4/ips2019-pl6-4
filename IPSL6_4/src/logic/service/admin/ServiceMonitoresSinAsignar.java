package logic.service.admin;

import java.util.List;

import logic.dto.MonitorDTO;
import logic.service.ServiceGet;
import logic.transactionScripts.admin.MonitoresSinAsignar;

/**
 * Implementacion del servicio que proporciona los monitores sin asignar
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class ServiceMonitoresSinAsignar implements ServiceGet<List<MonitorDTO>> {

	private List<MonitorDTO> data;

	@Override
	public void execute() {
		MonitoresSinAsignar rsm = new MonitoresSinAsignar();
		rsm.execute();
		this.data = rsm.monitores;
	}

	@Override
	public List<MonitorDTO> getData() {
		if (this.data == null)
			execute();
		return this.data;
	}
}
