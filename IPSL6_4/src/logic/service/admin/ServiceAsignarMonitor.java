package logic.service.admin;

import logic.dto.ImparteDTO;
import logic.service.ServiceSet;
import logic.transactionScripts.BusinessException;
import logic.transactionScripts.admin.AsignarMonitorSinAsignarEnMismaFecha;

/**
 * Implementacion del servicio que asigna un monitor a una reserva
 * 
 * @author Ivan Alvarez Lopez - UO264862
 * @version 19.10.2019
 */
public class ServiceAsignarMonitor implements ServiceSet<ImparteDTO> {

	private ImparteDTO imparticion;

	@Override
	public void execute() throws BusinessException {
		AsignarMonitorSinAsignarEnMismaFecha am = null;
		if (this.imparticion != null) {
			am = new AsignarMonitorSinAsignarEnMismaFecha(this.imparticion.monitorID, this.imparticion.reservaID);
			am.execute();
		}
	}

	@Override
	public void setData(ImparteDTO data) throws BusinessException {
		this.imparticion = data;
		execute();
	}

}
