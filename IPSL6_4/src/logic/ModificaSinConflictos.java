package logic;

import java.sql.Connection;
import java.sql.SQLException;

import logic.dto.ActividadDTO;
import logic.dto.Intensidad;
import persistence.gateway.ActividadGateway;
import persistence.impl.ActividadGatewayImpl;
import persistence.util.Jdbc;
/**
 * Clase para hacer modificaciones de actividades sin conflictos.
 * No se guardan en el historial porque el PO dijo que no era necesario
 * @author Ana Garcia
 *5-11-2019
 */
public class ModificaSinConflictos {

	public void modificaNombre(int idActividad, String nombre) {
		Connection con=null;
		try {
			con = Jdbc.getConnection();
		ActividadDTO a = encuentraActividad(idActividad,con);//Va a encontrar �a actividad porque la elije desde una tabla
		ActividadDTO b = new ActividadDTO();
		b.idActividad = a.idActividad;
		b.nombre =  nombre;
		b.descripcion = a.descripcion;
		b.intensidad = a.intensidad;
		actualiza(b,con);
		con.commit();
		}catch (SQLException e) {
			try {
				if(con!=null) {
					con.rollback();
				}	
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void modificaDescripcion(int idActividad,String descripcion) {
		Connection con=null;
		try {
			con = Jdbc.getConnection();
		ActividadDTO a = encuentraActividad(idActividad,con);//Va a encontrar �a actividad porque la elije desde una tabla
		ActividadDTO b = new ActividadDTO();
		b.idActividad = a.idActividad;
		b.nombre =  a.nombre;
		b.descripcion = descripcion;
		b.intensidad = a.intensidad;
		actualiza(b,con);
		con.commit();
		}catch (SQLException e) {
			try {
				if(con!=null) {
					con.rollback();
				}	
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void modificaIntensidad(int idActividad, Intensidad i ) {
		Connection con=null;
		try {
			con = Jdbc.getConnection();
		ActividadDTO a = encuentraActividad(idActividad,con);//Va a encontrar la actividad porque la elije desde una tabla
		ActividadDTO b = new ActividadDTO();
		b.idActividad = a.idActividad;
		b.nombre =  a.nombre;
		b.descripcion = a.descripcion;
		b.intensidad = i;
		actualiza(b,con);
		con.commit();
		}catch (SQLException e) {
			try {
				if(con!=null) {
					con.rollback();
				}	
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private void actualiza(ActividadDTO a, Connection con) {
		ActividadGateway ag = new ActividadGatewayImpl();
		ag.setConnection(con);
		ag.update(a);
		
	}
	private ActividadDTO encuentraActividad(int id, Connection con) throws SQLException {
		ActividadDTO dto = null;
		ActividadGateway ag = new ActividadGatewayImpl();
		ag.setConnection(con);
		dto = ag.findById(id);
		return dto;
	}
	
}
