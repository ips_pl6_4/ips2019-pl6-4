package logic;

import java.util.List;

import logic.util.container.Alquiler;
import persistence.VerHorarioActividadesPersistencia;

/**
 * Clase para ver el horario de las actividades a partir del dia de hoy
 * 
 * @author Ana Garcia 11-10-2019
 */
public class VerHorarioActividades {
	/**
	 * M�todo que devuelve el horario de las actividades por d�as
	 * 
	 * @return String con los d�as y sus actividades a partir del d�a actual
	 */
	public String getHorarioActividades() {
		VerHorarioActividadesPersistencia vha = new VerHorarioActividadesPersistencia();
		List<Alquiler> alquileres = vha.getActividades();
		if (alquileres.size() == 0) {
			throw new IllegalArgumentException("No hay actividades planeadas pr�ximamente");
		}
		StringBuilder sb = new StringBuilder();
		String DiaActual = "";
		for (Alquiler a : alquileres) {
			if (!a.getDia().toString().equals(DiaActual)) {
				sb.append(a.getDia().toString() + ": \n\n");
				DiaActual = a.getDia().toString();
			}
			sb.append("Actividad: " + a.getActividad() + " Hora inicio: " + a.getHoraInicio() + " Hora final: "
					+ a.getHoraFinal() + " Plazas limitadas: ");
			if (a.getPlazasLimitadas()) {
				sb.append("No\n\n");
			} else {
				sb.append("S�\n\n");
			}
		}
		return sb.toString();
	}
}
