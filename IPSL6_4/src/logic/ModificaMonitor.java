package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import logic.dto.ReservaDTO;
import persistence.gateway.ImparteGateway;
import persistence.gateway.ReservaGateway;
import persistence.impl.ImparteGatewayImpl;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

/**
 * Clase que cambia el monitor que imparte una reserva siempre que �ste tega hueco y la eserva no se haya impartido o
 * se est� impartiendo
 * @author Ana Garcia
 * 6-11-2019
 *
 */
public class ModificaMonitor {
	
	public void modificaMecanico(int idReserva, int idMonitor) {
		Connection con=null;
		try {
			con = Jdbc.getConnection();
		ReservaDTO reserva = obtenReserva(idReserva, con);
		compruebaReserva(reserva);
		List<ReservaDTO> reservasDeMonitor = obtenReservasMonitor(idMonitor,con);
		compruebaCoincidenciaHorarios(idMonitor,reserva, reservasDeMonitor);
		cambiaMonitorReserva(idMonitor, reserva, con);
		con.commit();
		}catch (SQLException e) {
			try {
				if(con!=null) {
					con.rollback();
				}	
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}catch(IllegalArgumentException e) {
			try {
				if(con!=null) {
					con.rollback();
				}	
				throw e;
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private void compruebaReserva(ReservaDTO reserva) {
		// Comprobacion Rango correcto de horas para hacer la reserva
				String fechaReserva = reserva.fecha.toString();
				// String fechaActual = Calendar.getInstance().getTime().toString();
				// //Integer.parseInt(new
				// Date(Calendar.getInstance().getTime().getTime()).toString().substring(8)))
				Calendar c = Calendar.getInstance();
				// int dia = c.get(Calendar.DATE);
				int mes = c.get(Calendar.MONTH) + 1;// el +1 es porque van de 0 a 11
				String sMes;
				if (mes < 10)
					sMes = "0" + mes;
				else
					sMes = "" + mes;
				String fechaActual = c.get(Calendar.YEAR) + "-" + sMes + "-" + c.get(Calendar.DATE);
				boolean condicion1 = fechaActual.equals(fechaReserva);
				boolean condicion2 = c.get(Calendar.HOUR_OF_DAY) >= reserva.horaInicial;
				if ((condicion1 && condicion2)||reserva.fecha.compareTo(new Date())<=0) 
					throw new IllegalArgumentException("La reserva con id "+reserva.idReserva+" est� siendo o ya ha sido impartida, no se puede modificar el monitor que la imparte");

		
	}

	private void cambiaMonitorReserva(int idMonitor, ReservaDTO reserva, Connection con) {
		ImparteGateway ig = new ImparteGatewayImpl();
		ig.setConnection(con);
		ig.update(idMonitor, reserva.idReserva);
		
	}

	private void compruebaCoincidenciaHorarios(int idMonitor,ReservaDTO reserva, List<ReservaDTO> reservasDeMonitor) {
		for(ReservaDTO r : reservasDeMonitor) {
			if(r.fecha.compareTo(reserva.fecha)==0) {
				if((r.horaInicial<=reserva.horaInicial || r.horaFinal >= reserva.horaFinal)) {
					throw new IllegalArgumentException("El monitor con id "
				+ idMonitor+" no tiene hueco en su horario el d�a "
							+reserva.fecha+" de "+reserva.horaInicial+" a "
				+reserva.horaFinal+" para impartir la reserva con id "
							+reserva.idInstalacion+" que ofrece la actividad con id "
								+reserva.idActividad);
				}
			}
		}
		
	}

	private List<ReservaDTO> obtenReservasMonitor(int idMonitor, Connection con) {
		ReservaGateway rg = new ReservaGatewayImpl();
		rg.setConnection(con);
		List<ReservaDTO> r = rg.obtenListaDeReservasMonitor(idMonitor);
		return r;//Lo va a encontrar porque sale de una tabla
		
	}

	
	private ReservaDTO obtenReserva(int idReserva, Connection con) {
		ReservaGateway rg = new ReservaGatewayImpl();
		rg.setConnection(con);
		ReservaDTO r = rg.findById(idReserva);
		return r;//Lo va a encontrar porque sale de una tabla
		
	}
}
