package logic;

import java.util.List;

import logic.dto.AlquilerDTO;
import logic.dto.InstalacionDTO;
import persistence.AlquilerSinPagarPersistencia;

public class AlquilerSinPagar {

	public List<AlquilerDTO> getAlquileres(int idSocio) {		
		return new AlquilerSinPagarPersistencia().getAlquileres(idSocio,"A PAGAR");
	}

	public List<InstalacionDTO> getInstalaciones() {		
		return new AlquilerSinPagarPersistencia().getInstalaciones();
	}

}
