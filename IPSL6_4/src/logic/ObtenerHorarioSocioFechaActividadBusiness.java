package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import logic.dto.ReservaDTO;
import persistence.gateway.ReservaGateway;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerHorarioSocioFechaActividadBusiness {

	private int idSocio;
	private Date fecha;

	public ObtenerHorarioSocioFechaActividadBusiness(int idSocio, Date fecha) {
		this.idSocio = idSocio;
		this.fecha = fecha;
	}

	public String[] execute() {
		String[] horas = new String[15];

		for (int i = 0; i < horas.length; i++) {
			horas[i] = "LIBRE";
		}

		Connection c = null;

		try {
			c = Jdbc.getConnection();

			ReservaGateway rg = new ReservaGatewayImpl();
			rg.setConnection(c);

			Calendar calendar = Calendar.getInstance();

			calendar.setTime(fecha);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);

			Date desde = calendar.getTime();

			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);

			Date hasta = calendar.getTime();

			List<ReservaDTO> reservas = rg.findReservasSocioFecha(idSocio, desde, hasta);

			int horaIni, horaFin;

			for (int i = 0; i < horas.length; i++) {
				horaIni = i + 8;
				horaFin = horaIni + 1;

				for (ReservaDTO r : reservas) {
					if (r.horaInicial <= horaIni && r.horaFinal >= horaFin) {
						horas[i] = "OTRA ACTIVIDAD";
						break;
					}
				}
			}

		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}

		return horas;
	}

}
