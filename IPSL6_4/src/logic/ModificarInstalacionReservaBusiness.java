package logic;

import java.sql.Connection;
import java.sql.SQLException;

import persistence.gateway.ReservaGateway;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class ModificarInstalacionReservaBusiness {

	private int idReserva;
	private int idInstalacion;
	private boolean plazasLimitadas;
	private int numPlazas;

	public ModificarInstalacionReservaBusiness(int idReserva, int idInstalacion, boolean plazasLimitadas,
			int numPlazas) {
		this.idReserva = idReserva;
		this.idInstalacion = idInstalacion;
		this.plazasLimitadas = plazasLimitadas;
		this.numPlazas = numPlazas;
	}

	public void execute() {
		Connection c = null;

		try {
			c = Jdbc.getConnection();

			ReservaGateway r = new ReservaGatewayImpl();
			r.setConnection(c);

			r.updateInstalacionReserva(idReserva, idInstalacion, plazasLimitadas, numPlazas);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
