package logic;

import java.util.Calendar;
import java.util.List;

import logic.dto.SocioDTO;
import logic.util.container.Reserva;
import persistence.ApuntarSocioPersistencia;

public class ApuntarSocio {
	ApuntarSocioPersistencia p = new ApuntarSocioPersistencia();

	public List<Reserva> getReservasMonitor(int idMonitor) {
		List<Reserva> toReturn = null;		
		toReturn = p.getReservasMonitorHoy(idMonitor);
		for(int i = toReturn.size()-1; i >= 0; i--) {
			if(!toReturn.get(i).isPlazasLimitadas() || !comprobarHora(toReturn.get(i)))
				toReturn.remove(i);
						
		}
		return toReturn;
	}
	
	/**
	 * Devuelve true si no ha pasado una hora desde que empezo la actividad
	 * y por lo tanto se mostrara la actividad
	 * @param reserva
	 * @return
	 */
	private boolean comprobarHora(Reserva reserva) {		
		Calendar c = Calendar.getInstance();
		int horaActual = c.get(Calendar.HOUR_OF_DAY);
		int horaActividad = reserva.getHoraInicio();
		if(horaActividad - horaActual == 1)//una hora antes ej Son las 4:14-> 4, horaInicio -> 5, 5-4 == 1
			return true;
		return false;
	}

	/**
	 * Metodo que a�ade un socio a la lista de asistencia
	 * @param idSocio
	 * @param idReserva
	 */
	public void execute(int idSocio, int idReserva) {
		
		List<Reserva> reservas = p.getActividades(idSocio);// reservas de ese socio
		Reserva reserva = p.getReserva(idReserva);
		
		//Comprobamos que no tiene m�s actividades programadas para ese momento
		for (Reserva theReserva : reservas) {
			compararFechasSimultaneas(reserva, theReserva);
		}
		
		//Codigo obsoleto
		//Si el socio no existe no vamos a poder a�adirlo
//		if(!p.socioExist(idSocio))
//			throw new RuntimeException("El socio no est� registrado");
		
		
		//Como la reserva sabemos que existe, procedemos a a�adir
		p.ApuntarSocio(idSocio, idReserva);
		
		
	}
	/**
	 * Return false si le coinciden las horas
	 * @param idSocio
	 * @param idReserva
	 * @return
	 */
	public boolean fechasSocio(int idSocio, int idReserva) {
		try {
		List<Reserva> reservas = p.getActividades(idSocio);// reservas de ese socio
		Reserva reserva = p.getReserva(idReserva);
		for (Reserva theReserva : reservas) {
			compararFechasSimultaneas(reserva, theReserva);		
			
		}
		}catch(RuntimeException e) {
			return false;
		}
		return true;		
		
		
	}
	
	//TODO corregir
	private void compararFechasSimultaneas(Reserva reserva, Reserva theReserva) throws RuntimeException {
		if (reserva.getFecha().equals(theReserva.getFecha()))// El dia es el mismo
			if (!(reserva.getHoraFinal() < theReserva.getHoraInicio()
					|| theReserva.getHoraFinal() < reserva.getHoraInicio()))// si true entonces es porque se solapan
				throw new RuntimeException("La actividad se solapa con otra");

	}

	public List<SocioDTO> getSociosApuntados(int idReserva) {
		return p.getSociosApuntados(idReserva);
	}

	public Reserva getReservaMonitor(int idMonitor) {
		List<Reserva> toReturn = null;		
		toReturn = p.getReservasMonitorHoy(idMonitor);
		for(int i = toReturn.size()-1; i >= 0; i--) {
			if(toReturn.get(i).isPlazasLimitadas() && comprobarHora(toReturn.get(i)))
				return toReturn.get(i);						
		}
		return null;
	}

}
