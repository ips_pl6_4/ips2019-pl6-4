package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import logic.dto.ReservaDTO;
import persistence.impl.ReservaGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerReservasBusiness {

	public List<ReservaDTO> execute() {
		List<ReservaDTO> lista = null;

		Connection c = null;

		try {
			c = Jdbc.getConnection();
			c.setAutoCommit(false);

			ReservaGatewayImpl pa = new ReservaGatewayImpl();
			pa.setConnection(c);

			lista = pa.findAsignados();

			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return lista;
	}
}
