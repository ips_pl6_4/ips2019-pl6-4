package logic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import logic.dto.AlquilerDTO;
import persistence.gateway.AlquilerGateway;
import persistence.impl.AlquilerGatewayImpl;
import persistence.util.Jdbc;

public class ObtenerAlquilerConflictoBusiness {

	private int idInstalacion;
	private int horaInicio;
	private int horaFin;
	private Date fecha;

	public ObtenerAlquilerConflictoBusiness(int idInstalacion, int horaInicio, int horaFin, Date fecha) {
		this.idInstalacion = idInstalacion;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.fecha = fecha;
	}

	public AlquilerDTO execute() {
		AlquilerDTO alquiler = null;

		try {
			List<AlquilerDTO> alquileres = obtenerAlquileres();

			for (AlquilerDTO a : alquileres) {
				if (a.horaInicial <= horaFin && a.horaFinal >= horaInicio) {
					alquiler = a;
					break;
				}
			}

		} catch (SQLException sqle) {
			throw new RuntimeException("Error de conexi�n");
		}

		return alquiler;
	}

	private List<AlquilerDTO> obtenerAlquileres() throws SQLException {
		Connection c;
		c = Jdbc.getConnection();

		AlquilerGateway ag = new AlquilerGatewayImpl();
		ag.setConnection(c);

		Calendar calendar = Calendar.getInstance();

		calendar.setTime(fecha);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		Date desde = calendar.getTime();

		calendar.add(Calendar.DATE, 1);

		Date hasta = calendar.getTime();

		List<AlquilerDTO> alquileres = ag.findAlquileresFecha(desde, hasta, idInstalacion);

		return alquileres;
	}

}
